﻿using System;
using System.Collections.Generic;
using Game.ConfigActions.GameObjects;
using Instantiator;
using NSubstitute;
using NUnit.Framework;
using UnitTest.Utils.TestFactories;
using UnityEngine;

namespace UnitTest.Game.ConfigActions.GameObjects
{
	[TestFixture]
	public class ConfigGameObjectFactoryTest
	{
		private TemporaryGameObjectFactory _goFactory;
		private IConfigGameObjects _configGameObjects;
		private IGameObjectFactory _gameObjectFactory;
		private ConfigActionGameObjectFactory _factory;

		[SetUp]
		public void SetUp()
		{
			_goFactory = new TemporaryGameObjectFactory();
			_configGameObjects = Substitute.For<IConfigGameObjects>();
			_gameObjectFactory = Substitute.For<IGameObjectFactory>();
			_factory = new ConfigActionGameObjectFactory(_configGameObjects,
				_gameObjectFactory);
		}

		[TearDown]
		public void TearDown()
		{
			_goFactory.DestroyAll();
		}

		[Test]
		public void CreatePrefabWithoutId()
		{
			var expectedGameObject = _goFactory.CreateGameObject();
            var asset = Substitute.For<IAsset>();
            _gameObjectFactory.Instantiate(asset).Returns(expectedGameObject);

			var actualGameObject = _factory.Instantiate(asset);

			Assert.AreSame(expectedGameObject, actualGameObject);
			_configGameObjects.Received(1).AddGameObject(expectedGameObject);
		}

		[Test]
		[ExpectedException(typeof(ArgumentNullException))]
		public void CreatePrefabWithoutIdWithNullPrefabPathThrowsException()
		{
			_factory.Instantiate(null);
		}

		[Test]
		public void CreatePrefabWithId()
		{
            var asset = Substitute.For<IAsset>();
			var expectedGameObject = _goFactory.CreateGameObject();
            _gameObjectFactory.Instantiate(asset).Returns(expectedGameObject);

		    var id = Substitute.For<IGameObjectId>();

            var actualGameObject = _factory.Instantiate(id, asset);

            Assert.AreSame(expectedGameObject, actualGameObject);
            _configGameObjects.Received(1).AddGameObject(id, expectedGameObject);
		}

		[Test]
		[ExpectedException(typeof(ArgumentNullException))]
		public void CreatePrefabWithIdWithNullIdThrowsException()
		{
			_factory.Instantiate(null, Substitute.For<IAsset>());
		}

		[Test]
		[ExpectedException(typeof(ArgumentNullException))]
		public void CreatePrefabWithIdWithNullPrefabPathThrowsException()
		{
            var id = Substitute.For<IGameObjectId>();
            _factory.Instantiate(id, null);
		}

		[Test]
		public void DestroyGameObject()
		{
			var gameObject = _goFactory.CreateGameObject();
            var id = Substitute.For<IGameObjectId>();
            _configGameObjects.RemoveGameObject(id).Returns(gameObject);

            _factory.DestroyGameObject(id);

            _gameObjectFactory.Received(1).Destroy(gameObject);
		}

		[Test]
		[ExpectedException(typeof(ArgumentNullException))]
		public void DestroyGameObjectWithNullIdThrowsException()
		{
			_factory.DestroyGameObject(null);
		}

		[Test]
		public void DestroyAll()
		{
			var gameObjects = new List<GameObject>
			{
				_goFactory.CreateGameObject(),
				_goFactory.CreateGameObject()
			};
			_configGameObjects.RemoveAll().Returns(gameObjects);

			_factory.DestroyAll();

            _gameObjectFactory.Received(1).Destroy(gameObjects[0]);
            _gameObjectFactory.Received(1).Destroy(gameObjects[1]);
		}
	}
}
﻿using System;
using Game.ConfigActions.GameObjects;
using Game.ConfigActions.Params;
using NSubstitute;
using NUnit.Framework;

namespace UnitTest.Game.ConfigActions.GameObjects
{
    [TestFixture]
    public class ConfigGameObjectIdFactoryTest
    {
        private GameObjectIdFactory _factory;

        [SetUp]
        public void SetUp()
        {
            _factory = new GameObjectIdFactory();
        }

        [Test]
        public void CreateId()
        {
            var gameEvent = Substitute.For<IGameEvent>();
            var idString = "TestId";

            var id = _factory.CreateId(gameEvent, idString);
            
            Assert.AreSame(gameEvent, id.GameEvent);
            Assert.AreEqual(idString, id.Id);
        }

        [Test]
        public void CreateIdWithNullArgumentsThrowsException()
        {
            Assert.Throws<ArgumentNullException>(() => _factory.CreateId(null, "Hello"));
            Assert.Throws<ArgumentNullException>(() => _factory.CreateId(Substitute.For<IGameEvent>(), null));
        }
    }
}
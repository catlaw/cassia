﻿using System;
using System.Collections.Generic;
using Game.ConfigActions.GameObjects;
using NSubstitute;
using NUnit.Framework;
using UnitTest.Utils.TestFactories;
using UnityEngine;

namespace UnitTest.Game.ConfigActions.GameObjects
{
	[TestFixture]
	public class ConfigGameObjectsTest
	{
		private TemporaryGameObjectFactory _goFactory;
		private ConfigGameObjects _factory;

		[SetUp]
		public void SetUp()
		{
			_goFactory = new TemporaryGameObjectFactory();
			_factory = new ConfigGameObjects();
		}

		[TearDown]
		public void TearDown()
		{
			_goFactory.DestroyAll();
		}

		[Test]
		public void AddGameObject()
		{
			var go = _goFactory.CreateGameObject();
		    var id = Substitute.For<IGameObjectId>();

            _factory.AddGameObject(id, go);

            Assert.AreSame(go, _factory.GetGameObject(id));
            Assert.IsTrue(_factory.ContainsId(id));
		}

		[Test]
		public void AddGameObjectWithoutId()
		{
			var go = _goFactory.CreateGameObject();

			_factory.AddGameObject(go);

			var gameObjects = _factory.RemoveAll();
			CollectionAssert.Contains(gameObjects, go);
		}

		[Test]
		public void RemoveGameObject()
		{
			var go = _goFactory.CreateGameObject();
            var id = Substitute.For<IGameObjectId>();

            _factory.AddGameObject(id, go);

            var removedGo = _factory.RemoveGameObject(id);

            Assert.AreSame(go, removedGo);
            Assert.IsFalse(_factory.ContainsId(id));
        }

		[Test]
		public void RemoveAll()
		{
			var go = _goFactory.CreateGameObject();
            var id = Substitute.For<IGameObjectId>();

            _factory.AddGameObject(id, go);

            var go2 = _goFactory.CreateGameObject();
            _factory.AddGameObject(go2);

            var gameObjects = _factory.RemoveAll();

            CollectionAssert.AreEqual(new List<GameObject> { go, go2 }, gameObjects);
            Assert.IsFalse(_factory.ContainsId(id));
		}

		[Test]
		[ExpectedException(typeof(ArgumentException),
			ExpectedMessage = "Could not get GameObject with unknown GameObjectId 'Poop'.")]
		public void GetNonExistantGameObjectThrowsException()
		{
            var id = Substitute.For<IGameObjectId>();
		    id.Id.Returns("Poop");

            _factory.GetGameObject(id);
		}

		[Test]
		[ExpectedException(typeof(ArgumentException),
			ExpectedMessage = "Could not remove GameObject with unknown GameObjectId 'Poop'.")]
		public void RemoveNonExistantGameObjectThrowsException()
		{
            var id = Substitute.For<IGameObjectId>();
		    id.Id.Returns("Poop");

            _factory.RemoveGameObject(id);
            Assert.Fail();
		}

		[Test]
		[ExpectedException(typeof(ArgumentNullException))]
		public void AddNullIdThrowsException()
		{
			_factory.AddGameObject(null, _goFactory.CreateGameObject());	
		}

		[Test]
		[ExpectedException(typeof(ArgumentNullException))]
		public void AddNullGameObjectThrowsException()
		{
            var id = Substitute.For<IGameObjectId>();
            _factory.AddGameObject(id, null);
		}

		[Test]
		[ExpectedException(typeof(ArgumentNullException))]
		public void AddNullGameObjectWithoutIdThrowsException()
		{
			_factory.AddGameObject(null);
		}

		[Test]
		[ExpectedException(typeof(ArgumentNullException))]
		public void GetNullIdThrowsException()
		{
			_factory.GetGameObject(null);
		}

		[Test]
		[ExpectedException(typeof(ArgumentNullException))]
		public void RemoveNullIdThrowsException()
		{
			_factory.RemoveGameObject(null);
		}

		[Test]
		[ExpectedException(typeof(ArgumentNullException))]
		public void ContainsNullIdThrowsException()
		{
			_factory.ContainsId(null);
		}
	}
}
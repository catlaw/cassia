﻿using System;
using Game.ConfigActions;
using Game.ConfigActions.Methods;
using NSubstitute;
using NUnit.Framework;

namespace UnitTest.Game.ConfigActions
{
    [TestFixture]
    public class ConfigActionSetTest
    {
        private ConfigActionSet _set;

        [SetUp]
        public void SetUp()
        {
            _set = new ConfigActionSet();
        }

        [Test]
        public void AddAndGet()
        {
            var configAction = Substitute.For<IConfigAction>();
            _set.Add(UnitActionEventType.PreHit, configAction);

            Assert.AreSame(configAction, _set.GetConfigAction(UnitActionEventType.PreHit));
        }

        [Test]
        public void GetNonExistentReturnsEmpty()
        {
            var configAction = _set.GetConfigAction(UnitActionEventType.Hit);
            Assert.IsInstanceOf<EmptyConfigAction>(configAction);
        }

        [Test]
        [ExpectedException(typeof(ArgumentException),
            ExpectedMessage = "ConfigAction with type 'Hit' already exists.")]
        public void AddDuplicateThrowsException()
        {
            _set.Add(UnitActionEventType.Hit, Substitute.For<IConfigAction>());
            _set.Add(UnitActionEventType.Hit, Substitute.For<IConfigAction>());
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common.Exceptions;
using Configs;
using Game.ConfigActions;
using Game.ConfigActions.Methods;
using Game.ConfigActions.Params;
using Game.Models.Occupants.Units.UnitActions;
using Game.Views;
using NSubstitute;
using NUnit.Framework;
using strange.extensions.injector.api;

namespace UnitTest.Game.ConfigActions
{
    [TestFixture]
    public class ConfigActionSequenceFactoryTest
    {
        private IInjectionBinder _injectionBinder;
        private IConfigActionFactory _configActionFactory;
        private ConfigActionSequenceFactory _sequenceFactory;

        [SetUp]
        public void SetUp()
        {
            _injectionBinder = Substitute.For<IInjectionBinder>();
            _configActionFactory = Substitute.For<IConfigActionFactory>();
            _sequenceFactory = new ConfigActionSequenceFactory(_injectionBinder, _configActionFactory);
        }

        [Test]
        public void CreateConfigActionSequence()
        {
            var createMethodParamsFactory = Substitute.For<IMethodParamsFactory>();
            var destroyMethodParamsFactory = Substitute.For<IMethodParamsFactory>();
            var config = new ConfigActionSequenceConfig
            {
                Root = "Create",
                Actions = new Dictionary<string, ConfigActionConfig>
                {
                    {
                        "Create", new ConfigActionConfig
                        {
                            Method = ConfigActionMethodType.Create,
                            MethodParamsFactory = createMethodParamsFactory,
                            Next = new List<string> {"Destroy"}
                        }
                    },
                    {
                        "Destroy", new ConfigActionConfig
                        {
                            Method = ConfigActionMethodType.Destroy,
                            MethodParamsFactory = destroyMethodParamsFactory,
                        }
                    }
                }
            };

            var createMethod = Substitute.For<IConfigActionMethod>();
            _injectionBinder.GetInstance<IConfigActionMethod>(ConfigActionMethodType.Create)
                .Returns(createMethod);

            var destroyMethod = Substitute.For<IConfigActionMethod>();
            _injectionBinder.GetInstance<IConfigActionMethod>(ConfigActionMethodType.Destroy)
                .Returns(destroyMethod);

            var destroyAction = _configActionFactory.CreateConfigAction(destroyMethod,
                 destroyMethodParamsFactory, Arg.Is<IEnumerable<IConfigAction>>(x => !x.Any()));
            var createAction = _configActionFactory.CreateConfigAction(createMethod,
                createMethodParamsFactory, Arg.Is<IEnumerable<IConfigAction>>(x =>
                x.First() == destroyAction && x.Count() == 1));

            Assert.AreSame(createAction, _sequenceFactory.CreateConfigActionSequence(config));
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void CreateConfigActionSequenceFromNullThrowsException()
        {
            _sequenceFactory.CreateConfigActionSequence(null);
        }

        [Test]
        [ExpectedException(typeof(InvalidConfigException),
            ExpectedMessage = "Could not find ConfigAction with id 'Poop'.")]
        public void GetNonExistantRootConfigAction()
        {
            var config = new ConfigActionSequenceConfig
            {
                Root = "Poop",
                Actions = new Dictionary<string, ConfigActionConfig>()
            };

            _sequenceFactory.CreateConfigActionSequence(config);
        }

        [Test]
        [ExpectedException(typeof(InvalidConfigException),
            ExpectedMessage = "Could not find ConfigAction with id 'Poop'.")]
        public void GetNonExistantNextConfigAction()
        {
            var config = new ConfigActionSequenceConfig
            {
                Root = "Root",
                Actions = new Dictionary<string, ConfigActionConfig>
                {
                    {
                        "Root", new ConfigActionConfig
                        {
                            Method = ConfigActionMethodType.Create,
                            MethodParamsFactory = Substitute.For<IMethodParamsFactory>(),
                            Next = new List<string> {"Poop"}
                        }
                    }
                }
            };

            _sequenceFactory.CreateConfigActionSequence(config);
        }
    }
}
﻿using System;
using System.Collections;
using Game.ConfigActions;
using Game.ConfigActions.Methods;
using Game.ConfigActions.Params;
using NSubstitute;
using NUnit.Framework;
using UnitTest.Utils.CoroutineRunner;

namespace UnitTest.Game.ConfigActions.Methods
{
    [TestFixture]
    public class TestConfigActionMethodTest
    {
        private FakeCoroutineRunner _runner;
        private IMethodParamsFactory _methodParamsFactory;

        private TestMethod _method;

        [SetUp]
        public void SetUp()
        {
	        _runner = FakeCoroutineRunner.CreateWithConstantDeltaTime();
            _methodParamsFactory = Substitute.For<IMethodParamsFactory>();

            _method = new TestMethod();
        }

        [TearDown]
        public void TearDown()
        {
			_runner.CleanUp();
        }

        private class TestMethod : Method<TestMethodParams>
        {
            public override IEnumerator CoExecute(TestMethodParams methodParams)
            {
                methodParams.TestAction();
                yield break;
            }

            public override bool IsInstant(TestMethodParams methodParams)
            {
                return methodParams.IsInstant;
            }
        }

        private class TestMethodParams
        {
            public Action TestAction { get; set; }
            public bool IsInstant { get; set; }
        }


        [Test]
        public void CoExecute()
        {
            var hasExecuted = false;
            var eventParams = Substitute.For<IGameEvent>();
            var methodParams = new TestMethodParams
            {
                TestAction = () => { hasExecuted = true; }
            };
            _methodParamsFactory.CreateMethodParams<TestMethodParams>(eventParams).Returns(methodParams);

            _runner.StartCoroutine(_method.CoExecute(eventParams, _methodParamsFactory));
            _runner.Run();

            Assert.IsTrue(hasExecuted);
        }

        [Test]
        public void GetDuration()
        {
            var eventParams = Substitute.For<IGameEvent>();
            var methodParams = new TestMethodParams
            {
                IsInstant = true
            };
            _methodParamsFactory.CreateMethodParams<TestMethodParams>(eventParams).Returns(methodParams);

            Assert.AreEqual(true, _method.IsInstant(eventParams, _methodParamsFactory));
        }
    }
}
﻿using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using Game.ConfigActions.GameObjects;
using Game.ConfigActions.Methods;
using Game.ConfigActions.Tweening;
using Game.ConfigActions.Util;
using NSubstitute;
using NUnit.Framework;
using UnitTest.Utils.CoroutineRunner;
using UnitTest.Utils.TestFactories;
using UnityEngine;

namespace UnitTest.Game.ConfigActions.Methods
{
	[TestFixture]
	public class MovePathMethodTest
	{
	    private TemporaryGameObjectFactory _goFactory;
	    private FakeCoroutineRunner _runner;

        private IConfigGameObjects _configGameObjects;
	    private IDurationCalculator _durationCalculator;
	    private IPathWaypointsCalculator _pathWaypointsCalculator;
	    private ITweenFactory _tweenFactory;
		private MovePathMethod _method;

		[SetUp]
		public void SetUp()
		{
            _goFactory = new TemporaryGameObjectFactory();
            _runner = FakeCoroutineRunner.CreateWithConstantDeltaTime();

		    _configGameObjects = Substitute.For<IConfigGameObjects>();
		    _durationCalculator = Substitute.For<IDurationCalculator>();
            _pathWaypointsCalculator = Substitute.For<IPathWaypointsCalculator>();
		    _tweenFactory = Substitute.For<ITweenFactory>();
            _method = new MovePathMethod(_configGameObjects,
                _pathWaypointsCalculator, _durationCalculator, _tweenFactory);
        }

	    [TearDown]
	    public void TearDown()
	    {
	        _goFactory.DestroyAll();
            _runner.CleanUp();
	    }

		[Test]
		public void CoExecute()
		{
		    var id = Substitute.For<IGameObjectId>();
		    var methodParams = new MovePathParams
		    {
                Id = id,
                Speed = 2.4f,
                Ease = Ease.InBounce,
                PathType = PathType.CatmullRom,
                ParametricWaypoints = new List<ParametricPoint>
                {
                    new ParametricPoint { Parameter = 0.5f, Offset = new Vector3(1, 1, 1)}
                },
                Position = new Vector3(2, 3, 4)
		    };

		    var target = _goFactory.CreateGameObject();
            target.transform.position = new Vector3(1, 2, 3);
            _configGameObjects.GetGameObject(id).Returns(target);

            var expectedWaypoints = new List<Vector3>
            {
                new Vector3(1, 1, 1),
                new Vector3(2, 2, 2)
            };
		    _pathWaypointsCalculator.GetPathWaypoints(new Vector3(1, 2, 3), new Vector3(2, 3, 4),
		        methodParams.ParametricWaypoints).Returns(expectedWaypoints);

		    var tween = Substitute.For<ITween>();
		    _tweenFactory.GetPathTween(target.transform, expectedWaypoints, 6.6f, PathType.CatmullRom)
		        .Returns(tween);
		    tween.PathLength().Returns(88f);

            _durationCalculator.GetDuration(Arg.Is<IEnumerable<Vector3>>(x => 
            x.ElementAt(0) == new Vector3(1, 2, 3) 
            && x.ElementAt(1) == new Vector3(1, 1, 1)
            && x.ElementAt(2) == new Vector3(2, 2, 2)), 2.4f).Returns(6.6f);

            _runner.StartCoroutine(_method.CoExecute(methodParams));
		    _runner.Run();

		    tween.Received(1).SetEase(Ease.InBounce);
		}

	    [Test]
	    public void IsInstant()
	    {
            var methodParams = new MovePathParams();
            Assert.IsFalse(_method.IsInstant(methodParams));
        }
	}
}
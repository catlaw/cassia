﻿using System;
using Common.Util;
using Game.ConfigActions.GameObjects;
using Game.ConfigActions.Methods;
using NSubstitute;
using NUnit.Framework;
using TMPro;
using UnitTest.Utils.CoroutineRunner;
using UnitTest.Utils.TestFactories;

namespace UnitTest.Game.ConfigActions.Methods
{
    [TestFixture]
    public class SetTextMethodTest
    {
        private TemporaryGameObjectFactory _goFactory;
        private FakeCoroutineRunner _runner;

        private ILoggyLog _loggyLog;
        private IConfigGameObjects _configGameObjects;
        private SetTextMethod _method;

        [SetUp]
        public void SetUp()
        {
            _goFactory = new TemporaryGameObjectFactory();
            _runner = FakeCoroutineRunner.CreateWithConstantDeltaTime();

            _loggyLog = Substitute.For<ILoggyLog>();
            _configGameObjects = Substitute.For<IConfigGameObjects>();
            _method = new SetTextMethod(_configGameObjects, _loggyLog);
        }

        [TearDown]
        public void TearDown()
        {
            _goFactory.DestroyAll();
            _runner.CleanUp();
        }

        [Test]
        public void CoExecute()
        {
            var methodParams = new SetTextParams
            {
                GameObjectId = Substitute.For<IGameObjectId>(),
                GameObjectPath = "Poop",
                Text = "HelloWorld"
            };

            var gameObject = _goFactory.CreateGameObject();
            var child = _goFactory.CreateGameObject("Poop");
            child.transform.SetParent(gameObject.transform);
            child.AddComponent<TextMeshPro>();

            _configGameObjects.GetGameObject(methodParams.GameObjectId).Returns(gameObject);

            _runner.StartCoroutine(_method.CoExecute(methodParams));
            _runner.Run();

            Assert.AreEqual("HelloWorld", child.GetComponent<TextMeshPro>().text);
        }

        [Test]
        public void CoExecuteWithNullPath()
        {
            var methodParams = new SetTextParams
            {
                GameObjectId = Substitute.For<IGameObjectId>(),
                Text = "HelloWorld"
            };

            var gameObject = _goFactory.CreateGameObject();
            gameObject.AddComponent<TextMeshPro>();

            _configGameObjects.GetGameObject(methodParams.GameObjectId).Returns(gameObject);

            _runner.StartCoroutine(_method.CoExecute(methodParams));
            _runner.Run();

            Assert.AreEqual("HelloWorld", gameObject.GetComponent<TextMeshPro>().text);
        }

        [Test]
        public void CoExecuteWithEmptyPath()
        {
            var methodParams = new SetTextParams
            {
                GameObjectId = Substitute.For<IGameObjectId>(),
                GameObjectPath = "",
                Text = "HelloWorld"
            };

            var gameObject = _goFactory.CreateGameObject();
            gameObject.AddComponent<TextMeshPro>();

            _configGameObjects.GetGameObject(methodParams.GameObjectId).Returns(gameObject);

            _runner.StartCoroutine(_method.CoExecute(methodParams));
            _runner.Run();

            Assert.AreEqual("HelloWorld", gameObject.GetComponent<TextMeshPro>().text);
        }

        [Test]
        public void CoExecuteWithoutTextMeshProComponent()
        {
            var methodParams = new SetTextParams
            {
                GameObjectId = Substitute.For<IGameObjectId>()
            };
            methodParams.GameObjectId.Id.Returns("Poop");

            var gameObject = _goFactory.CreateGameObject();

            _configGameObjects.GetGameObject(methodParams.GameObjectId).Returns(gameObject);

            _runner.StartCoroutine(_method.CoExecute(methodParams));
            _runner.Run();

            _loggyLog.Received(1).LogError("Cannot SetText on GameObject with GameObjectId " +
                                           "'Poop' because no TextMeshPro component found.");
        }


        [Test]
        public void IsInstant()
        {
            Assert.IsTrue(_method.IsInstant(new SetTextParams()));
        }
    }
}
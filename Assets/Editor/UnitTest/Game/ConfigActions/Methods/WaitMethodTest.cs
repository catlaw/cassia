﻿using Game.ConfigActions.Methods;
using NUnit.Framework;
using UnitTest.Utils.Asserts;
using UnitTest.Utils.CoroutineRunner;

namespace UnitTest.Game.ConfigActions.Methods
{
    [TestFixture]
    public class WaitMethodTest
    {
        private FakeCoroutineRunner _runner;
        private WaitMethod _method;

        [SetUp]
        public void SetUp()
        {
            _runner = FakeCoroutineRunner.CreateWithConstantDeltaTime();
            _method = new WaitMethod(_runner);
        }

        [TearDown]
        public void TearDown()
        {
            _runner.CleanUp();
        }

        [Test]
        public void CoExecute()
        {
            var methodParams = new WaitParams {Duration = 4.5f};
            _runner.StartCoroutine(_method.CoExecute(methodParams));
            _runner.Run();

            AssertTime.Elapsed(4.5f, _runner);
        }

        [Test]
        public void IsInstant()
        {
            Assert.IsTrue(_method.IsInstant(new WaitParams { Duration = 0 }));
            Assert.IsFalse(_method.IsInstant(new WaitParams { Duration = 10 }));
        }
    }
}
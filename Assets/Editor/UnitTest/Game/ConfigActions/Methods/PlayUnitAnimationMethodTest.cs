﻿using Game.ConfigActions.Methods;
using Game.Models.Occupants.Units;
using Game.Views.Animations;
using NSubstitute;
using NUnit.Framework;
using UnitTest.Utils.Asserts;
using UnitTest.Utils.CoroutineRunner;

namespace UnitTest.Game.ConfigActions.Methods
{
    [TestFixture]
    public class PlayUnitAnimationMethodTest
    {
        private FakeCoroutineRunner _runner;
        private IPlayableUnitAnimationSelector _animationSelector;
        private PlayUnitAnimationMethod _method;

        [SetUp]
        public void SetUp()
        {
            _runner = FakeCoroutineRunner.CreateWithConstantDeltaTime();
            _animationSelector = Substitute.For<IPlayableUnitAnimationSelector>();
            _method = new PlayUnitAnimationMethod(_animationSelector, _runner);
        }

        [TearDown]
        public void TearDown()
        {
            _runner.CleanUp();
        }

        [Test]
        public void CoExecute()
        {
            var targetUnit = Substitute.For<IUnit>();
            var expectedPlayableAnimation = Substitute.For<IPlayableUnitAnimation>();
            expectedPlayableAnimation.HitDelay.Returns(0.47f);
            _animationSelector.GetAnimation(targetUnit, AnimationType.Cast, 3).Returns(expectedPlayableAnimation);

            var methodParams = new PlayUnitAnimationParams
            {
                Unit = targetUnit,
                AnimationType = AnimationType.Cast,
                PowerTier = 3
            };

            _runner.StartCoroutine(_method.CoExecute(methodParams));
            _runner.Run();

            targetUnit.TriggerUnitAnimationViewSignal.Received(1).Dispatch(expectedPlayableAnimation);
            AssertTime.Elapsed(0.47f, _runner);
        }

        [Test]
        public void IsNotInstant()
        {
            var methodParams = new PlayUnitAnimationParams
            {
                Unit = Substitute.For<IUnit>(),
                AnimationType = AnimationType.Cast,
                PowerTier = 3
            };
            _animationSelector.GetAnimation(methodParams.Unit, AnimationType.Cast, 3)
                .HitDelay.Returns(0.4f);

            Assert.IsFalse(_method.IsInstant(methodParams));
        }

        [Test]
        public void IsInstant()
        {
            var methodParams = new PlayUnitAnimationParams
            {
                Unit = Substitute.For<IUnit>(),
                AnimationType = AnimationType.Cast,
                PowerTier = 3
            };
            _animationSelector.GetAnimation(methodParams.Unit, AnimationType.Cast, 3)
                .HitDelay.Returns(0);

            Assert.IsTrue(_method.IsInstant(methodParams));
        }
    }
}
﻿using Configs;
using Game.ConfigActions;
using Game.ConfigActions.GameObjects;
using Game.ConfigActions.Methods;
using Instantiator;
using NSubstitute;
using NUnit.Framework;
using UnitTest.Utils.CoroutineRunner;
using UnitTest.Utils.TestFactories;
using UnityEngine;

namespace UnitTest.Game.ConfigActions.Methods
{
	[TestFixture]
	public class CreateMethodTest
	{
		private FakeCoroutineRunner _runner;
		private TemporaryGameObjectFactory _goFactory;

		private IConfigActionGameObjectFactory _gameObjectFactory;
		private CreateMethod _method;

		[SetUp]
		public void SetUp()
		{
			_runner = FakeCoroutineRunner.CreateWithConstantDeltaTime();
			_goFactory = new TemporaryGameObjectFactory();

			_gameObjectFactory = Substitute.For<IConfigActionGameObjectFactory>();
			_method = new CreateMethod(_gameObjectFactory);
		}

		[TearDown]
		public void TearDown()
		{
			_runner.CleanUp();
			_goFactory.DestroyAll();
		}

		[Test]
		public void CoExecuteWithId()
		{
		    var id = Substitute.For<IGameObjectId>();
			var methodParams = new CreateParams
			{
				GameObjectId = id,
				Asset = new AssetConfig(),
				Offset = new Vector3(1, 2, 3),
				Position = new Vector3(2, 3, 4),
				Scale = new Vector3(1, 1, 1)
			};

			var go = _goFactory.CreateGameObject();
			_gameObjectFactory.Instantiate(id, methodParams.Asset).Returns(go);

			_runner.StartCoroutine(_method.CoExecute(methodParams));
			_runner.Run();

			Assert.AreEqual(methodParams.Position + methodParams.Offset, go.transform.position);
			Assert.AreEqual(methodParams.Scale, go.transform.localScale);
		}

		[Test]
		public void CoExecuteWithoutId()
		{
			var methodParams = new CreateParams
			{
				Asset = new AssetConfig(),
				Offset = new Vector3(1, 2, 3),
				Position = new Vector3(2, 3, 4),
				Scale = new Vector3(1, 1, 1)
			};

			var go = _goFactory.CreateGameObject();
			_gameObjectFactory.Instantiate(methodParams.Asset).Returns(go);

			_runner.StartCoroutine(_method.CoExecute(methodParams));
			_runner.Run();

			Assert.AreEqual(methodParams.Position + methodParams.Offset, go.transform.position);
			Assert.AreEqual(methodParams.Scale, go.transform.localScale);
		}

		[Test]
		public void IsInstant()
		{
			var isInstant = _method.IsInstant(new CreateParams());

			Assert.IsTrue(isInstant);
		}
	}
}
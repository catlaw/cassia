﻿using DG.Tweening;
using Game.ConfigActions.GameObjects;
using Game.ConfigActions.Methods;
using Game.ConfigActions.Tweening;
using Game.ConfigActions.Util;
using NSubstitute;
using NUnit.Framework;
using UnitTest.Utils.CoroutineRunner;
using UnitTest.Utils.TestFactories;
using UnityEngine;

namespace UnitTest.Game.ConfigActions.Methods
{
	[TestFixture]
	public class MoveToMethodTest
	{
		private FakeCoroutineRunner _runner;
		private TemporaryGameObjectFactory _goFactory;

		private ITweenFactory _tweenFactory;
		private IDoDelegateFactory _delegateFactory;
		private IConfigGameObjects _configGameObjects;
		private IDurationCalculator _durationCalculator;
		private MoveToMethod _method;

		[SetUp]
		public void SetUp()
		{
			_runner = FakeCoroutineRunner.CreateWithConstantDeltaTime();
			_goFactory = new TemporaryGameObjectFactory();

			_tweenFactory = Substitute.For<ITweenFactory>();
			_delegateFactory = Substitute.For<IDoDelegateFactory>();
			_configGameObjects = Substitute.For<IConfigGameObjects>();
			_durationCalculator = Substitute.For<IDurationCalculator>();
			_method = new MoveToMethod(_tweenFactory, _delegateFactory,
				_configGameObjects, _durationCalculator);
		}

		[TearDown]
		public void TearDown()
		{
            _runner.CleanUp();
			_goFactory.DestroyAll();
		}

		[Test]
		public void CoExecute()
		{
		    var id = Substitute.For<IGameObjectId>();
			var methodParams = new MoveToParams
			{
				GameObjectId = id,
				Position = new Vector3(1, 2, 3),
				Speed = 2.5f,
				Ease = Ease.InBack
			};

			var target = _goFactory.CreateGameObject();
			_configGameObjects.GetGameObject(id).Returns(target);
			target.transform.position = new Vector3(5, 5, 5);

			_durationCalculator.GetDuration(new Vector3(5, 5, 5), new Vector3(1, 2, 3), 2.5f)
				.Returns(5.1f);

			var getter = Substitute.For<IDOGetter<Vector3>>();
			_delegateFactory.GetPositionGetter(target.transform).Returns(getter);

			var setter = Substitute.For<IDOSetter<Vector3>>();
			_delegateFactory.GetPositionSetter(target.transform).Returns(setter);

			var tween = Substitute.For<ITween>();
			_tweenFactory.GetTween(getter, setter, new Vector3(1, 2, 3), 5.1f).Returns(tween);

			_runner.StartCoroutine(_method.CoExecute(methodParams));
			_runner.Run();

			_tweenFactory.Received(1).GetTween(getter, setter, new Vector3(1, 2, 3), 5.1f);
			tween.Received(1).SetEase(Ease.InBack);
		}

		[Test]
		public void IsInstant()
		{
			var methodParams = new MoveToParams();
			Assert.IsFalse(_method.IsInstant(methodParams));
		}
	}
}
using System.Collections.Generic;
using Game.ConfigActions;
using Game.ConfigActions.Methods;
using Game.ConfigActions.Params;
using NSubstitute;
using NUnit.Framework;
using UnitTest.Utils.CoroutineRunner;

namespace UnitTest.Game.ConfigActions
{
    [TestFixture]
    public class ConfigActionFactoryTest
    {
        private FakeCoroutineRunner _runner;

        private IConfigActionMethod _method;
        private IEnumerable<IConfigAction> _next;
        private ConfigActionFactory _factory;

        [SetUp]
        public void SetUp()
        {
	        _runner = FakeCoroutineRunner.CreateWithConstantDeltaTime();

            _method = Substitute.For<IConfigActionMethod>();
            _next = new List<IConfigAction>
            {
                Substitute.For<IConfigAction>(),
                Substitute.For<IConfigAction>()
            };

            _factory = new ConfigActionFactory();

        }

        [TearDown]
        public void TearDown()
        {
			_runner.CleanUp();
        }

        [Test]
        public void Next()
        {
            var methodParamsFactory = Substitute.For<IMethodParamsFactory>();
            var animation = _factory.CreateConfigAction(_method, methodParamsFactory, _next);
            CollectionAssert.AreEqual(_next, animation.Next);
        }

        [Test]
        public void CoExecute()
        {
            var eventParams = Substitute.For<IGameEvent>();
            var methodParamsFactory = Substitute.For<IMethodParamsFactory>();

            var animation = _factory.CreateConfigAction(_method, methodParamsFactory, _next);

            _runner.StartCoroutine(animation.CoExecute(eventParams));
            _runner.Run();

            _method.Received(1).CoExecute(eventParams, methodParamsFactory);
        }

        [Test]
        public void IsInstant()
        {
            var eventParams = Substitute.For<IGameEvent>();
            var methodParamsFactory = Substitute.For<IMethodParamsFactory>();

            _method.IsInstant(eventParams, methodParamsFactory).Returns(true);

            var animation = _factory.CreateConfigAction(_method, methodParamsFactory, _next);

            Assert.AreEqual(true, animation.IsInstant(eventParams));
        }
    }
}
﻿using Common.Util;
using Game.ConfigActions;
using Game.ConfigActions.Events;
using Game.ConfigActions.Methods;
using Game.ConfigActions.Params;
using Game.Views.Animations;
using NSubstitute;
using NUnit.Framework;

namespace UnitTest.Game.ConfigActions.Events
{
	[TestFixture]
	public class UnitActionEventExecutorTest
	{
		private ICoroutineRunner _coroutineRunner;
		private IUnitAnimationFactory _unitAnimationFactory;
		private IConfigActionExecutor _configActionExecutor;
		private IUnitActionEventFactory _eventFactory;
		private UnitActionEventExecutor _executor;

		[SetUp]
		public void SetUp()
		{
			_coroutineRunner = Substitute.For<ICoroutineRunner>();
			_unitAnimationFactory = Substitute.For<IUnitAnimationFactory>();
			_configActionExecutor = Substitute.For<IConfigActionExecutor>();
			_eventFactory = Substitute.For<IUnitActionEventFactory>();
			_executor = new UnitActionEventExecutor(_unitAnimationFactory, _configActionExecutor,
				_eventFactory, _coroutineRunner);
		}

		[Test]
		public void Execute()
		{
			var configActionSet = Substitute.For<IConfigActionSet>();
			_unitAnimationFactory.GetActorUnitAnimation().ConfigActionSet
				.Returns(configActionSet);

			var coroutine = _executor.Execute(UnitActionEventType.PreHit);

			Assert.AreSame(_coroutineRunner.StartCoroutine(_configActionExecutor.CoExecute(configActionSet.GetConfigAction(UnitActionEventType.PreHit),
				_eventFactory.CreateEvent())), coroutine);
		}
	}
}
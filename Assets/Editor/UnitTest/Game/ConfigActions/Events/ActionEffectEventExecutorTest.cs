﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Common.Util;
using Game.ConfigActions;
using Game.ConfigActions.Events;
using Game.ConfigActions.Methods;
using Game.ConfigActions.Params;
using Game.Views.Animations;
using NSubstitute;
using NUnit.Framework;
using UnitTest.Utils.CoroutineRunner;

namespace UnitTest.Game.ConfigActions.Events
{
	[TestFixture]
	public class ActionEffectEventExecutorTest
	{
	    private FakeCoroutineRunner _coroutineRunner;
	    private IParallelCoroutineRunner _parallelCoroutineRunner;
		private IConfigActionExecutor _configActionExecutor;
		private IActionEffectedUnitFactory _actionEffectedUnitFactory;
	    private IActionEffectEventFactory _actionEffectEventFactory;
		private IActionEffectConfigActions _actionEffectConfigActions;
		private ActionEffectEventExecutor _executor;

		[SetUp]
		public void SetUp()
		{
            _coroutineRunner = FakeCoroutineRunner.CreateWithConstantDeltaTime();
		    _parallelCoroutineRunner = new ParallelCoroutineRunner(_coroutineRunner);
			_configActionExecutor = Substitute.For<IConfigActionExecutor>();
			_actionEffectedUnitFactory = Substitute.For<IActionEffectedUnitFactory>();
		    _actionEffectEventFactory = Substitute.For<IActionEffectEventFactory>();
			_actionEffectConfigActions = Substitute.For<IActionEffectConfigActions>();
			_executor = new ActionEffectEventExecutor(_coroutineRunner,
                _parallelCoroutineRunner, _configActionExecutor,
				_actionEffectedUnitFactory, _actionEffectEventFactory,
                _actionEffectConfigActions);
		}

	    [TearDown]
	    public void TearDown()
	    {
            _coroutineRunner.CleanUp();
	    }

		[Test]
		public void Execute()
		{
		    var expectedAffectedUnit0 = Substitute.For<IActionEffectedUnit>();
            var expectedAffectedUnit1 = Substitute.For<IActionEffectedUnit>();
            var expectedAffectedUnit2 = Substitute.For<IActionEffectedUnit>();

            var actionEffectUnits = new List<IEnumerable<IActionEffectedUnit>>
			{
				new List<IActionEffectedUnit>
				{
					expectedAffectedUnit0,
					expectedAffectedUnit1
				},
				new List<IActionEffectedUnit>
				{
                    expectedAffectedUnit2
                }
			};
            _actionEffectedUnitFactory.GetActionEffectedUnits().Returns(actionEffectUnits);

            _executor.Execute(ActionEffectEventType.Damage);
		    _coroutineRunner.Run();

			var damageConfigAction = _actionEffectConfigActions.Get(ActionEffectEventType.Damage);

            // can't test timing
            Received.InOrder(() =>
            {
                expectedAffectedUnit0.AffectedUnit.Unit.ApplyActionEffectViewSignal.Dispatch(expectedAffectedUnit0.ActionEffect);
                _configActionExecutor.CoExecute(damageConfigAction,
                _actionEffectEventFactory.CreateEvent(expectedAffectedUnit0.AffectedUnit, expectedAffectedUnit0.ActionEffect));

                expectedAffectedUnit1.AffectedUnit.Unit.ApplyActionEffectViewSignal.Dispatch(expectedAffectedUnit1.ActionEffect);
                _configActionExecutor.CoExecute(damageConfigAction,
                _actionEffectEventFactory.CreateEvent(expectedAffectedUnit1.AffectedUnit, expectedAffectedUnit1.ActionEffect));

                expectedAffectedUnit2.AffectedUnit.Unit.ApplyActionEffectViewSignal.Dispatch(expectedAffectedUnit2.ActionEffect);
                _configActionExecutor.CoExecute(damageConfigAction,
                 _actionEffectEventFactory.CreateEvent(expectedAffectedUnit2.AffectedUnit, expectedAffectedUnit2.ActionEffect));
            });
		}
	}
}
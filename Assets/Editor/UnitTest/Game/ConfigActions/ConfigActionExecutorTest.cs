﻿using System;
using System.Collections;
using System.Collections.Generic;
using Game.ConfigActions;
using Game.ConfigActions.Params;
using Game.Views.Animations;
using NSubstitute;
using NUnit.Framework;
using UnitTest.Utils;
using UnitTest.Utils.Asserts;
using UnitTest.Utils.CoroutineRunner;

namespace UnitTest.Game.ConfigActions
{
    [TestFixture]
    public class ConfigActionExecutorTest
    {
        private FakeCoroutineRunner _runner;
        private IGameEvent _gameEvent;
        private ConfigActionExecutor _animator;

        [SetUp]
        public void SetUp()
        {
            _runner = FakeCoroutineRunner.CreateWithConstantDeltaTime();
            _gameEvent = Substitute.For<IGameEvent>();
            _animator = new ConfigActionExecutor(_runner);
        }

        [TearDown]
        public void TearDown()
        {
			_runner.CleanUp();
        }

        [Test]
        public void PlayInSequence()
        {
            var count = 0;

            var animation0 = GetConfigurableAnimation(0.1f, false, () => { count++; });
            var animation1 = GetConfigurableAnimation(0.1f, false, () => { count++; });
            var animation2 = GetConfigurableAnimation(0.1f, false, () => { count++; });

            animation0.Next.Returns(new List<IConfigAction> {animation1});
            animation1.Next.Returns(new List<IConfigAction> {animation2});

            _runner.StartCoroutine(_animator.CoExecute(animation0, _gameEvent));
            _runner.Run();

            // this is a bit annoying, but it costs one frame to start next for now.
            AssertTime.Elapsed(0.3f + 0.2f, _runner);
            Assert.AreEqual(3, count);
        }

        private IConfigAction GetConfigurableAnimation(float duration, bool isInstant, Action action)
        {
            var animation = Substitute.For<IConfigAction>();
            animation.CoExecute(_gameEvent).Returns(CoGetTestCoroutine(duration, action));
            animation.IsInstant(_gameEvent).Returns(isInstant);
            return animation;
        }

        private IEnumerator CoGetTestCoroutine(float duration, Action action)
        {
            action();
	        var elapsed = 0f;
            while (elapsed < duration)
            {
                yield return null;
                elapsed += _runner.DeltaTime;
            }
        }

        [Test]
        public void PlayInSequenceInstantly()
        {
            var count = 0;

            var animation0 = GetConfigurableAnimation(0f, true, () => { count++; });
            var animation1 = GetConfigurableAnimation(0f, true, () => { count++; });
            var animation2 = GetConfigurableAnimation(0f, true, () => { count++; });
            var animation3 = GetConfigurableAnimation(0f, true, () => { count++; });

            animation0.Next.Returns(new List<IConfigAction> { animation1 });
            animation1.Next.Returns(new List<IConfigAction> { animation2 });
            animation2.Next.Returns(new List<IConfigAction> { animation3 });

            _runner.StartCoroutine(_animator.CoExecute(animation0, _gameEvent));
            _runner.Run();

            AssertTime.Elapsed(0f, _runner);
            Assert.AreEqual(1, _runner.ElapsedFrames);
            Assert.AreEqual(4, count);
        }

        [Test]
        public void PlayInParallel()
        {
            var count = 0;

            var animation0 = GetConfigurableAnimation(1f, false, () => { count++; });
            var animation1 = GetConfigurableAnimation(0.1f, false, () => { count++; });
            var animation2 = GetConfigurableAnimation(0.2f, false, () => { count++; });

            animation0.Next.Returns(new List<IConfigAction> { animation1, animation2 });

            _runner.StartCoroutine(_animator.CoExecute(animation0, _gameEvent));
            _runner.Run();

            // this is a bit annoying, but it costs one frame to start next for now.
            AssertTime.Elapsed(1.2f + 0.1f, _runner);
            Assert.AreEqual(3, count);
        }

        [Test]
        public void PlayInParallelInstantly()
        {
            var count = 0;

            var animation0 = GetConfigurableAnimation(0f, true, () => { count++; });
            var animation1 = GetConfigurableAnimation(0f, true, () => { count++; });
            var animation2 = GetConfigurableAnimation(0f, true, () => { count++; });

            animation0.Next.Returns(new List<IConfigAction> { animation1, animation2 });

            _runner.StartCoroutine(_animator.CoExecute(animation0, _gameEvent));
            _runner.Run();

            AssertTime.Elapsed(0f, _runner);
            Assert.AreEqual(1, _runner.ElapsedFrames);
            Assert.AreEqual(3, count);
        }

        // coroutines are weird and this can't be tested
        public void PlayWithNullConfigurableAnimationsThrowsException()
        {
	        Assert.Throws<ArgumentNullException>(() =>
	        {
		        _animator.CoExecute(null, _gameEvent);

	        });

	        Assert.Throws<ArgumentNullException>(() =>
	        {
		        _animator.CoExecute(Substitute.For<IConfigAction>(), null);
	        });
        }
    }
}
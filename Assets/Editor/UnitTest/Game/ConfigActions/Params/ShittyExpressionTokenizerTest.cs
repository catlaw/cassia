﻿using System;
using System.Linq;
using Game.ConfigActions.Params;
using Game.Expressions;
using Game.Expressions.Operators;
using Newtonsoft.Json.Linq;
using NSubstitute;
using NSubstitute.Core;
using NUnit.Framework;
using UnityEngine;

namespace UnitTest.Game.ConfigActions.Params
{
	[TestFixture]
	public class ShittyExpressionTokenizerTest
	{
		private IOperatorTokenMap _operatorTokenMap;
		private IPrimitiveTokenParser _primitiveTokenParser;
		private ShittyExpressionTokenizer _tokenizer;

		[SetUp]
		public void SetUp()
		{
			_operatorTokenMap = Substitute.For<IOperatorTokenMap>();
			_primitiveTokenParser = Substitute.For<IPrimitiveTokenParser>();
			_tokenizer = new ShittyExpressionTokenizer(_operatorTokenMap, _primitiveTokenParser);
		}

		[Test]
		public void Parse()
		{
			var operatorToken = Substitute.For<IExpressionToken>();
			_operatorTokenMap.GetOperator("+").Returns(operatorToken);

			var jArray = new JArray
			{
				"three",
				"plus",
				"eight"
			};
			_primitiveTokenParser.Parse(typeof(int), jArray[0]).Returns(3);
			_primitiveTokenParser.Parse(typeof(string), jArray[1]).Returns("+");
			_primitiveTokenParser.Parse(typeof(int), jArray[2]).Returns(5);

			var tokens = _tokenizer.GetTokens(typeof(int), jArray).ToList();

			Assert.AreEqual(3, tokens.Count);
			Assert.AreEqual(3, tokens[0].GetValue());
			Assert.AreEqual(5, tokens[1].GetValue());
			Assert.AreSame(operatorToken, tokens[2]);
		}

		[Test]
		public void ParseOneValue()
		{
			var jArray = new JArray
			{
				"@threepointfive"
			};
			_primitiveTokenParser.Parse(typeof(float), jArray[0]).Returns(3.5f);

			var tokens = _tokenizer.GetTokens(typeof(float), jArray).ToList();

			Assert.AreEqual(1, tokens.Count);
			Assert.AreEqual(3.5f, tokens[0].GetValue());
		}

		[Test]
		public void ParseVector3()
		{
			var operatorToken = Substitute.For<IExpressionToken>();
			_operatorTokenMap.GetOperator("+").Returns(operatorToken);

			var jArray = new JArray
			{
				new JObject
				{
					{ "x", "@One" },
					{ "y", 2 },
					{ "z", 3 }
				},
				"plus",
				"@Test"
			};
			_primitiveTokenParser.Parse(typeof(float), jArray[0]["x"]).Returns(1);
			_primitiveTokenParser.Parse(typeof(float), jArray[0]["y"]).Returns(2);
			_primitiveTokenParser.Parse(typeof(float), jArray[0]["z"]).Returns(3);
			_primitiveTokenParser.Parse(typeof(string), jArray[1]).Returns("+");
			_primitiveTokenParser.Parse(typeof(Vector3), jArray[2]).Returns(new Vector3(2, 3, 4));

			var tokens = _tokenizer.GetTokens(typeof(Vector3), jArray).ToList();

			Assert.AreEqual(3, tokens.Count);
			Assert.AreEqual(new Vector3(1, 2, 3), tokens[0].GetValue());
			Assert.AreEqual(new Vector3(2, 3, 4), tokens[1].GetValue());
			Assert.AreSame(operatorToken, tokens[2]);
		}

        [Test]
		public void ParseObjectThrowsException()
		{
			var jArray = new JArray { "Hi" };
		    var exception = Assert.Throws<ArgumentException>(() =>
		    {
		        _tokenizer.GetTokens(typeof(TestObject), jArray);
		    });

		    Assert.AreEqual("Cannot get tokens for type 'TestObject'.", exception.Message);
		}

		private class TestObject { }

        [Test]
        public void ParseNoValuesThrowsException()
        {
            var jArray = new JArray();

            var exception = Assert.Throws<ArgumentException>(() =>
            {
                _tokenizer.GetTokens(typeof(int), jArray);
            });
            Assert.AreEqual("Expression array is of wrong size: Expecting 1 or 3. Shit!", exception.Message);
        }

        [Test]
        public void ParseTooManyValuesThrowsException()
        {
            var jArray = new JArray
                  {
                      1,
                      2,
                      3,
                      4
                  };

            var exception = Assert.Throws<ArgumentException>(() =>
            {
                _tokenizer.GetTokens(typeof(int), jArray);
            });
            Assert.AreEqual("Expression array is of wrong size: Expecting 1 or 3. Shit!", exception.Message);
        }

        // TODO null arguments
    }
}
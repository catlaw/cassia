﻿using System;
using Game.ConfigActions.Params;
using Newtonsoft.Json.Linq;
using NSubstitute;
using NUnit.Framework;
using UnityEngine;

namespace UnitTest.Game.ConfigActions.Params
{
	[TestFixture]
	public class PrimitiveTokenParserTest
	{
		private IEventParamsParser _eventParamsParser;
		private PrimitiveTokenParser _parser;

		[SetUp]
		public void SetUp()
		{
			_eventParamsParser = Substitute.For<IEventParamsParser>();
			_parser = new PrimitiveTokenParser(_eventParamsParser);
		}

		[Test]
		public void ParseWithoutReplacing()
		{
			var type = typeof(int);
			var jToken = new JValue(3);

            var result = _parser.Parse(type, jToken);

			Assert.AreEqual(3, result);
		}

		[Test]
		public void ParseStringWithoutReplacing()
		{
			var type = typeof(string);
			var jToken = new JValue("three");
		    _eventParamsParser.IsVariable("three").Returns(false);

			var result = _parser.Parse(type, jToken);

			Assert.AreEqual("three", result);
		}

        [Test]
        public void ParseStringWithReplacing()
        {
            var type = typeof(string);
            var jToken = new JValue("three");
            _eventParamsParser.IsVariable("three").Returns(true);
            _eventParamsParser.ParseVariable("three").Returns("four");
;
            var result = _parser.Parse(type, jToken);

            Assert.AreEqual("four", result);
        }

        [Test]
		public void ParseVector3()
		{
			var type = typeof(Vector3);
			var jToken = new JObject
			{
				{"x", 3},
				{"y", 3},
				{"z", 3}
			};

			var result = _parser.Parse(type, jToken);

			Assert.AreEqual(new Vector3(3, 3, 3), result);
		}

        [Test]
        public void ParseVector3Incomplete()
        {
            var type = typeof(Vector3);
            var jToken = new JObject
            {
                {"y", 3}
            };

            var result = _parser.Parse(type, jToken);

            Assert.AreEqual(new Vector3(0, 3, 0), result);
        }

        [Test]
		public void ParseEventParam()
		{
			var type = typeof(Vector3);
			var jToken = new JValue("three");
		    _eventParamsParser.IsVariable(jToken).Returns(true);
			_eventParamsParser.ParseVariable("three").Returns(new Vector3(3, 3, 3));

			var result = _parser.Parse(type, jToken);

			Assert.AreEqual(new Vector3(3, 3, 3), result);
		}

	    [Test]
	    public void ParseNullJToken()
	    {
	        var type = typeof(int);
	        var result = _parser.Parse(type, null);

            Assert.AreEqual(0, result);
	    }

	    [Test]
	    public void ParseNullTypeThrowsException()
	    {
	        Assert.Throws<ArgumentNullException>(() => { _parser.Parse(null, new JValue("Oo")); });
	    }

		[Test]
		public void TypeMismatchThrowsException()
		{
			var type = typeof(int);
			var jToken = new JValue("hello");

			var exception = Assert.Throws<ArgumentException>(() => { _parser.Parse(type, jToken); });
			Assert.AreEqual(exception.Message, "Could not parse token: Failed to convert 'hello' to type 'Int32'.");
		}

		[Test]
		public void NonSupportedTypeThrowsException()
		{
			var type = typeof(TestObject);
			var jToken = new JValue("Hello");

			var exception = Assert.Throws<ArgumentException>(() => { _parser.Parse(type, jToken); });
			Assert.AreEqual(exception.Message, "Could not parse token: Type 'TestObject' is not a supported type.");
		}

		private class TestObject { }
	}
}
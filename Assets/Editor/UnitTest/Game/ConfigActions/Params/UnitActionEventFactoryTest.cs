﻿using Common.DataContainers;
using Game.ConfigActions;
using Game.ConfigActions.Params;
using Game.Controllers;
using Game.Models.TurnTransactions;
using NSubstitute;
using NUnit.Framework;
using UnityEngine;

namespace UnitTest.Game.ConfigActions.Params
{
    [TestFixture]
    public class UnitActionEventFactoryTest
    {
        private ITurnTransaction _turnTransaction;
        private IWorldGridCoordsConverter _gridCoordsConverter;
        private UnitActionEventFactory _factory;

        [SetUp]
        public void SetUp()
        {
            _turnTransaction = Substitute.For<ITurnTransaction>();
            _gridCoordsConverter = Substitute.For<IWorldGridCoordsConverter>();
            _factory = new UnitActionEventFactory(_turnTransaction, _gridCoordsConverter);
        }

        [Test]
        public void CreateEventParams()
        {
            _turnTransaction.MovePosition.Returns(new GridCoords(1, 2));
            _turnTransaction.TargetPosition.Returns(new GridCoords(2, 3));

            _gridCoordsConverter.GetWorldPosition(new GridCoords(1, 2)).Returns(new Vector3(1, 2, 3));
            _gridCoordsConverter.GetWorldPosition(new GridCoords(2, 3)).Returns(new Vector3(2, 3, 4));

            var eventParams = (UnitActionEvent)_factory.CreateEvent();

            Assert.AreEqual(_turnTransaction.Actor, eventParams.Actor);
            Assert.AreEqual(new Vector3(1, 2, 3), eventParams.ActorPosition);
            Assert.AreEqual(new Vector3(2, 3, 4), eventParams.TargetPosition);
        }
    }
}
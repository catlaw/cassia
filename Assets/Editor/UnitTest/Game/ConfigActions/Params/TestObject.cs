﻿using System;

namespace UnitTest.Game.ConfigActions.Params
{
	internal class TestObject
	{
		public string TestString { get; set; }
		public int TestInt { get; set; }

		private bool Equals(TestObject other)
		{
			return String.Equals(TestString, other.TestString) && TestInt == other.TestInt;
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			if (obj.GetType() != this.GetType()) return false;
			return Equals((TestObject) obj);
		}

		public override int GetHashCode()
		{
			unchecked
			{
				return ((TestString != null ? TestString.GetHashCode() : 0)*397) ^ TestInt;
			}
		}

		public static bool operator ==(TestObject left, TestObject right)
		{
			return Equals(left, right);
		}

		public static bool operator !=(TestObject left, TestObject right)
		{
			return !Equals(left, right);
		}
	}
}
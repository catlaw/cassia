﻿using System.Collections.Generic;
using System.Linq;
using Game.ConfigActions.Params;
using Game.Models.Occupants.Units.UnitActions;
using Game.Models.TurnTransactions;
using NSubstitute;
using NUnit.Framework;

namespace UnitTest.Game.ConfigActions.Params
{
	[TestFixture]
	public class ActionEffectedUnitFactoryTest
	{
		private ITurnTransaction _turnTransaction;
		private ActionEffectedUnitFactory _factory;

		[SetUp]
		public void SetUp()
		{
			_turnTransaction = Substitute.For<ITurnTransaction>();
			_factory = new ActionEffectedUnitFactory(_turnTransaction);
		}

		[Test]
		public void CreateEvents()
		{
			var actionOutcome0 = Substitute.For<IActionOutcome>();
			var actionEffects0 = new List<IActionEffect>
			{
				Substitute.For<IActionEffect>(),
				Substitute.For<IActionEffect>()
			};
			actionOutcome0.Effects.Returns(actionEffects0);

			var actionOutcome1 = Substitute.For<IActionOutcome>();
			var actionEffects1 = new List<IActionEffect>
			{
				Substitute.For<IActionEffect>()
			};
			actionOutcome1.Effects.Returns(actionEffects1);

			_turnTransaction.Outcome.ActionOutcomes.Returns(new List<IActionOutcome>
			{
				actionOutcome0,
				actionOutcome1
			});

		    var actionAffectedUnits = _factory.GetActionEffectedUnits().Select(x => x.ToList()).ToList();

            Assert.AreSame(actionOutcome0.AffectedUnit, actionAffectedUnits[0][0].AffectedUnit);
		    Assert.AreSame(actionEffects0[0], actionAffectedUnits[0][0].ActionEffect);

            Assert.AreSame(actionOutcome1.AffectedUnit, actionAffectedUnits[0][1].AffectedUnit);
            Assert.AreSame(actionEffects1[0], actionAffectedUnits[0][1].ActionEffect);

            Assert.AreSame(actionOutcome0.AffectedUnit, actionAffectedUnits[1][0].AffectedUnit);
            Assert.AreSame(actionEffects0[1], actionAffectedUnits[1][0].ActionEffect);
        }
	}
}
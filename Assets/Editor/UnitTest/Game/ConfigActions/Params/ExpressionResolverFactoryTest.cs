﻿using Game.Expressions;
using NUnit.Framework;

namespace UnitTest.Game.ConfigActions.Params
{
    [TestFixture]
    public class ExpressionResolverFactoryTest
    {
        private ExpressionResolverFactory _factory;

        [SetUp]
        public void SetUp()
        {
            _factory = new ExpressionResolverFactory();
        }

        [Test]
        public void CreateExpressionResolver()
        {
            var resolver = _factory.CreateExpressionResolver();

            Assert.IsInstanceOf<IExpressionResolver>(resolver);
        }
    }
}
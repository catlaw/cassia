﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common.DataContainers;
using Game.ConfigActions.Params;
using Game.Controllers;
using Game.Controllers.Units;
using Game.Models.Occupants.Units;
using Game.Models.Occupants.Units.UnitActions;
using Game.Models.TurnTransactions;
using NSubstitute;
using NUnit.Framework;
using UnityEngine;

namespace UnitTest.Game.ConfigActions.Params
{
	[TestFixture]
	public class ActionEffectEventFactoryTest
	{
		private IWorldGridCoordsConverter _converter;
		private ActionEffectEventFactory _factory;

		[SetUp]
		public void SetUp()
		{
			_converter = Substitute.For<IWorldGridCoordsConverter>();
			_factory = new ActionEffectEventFactory(_converter);
		}

		[Test]
		public void CreateEvent()
		{
			var affectedUnit = Substitute.For<IAffectedUnit>();
			affectedUnit.GridCoords.Returns(new GridCoords(1, 2));

			var actionEffect = Substitute.For<IActionEffect>();
			actionEffect.StatType.Returns(StatType.Health);
			actionEffect.Delta.Returns(-11);

			_converter.GetWorldPosition(new GridCoords(1, 2)).Returns(new Vector3(1, 2, 3));

			var gameEvent = (ActionEffectEvent)_factory.CreateEvent(affectedUnit, actionEffect);

            Assert.AreSame(affectedUnit.Unit, gameEvent.AffectedUnit);
			Assert.AreEqual(new Vector3(1, 2, 3), gameEvent.TargetPosition);
			Assert.AreEqual(StatType.Health, gameEvent.StatType);
			Assert.AreEqual(-11, gameEvent.Delta);
		}

		[Test]
		public void NullArgumentsThrowsException()
		{
			Assert.Throws<ArgumentNullException>(() =>
			{
				_factory.CreateEvent(null, Substitute.For<IActionEffect>());
			});

			Assert.Throws<ArgumentNullException>(() =>
			{
				_factory.CreateEvent(Substitute.For<IAffectedUnit>(), null);
			});
		}
	}
}
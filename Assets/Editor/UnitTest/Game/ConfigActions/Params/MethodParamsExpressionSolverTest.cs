﻿using System;
using System.Collections.Generic;
using Game.ConfigActions.Params;
using Game.Expressions;
using Newtonsoft.Json.Linq;
using NSubstitute;
using NUnit.Framework;
using strange.framework.api;

namespace UnitTest.Game.ConfigActions.Params
{
	[TestFixture]
	public class MethodParamsExpressionSolverTest
	{
		private IExpressionTokenizer _tokenizer;
	    private IExpressionResolverFactory _resolverFactory;
		private MethodParamsExpressionSolver _solver;

		[SetUp]
		public void SetUp()
		{
			_tokenizer = Substitute.For<IExpressionTokenizer>();
		    _resolverFactory = Substitute.For<IExpressionResolverFactory>();
			_solver = new MethodParamsExpressionSolver(_tokenizer, _resolverFactory);
		}

		[Test]
		public void Solve()
		{
			var jArray = new JArray();
			var expressionTokens = new List<IExpressionToken>
			{
				Substitute.For<IExpressionToken>(),
				Substitute.For<IExpressionToken>(),
				Substitute.For<IExpressionToken>()
			};
			_tokenizer.GetTokens(typeof(int), jArray).Returns(expressionTokens);

		    var resolver = Substitute.For<IExpressionResolver>();
		    _resolverFactory.CreateExpressionResolver().Returns(resolver);

		    resolver.Resolve().Returns(88);

			var result = _solver.Solve(typeof(int), jArray);

			Received.InOrder(() =>
			{
				expressionTokens[0].Apply(typeof(int), resolver);
				expressionTokens[1].Apply(typeof(int), resolver);
				expressionTokens[2].Apply(typeof(int), resolver);
			});

            Assert.AreEqual(88, result);
		}

		[Test]
		public void SolveWithNullArgsThrowsException()
		{
			Assert.Throws<ArgumentNullException>(() =>
			{
				_solver.Solve(null, new JArray());
			});

			Assert.Throws<ArgumentNullException>(() =>
			{
				_solver.Solve(typeof(string), null);
			});
		}
	}
}
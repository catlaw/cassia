﻿using System;
using System.Collections;
using System.Collections.Generic;
using Game.ConfigActions.GameObjects;
using Game.ConfigActions.Params;
using Newtonsoft.Json.Linq;
using NSubstitute;
using NSubstitute.Core;
using NUnit.Framework;
using UnityEngine;

namespace UnitTest.Game.ConfigActions.Params
{
    [TestFixture]
    public class MethodParamParserTest
    {
	    private IMethodParamsExpressionSolver _expressionSolver;
        private IEventParamsParser _eventParamsParser;
        private MethodParamParser _parser;

        [SetUp]
        public void SetUp()
        {
	        _expressionSolver = Substitute.For<IMethodParamsExpressionSolver>();
            _eventParamsParser = Substitute.For<IEventParamsParser>();
			_parser = new MethodParamParser(_expressionSolver, _eventParamsParser);
        }

        [Test]
        public void ParseString()
        {
            var jValue = new JValue("helloworld");
            _eventParamsParser.IsVariable(jValue).Returns(false);

            var result = _parser.Parse(jValue, typeof(string));
            
            Assert.AreEqual("helloworld", result);
        }

        [Test]
        public void ParseReplaced()
        {
            var jValue = new JValue("helloworld");
            _eventParamsParser.IsVariable(jValue).Returns(true);
            _eventParamsParser.ParseVariable(jValue).Returns("byeworld");

            var result = _parser.Parse(jValue, typeof(string));
            
            Assert.AreEqual("byeworld", result);
        }

        [Test]
        public void ParseExpression()
        {
            var jArray = new JArray {3, "+", 2};
            _eventParamsParser.IsVariable(jArray).Returns(false);
            _expressionSolver.Solve(typeof(int), jArray.Children()).Returns(5);

            var result = _parser.Parse(jArray, typeof(int));

            Assert.AreEqual(5, result);
        }

        [Test]
        public void ParseObject()
        {
            var jObject = new JObject
            {
                {"TestString", "HelloWorld"},
                {"TestFloat", 4.5f}
            };
            _eventParamsParser.IsVariable(jObject).Returns(false);

            var result = (TestObject) _parser.Parse(jObject, typeof(TestObject));

            Assert.AreEqual("HelloWorld", result.TestString);
            Assert.AreEqual(4.5f, result.TestFloat);
        }

        private class TestObject
        {
            public string TestString { get; set; }
            public float TestFloat { get; set; }
        }

        [Test]
        public void ParseList()
        {
            var jArray = new JArray { "one", "two", "three" };
            _eventParamsParser.IsVariable(jArray[0]).Returns(false);
            _eventParamsParser.IsVariable(jArray[1]).Returns(false);
            _eventParamsParser.IsVariable(jArray[2]).Returns(false);

            var result = _parser.Parse(jArray, typeof(IEnumerable<string>));

            CollectionAssert.AreEqual(new List<string> { "one", "two", "three" }, (IEnumerable<string>) result);
        }

        [Test]
        public void ParseNestedList()
        {
            var jArray = new JArray
            {
                new JArray { 1, "+", 2 },
                "@Hello"
            };

            _expressionSolver.Solve(typeof(int), jArray[0].Children()).Returns(4);

            _eventParamsParser.IsVariable(jArray[0][0]).Returns(false);
            _eventParamsParser.IsVariable(jArray[0][1]).Returns(false);
            _eventParamsParser.IsVariable(jArray[0][2]).Returns(false);
            _eventParamsParser.IsVariable(jArray[1]).Returns(true);
            _eventParamsParser.ParseVariable(jArray[1]).Returns(3);

            var result = _parser.Parse(jArray, typeof(IEnumerable<int>));

            CollectionAssert.AreEqual(new List<int> { 4, 3 }, (IEnumerable<int>)result);

        }

        // TODO expression or whatever in list

        // TODO dictionaries?

        [Test]
        public void ParseConfigGameObjectId()
        {
            var jValue = new JValue("Poop");
            _eventParamsParser.IsVariable(jValue).Returns(false);

            var result = (IGameObjectId) _parser.Parse(jValue, typeof(IGameObjectId));

            Assert.AreSame(_eventParamsParser.ParseConfigGameObjectId(jValue), result);
        }

        [Test]
        public void WrongTypeThrowsException()
        {
            var jValue = new JValue("Poop");

            var exception = Assert.Throws<ArgumentException>(() =>
            {
                _parser.Parse(jValue, typeof(int));
            });
            Assert.AreEqual("Error parsing Method Param: Could not convert 'Poop' to 'Int32'.", 
                exception.Message);
        }

        [Test]
        public void ParseNullThrowsException()
        {
            Assert.Throws<ArgumentNullException>(() => { _parser.Parse(null, typeof(string)); });
            Assert.Throws<ArgumentNullException>(() => { _parser.Parse(new JValue("hi"), null); });
        }
    }
}
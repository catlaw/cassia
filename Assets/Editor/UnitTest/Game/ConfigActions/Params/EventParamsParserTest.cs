﻿using System;
using Game.ConfigActions.Params;
using Newtonsoft.Json.Linq;
using NSubstitute;
using NUnit.Framework;
using System.Collections.Generic;
using Game.ConfigActions.GameObjects;

namespace UnitTest.Game.ConfigActions.Params
{
	[TestFixture]
	public class EventParamsParserTest
	{
		private IEventPropertyGetter _propertyGetter;
	    private IGameObjectIdFactory _gameObjectIdFactory;
		private EventParamsParser _parser;

		[SetUp]
		public void SetUp()
		{
			_propertyGetter = Substitute.For<IEventPropertyGetter>();
		    _gameObjectIdFactory = Substitute.For<IGameObjectIdFactory>();
			_parser = new EventParamsParser(_propertyGetter, _gameObjectIdFactory);
		}

		[Test]
		public void ParseVariable()
		{
			_propertyGetter.GetPropertyValue("Hello").Returns("Goodbye");

            var jValue = new JValue("@Hello");
			var result = _parser.ParseVariable(jValue);

			Assert.AreEqual("Goodbye", result);
		}

        [Test]
        public void ParseVariableWithNullArgumentThrowsException()
        {
            Assert.Throws<ArgumentNullException>(() => { _parser.ParseVariable(null); });
        }

        [Test]
	    public void IsVariable()
	    {
	        Assert.IsTrue(_parser.IsVariable(new JValue("@Hello")));
	        Assert.IsFalse(_parser.IsVariable(new JValue("Hello")));
            Assert.IsFalse(_parser.IsVariable(new JValue(3)));
        }

	    [Test]
	    public void IsVariableWithNullArgumentThrowsException()
	    {
	        Assert.Throws<ArgumentNullException>(() => { _parser.IsVariable(null); });
	    }

	    [Test]
	    public void ParseConfigGameObjectId()
	    {
	        var gameEvent = Substitute.For<IGameEvent>();
	        _propertyGetter.GetEvent().Returns(gameEvent);

	        var jValue = new JValue("TestId");
	        var result = _parser.ParseConfigGameObjectId(jValue);

            Assert.AreSame(_gameObjectIdFactory.CreateId(gameEvent, "TestId"), result);
	    }

	    [Test]
	    public void ParseConfigGameObjectIdWithNullArgumentThrowsException()
	    {
	        Assert.Throws<ArgumentNullException>(() => { _parser.ParseConfigGameObjectId(null); });
	    }
	}
}
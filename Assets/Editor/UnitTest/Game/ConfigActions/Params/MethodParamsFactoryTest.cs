﻿using System;
using Game.ConfigActions;
using Game.ConfigActions.GameObjects;
using Game.ConfigActions.Params;
using Newtonsoft.Json.Linq;
using NSubstitute;
using NUnit.Framework;
using UnityEngine;

namespace UnitTest.Game.ConfigActions.Params
{
    [TestFixture]
    public class MethodParamsFactoryTest
    {
        private IMethodParamParserFactory _methodParamParserFactory;

        [SetUp]
        public void SetUp()
        {
            _methodParamParserFactory = Substitute.For<IMethodParamParserFactory>();
        }

        private class TestMethodParams
        {
            public Vector3 TestPosition { get; set; }
            public int TestInt { get; set; }
            public float TestFloat { get; set; }
            public string TestString { get; set; }
            public bool TestBool { get; set; }
			public TestEnum TestEnum { get; set; }
            public IGameObjectId TestId { get; set; }
        }

	    public enum TestEnum { One, Two, Three }

        [Test]
        public void CreateMethodParamsFactoryWithNullArgumentsThrowsException()
        {
            Assert.Throws<ArgumentNullException>(() =>
            {
                new MethodParamsFactory(null, _methodParamParserFactory);
            });

            Assert.Throws<ArgumentNullException>(() =>
            {
                new MethodParamsFactory(null, _methodParamParserFactory);
            });
        }

        [Test]
        public void CreateMethodParamsForProperties()
        {
            var jObject = new JObject
            {
                { "TestPosition", 0 },
                { "TestInt", 0 },
                { "TestFloat", 0 },
                { "TestString", 0 },
                { "TestBool", 0 },
                { "TestEnum", 0 },
                { "TestId", 0 }
            };

            var eventParams = Substitute.For<IGameEvent>();
            var configGameObjectId = Substitute.For<IGameObjectId>();

            var methodParamParser = Substitute.For<IMethodParamParser>();
            methodParamParser.Parse(jObject["TestPosition"], typeof(Vector3)).Returns(new Vector3(1, 2, 3));
            methodParamParser.Parse(jObject["TestInt"], typeof(int)).Returns(3);
            methodParamParser.Parse(jObject["TestFloat"], typeof(float)).Returns(3.5f);
            methodParamParser.Parse(jObject["TestString"], typeof(string)).Returns("THREE");
            methodParamParser.Parse(jObject["TestBool"], typeof(bool)).Returns(true);
            methodParamParser.Parse(jObject["TestEnum"], typeof(TestEnum)).Returns(TestEnum.Two);
            methodParamParser.Parse(jObject["TestId"], typeof(IGameObjectId)).Returns(configGameObjectId);

            _methodParamParserFactory.CreateMethodParamParser(eventParams).Returns(methodParamParser);

            var factory = new MethodParamsFactory(jObject, _methodParamParserFactory);
            var methodParams = factory.CreateMethodParams<TestMethodParams>(eventParams);

            Assert.AreEqual(new Vector3(1, 2, 3), methodParams.TestPosition);
            Assert.AreEqual(3, methodParams.TestInt);
            Assert.AreEqual(3.5f, methodParams.TestFloat);
            Assert.AreEqual("THREE", methodParams.TestString);
            Assert.IsTrue(methodParams.TestBool);
            Assert.AreEqual(TestEnum.Two, methodParams.TestEnum);
            Assert.AreSame(configGameObjectId, methodParams.TestId);
        }

        [Test]
        public void CreateMethodParamsWithNonMatchedParsedValue()
        {
            var jObject = new JObject
            {
                {"TestString", 0}
            };

            var eventParams = Substitute.For<IGameEvent>();

            var methodParamsParser = Substitute.For<IMethodParamParser>();
            methodParamsParser.Parse(jObject["TestString"], typeof(string)).Returns(42);

            _methodParamParserFactory.CreateMethodParamParser(eventParams).Returns(methodParamsParser);

            var factory = new MethodParamsFactory(jObject, _methodParamParserFactory);
            var methodParams = factory.CreateMethodParams<TestMethodParams>(eventParams);

            Assert.AreEqual("42", methodParams.TestString);
        }

        [Test]
        public void CreateMethodParamsWithUnknownPropertyThrowsException()
        {
            var jObject = new JObject
            {
                {"Poop", 0}
            };
            var factory = new MethodParamsFactory(jObject, _methodParamParserFactory);
            var eventParams = Substitute.For<IGameEvent>();

            var exception = Assert.Throws<ArgumentException>(() => { factory.CreateMethodParams<TestMethodParams>(eventParams); });
            Assert.AreEqual("Cannot create 'TestMethodParams' with unknown property 'Poop'.", exception.Message);
        }

        [Test]
        public void CreateMethodParamsWithNullEventParamsThrowsException()
        {
            var jObject = new JObject();
            var factory = new MethodParamsFactory(jObject, _methodParamParserFactory);

            Assert.Throws<ArgumentNullException>(() =>
            {
                factory.CreateMethodParams<TestMethodParams>(null);
            });
        }
    }
}
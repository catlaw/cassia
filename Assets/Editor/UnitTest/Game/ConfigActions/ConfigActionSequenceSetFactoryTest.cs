﻿using System.Collections.Generic;
using Common.Exceptions;
using Configs;
using Game.ConfigActions;
using Game.ConfigActions.Methods;
using Game.Views;
using NSubstitute;
using NUnit.Framework;
using strange.framework.api;

namespace UnitTest.Game.ConfigActions
{
    [TestFixture]
    public class ConfigActionSequenceSetFactoryTest
    {
        private IInstanceProvider _instanceProvider;
        private IConfigActionSequenceFactory _sequenceFactory;
        private ConfigActionSequenceSetFactory _factory;

        [SetUp]
        public void SetUp()
        {
            _instanceProvider = Substitute.For<IInstanceProvider>();
            _sequenceFactory = Substitute.For<IConfigActionSequenceFactory>();
            _factory = new ConfigActionSequenceSetFactory(_instanceProvider, _sequenceFactory);
        }

        [Test]
        public void CreateConfigActionSequenceSet()
        {
            var startSequenceConfig = new ConfigActionSequenceConfig();
            var hitSequenceConfig = new ConfigActionSequenceConfig();

            var configActions = new Dictionary<UnitActionEventType, ConfigActionSequenceConfig>
            {
                { UnitActionEventType.PreHit, startSequenceConfig },
                { UnitActionEventType.Hit, hitSequenceConfig }
            };

            var preHitSequence = Substitute.For<IConfigAction>();
            var hitSequence = Substitute.For<IConfigAction>();

            _sequenceFactory.CreateConfigActionSequence(startSequenceConfig).Returns(preHitSequence);
            _sequenceFactory.CreateConfigActionSequence(hitSequenceConfig).Returns(hitSequence);

            var expectedSet = Substitute.For<IConfigActionSet>();
            _instanceProvider.GetInstance<IConfigActionSet>().Returns(expectedSet);

            var actualSet = _factory.CreateConfigActionSequenceSet(configActions);

            Assert.AreSame(expectedSet, actualSet);
            expectedSet.Received(1).Add(UnitActionEventType.PreHit, preHitSequence);
            expectedSet.Received(1).Add(UnitActionEventType.Hit, hitSequence);
        }

        [Test]
        public void CreateEmpty()
        {
            var set = _factory.CreateEmpty();

            Assert.AreSame(_instanceProvider.GetInstance<IConfigActionSet>(), set);
        }
    }
}
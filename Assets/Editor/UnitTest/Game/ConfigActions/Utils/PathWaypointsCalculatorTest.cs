﻿using System;
using System.Collections.Generic;
using Game.ConfigActions.Methods;
using Game.ConfigActions.Util;
using NSubstitute;
using NUnit.Framework;
using UnityEngine;

namespace UnitTest.Game.ConfigActions.Utils
{
    [TestFixture]
    public class PathWaypointsCalculatorTest
    {
        private IParametricPointCalculator _parametricPointCalculator;
        private PathWaypointsCalculator _calculator;

        [SetUp]
        public void SetUp()
        {
            _parametricPointCalculator = Substitute.For<IParametricPointCalculator>();
            _calculator = new PathWaypointsCalculator(_parametricPointCalculator);
        }

        [Test]
        public void GetPathWaypoints()
        {
            var origin = new Vector3(1, 1, 1);
            var destination = new Vector3(2, 2, 2);
            var parametricPoints = new List<ParametricPoint>
            {
                new ParametricPoint { Parameter = 0.2f, Offset = new Vector3(3, 3, 3) },
                new ParametricPoint { Parameter = 0.8f, Offset = new Vector3(4, 4, 4) }
            };

            _parametricPointCalculator.GetParametricPoint(0.2f, new Vector3(3, 3, 3),
                origin, destination).Returns(new Vector3(5, 5, 5));

            _parametricPointCalculator.GetParametricPoint(0.8f, new Vector3(4, 4, 4),
                origin, destination).Returns(new Vector3(6, 6, 6));

            var expectedWaypoints = new List<Vector3>
            {
                new Vector3(5, 5, 5),
                new Vector3(6, 6, 6),
                new Vector3(2, 2, 2)
            };

            var actualWaypoints = _calculator.GetPathWaypoints(origin, destination, parametricPoints);

            CollectionAssert.AreEqual(expectedWaypoints, actualWaypoints);
        }

        [Test]
        public void NullParametricPointsThrowsException()
        {
            Assert.Throws<ArgumentNullException>(() =>
            {
                _calculator.GetPathWaypoints(new Vector3(1, 2, 3), new Vector3(1, 2, 3), null);
            });
        }
    }
}
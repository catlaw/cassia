﻿using Game.ConfigActions.Util;
using NUnit.Framework;
using UnityEngine;

namespace UnitTest.Game.ConfigActions.Utils
{
	[TestFixture]
	public class ParametricPointCalculatorTest
	{
		private ParametricPointCalculator _calculator;

		[SetUp]
		public void SetUp()
		{
			_calculator = new ParametricPointCalculator();	
		}

		[Test]
		public void GetMidPoint()
		{
			var origin = new Vector3(0, 0, 0);
			var destination = new Vector3(10, 0, 0);
			var parameter = 0.5f;
			var offset = new Vector3(0, 2, 0);

			var point = _calculator.GetParametricPoint(parameter, offset, origin, destination);

			Assert.AreEqual(new Vector3(5, 2, 0), point);
		}

		[Test]
		public void GetStartingPoint()
		{
			var origin = new Vector3(1, 2, 3);
			var destination = new Vector3(4, 5, 6);
			var parameter = 0f;
			var offset = new Vector3(1, 1, 1);

			var point = _calculator.GetParametricPoint(parameter, offset, origin, destination);

			Assert.AreEqual(new Vector3(2, 3, 4), point);
		}

		[Test]
		public void GetEndingPoint()
		{
			var origin = new Vector3(1, 2, 3);
			var destination = new Vector3(4, 5, 6);
			var parameter = 1f;
			var offset = new Vector3(1, 1, 1);

			var point = _calculator.GetParametricPoint(parameter, offset, origin, destination);

			Assert.AreEqual(new Vector3(5, 6, 7), point);
		}

		[Test]
		public void GetParameterLargerThanOne()
		{
			var origin = new Vector3(0, 5, 0);
			var destination = new Vector3(0, -5, 0);
			var parameter = 2f;
			var offset = new Vector3(1, 1, 1);

			var point = _calculator.GetParametricPoint(parameter, offset, origin, destination);

			Assert.AreEqual(new Vector3(1, -14, 1), point);
		}

		[Test]
		public void GetParameterLessThanZero()
		{
			var origin = new Vector3(0, 5, 0);
			var destination = new Vector3(0, -5, 0);
			var parameter = -2f;
			var offset = new Vector3(1, 1, 1);

			var point = _calculator.GetParametricPoint(parameter, offset, origin, destination);

			Assert.AreEqual(new Vector3(1, 26, 1), point);
		}
	}
}
﻿using System;
using System.Collections.Generic;
using Configs;
using Game.ConfigActions.Util;
using NSubstitute;
using NUnit.Framework;
using UnityEngine;

namespace UnitTest.Game.ConfigActions.Utils
{
    [TestFixture]
    public class DurationCalculatorTest
    {
	    private IConfig<MapViewConfig> _mapViewConfig;
        private DurationCalculator _calculator;

        [SetUp]
        public void SetUp()
        {
	        _mapViewConfig = Substitute.For<IConfig<MapViewConfig>>();
            _calculator = new DurationCalculator(_mapViewConfig);
        }

        [Test]
        public void GetDuration()
        {
            Assert.AreEqual(20f, _calculator.GetDuration(new Vector3(0, 0, 0), new Vector3(10, 0, 0), 0.5f));
            Assert.AreEqual(2.5f, _calculator.GetDuration(new Vector3(0, 5, 0), new Vector3(0, 0, 0), 2f));
            Assert.AreEqual(0f, _calculator.GetDuration(new Vector3(0, 0, 0), new Vector3(0, 0, 0), 0.1f));
        }

        [Test]
        public void GetDurationForPath()
        {
            var path = new List<Vector3>
            {
                new Vector3(0, 0, 0),
                new Vector3(0, 0, 10),
                new Vector3(10, 0, 10)
            };
            Assert.AreEqual(20f/2.4f, _calculator.GetDuration(path, 2.4f));
        }

        [Test]
        public void GetDurationForPathWith0Speed()
        {
            var path = new List<Vector3>
            {
                new Vector3(1, 1, 1),
                new Vector3(2, 2, 2)
            };
            var exception = Assert.Throws<ArgumentException>(() =>
            {
                _calculator
                .GetDuration(path, 0);
            });

            Assert.AreEqual("Cannot get duration with 0 or less speed.", exception.Message);
        }

        [Test]
        public void GetDurationForPathWithOnePoint()
        {
            var path = new List<Vector3> {new Vector3(1, 2, 3)};
            Assert.AreEqual(0, _calculator.GetDuration(path, 1.1f));
        }

        [Test]
        public void GetDurationWithEmptyPathThrowsException()
        {
            var exception = Assert.Throws<ArgumentException>(() =>
            {
                _calculator.GetDuration(new List<Vector3>(), 1);
            });

            Assert.AreEqual("Cannot get duration for empty path.", exception.Message);
        }

        [Test]
        public void GetDurationWithNullPathThrowsException()
        {
            Assert.Throws<ArgumentNullException>(() => _calculator.GetDuration(null, 1));
        }

        [Test]
        public void GetDurationWith0Speed()
        {
            var exception = Assert.Throws<ArgumentException>(() =>
            {
                _calculator.GetDuration(new Vector3(0, 0, 0), new Vector3(1, 2, 3), 0);
            });

            Assert.AreEqual("Cannot get duration with 0 or less speed.", exception.Message);
        }

	    [Test]
	    public void GetDurationForOneSquare()
	    {
		    _mapViewConfig.Get().Returns(new MapViewConfig {ChunkSize = 0.5f});

		    var expectedDuration = (0.5f*3)/1.4f;
		    var actualDuration = _calculator.GetUnitMoveDuration(3, 1.4f);

			Assert.AreEqual(expectedDuration, actualDuration);
	    }

	    [Test]
	    public void GetDurationForOneSquareWith0Speed()
	    {
			var exception = Assert.Throws<ArgumentException>(() =>
			{
				_calculator.GetUnitMoveDuration(1, 0);
			});

			Assert.AreEqual("Cannot get duration with 0 or less speed.", exception.Message);

		}
    }
}
﻿using Common.DataStructures;
using Configs;
using Game.Views;
using NSubstitute;
using NUnit.Framework;

namespace UnitTest.Game.Views
{
	[TestFixture]
	public class UnitActionViewsStoreTest
	{
		private IConfig<AnimationsConfig> _config;
		private UnitActionViewsStore _store;

		[SetUp]
		public void SetUp()
		{
			_config = Substitute.For<IConfig<AnimationsConfig>>();
			_store = new UnitActionViewsStore(_config);
		}

		[Test]
		public void Get()
		{
			var config = new AnimationsConfig
			{
				UnitActions = Substitute.For<ILibrary<UnitActionViewConfig>>()
			};
			var expectedUnitActionViewConfig = new UnitActionViewConfig();
			config.UnitActions.GetById("TestId").Returns(expectedUnitActionViewConfig);

			_config.Get().Returns(config);

			var actualUnitViewConfig = _store.GetByViewId("TestId");

			Assert.AreSame(expectedUnitActionViewConfig, actualUnitViewConfig);
		}
	}
}
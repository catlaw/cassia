﻿using System.Collections.Generic;
using Configs;
using Game.ConfigActions;
using Game.ConfigActions.Methods;
using Game.Views.Animations;
using NSubstitute;
using NUnit.Framework;

namespace UnitTest.Game.Views.Animations
{
    [TestFixture]
    public class CommonConfigActionsTest
    {
        private IConfig<AnimationsConfig> _animationsConfig;
        private IConfigActionSequenceFactory _sequenceFactory;
        private ActionEffectConfigActions _sequences;

        [SetUp]
        public void SetUp()
        {
            _animationsConfig = Substitute.For<IConfig<AnimationsConfig>>();
            _sequenceFactory = Substitute.For<IConfigActionSequenceFactory>();
            _sequences = new ActionEffectConfigActions(_animationsConfig, _sequenceFactory);
        }

        [Test]
        public void Get()
        {
            var configActionSequenceConfig = new ConfigActionSequenceConfig();
            var config = new AnimationsConfig
            {
                ActionEffects = new Dictionary<ActionEffectEventType, ConfigActionSequenceConfig>
                {
                    {ActionEffectEventType.Damage, configActionSequenceConfig}
                }
            };
            _animationsConfig.Get().Returns(config);

            var expectedConfigAction = _sequenceFactory.CreateConfigActionSequence(configActionSequenceConfig);
            var actualConfigAction = _sequences.Get(ActionEffectEventType.Damage);

            Assert.AreSame(expectedConfigAction, actualConfigAction);
        }

        [Test]
        public void GetNonExistant()
        {
            var config = new AnimationsConfig
            {
                ActionEffects = new Dictionary<ActionEffectEventType, ConfigActionSequenceConfig>()
            };
            _animationsConfig.Get().Returns(config);

            var configAction = _sequences.Get(ActionEffectEventType.Damage);

            Assert.IsInstanceOf<EmptyConfigAction>(configAction);
        }
    }
}
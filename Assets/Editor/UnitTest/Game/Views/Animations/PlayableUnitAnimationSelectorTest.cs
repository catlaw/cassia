﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common.DataStructures;
using Configs;
using Game.Models.Occupants.Units;
using Game.Views.Animations;
using NSubstitute;
using NUnit.Framework;

namespace UnitTest.Game.Views.Animations
{
	[TestFixture]
	public class PlayableUnitAnimationSelectorTest
	{
		private IUnit _unit;
		private IUnitViewMap _unitViewMap;
		private PlayableUnitAnimationSelector _selector;

		[SetUp]
		public void SetUp()
		{
			_unit = Substitute.For<IUnit>();

			_unitViewMap = Substitute.For<IUnitViewMap>();
			_selector = new PlayableUnitAnimationSelector(_unitViewMap);
		}

		[Test]
		public void GetAnimation()
		{
			var expectedAnimations = new List<PlayableUnitAnimationConfig>
			{
				new PlayableUnitAnimationConfig(),
				new PlayableUnitAnimationConfig()
			};

			var animations = Substitute.For<ILibrary<AnimationType, IList<PlayableUnitAnimationConfig>>>();
			animations.GetById(AnimationType.Walk).Returns(expectedAnimations);

			var unitView = Substitute.For<IUnitViewData>();
			unitView.Animations.Returns(animations);
			_unitViewMap.GetView(_unit).Returns(unitView);

			var animation = _selector.GetAnimation(_unit, AnimationType.Walk, 1);

			Assert.AreSame(animation, expectedAnimations[1]);
		}

		[Test]
		public void GetAnimationWithFallback()
		{
			var expectedAnimations = new List<PlayableUnitAnimationConfig>
			{
				new PlayableUnitAnimationConfig(), new PlayableUnitAnimationConfig()
			};

			var animations = Substitute.For<ILibrary<AnimationType, IList<PlayableUnitAnimationConfig>>>();
			animations.GetById(AnimationType.Walk).Returns(expectedAnimations);

			var unitView = Substitute.For<IUnitViewData>();
			unitView.Animations.Returns(animations);
			_unitViewMap.GetView(_unit).Returns(unitView);

			var animation = _selector.GetAnimation(_unit, AnimationType.Walk, 1);

			Assert.AreSame(expectedAnimations[1], animation);
		}

		[Test]
		public void GetNoneAnimation()
		{
			var animations = Substitute.For<ILibrary<AnimationType, IList<PlayableUnitAnimationConfig>>>();

			var unitView = Substitute.For<IUnitViewData>();
			unitView.Animations.Returns(animations);
			_unitViewMap.GetView(_unit).Returns(unitView);

			var unitAnimation = Substitute.For<IUnitAnimation>();
			unitAnimation.Unit.Returns(_unit);

			var animation = _selector.GetAnimation(_unit, AnimationType.None, 9001);

			Assert.IsInstanceOf<NonePlayableUnitAnimation>(animation);
		}
	}
}
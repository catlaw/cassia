﻿using System;
using System.Collections.Generic;
using Configs;
using Game.ConfigActions;
using Game.ConfigActions.Methods;
using Game.Models.Occupants.Units;
using Game.Models.Occupants.Units.UnitActions;
using Game.Models.TurnTransactions;
using Game.Views;
using Game.Views.Animations;
using NSubstitute;
using NUnit.Framework;

namespace UnitTest.Game.Views.Animations
{
	[TestFixture]
	public class ConfigActionSetsTest
	{
		private IUnitActionViewsStore _unitActionViewsStore;
	    private IConfigActionSequenceSetFactory _configActionSequenceSetFactory;
		private ConfigActionSets _factory;

		[SetUp]
		public void SetUp()
		{
			_unitActionViewsStore = Substitute.For<IUnitActionViewsStore>();
		    _configActionSequenceSetFactory = Substitute.For<IConfigActionSequenceSetFactory>();
			_factory = new ConfigActionSets(_unitActionViewsStore,
                _configActionSequenceSetFactory);
		}

		[Test]
		public void GetConfigActionSet()
		{
			var unitAction = Substitute.For<IUnitAction>();
			unitAction.ViewId.Returns("ViewId");

			var unitActionView = Substitute.For<IUnitActionView>();
		    unitActionView.ConfigActions.Returns(new Dictionary<UnitActionEventType, ConfigActionSequenceConfig>());
			_unitActionViewsStore.GetByViewId("ViewId").Returns(unitActionView);

		    var sequenceSet = Substitute.For<IConfigActionSet>();
		    _configActionSequenceSetFactory.CreateConfigActionSequenceSet(unitActionView.ConfigActions).Returns(sequenceSet);

			var set = _factory.GetConfigActionSet(unitAction);
			
		    Assert.AreSame(sequenceSet, set);
		}

	    [Test]
	    public void GetConfigActionSetWithNonExistantViewIdReturnsEmpty()
	    {
            var unitAction = Substitute.For<IUnitAction>();
            unitAction.ViewId.Returns(default(string));

	        var set = _factory.GetConfigActionSet(unitAction);
	        Assert.AreSame(_configActionSequenceSetFactory.CreateEmpty(), set);
	    }

		[Test]
		[ExpectedException(typeof(ArgumentNullException))]
		public void GetUnitAnimationStyleWithNullThrowsException()
		{
			_factory.GetConfigActionSet(null);
		}
	}
}
﻿using System;
using System.Collections.Generic;
using Game.ConfigActions;
using Game.Controllers.Units;
using Game.Models.Occupants.Units;
using Game.Models.Occupants.Units.UnitActions;
using Game.Models.TurnTransactions;
using Game.Views.Animations;
using NSubstitute;
using NUnit.Framework;

namespace UnitTest.Game.Views.Animations
{
	[TestFixture]
	public class UnitAnimationFactoryTest
	{
		private ITurnTransaction _turnTransaction;
		private IConfigActionSets _configActionSets;
		private UnitAnimationFactory _factory;

		[SetUp]
		public void SetUp()
		{
			_turnTransaction = Substitute.For<ITurnTransaction>();
			_configActionSets = Substitute.For<IConfigActionSets>();
			_factory = new UnitAnimationFactory(_turnTransaction, _configActionSets);
		}

		[Test]
		public void GetActorUnitAnimation()
		{
			_turnTransaction.IsReady.Returns(true);

			var actor = Substitute.For<IUnit>();
			_turnTransaction.Actor.Returns(actor);

			var unitAction = Substitute.For<IUnitAction>();
			_turnTransaction.UnitAction.Returns(unitAction);

			var actorSet = Substitute.For<IConfigActionSet>();
			_configActionSets.GetConfigActionSet(unitAction).Returns(actorSet);

			var expectedUnitAnimation = new UnitAnimation {Unit = actor, ConfigActionSet = actorSet};

			Assert.AreEqual(expectedUnitAnimation, _factory.GetActorUnitAnimation());
		}

		[Test]
		[ExpectedException(typeof(InvalidOperationException),
			ExpectedMessage = "Cannot get actor UnitAnimation if TurnTransaction is not ready.")]
		public void GetActorUnitAnimationWhenNotReadyThrowsException()
		{
			_turnTransaction.IsReady.Returns(false);
			_factory.GetActorUnitAnimation();
		}
	}
}
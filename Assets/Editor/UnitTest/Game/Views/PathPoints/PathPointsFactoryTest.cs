﻿using System;
using System.Collections.Generic;
using Common.DataContainers;
using Configs;
using Game.ConfigActions.Util;
using Game.Controllers;
using Game.Models.Occupants.Units;
using Game.Models.Stage;
using Game.Views.PathPoints;
using NSubstitute;
using NUnit.Framework;
using UnityEngine;

namespace UnitTest.Game.Views.PathPoints
{
	[TestFixture]
	public class PathPointsFactoryTest
	{
		private IWorldGridCoordsConverter _worldGridCoordsConverter;
		private IDurationCalculator _durationCalculator;
		private IMap _map;
		private PathPointsFactory _factory;

		[SetUp]
		public void SetUp()
		{
			_worldGridCoordsConverter = Substitute.For<IWorldGridCoordsConverter>();
			_durationCalculator = Substitute.For<IDurationCalculator>();
			_map = Substitute.For<IMap>();
			_factory = new PathPointsFactory(_worldGridCoordsConverter, _durationCalculator,
				_map);
		}

		[Test]
		public void GetPathPoints()
		{
			var path = new List<GridCoords>
			{
				new GridCoords(0, 1),
				new GridCoords(1, 2),
				new GridCoords(2, 3),
				new GridCoords(3, 4)
			};

			_worldGridCoordsConverter.GetWorldPosition(new GridCoords(0, 1)).Returns(new Vector3(0, 1, 2));
			_worldGridCoordsConverter.GetWorldPosition(new GridCoords(1, 2)).Returns(new Vector3(1, 2, 3));
			_worldGridCoordsConverter.GetWorldPosition(new GridCoords(2, 3)).Returns(new Vector3(2, 3, 4));
			_worldGridCoordsConverter.GetWorldPosition(new GridCoords(3, 4)).Returns(new Vector3(3, 4, 5));

			var unitView = Substitute.For<IUnitViewData>();
			unitView.MoveSpeed.Returns(3.5f);
			unitView.MaximumWalkHeight.Returns(2);

			_durationCalculator.GetUnitMoveDuration(1, 3.5f).Returns(1.1f);

			_map.GetSquare(new GridCoords(0, 1)).Height.Returns(0);
			_map.GetSquare(new GridCoords(1, 2)).Height.Returns(2);
			_map.GetSquare(new GridCoords(2, 3)).Height.Returns(5);
			_map.GetSquare(new GridCoords(3, 4)).Height.Returns(2);

			var expectedPathPoints = new List<IPathPoint>
			{
				new PathPoint
				{
					Position = new Vector3(1, 2, 3),
					LookPosition = new Vector3(1, 1, 3),
					Duration = 1.1f,
					Jump = false
				},
				new PathPoint
				{
					Position = new Vector3(2, 3, 4),
					LookPosition = new Vector3(2, 2, 4),
					Duration = 1.1f,
					Jump = true
				},
				new PathPoint
				{
					Position = new Vector3(3, 4, 5),
					LookPosition = new Vector3(3, 3, 5),
					Duration = 1.1f,
					Jump = true
				}
			};
			var actualPathPoints = _factory.GetPathPoints(unitView, path);

			CollectionAssert.AreEqual(expectedPathPoints, actualPathPoints);
		}

		[Test]
		public void GetPathPointsWithPathCountOne()
		{
			var unitView = Substitute.For<IUnitViewData>();
			var path = new List<GridCoords>
			{
				new GridCoords(1, 2)
			};

			var pathPoints = _factory.GetPathPoints(unitView, path);
			CollectionAssert.IsEmpty(pathPoints);
		}

		[Test]
		public void GetPathPointsWithEmptyPathThrowsException()
		{
			var unitView = Substitute.For<IUnitViewData>();

			var exception = Assert.Throws<ArgumentException>(() =>
			{
				_factory.GetPathPoints(unitView, new List<GridCoords>());
			});

			Assert.AreEqual("Cannot get path points for empty path.", exception.Message);
		}

		[Test]
		public void GetPathPointsWithNullArgumentsThrowsException()
		{
			var unitView = Substitute.For<IUnitViewData>();
			var path = new List<GridCoords>();

			Assert.Throws<ArgumentNullException>(() =>
			{
				_factory.GetPathPoints(null, path);
			});

			Assert.Throws<ArgumentNullException>(() =>
			{
				_factory.GetPathPoints(unitView, null);
			});
		}
	}
}
﻿using Common.DataContainers;
using Game.Inputs;
using Game.Inputs.EventSignals;
using Game.Models.Occupants.Units;
using NSubstitute;
using NUnit.Framework;

namespace UnitTest.Game.Inputs
{
	[TestFixture]
	public class InputEventHandlerTest
	{
		private IMapEventSignal _mapEventSignal;
		private IUnitEventSignal _unitEventSignal;
		private IButtonEventSignal _buttonEventSignal;
		private InputEventHandler _handler;

		[SetUp]
		public void SetUp()
		{
			_mapEventSignal = Substitute.For<IMapEventSignal>();
			_unitEventSignal = Substitute.For<IUnitEventSignal>();
			_buttonEventSignal = Substitute.For<IButtonEventSignal>();

			_handler = new InputEventHandler(_mapEventSignal, _unitEventSignal, _buttonEventSignal);
		}

		[Test]
		public void DispatchMapEvent()
		{
			_handler.DispatchMapEvent(new GridCoords(2, 3));

			_mapEventSignal.Received(1).Dispatch(new GridCoords(2, 3));
			_unitEventSignal.DidNotReceive().Dispatch(Arg.Any<IUnit>(), Arg.Any<GridCoords>());
			_buttonEventSignal.DidNotReceive().Dispatch(Arg.Any<ButtonType>(), Arg.Any<int>());
		}

		[Test]
		public void DispatchUnitEvent()
		{
			var unit = Substitute.For<IUnit>();
			_handler.DispatchUnitEvent(unit, new GridCoords(1, 1));

			_unitEventSignal.Received(1).Dispatch(unit, new GridCoords(1, 1));
			_mapEventSignal.DidNotReceive().Dispatch(Arg.Any<GridCoords>());
			_buttonEventSignal.DidNotReceive().Dispatch(Arg.Any<ButtonType>(), Arg.Any<int>());
		}

		[Test]
		public void DispatchButtonEvent()
		{
			_handler.DispatchButtonEvent(ButtonType.UnitAction, 3);

			_buttonEventSignal.Received(1).Dispatch(ButtonType.UnitAction, 3);
			_mapEventSignal.DidNotReceive().Dispatch(Arg.Any<GridCoords>());
			_unitEventSignal.DidNotReceive().Dispatch(Arg.Any<IUnit>(), Arg.Any<GridCoords>());
		}
	}
}
﻿using Common.DataContainers;
using Game.Controllers;
using Game.Inputs;
using Game.Inputs.TapSignals;
using Game.Models.Occupants.Units;
using NSubstitute;
using NUnit.Framework;
using UnityEngine;

namespace UnitTest.Game.Inputs
{
    [TestFixture]
    public class InputDemuxerTest
    {
        private IWorldGridCoordsConverter _gridCoordsConverter;
        private IInputEventHandler _inputEventHandler;
        private IButtonTapSignal _buttonTapSignal;
        private IMapTapSignal _mapTapSignal;
        private IUnitTapSignal _unitTapSignal;
        private InputDemuxer _demuxer;

        [SetUp]
        public void SetUp()
        {
            _gridCoordsConverter = Substitute.For<IWorldGridCoordsConverter>();
            _inputEventHandler = Substitute.For<IInputEventHandler>();
            _buttonTapSignal = new ButtonTapSignal();
            _mapTapSignal = new MapTapSignal();
            _unitTapSignal = new UnitTapSignal();
            _demuxer = new InputDemuxer(_gridCoordsConverter, _inputEventHandler,
                _buttonTapSignal, _mapTapSignal, _unitTapSignal);
	        _demuxer.Init();
        }

	    [Test]
	    public void DispatchButtonEvent()
	    {
		    _buttonTapSignal.Dispatch(ButtonType.UnitAction, 1);
		    _inputEventHandler.DidNotReceive().DispatchButtonEvent(Arg.Any<ButtonType>(), Arg.Any<int>());

			_demuxer.FlushEvents();

		    _inputEventHandler.Received(1).DispatchButtonEvent(ButtonType.UnitAction, 1);
	    }

	    [Test]
	    public void DispatchMapEvent()
	    {
	        _gridCoordsConverter.GetGridCoords(new Vector3(1, 2, 3)).Returns(new GridCoords(3, 4));
		    _mapTapSignal.Dispatch(new Vector3(1, 2, 3));

            _inputEventHandler.DidNotReceive().DispatchMapEvent(Arg.Any<GridCoords>());
			
			_demuxer.FlushEvents();

			_inputEventHandler.Received(1).DispatchMapEvent(new GridCoords(3, 4));
	    }

	    [Test]
	    public void DispatchUnitEvent()
	    {
		    var unit = Substitute.For<IUnit>();
	        _gridCoordsConverter.GetGridCoords(new Vector3(1, 2, 3)).Returns(new GridCoords(1, 2));
		    _unitTapSignal.Dispatch(unit, new Vector3(1, 2, 3));
			_inputEventHandler.DidNotReceive().DispatchUnitEvent(Arg.Any<IUnit>(), Arg.Any<GridCoords>());

			_demuxer.FlushEvents();

			_inputEventHandler.Received(1).DispatchUnitEvent(unit, new GridCoords(1, 2));
	    }

        [Test]
        public void DispatchFirstEventOnly()
        {
            _gridCoordsConverter.GetGridCoords(new Vector3(1, 2, 3)).Returns(new GridCoords(1, 2));
            _mapTapSignal.Dispatch(new Vector3(1, 2, 3));
            _unitTapSignal.Dispatch(Substitute.For<IUnit>(), new Vector3(1, 2, 3));

            _demuxer.FlushEvents();

            _inputEventHandler.Received(1).DispatchMapEvent(new GridCoords(1, 2));
            _inputEventHandler.DidNotReceive().DispatchUnitEvent(Arg.Any<IUnit>(), Arg.Any<GridCoords>());
        }

	    [Test]
	    public void DispatchButtonEventOnly()
	    {
            _gridCoordsConverter.GetGridCoords(new Vector3(1, 2, 3)).Returns(new GridCoords(1, 2));
            _mapTapSignal.Dispatch(new Vector3(1, 2, 3));
            _buttonTapSignal.Dispatch(ButtonType.ApplyTransaction, 0);

			_demuxer.FlushEvents();

			_inputEventHandler.Received(1).DispatchButtonEvent(ButtonType.ApplyTransaction, 0);
			_inputEventHandler.DidNotReceive().DispatchMapEvent(Arg.Any<GridCoords>());
	    }

	    [Test]
	    public void DispatchFirstButton()
	    {
            _gridCoordsConverter.GetGridCoords(new Vector3(1, 2, 3)).Returns(new GridCoords(1, 2));
            _mapTapSignal.Dispatch(new Vector3(1, 2, 3));
            _buttonTapSignal.Dispatch(ButtonType.ApplyTransaction, 0);
			_buttonTapSignal.Dispatch(ButtonType.UnitAction, 2);

			_demuxer.FlushEvents();

			_inputEventHandler.Received(1).DispatchButtonEvent(ButtonType.ApplyTransaction, 0);
			_inputEventHandler.DidNotReceive().DispatchButtonEvent(ButtonType.UnitAction, 2);
		}

	    [Test]
	    public void NothingTappedDoesNotDispatchEvent()
	    {
		    _demuxer.FlushEvents();

			_inputEventHandler.DidNotReceive().DispatchButtonEvent(Arg.Any<ButtonType>(), Arg.Any<int>());
			_inputEventHandler.DidNotReceive().DispatchMapEvent(Arg.Any<GridCoords>());
			_inputEventHandler.DidNotReceive().DispatchUnitEvent(Arg.Any<IUnit>(), Arg.Any<GridCoords>());
	    }

	    [Test]
	    public void FlushClearsEvents()
	    {
			_buttonTapSignal.Dispatch(ButtonType.UnitAction, 1);

			_demuxer.FlushEvents();

			var unit = Substitute.For<IUnit>();
            _gridCoordsConverter.GetGridCoords(new Vector3(1, 2, 3)).Returns(new GridCoords(1, 2));
            _unitTapSignal.Dispatch(unit, new Vector3(1, 2, 3));
            _inputEventHandler.DidNotReceive().DispatchUnitEvent(Arg.Any<IUnit>(), Arg.Any<GridCoords>());

			_demuxer.FlushEvents();

			_inputEventHandler.Received(1).DispatchButtonEvent(ButtonType.UnitAction, 1);
			_inputEventHandler.Received(1).DispatchUnitEvent(unit, new GridCoords(1, 2));
		}
    }
}
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using Configs;
using Game.Controllers.Units;
using Game.Models.Occupants.Units;
using Game.Models.Occupants.Units.UnitActions;
using NSubstitute;
using NUnit.Framework;

namespace UnitTest.Game.Controllers.Units
{
    [TestFixture]
    public class ActionEffectBuildersFactoryTest
    {
	    private IStatsDeltaDelegateFactory _deltaDelegateFactory;
	    private ActionEffectBuildersFactory _factory;

	    [SetUp]
	    public void SetUp()
	    {
			_deltaDelegateFactory = Substitute.For<IStatsDeltaDelegateFactory>();
			_factory = new ActionEffectBuildersFactory(_deltaDelegateFactory);
	    }

        [Test]
        public void GetSinglePermanentEffect()
        {
            var effectConfigs = new List<ActionEffectConfig>
            {
                new ActionEffectConfig
                {
                    TargetStatType = StatType.Health,
					ActorMultiplier = new StatsMultiplierConfig
					{
						StatType = StatType.Power,
						Value = -1.5f
					},
					TargetMultiplier = new StatsMultiplierConfig
					{
						StatType = StatType.Defense,
						Value = 1
					},
                    Accuracy = 1,
                    IsStackable = true,
                    IsRemoveable = true
                }
            };

			var deltaDelegate = new StatsDeltaDelegate((x, y) => 88);
	        _deltaDelegateFactory.GetStatsDeltaDelegate(effectConfigs[0].ActorMultiplier,
		        effectConfigs[0].TargetMultiplier).Returns(deltaDelegate);

            var unitAction = Substitute.For<IUnitAction>();
            var effects = _factory.GetEffectBuilders(unitAction, effectConfigs).ToList();

			var actorStats = Substitute.For<IUnitStats>();
			actorStats.Get(StatType.Power).Returns(10);

	        var targetStats = Substitute.For<IUnitStats>();
	        targetStats.Get(StatType.Defense).Returns(10);

			Assert.AreEqual(unitAction, effects[0].UnitAction);
            Assert.AreEqual(1, effects.Count);
            Assert.AreEqual(StatType.Health, effects[0].StatType);
            Assert.AreSame(deltaDelegate, effects[0].GetDelta);
            Assert.AreEqual(1, effects.First().Accuracy);
            Assert.IsNull(effects[0].AbnormalStatus);
            Assert.IsNull(effects[0].Duration);
            Assert.IsTrue(effects[0].IsStackable);
            Assert.IsTrue(effects[0].IsRemoveable);
        }

        [Test]
        public void GetSingleTimedEffect()
        {
            var effectConfigs = new List<ActionEffectConfig>
            {
                new ActionEffectConfig
                {
                    AbnormalStatus = AbnormalStatusType.Weaken,
                    Duration = 3,
                    Accuracy = 0.5f
                }
            };

            var unitAction = Substitute.For<IUnitAction>();
            var effects = _factory.GetEffectBuilders(unitAction, effectConfigs).ToList();

            Assert.AreEqual(unitAction, effects[0].UnitAction);
            Assert.AreEqual(1, effects.Count);
            Assert.AreEqual(0.5f, effects[0].Accuracy);
            Assert.AreEqual(AbnormalStatusType.Weaken, effects[0].AbnormalStatus);
            Assert.AreEqual(3, effects[0].Duration);
        }

        [Test]
        public void GetMultipleEffects()
        {
            var effectConfigs = new List<ActionEffectConfig>
            {
                new ActionEffectConfig
                {
                    TargetStatType = StatType.Health,
					ActorMultiplier = new StatsMultiplierConfig
					{
						StatType = StatType.Power,
						Value = -1
					},
					TargetMultiplier = new StatsMultiplierConfig
					{
						StatType = StatType.Defense,
						Value = 0.5f
					},
					Accuracy = 1
                },
                new ActionEffectConfig
                {
                    TargetStatType = StatType.Defense,
					ActorMultiplier = new StatsMultiplierConfig
					{
						StatType = StatType.Power,
						Value = -0.5f
					},
					TargetMultiplier = new StatsMultiplierConfig
					{
						StatType = StatType.Defense,
						Value = -0.5f
					},
					Accuracy = 0.5f
                }
            };

			var deltaDelegate0 = new StatsDeltaDelegate((x, y) => 88);
			_deltaDelegateFactory.GetStatsDeltaDelegate(effectConfigs[0].ActorMultiplier,
				effectConfigs[0].TargetMultiplier).Returns(deltaDelegate0);

			var deltaDelegate1 = new StatsDeltaDelegate((x, y) => 88);
			_deltaDelegateFactory.GetStatsDeltaDelegate(effectConfigs[1].ActorMultiplier,
				effectConfigs[1].TargetMultiplier).Returns(deltaDelegate1);

            var unitAction = Substitute.For<IUnitAction>();
			var effects = _factory.GetEffectBuilders(unitAction, effectConfigs).ToList();

			var actorStats = Substitute.For<IUnitStats>();
            actorStats.Get(StatType.Power).Returns(10);

	        var targetStats = Substitute.For<IUnitStats>();
	        targetStats.Get(StatType.Defense).Returns(10);

            Assert.AreEqual(2, effects.Count);

            Assert.AreEqual(unitAction, effects[0].UnitAction);
            Assert.AreEqual(StatType.Health, effects[0].StatType);
            Assert.AreSame(deltaDelegate0, effects[0].GetDelta);
            Assert.AreEqual(1, effects[0].Accuracy);

            Assert.AreEqual(unitAction, effects[1].UnitAction);
            Assert.AreEqual(StatType.Defense, effects[1].StatType);
            Assert.AreSame(deltaDelegate1, effects[1].GetDelta);
            Assert.AreEqual(0.5f, effects[1].Accuracy);
        }
	}
}
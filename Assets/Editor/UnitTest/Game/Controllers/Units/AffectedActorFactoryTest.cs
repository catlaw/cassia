﻿using Common.DataContainers;
using Game.Controllers.Units;
using Game.Models.TurnTransactions;
using NSubstitute;
using NUnit.Framework;

namespace UnitTest.Game.Controllers.Units
{
	[TestFixture]
	public class AffectedActorFactoryTest
	{
		private ITurnTransaction _turnTransaction;
		private AffectedActorFactory _factory;

		[SetUp]
		public void SetUp()
		{
			_turnTransaction = Substitute.For<ITurnTransaction>();
			_factory = new AffectedActorFactory(_turnTransaction);
		}

		[Test]
		public void CreateAffectedActor()
		{
			_turnTransaction.MovePosition.Returns(new GridCoords(1, 2));

			var affectedUnit = _factory.CreateAffectedActor();

			Assert.AreSame(_turnTransaction.Actor, affectedUnit.Unit);
			Assert.AreEqual(new GridCoords(1, 2), affectedUnit.GridCoords);
		}
	}
}
﻿using System.Collections.Generic;
using Common.DataContainers;
using Game.Controllers.Units;
using Game.Models.Stage;
using NSubstitute;
using NUnit.Framework;
using UnityEngineInternal;

namespace UnitTest.Game.Controllers.Units
{
    [TestFixture]
    public class UnitActionRangeCalculatorTest
    {
        private UnitActionRangeCalculator _calculator;

        [SetUp]
        public void SetUp()
        {
            _calculator = new UnitActionRangeCalculator();
            _calculator.Map = Substitute.For<IMap>();
            _calculator.Map.IsWithinLevel(Arg.Any<GridCoords>()).Returns(true);
        }

        [Test]
        public void GetRangeForward()
        {
            var directionalOffset = new List<GridCoordsOffset> {new GridCoordsOffset(2, 0), new GridCoordsOffset(1, 1)};
            var expectedTargetRange = new List<GridCoords>
            {
                new GridCoords(7, 5), new GridCoords(6, 6)
            };

            var actualTargetRange = _calculator.GetAreaOfEffect(new GridCoords(5, 5), Facing.Forward, directionalOffset);
            CollectionAssert.AreEquivalent(expectedTargetRange, actualTargetRange);
        }

        [Test]
        public void GetRangeRight()
        {
            var directionalOffset = new List<GridCoordsOffset> { new GridCoordsOffset(2, 0), new GridCoordsOffset(1, 1) };
            var expectedTargetRange = new List<GridCoords>
            {
                new GridCoords(5, 7), new GridCoords(4, 6)
            };

            var actualTargetRange = _calculator.GetAreaOfEffect(new GridCoords(5, 5), Facing.Right, directionalOffset);
            CollectionAssert.AreEquivalent(expectedTargetRange, actualTargetRange);
        }

        [Test]
        public void GetRangeBack()
        {
            var directionalOffset = new List<GridCoordsOffset> { new GridCoordsOffset(2, 0), new GridCoordsOffset(1, 1) };
            var expectedTargetRange = new List<GridCoords>
            {
                new GridCoords(3, 5), new GridCoords(4, 4)
            };

            var actualTargetRange = _calculator.GetAreaOfEffect(new GridCoords(5, 5), Facing.Back, directionalOffset);
            CollectionAssert.AreEquivalent(expectedTargetRange, actualTargetRange);
        }

        [Test]
        public void GetRangeLeft()
        {
            var directionalOffset = new List<GridCoordsOffset> { new GridCoordsOffset(2, 0), new GridCoordsOffset(1, 1) };
            var expectedTargetRange = new List<GridCoords>
            {
                new GridCoords(5, 3), new GridCoords(6, 4)
            };

            var actualTargetRange = _calculator.GetAreaOfEffect(new GridCoords(5, 5), Facing.Left, directionalOffset);
            CollectionAssert.AreEquivalent(expectedTargetRange, actualTargetRange);
        }

        [Test]
        public void GetRotatedRange()
        {
            var directionalOffset = new List<GridCoordsOffset> { new GridCoordsOffset(2, 0), new GridCoordsOffset(1, 1) };
            var expectedTargetRange = new List<GridCoords>
            {
                new GridCoords(7, 5), new GridCoords(6, 6),
                new GridCoords(5, 7), new GridCoords(4, 6),
                new GridCoords(3, 5), new GridCoords(4, 4),
                new GridCoords(5, 3), new GridCoords(6, 4)
            };

            var actualTargetRange = _calculator.GetRotatedRange(new GridCoords(5, 5), directionalOffset);
            CollectionAssert.AreEquivalent(expectedTargetRange, actualTargetRange);
        }

        [Test]
        public void GetUniqueRotatedRange()
        {
            var directionalOffset = new List<GridCoordsOffset> { new GridCoordsOffset(0, 0) };
            var expectedTargetRange = new List<GridCoords> { new GridCoords(5, 5) };

            var actualTargetRange = _calculator.GetRotatedRange(new GridCoords(5, 5), directionalOffset);
            CollectionAssert.AreEquivalent(expectedTargetRange, actualTargetRange);
        }

        [Test]
        public void GetRangeNotInMap()
        {
            _calculator.Map.IsWithinLevel(new GridCoords(7, 5)).Returns(true);
            _calculator.Map.IsWithinLevel(new GridCoords(6, 6)).Returns(false);

            var directionalOffset = new List<GridCoordsOffset> { new GridCoordsOffset(2, 0), new GridCoordsOffset(1, 1) };
            var expectedTargetRange = new List<GridCoords>
            {
                new GridCoords(7, 5)
            };

            var actualTargetRange = _calculator.GetAreaOfEffect(new GridCoords(5, 5), Facing.Forward, directionalOffset);
            CollectionAssert.AreEquivalent(expectedTargetRange, actualTargetRange);
        }

        [Test]
        public void GetRotatedRangeNotInMap()
        {
            _calculator.Map.IsWithinLevel(new GridCoords(7, 5)).Returns(true);
            _calculator.Map.IsWithinLevel(new GridCoords(5, 7)).Returns(false);
            _calculator.Map.IsWithinLevel(new GridCoords(3, 6)).Returns(true);
            _calculator.Map.IsWithinLevel(new GridCoords(5, 3)).Returns(false);

            var directionalOffset = new List<GridCoordsOffset> { new GridCoordsOffset(2, 0) };
            var expectedTargetRange = new List<GridCoords>
            {
                new GridCoords(7, 5),
                new GridCoords(3, 5),
            };

            var actualTargetRange = _calculator.GetRotatedRange(new GridCoords(5, 5), directionalOffset);
            CollectionAssert.AreEquivalent(expectedTargetRange, actualTargetRange);
        }
    }
}

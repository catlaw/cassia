﻿using System;
using System.Collections.Generic;
using Configs;
using Game.Controllers.Units.UnitActions;
using Game.Models.Occupants.Units.UnitActions;
using NSubstitute;
using NUnit.Framework;

namespace UnitTest.Game.Controllers.Units.UnitActions
{
	[TestFixture]
	public class EffectorTest
	{
		private Effector _effector;

		[SetUp]
		public void SetUp()
		{
			_effector = new Effector
			{
				EffectBuildersFactory = Substitute.For<IActionEffectBuildersFactory>()
			};
		}

		[Test]
		public void LoadConfigAndGetEffects()
		{
			var config = new EffectorConfig
			{
				TargetEffects = new List<ActionEffectConfig>(),
				ActorEffects = new List<ActionEffectConfig>()
			};

		    var unitAction = Substitute.For<IUnitAction>();
			_effector.LoadConfig(unitAction, config);

			var expectedTargetEffects = new List<IPotentialActionEffectBuilder>();
			_effector.EffectBuildersFactory.GetEffectBuilders(unitAction, config.TargetEffects).Returns(expectedTargetEffects);

			var expectedSelfEffects = new List<IPotentialActionEffectBuilder>();
			_effector.EffectBuildersFactory.GetEffectBuilders(unitAction, config.ActorEffects).Returns(expectedSelfEffects);

			Assert.AreEqual(expectedTargetEffects, _effector.TargetEffectBuilders);
			Assert.AreEqual(expectedSelfEffects, _effector.SelfEffectBuilders);
			Assert.IsTrue(_effector.HasEffect);
		}

		[Test]
		public void LoadNullConfig()
		{
			_effector.LoadConfig(Substitute.For<IUnitAction>(), null);

			CollectionAssert.AreEqual(new List<IPotentialActionEffectBuilder>(), _effector.TargetEffectBuilders);
			CollectionAssert.AreEqual(new List<IPotentialActionEffectBuilder>(), _effector.SelfEffectBuilders);
			Assert.IsFalse(_effector.HasEffect);
		}

		[Test]
		[ExpectedException(typeof(ArgumentNullException))]
		public void LoadNullUnitActionId()
		{
			_effector.LoadConfig(null, new EffectorConfig());
		}
	}
}
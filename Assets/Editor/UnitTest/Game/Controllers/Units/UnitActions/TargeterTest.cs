﻿using System;
using System.Collections.Generic;
using Common.DataContainers;
using Configs;
using Game.Controllers.Units;
using Game.Controllers.Units.UnitActions;
using Game.Models.Occupants.Units.UnitActions;
using NSubstitute;
using NUnit.Framework;

namespace UnitTest.Game.Controllers.Units.UnitActions
{
	[TestFixture]
	public class TargeterTest
	{
		private Targeter _targeter;

		[SetUp]
		public void SetUp()
		{
			_targeter = new Targeter
			{
				RangeCalculator = Substitute.For<IUnitActionRangeCalculator>()
			};
		}

		[Test]
		public void LoadConfig()
		{
			var config = new TargeterConfig
			{
				TargetRange = new List<GridCoordsOffset> { new GridCoordsOffset(0, 1) },
				AreaOfEffect = new List<GridCoordsOffset> { new GridCoordsOffset(1, 2) }
			};

			_targeter.LoadConfig(config);

			CollectionAssert.AreEqual(new List<GridCoordsOffset> { new GridCoordsOffset(0, 1) }, _targeter.TargetRange);
			CollectionAssert.AreEqual(new List<GridCoordsOffset> { new GridCoordsOffset(1, 2) }, _targeter.AreaOfEffect);
			Assert.IsTrue(_targeter.CanTarget);
		}

		[Test]
		public void LoadNullConfig()
		{
			_targeter.LoadConfig(null);
			Assert.IsFalse(_targeter.CanTarget);
		}

		[Test]
		public void GetRotatedRange()
		{
			var config = new TargeterConfig
			{
				TargetRange = new List<GridCoordsOffset>()
			};

			_targeter.LoadConfig(config);

			var expectedTargetRange = new List<GridCoords> { new GridCoords(1, 2), new GridCoords(3, 4) };
			_targeter.RangeCalculator.GetRotatedRange(new GridCoords(1, 1), config.TargetRange).Returns(expectedTargetRange);

			CollectionAssert.AreEqual(expectedTargetRange, _targeter.GetTargetRange(new GridCoords(1, 1)));
		}

		[Test]
		public void GetAreaOfEffect()
		{
			var config = new TargeterConfig
			{
				AreaOfEffect = new List<GridCoordsOffset>()
			};

			_targeter.LoadConfig(config);

			var expectedAreaOfEffect = new List<GridCoords> { new GridCoords(1, 2), new GridCoords(3, 4) };
			_targeter.RangeCalculator.GetAreaOfEffect(new GridCoords(1, 1), Facing.Left, config.AreaOfEffect).Returns(expectedAreaOfEffect);

			CollectionAssert.AreEqual(expectedAreaOfEffect, _targeter.GetAreaOfEffect(new GridCoords(1, 1), Facing.Left));
		}

	}
}
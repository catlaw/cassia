﻿using Configs;
using Game.Models.Occupants.Units;
using Game.Models.Occupants.Units.UnitActions;
using NSubstitute;
using NUnit.Framework;

namespace UnitTest.Game.Controllers.Units.UnitActions
{
	[TestFixture]
	public class StatsDeltaDelegateFactoryTest
	{
		private StatsDeltaDelegateFactory _factory;

		[SetUp]
		public void SetUp()
		{
			_factory = new StatsDeltaDelegateFactory();
		}

		[Test]
		public void GetStatsDeltaDelegate()
		{
			var actorStatsMultiplier = new StatsMultiplierConfig
			{
				StatType = StatType.Power,
				Value = 2
			};

			var targetStatsMultiplier = new StatsMultiplierConfig
			{
				StatType = StatType.Defense,
				Value = -1
			};

			var statsDeltaDelegate = _factory.GetStatsDeltaDelegate(actorStatsMultiplier, targetStatsMultiplier);

			var actorStats = Substitute.For<IUnitStats>();
			actorStats.Get(StatType.Power).Returns(5);

			var targetStats = Substitute.For<IUnitStats>();
			targetStats.Get(StatType.Defense).Returns(2);

			Assert.AreEqual(8, statsDeltaDelegate(actorStats, targetStats));
		}

		[Test]
		public void GetRoundedDownStatsDeltaDelegate()
		{
			var actorStatsMultiplier = new StatsMultiplierConfig
			{
				StatType = StatType.Power,
				Value = 1.4f
			};
			 
			var targetStatsMultiplier = new StatsMultiplierConfig();

			var statsDeltaDelegate = _factory.GetStatsDeltaDelegate(actorStatsMultiplier, targetStatsMultiplier);

			var actorStats = Substitute.For<IUnitStats>();
			actorStats.Get(StatType.Power).Returns(1);

			var targetStats = Substitute.For<IUnitStats>();

			var delta = statsDeltaDelegate(actorStats, targetStats);

			Assert.AreEqual(1, delta);
		}

		[Test]
		public void GetRoundedUpStatsDeltaDelegate()
		{
			var actorStatsMultiplier = new StatsMultiplierConfig
			{
				StatType = StatType.Power,
				Value = 1.5f
			};

			var targetStatsMultiplier = new StatsMultiplierConfig();

			var statsDeltaDelegate = _factory.GetStatsDeltaDelegate(actorStatsMultiplier, targetStatsMultiplier);

			var actorStats = Substitute.For<IUnitStats>();
			actorStats.Get(StatType.Power).Returns(1);

			var targetStats = Substitute.For<IUnitStats>();

			var delta = statsDeltaDelegate(actorStats, targetStats);

			Assert.AreEqual(2, delta);
		}

		[Test]
		public void GetNullTargetMultiplierStatsDeltaDelegate()
		{
			var actorStatsMultiplier = new StatsMultiplierConfig
			{
				StatType = StatType.Power,
				Value = -1.5f
			};

			var statsDeltaDelegate = _factory.GetStatsDeltaDelegate(actorStatsMultiplier, null);

			var actorStats = Substitute.For<IUnitStats>();
			actorStats.Get(StatType.Power).Returns(10);

			var targetStats = Substitute.For<IUnitStats>();

			var delta = statsDeltaDelegate(actorStats, targetStats);

			Assert.AreEqual(-15, delta);
		}

		[Test]
		public void TargetCanOnlyPreventDamage()
		{
			var actorStatsMultiplier = new StatsMultiplierConfig
			{
				StatType = StatType.Power,
				Value = -1
			};

			var targetStatsMultiplier = new StatsMultiplierConfig
			{
				StatType = StatType.Defense,
				Value = 1
			};

			var statsDeltaDelegate = _factory.GetStatsDeltaDelegate(actorStatsMultiplier, targetStatsMultiplier);

			var actorStats = Substitute.For<IUnitStats>();
			actorStats.Get(StatType.Power).Returns(10);

			var targetStats = Substitute.For<IUnitStats>();
			targetStats.Get(StatType.Defense).Returns(20);

			Assert.AreEqual(0, statsDeltaDelegate(actorStats, targetStats));
		}

		[Test]
		public void TargetCanOnlyPreventHeal()
		{
			var actorStatsMultiplier = new StatsMultiplierConfig
			{
				StatType = StatType.Power,
				Value = 1
			};

			var targetStatsMultiplier = new StatsMultiplierConfig
			{
				StatType = StatType.Defense,
				Value = -1
			};

			var statsDeltaDelegate = _factory.GetStatsDeltaDelegate(actorStatsMultiplier, targetStatsMultiplier);

			var actorStats = Substitute.For<IUnitStats>();
			actorStats.Get(StatType.Power).Returns(10);

			var targetStats = Substitute.For<IUnitStats>();
			targetStats.Get(StatType.Defense).Returns(20);

			Assert.AreEqual(0, statsDeltaDelegate(actorStats, targetStats));
		}

		[Test]
		public void TargetCanIncreaseDamage()
		{
			var actorStatsMultiplier = new StatsMultiplierConfig
			{
				StatType = StatType.Power,
				Value = -1
			};

			var targetStatsMultiplier = new StatsMultiplierConfig
			{
				StatType = StatType.Defense,
				Value = -1
			};

			var statsDeltaDelegate = _factory.GetStatsDeltaDelegate(actorStatsMultiplier, targetStatsMultiplier);

			var actorStats = Substitute.For<IUnitStats>();
			actorStats.Get(StatType.Power).Returns(10);

			var targetStats = Substitute.For<IUnitStats>();
			targetStats.Get(StatType.Defense).Returns(20);

			Assert.AreEqual(-30, statsDeltaDelegate(actorStats, targetStats));
		}

		[Test]
		public void TargetCanIncreaseHeal()
		{
			var actorStatsMultiplier = new StatsMultiplierConfig
			{
				StatType = StatType.Power,
				Value = 1
			};

			var targetStatsMultiplier = new StatsMultiplierConfig
			{
				StatType = StatType.Defense,
				Value = 1
			};

			var statsDeltaDelegate = _factory.GetStatsDeltaDelegate(actorStatsMultiplier, targetStatsMultiplier);

			var actorStats = Substitute.For<IUnitStats>();
			actorStats.Get(StatType.Power).Returns(10);

			var targetStats = Substitute.For<IUnitStats>();
			targetStats.Get(StatType.Defense).Returns(20);

			Assert.AreEqual(30, statsDeltaDelegate(actorStats, targetStats));
		}

		[Test]
		public void GetNullActorMultiplierStatsDeltaDelegate()
		{
			var targetStatsMultiplier = new StatsMultiplierConfig
			{
				StatType = StatType.Defense,
				Value = -1
			};

			var statsDeltaDelegate = _factory.GetStatsDeltaDelegate(null, targetStatsMultiplier);

			var actorStats = Substitute.For<IUnitStats>();
			actorStats.Get(StatType.Power).Returns(10);

			var targetStats = Substitute.For<IUnitStats>();
			targetStats.Get(StatType.Defense).Returns(20);

			Assert.AreEqual(-20, statsDeltaDelegate(actorStats, targetStats));
		}
	}
}
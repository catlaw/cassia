﻿using System.Collections.Generic;
using System.Linq;
using Common.DataContainers;
using Game.Controllers.Units;
using Game.Models.Occupants;
using Game.Models.Occupants.Units;
using Game.Models.Stage;
using Game.Models.TurnTransactions;
using NSubstitute;
using NUnit.Framework;

namespace UnitTest.Game.Controllers.Units
{
    [TestFixture]
    public class AffectedUnitsLookupTest
    {
        private AffectedUnitsLookup _affectedUnitsLookup;

        [SetUp]
        public void SetUp()
        {
	        _affectedUnitsLookup = new AffectedUnitsLookup
	        {
		        TransactionalUnitMap = Substitute.For<ITransactionalUnitMap>()
	        };
        }

        [Test]
        public void GetUnitsInArea()
        {
            var unit12 = Substitute.For<IUnit>();
            var unitState12 = Substitute.For<ITransactionalUnitState>();
            unitState12.Unit.Returns(unit12);
            _affectedUnitsLookup.TransactionalUnitMap.GetUnitAt(new GridCoords(1, 2)).Returns(unitState12);

            var unit34 = Substitute.For<IUnit>();
            var unitState34 = Substitute.For<ITransactionalUnitState>();
			unitState34.Unit.Returns(unit34);
            _affectedUnitsLookup.TransactionalUnitMap.GetUnitAt(new GridCoords(3, 4)).Returns(unitState34);

	        var unitState56 = Substitute.For<ITransactionalUnitState>();
	        unitState56.Unit.Returns(EmptyUnit.None);
	        _affectedUnitsLookup.TransactionalUnitMap.GetUnitAt(new GridCoords(5, 6)).Returns(unitState56);

            var area = new List<GridCoords>
            {
                new GridCoords(1, 2),
                new GridCoords(3, 4),
                new GridCoords(5, 6)
            };

            var affectedUnits = _affectedUnitsLookup.GetUnits(area).ToList();

            Assert.AreSame(unit12, affectedUnits[0].Unit);
            Assert.AreEqual(new GridCoords(1, 2), affectedUnits[0].GridCoords);
            
            Assert.AreSame(unit34, affectedUnits[1].Unit);
            Assert.AreEqual(new GridCoords(3, 4), affectedUnits[1].GridCoords);
        }

        [Test]
        public void DoNotGetNoneUnitInArea()
        {
	        var unitState = Substitute.For<ITransactionalUnitState>();
	        unitState.Unit.Returns(EmptyUnit.None);
            _affectedUnitsLookup.TransactionalUnitMap.GetUnitAt(new GridCoords(1, 1)).Returns(unitState);

            var area = new List<GridCoords> {new GridCoords(1, 1)};

            CollectionAssert.IsEmpty(_affectedUnitsLookup.GetUnits(area));
        }
    }
}

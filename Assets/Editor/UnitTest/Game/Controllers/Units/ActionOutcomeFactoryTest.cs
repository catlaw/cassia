﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common.Util;
using Game.Controllers;
using Game.Controllers.Units;
using Game.Models.Occupants.Units;
using Game.Models.Occupants.Units.UnitActions;
using NSubstitute;
using NUnit.Framework;

namespace UnitTest.Game.Controllers.Units
{
	[TestFixture]
	public class ActionOutcomeFactoryTest
	{
		private IRandomNumberGenerator _randomNumberGenerator;
		private ActionOutcomeFactory _factory;

		[SetUp]
		public void SetUp()
		{
			_randomNumberGenerator = Substitute.For<IRandomNumberGenerator>();
			_factory = new ActionOutcomeFactory(_randomNumberGenerator);
		}

		[Test]
		public void GetActionOutcome()
		{
			var potentialActionOutcome = Substitute.For<IPotentialActionOutcome>();

			var affectedUnit = Substitute.For<IAffectedUnit>();
			potentialActionOutcome.AffectedUnit.Returns(affectedUnit);

			var potentialActionEffects = new List<IPotentialActionEffect>
			{
				Substitute.For<IPotentialActionEffect>(),
				Substitute.For<IPotentialActionEffect>()
			};
			potentialActionEffects[0].Accuracy.Returns(1f);
			potentialActionEffects[1].Accuracy.Returns(0f);
			potentialActionOutcome.Effects.Returns(potentialActionEffects);

			_randomNumberGenerator.GetValue().Returns(0.5f);

			var actionOutcome = _factory.GetActionOutcome(potentialActionOutcome);

			Assert.AreSame(affectedUnit, actionOutcome.AffectedUnit);
			CollectionAssert.AreEqual(actionOutcome.Effects, new List<IActionEffect> {potentialActionEffects[0]});
		}

		[Test]
		public void Accuracy1AlwaysHits()
		{
			var potentialActionOutcome = Substitute.For<IPotentialActionOutcome>();
			potentialActionOutcome.AffectedUnit.Returns(Substitute.For<IAffectedUnit>());

			var potentialActionEffects = new List<IPotentialActionEffect>
			{
				Substitute.For<IPotentialActionEffect>(),
			};
			potentialActionEffects[0].Accuracy.Returns(1f);
			potentialActionOutcome.Effects.Returns(potentialActionEffects);

			_randomNumberGenerator.GetValue().Returns(0.0f);

			var actionOutcome = _factory.GetActionOutcome(potentialActionOutcome);

			Assert.AreSame(potentialActionEffects[0], actionOutcome.Effects.First());
		}

		[Test]
		public void Accuracy0AlwaysMisses()
		{
			var potentialActionOutcome = Substitute.For<IPotentialActionOutcome>();
			potentialActionOutcome.AffectedUnit.Returns(Substitute.For<IAffectedUnit>());

			var potentialActionEffects = new List<IPotentialActionEffect>
			{
				Substitute.For<IPotentialActionEffect>(),
			};
			potentialActionEffects[0].Accuracy.Returns(0f);
			potentialActionOutcome.Effects.Returns(potentialActionEffects);

			_randomNumberGenerator.GetValue().Returns(1.0f);

			var actionOutcome = _factory.GetActionOutcome(potentialActionOutcome);

			Assert.IsEmpty(actionOutcome.Effects);
		}

		[Test]
		[ExpectedException(typeof(ArgumentNullException))]
		public void GetActionOutcomeForNullThrowsException()
		{
			_factory.GetActionOutcome(null);
		}
	}
}
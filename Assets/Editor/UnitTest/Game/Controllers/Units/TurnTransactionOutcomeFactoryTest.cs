﻿using System.Collections.Generic;
using System.Linq;
using Common.DataContainers;
using Game.Controllers;
using Game.Controllers.Units;
using Game.Models.Occupants;
using Game.Models.Occupants.Units;
using Game.Models.Occupants.Units.UnitActions;
using Game.Models.Stage;
using Game.Models.TurnTransactions;
using NSubstitute;
using NUnit.Framework;

namespace UnitTest.Game.Controllers.Units
{
    [TestFixture]
    public class TurnTransactionOutcomeFactoryTest
    {
	    private IDirectionCalculator _directionCalculator;
	    private IAffectedActorFactory _affectedActorFactory;
	    private IAffectedUnitsLookup _affectedUnitsLookup;
	    private IPotentialActionOutcomeFactory _potentialActionOutcomeFactory;
	    private IActionOutcomeFactory _actionOutcomeFactory;
		private TurnTransactionOutcomeFactory _factory;

        [SetUp]
        public void SetUp()
        {
	        _directionCalculator = Substitute.For<IDirectionCalculator>();
	        _affectedActorFactory = Substitute.For<IAffectedActorFactory>();
	        _affectedUnitsLookup = Substitute.For<IAffectedUnitsLookup>();
	        _potentialActionOutcomeFactory = Substitute.For<IPotentialActionOutcomeFactory>();
	        _actionOutcomeFactory = Substitute.For<IActionOutcomeFactory>();

	        _factory = new TurnTransactionOutcomeFactory(_directionCalculator, _affectedActorFactory,
				_affectedUnitsLookup, _potentialActionOutcomeFactory, _actionOutcomeFactory);
        }

        [Test]
        public void GetActionOutcome()
        {
            var actor = Substitute.For<IUnit>();
            var unitActionOrigin = UnitActionOrigin.CreateFromActorAndPosition(actor, new GridCoords(1, 1));
            var unitAction = Substitute.For<IUnitAction>();
            var selfEffectBuilders = new List<IPotentialActionEffectBuilder> {Substitute.For<IPotentialActionEffectBuilder>()};
	        var selfPotentialOutcome = Substitute.For<IPotentialActionOutcome>();
	        var selfOutcome = Substitute.For<IActionOutcome>();
            var targetEffectBuilders = new List<IPotentialActionEffectBuilder> { Substitute.For<IPotentialActionEffectBuilder>() };
			var targetPotentialOutcomes = new List<IPotentialActionOutcome> { Substitute.For<IPotentialActionOutcome>(), Substitute.For<IPotentialActionOutcome>()};
			var targetOutcomes = new List<IActionOutcome> { Substitute.For<IActionOutcome>(), Substitute.For<IActionOutcome>() };
			var areaOfEffect = new List<GridCoords> {new GridCoords(1, 2), new GridCoords(2, 3)};
            var affectedUnits = new List<IAffectedUnit> {Substitute.For<IAffectedUnit>(), Substitute.For<IAffectedUnit>()};

            unitAction.SelfEffectBuilders.Returns(selfEffectBuilders);
            unitAction.TargetEffectBuilders.Returns(targetEffectBuilders);
            unitAction.GetAreaOfEffect(new GridCoords(2, 2), Facing.Forward).Returns(areaOfEffect);

			_directionCalculator.GetFacing(new GridCoords(1, 1), new GridCoords(2, 2)).Returns(Facing.Forward);
			_affectedUnitsLookup.GetUnits(Arg.Is<List<GridCoords>>(aoe => aoe[0] == new GridCoords(1, 2) && aoe[1] == new GridCoords(2, 3))).Returns(affectedUnits);
			_potentialActionOutcomeFactory.CreateActionOutcome(actor.Stats, _affectedActorFactory.CreateAffectedActor(), selfEffectBuilders).Returns(selfPotentialOutcome);
			_potentialActionOutcomeFactory.CreateActionOutcome(actor.Stats, affectedUnits[0], targetEffectBuilders).Returns(targetPotentialOutcomes[0]);
			_potentialActionOutcomeFactory.CreateActionOutcome(actor.Stats, affectedUnits[1], targetEffectBuilders).Returns(targetPotentialOutcomes[1]);
	        _actionOutcomeFactory.GetActionOutcome(selfPotentialOutcome).Returns(selfOutcome);
	        _actionOutcomeFactory.GetActionOutcome(targetPotentialOutcomes[0]).Returns(targetOutcomes[0]);
	        _actionOutcomeFactory.GetActionOutcome(targetPotentialOutcomes[1]).Returns(targetOutcomes[1]);

			var outcome = _factory.GetOutcome(unitActionOrigin, unitAction, new GridCoords(2, 2));

            CollectionAssert.AreEquivalent(areaOfEffect, outcome.AreaOfEffect);
	        CollectionAssert.AreEquivalent(new List<IPotentialActionOutcome> { selfPotentialOutcome, targetPotentialOutcomes[0], targetPotentialOutcomes[1] }, outcome.PotentialActionOutcomes);
	        CollectionAssert.AreEquivalent(new List<IActionOutcome> { selfOutcome, targetOutcomes[0], targetOutcomes[1] }, outcome.ActionOutcomes);
        }

        [Test]
        public void GetActionOutcomeWithoutSelf()
        {
            var actor = Substitute.For<IUnit>();
            var unitActionOrigin = UnitActionOrigin.CreateFromActorAndPosition(actor, new GridCoords(1, 1));
            var unitAction = Substitute.For<IUnitAction>();
            var targetEffectBuilders = new List<IPotentialActionEffectBuilder> { PotentialActionEffectBuilder.Create() };
			var targetPotentialOutcomes = new List<IPotentialActionOutcome> { Substitute.For<IPotentialActionOutcome>(), Substitute.For<IPotentialActionOutcome>() };
			var targetOutcomes = new List<IActionOutcome> { Substitute.For<IActionOutcome>(), Substitute.For<IActionOutcome>() };
			var areaOfEffect = new List<GridCoords> { new GridCoords(1, 2), new GridCoords(2, 3) };
            var affectedUnits = new List<IAffectedUnit> { Substitute.For<IAffectedUnit>(), Substitute.For<IAffectedUnit>() };

            unitAction.SelfEffectBuilders.Returns(new List<IPotentialActionEffectBuilder>());
            unitAction.TargetEffectBuilders.Returns(targetEffectBuilders);
            unitAction.GetAreaOfEffect(new GridCoords(2, 2), Facing.Forward).Returns(areaOfEffect);

			_directionCalculator.GetFacing(new GridCoords(1, 1), new GridCoords(2, 2)).Returns(Facing.Forward);
			_affectedUnitsLookup.GetUnits(Arg.Is<List<GridCoords>>(aoe => aoe[0] == new GridCoords(1, 2) && aoe[1] == new GridCoords(2, 3))).Returns(affectedUnits);
			_potentialActionOutcomeFactory.CreateActionOutcome(actor.Stats, affectedUnits[0], targetEffectBuilders).Returns(targetPotentialOutcomes[0]);
			_potentialActionOutcomeFactory.CreateActionOutcome(actor.Stats, affectedUnits[1], targetEffectBuilders).Returns(targetPotentialOutcomes[1]);
	        _actionOutcomeFactory.GetActionOutcome(targetPotentialOutcomes[0]).Returns(targetOutcomes[0]);
	        _actionOutcomeFactory.GetActionOutcome(targetPotentialOutcomes[1]).Returns(targetOutcomes[1]);

			var outcome = _factory.GetOutcome(unitActionOrigin, unitAction, new GridCoords(2, 2));

            Assert.AreEqual(2, outcome.PotentialActionOutcomes.Count());

			CollectionAssert.AreEquivalent(areaOfEffect, outcome.AreaOfEffect);
			CollectionAssert.AreEquivalent(targetPotentialOutcomes, outcome.PotentialActionOutcomes);
	        CollectionAssert.AreEquivalent(targetOutcomes, outcome.ActionOutcomes);
        }

        [Test]
        public void GetActionOutcomeOnlySelf()
        {
            var actor = Substitute.For<IUnit>();
            var unitActionOrigin = UnitActionOrigin.CreateFromActorAndPosition(actor, new GridCoords(1, 1));
            var unitAction = Substitute.For<IUnitAction>();
            var selfEffectBuilders = new List<IPotentialActionEffectBuilder> { PotentialActionEffectBuilder.Create() };
			var selfPotentialOutcome = Substitute.For<IPotentialActionOutcome>();
	        var selfOutcome = Substitute.For<IActionOutcome>();
			var areaOfEffect = new List<GridCoords> { new GridCoords(1, 2), new GridCoords(2, 3) };
            var affectedUnits = new List<IAffectedUnit> { Substitute.For<IAffectedUnit>(), Substitute.For<IAffectedUnit>() };

            unitAction.SelfEffectBuilders.Returns(selfEffectBuilders);
            unitAction.TargetEffectBuilders.Returns(new List<IPotentialActionEffectBuilder>());
            unitAction.GetAreaOfEffect(new GridCoords(2, 2), Facing.Forward).Returns(areaOfEffect);

			_directionCalculator.GetFacing(new GridCoords(1, 1), new GridCoords(2, 2)).Returns(Facing.Forward);
			_affectedUnitsLookup.GetUnits(Arg.Is<List<GridCoords>>(aoe => aoe[0] == new GridCoords(1, 2) && aoe[1] == new GridCoords(2, 3))).Returns(affectedUnits);
			_potentialActionOutcomeFactory.CreateActionOutcome(actor.Stats, _affectedActorFactory.CreateAffectedActor(), selfEffectBuilders).Returns(selfPotentialOutcome);
            _actionOutcomeFactory.GetActionOutcome(selfPotentialOutcome).Returns(selfOutcome);

			var outcome = _factory.GetOutcome(unitActionOrigin, unitAction, new GridCoords(2, 2));

            CollectionAssert.AreEquivalent(areaOfEffect, outcome.AreaOfEffect);
            CollectionAssert.AreEquivalent(new List<IPotentialActionOutcome> {selfPotentialOutcome}, outcome.PotentialActionOutcomes);
	        CollectionAssert.AreEquivalent(new List<IActionOutcome> { selfOutcome }, outcome.ActionOutcomes);
        }
    }
}

﻿using Common.DataContainers;
using Game.Controllers;
using NUnit.Framework;
using UnityEngine;

namespace UnitTest.Game.Controllers
{
	[TestFixture]
	public class DistanceCalculatorTest
	{
		private DistanceCalculator _calculator;

		[SetUp]
		public void SetUp()
		{
			_calculator = new DistanceCalculator();
		}

		[Test]
		public void GetStraightDistance()
		{
			var first = new GridCoords(0, 0);
			var second = new GridCoords(0, 4);

			var distance = _calculator.GetDistanceBetween(first, second);

			Assert.AreEqual(4, distance);
		}

		[Test]
		public void GetDiagonalDistance()
		{
			var first = new GridCoords(4, 1);
			var second = new GridCoords(2, 4);

			var distance = _calculator.GetDistanceBetween(first, second);

			Assert.AreEqual(Mathf.Sqrt(13), distance);
		}
	}
}
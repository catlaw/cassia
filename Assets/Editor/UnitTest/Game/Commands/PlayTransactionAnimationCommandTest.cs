﻿using System;
using System.Collections.Generic;
using Common.DataContainers;
using Common.TimedSequencing;
using Common.Util;
using Game.Commands;
using Game.ConfigActions;
using Game.ConfigActions.Events;
using Game.ConfigActions.GameObjects;
using Game.ConfigActions.Methods;
using Game.ConfigActions.Params;
using Game.Models.Occupants.Units;
using Game.Models.TurnTransactions;
using Game.Signals.ControllerSignals;
using Game.Views.Animations;
using Instantiator;
using NSubstitute;
using NUnit.Framework;
using UnitTest.Common.TimedSequencing;
using UnitTest.Utils;
using UnitTest.Utils.CoroutineRunner;
using UnityEngine;

namespace UnitTest.Game.Commands
{
	[TestFixture]
	public class PlayTransactionAnimationCommandTest
	{
	    private FakeCoroutineRunner _coroutineRunner;
	    private IParallelCoroutineRunner _parallelCoroutineRunner;
	    private IUnitActionEventExecutor _unitActionEventExecutor;
	    private IActionEffectEventExecutor _actionEffectEventExecutor;
	    private IConfigActionGameObjectFactory _gameObjectFactory;
	    private ITransactionAnimationCompleteSignal _animationCompleteSignal;
		private PlayTransactionAnimationCommand _command;

		[SetUp]
		public void SetUp()
		{
		    _coroutineRunner = FakeCoroutineRunner.CreateWithConstantDeltaTime();
		    _parallelCoroutineRunner = Substitute.For<IParallelCoroutineRunner>();
            _unitActionEventExecutor = Substitute.For<IUnitActionEventExecutor>();
		    _actionEffectEventExecutor = Substitute.For<IActionEffectEventExecutor>();
		    _gameObjectFactory = Substitute.For<IConfigActionGameObjectFactory>();
		    _animationCompleteSignal = Substitute.For<ITransactionAnimationCompleteSignal>();
            
		    _command = new PlayTransactionAnimationCommand(
                _coroutineRunner,
                _parallelCoroutineRunner,
                _unitActionEventExecutor,
                _actionEffectEventExecutor,
                _gameObjectFactory,
                _animationCompleteSignal);
		}

	    [TearDown]
	    public void TearDown()
	    {
			_coroutineRunner.CleanUp();
	    }

		[Test]
		public void Execute()
		{
            _command.Execute();
		    _coroutineRunner.Run();

            // can't test timing
            Received.InOrder(() =>
            {
                _unitActionEventExecutor.Execute(UnitActionEventType.PreHit);
                var damage = _actionEffectEventExecutor.Execute(ActionEffectEventType.Damage);
                var hit = _unitActionEventExecutor.Execute(UnitActionEventType.Hit);

                _parallelCoroutineRunner.Received(1).RunInParallel(Arg.Is<List<Coroutine>>(x =>
                    x.Contains(hit) &&
                    x.Contains(damage)));

                _gameObjectFactory.DestroyAll();
                _animationCompleteSignal.Dispatch();
            });
        }
    }
}

﻿using Common.DataContainers;
using Game.Commands;
using Game.Models.Occupants.Units;
using Game.Models.TurnTransactions;
using Game.Signals.CommandSignals;
using NSubstitute;
using NUnit.Framework;

namespace UnitTest.Game.Commands
{
    [TestFixture]
    public class SelectUnitActionTargetCommandTest
    {
        private SelectUnitActionTargetCommand _command;

        [SetUp]
        public void SetUp()
        {
            _command = new SelectUnitActionTargetCommand();
            _command.TurnTransaction = Substitute.For<ITurnTransaction>();
        }

        [Test]
        public void Execute()
        {
            _command.TargetPosition = new GridCoords(4, 5);

            _command.Execute();

            _command.TurnTransaction.Received(1).SelectTargetPosition(new GridCoords(4, 5));
        }
    }
}

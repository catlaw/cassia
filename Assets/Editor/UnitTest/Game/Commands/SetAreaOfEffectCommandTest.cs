﻿using System.Collections.Generic;
using Common.DataContainers;
using Game.Commands;
using Game.Models.Stage;
using Game.Models.TurnTransactions;
using Game.Signals.ViewSignals;
using NSubstitute;
using NUnit.Framework;
using UnityEngine;

namespace UnitTest.Game.Commands
{
    [TestFixture]
    public class SetAreaOfEffectCommandTest
    {
        private ITurnTransaction _turnTransaction;
        private IAreaOfEffectSquares _areaOfEffectSquares;
        private IHighlightSquaresViewSignal _highlightSquaresViewSignal;
        private IMap _map;

        [SetUp]
        public void SetUp()
        {
            _turnTransaction = Substitute.For<ITurnTransaction>();
            _highlightSquaresViewSignal = Substitute.For<IHighlightSquaresViewSignal>();
            _map = Substitute.For<IMap>();
            _areaOfEffectSquares = Substitute.For<IAreaOfEffectSquares>();
        }

        [Test]
        public void SetAreaOfEffect()
        {
            var square0 = _map.GetSquare(new GridCoords(1, 2));
            var square1 = _map.GetSquare(new GridCoords(2, 3));
            var square2 = _map.GetSquare(new GridCoords(3, 4));

            var areaOfEffect = new List<GridCoords>
            {
                new GridCoords(1, 2),
                new GridCoords(2, 3)
            };

            _areaOfEffectSquares.Get().Returns(new List<GridCoords>
            {
                new GridCoords(2, 3),
                new GridCoords(3, 4)
            });

            var command = new SetAreaOfEffectCommand(areaOfEffect, _highlightSquaresViewSignal,
                _map, _turnTransaction, _areaOfEffectSquares);

            command.Execute();

            square0.Occupant.HighlightViewSignal.Received(1).Dispatch(true, Color.red);
            square0.Occupant.HighlightViewSignal.DidNotReceive().Dispatch(false, Arg.Any<Color>());

            square1.Occupant.HighlightViewSignal.DidNotReceive().Dispatch(Arg.Any<bool>(), Arg.Any<Color>());

            square2.Occupant.HighlightViewSignal.Received(1).Dispatch(false, Arg.Any<Color>());
            square2.Occupant.HighlightViewSignal.DidNotReceive().Dispatch(true, Arg.Any<Color>());

            _highlightSquaresViewSignal.Received(1).Dispatch(areaOfEffect);
            _areaOfEffectSquares.Received(1).Set(areaOfEffect);
        }

        [Test]
        public void SetAreaOfEffectWithActorNotHighlighted()
        {
            var square = _map.GetSquare(new GridCoords(8, 8));
            square.Occupant.Returns(_turnTransaction.Actor);

            var areaOfEffect = new List<GridCoords>
            {
                new GridCoords(8, 8)
            };
            _areaOfEffectSquares.Get().Returns(new List<GridCoords>());

            var command = new SetAreaOfEffectCommand(areaOfEffect, _highlightSquaresViewSignal,
                _map, _turnTransaction, _areaOfEffectSquares);

            command.Execute();

            _turnTransaction.Actor.HighlightViewSignal.DidNotReceive().Dispatch(Arg.Any<bool>(), Arg.Any<Color>());
        }

        [Test]
        public void SetAreaOfEffectWithActorHighlighted()
        {
            var areaOfEffect = new List<GridCoords>
            {
                new GridCoords(8, 8)
            };
            _areaOfEffectSquares.Get().Returns(new List<GridCoords>());

            _turnTransaction.MovePosition.Returns(new GridCoords(8, 8));

            var command = new SetAreaOfEffectCommand(areaOfEffect, _highlightSquaresViewSignal,
                _map, _turnTransaction, _areaOfEffectSquares);

            command.Execute();

            _turnTransaction.Actor.HighlightViewSignal.Received(1).Dispatch(true, Color.red);
        }
    }
}
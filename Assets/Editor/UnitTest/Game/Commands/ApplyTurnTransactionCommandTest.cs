﻿using System;
using System.Collections.Generic;
using Common.DataContainers;
using Game.Commands;
using Game.Controllers.Units;
using Game.Models.Occupants.Units;
using Game.Models.Occupants.Units.UnitActions;
using Game.Models.Stage;
using Game.Models.TurnTransactions;
using Game.Signals.CommandSignals;
using NSubstitute;
using NUnit.Framework;

namespace UnitTest.Game.Commands
{
    [TestFixture]
    public class ApplyTurnTransactionCommandTest
    {
	    private ITurnTransaction _turnTransaction;
	    private IMap _map;
        private ApplyTurnTransactionCommand _command;

        [SetUp]
        public void SetUp()
        {
	        _turnTransaction = Substitute.For<ITurnTransaction>();
	        _map = Substitute.For<IMap>();

	        _command = new ApplyTurnTransactionCommand(_turnTransaction, _map);
        }

        [Test]
        public void Execute()
        {
			var actor = Substitute.For<IUnit>();
			_turnTransaction.Actor.Returns(actor);

			var unitAction = Substitute.For<IUnitAction>();
			_turnTransaction.UnitAction.Returns(unitAction);
			_turnTransaction.Facing.Returns(Facing.Left);

			_turnTransaction.MovePosition.Returns(new GridCoords(1, 2));

			var affectedUnit0 = Substitute.For<IAffectedUnit>();
			var effects0 = new List<IActionEffect> { Substitute.For<IActionEffect>(),
				Substitute.For<IActionEffect>() };
			var affectedUnit1 = Substitute.For<IAffectedUnit>();
			var effects1 = new List<IActionEffect> { Substitute.For<IActionEffect>() };

			var outcomes = new List<IActionOutcome>
			{
				new ActionOutcome { AffectedUnit = affectedUnit0, Effects = effects0 },
				new ActionOutcome { AffectedUnit = affectedUnit1, Effects = effects1 }
			};
			var transactionOutcome = Substitute.For<ITurnTransactionOutcome>();
			transactionOutcome.ActionOutcomes.Returns(outcomes);
			_turnTransaction.Outcome.Returns(transactionOutcome);
			_turnTransaction.IsReady.Returns(true);

			_command.Execute();

			_map.Received(1).SetOccupant(actor, new GridCoords(1, 2));
			actor.Received(1).Facing = Facing.Left;
			affectedUnit0.Unit.Received(1).ApplyActionEffect(effects0[0], actor.Stats);
			affectedUnit0.Unit.Received(1).ApplyActionEffect(effects0[1], actor.Stats);
			affectedUnit1.Unit.Received(1).ApplyActionEffect(effects1[0], actor.Stats);
        }

        [Test]
		[ExpectedException(typeof(ArgumentException), ExpectedMessage = "Cannot apply UnitTurnTransaction if it is not ready.")]
        public void ExecuteWhenUnitTurnTransactionNotReady()
        {
	        _turnTransaction.IsReady.Returns(false);
			_command.Execute();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using Common.DataContainers;
using Game.Commands;
using Game.Models.Stage;
using Game.Signals.ViewSignals;
using NSubstitute;
using NUnit.Framework;

namespace UnitTest.Game.Commands
{
    [TestFixture]
    public class SetSelectableSquaresCommandTest
    {
        private IHighlightSquaresViewSignal _highlightSquaresViewSignal;
        private ISelectableSquares _selectableSquares;

	    [SetUp]
	    public void SetUp()
	    {
	        _highlightSquaresViewSignal = Substitute.For<IHighlightSquaresViewSignal>();
	        _selectableSquares = Substitute.For<ISelectableSquares>();
	    }

	    [Test]
	    public void Execute()
	    {
            var expectedSquares = new List<GridCoords>
			{
				new GridCoords(0, 1), new GridCoords(2, 3), new GridCoords(4, 5)
			};
            var command = new SetSelectableSquaresCommand(_highlightSquaresViewSignal, 
                _selectableSquares, expectedSquares);

            command.Execute();

		    _selectableSquares.Received(1).Set(expectedSquares);
		    _highlightSquaresViewSignal.Received(1).Dispatch(expectedSquares);
	    }

	    [Test]
	    public void ExecuteWithNullSquares()
	    {
	        Assert.Throws<ArgumentNullException>(() =>
	        {
                var command = new SetSelectableSquaresCommand(_highlightSquaresViewSignal, 
                    _selectableSquares, null);
            });
	    }
    }
}

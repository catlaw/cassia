﻿using System;
using System.Collections.Generic;
using Game.Models.Occupants.Units;
using NSubstitute;
using NUnit.Framework;
using UnityEditor;

namespace UnitTest.Game.Models.Occupants.Units
{
	[TestFixture]
	public class UnitViewMapTest
	{
		private UnitViewMap _map;

		[SetUp]
		public void SetUp()
		{
			_map = new UnitViewMap();
		}

		[Test]
		public void GetViewFromUnit()
		{
			var view = Substitute.For<IUnitViewData>();
			var unit = Substitute.For<IUnit>();

			_map.Add(unit, view);
			var actualView = _map.GetView(unit);

			Assert.AreSame(view, actualView);
		}

		[Test]
		[ExpectedException(typeof(KeyNotFoundException),
			ExpectedMessage = "No view found for 'TestUnit'.")]
		public void GetNonExistantViewFromUnit()
		{
			var unit = Substitute.For<IUnit>();
			unit.Name.Returns("TestUnit");

			_map.GetView(unit);
		}

		[Test]
		[ExpectedException(typeof(ArgumentException),
			ExpectedMessage = "Cannot add unit 'TestUnit' twice.")]
		public void AddDuplicateUnit()
		{
			var unit = Substitute.For<IUnit>();
			unit.Name.Returns("TestUnit");
			var view0 = Substitute.For<IUnitViewData>();
			var view1 = Substitute.For<IUnitViewData>();

			_map.Add(unit, view0);
			_map.Add(unit, view1);
		}

		[Test]
		public void GetUnitFromView()
		{
			var view = Substitute.For<IUnitViewData>();
			var unit = Substitute.For<IUnit>();

			_map.Add(unit, view);
			var actualUnit = _map.GetUnit(view);

			Assert.AreSame(unit, actualUnit);
		}

		[Test]
		[ExpectedException(typeof(KeyNotFoundException),
			ExpectedMessage = "No unit found for a unit view.")]
		public void GetNonExistantUnitFromView()
		{
			var view = Substitute.For<IUnitViewData>();

			_map.GetUnit(view);
		}

		[Test]
		[ExpectedException(typeof(ArgumentException),
			ExpectedMessage = "Cannot assign same view to both 'UNIT0' and 'UNIT1'.")]
		public void AddDuplicateView()
		{
			var unit0 = Substitute.For<IUnit>();
			unit0.Name.Returns("UNIT0");

			var unit1 = Substitute.For<IUnit>();
			unit1.Name.Returns("UNIT1");

			var view = Substitute.For<IUnitViewData>();

			_map.Add(unit0, view);
			_map.Add(unit1, view);
		}
	}
}
﻿using System;
using System.Collections.Generic;
using Common.DataContainers;
using Configs;
using Game.Models.Occupants.Units;
using Game.Models.Occupants.Units.UnitActions;
using Game.Models.Pathing;
using Game.Signals.CommandSignals;
using Game.Signals.ControllerSignals;
using Game.Signals.ViewSignals;
using NSubstitute;
using NUnit.Framework;

namespace UnitTest.Game.Models.Occupants.Units
{
	[TestFixture]
	public class UnitTest
	{
		private IPathfinder _pathfinder;
		private IUnitActionStore _unitActionStore;
		private IUnitStats _unitStats;
		private Unit _unit;

		[SetUp]
		public void SetUp()
		{
			_pathfinder = Substitute.For<IPathfinder>();
			_unitActionStore = Substitute.For<IUnitActionStore>();
			_unitStats = Substitute.For<IUnitStats>();
			_unit = new Unit(_pathfinder, _unitActionStore, _unitStats,
				null, null, null, null, null, null);
		}

		[Test]
		public void LoadConfig()
		{
			var config = new UnitConfig
			{
                Name = "Test",
                Stats = new UnitStatsConfig()
			};
			_unit.LoadConfig(config);

		    _unit.Stats.Received(1).LoadConfig(config.Stats);
		}

		[Test]
		public void IsSelectable()
		{
			Assert.IsTrue(_unit.IsSelectable);
		}

		[Test]
		public void GetPathTo()
		{
			var expectedPath = new List<GridCoords> {new GridCoords(1, 2), new GridCoords(3, 4)};
			_pathfinder.GetPathTo(_unit, new GridCoords(1, 2)).Returns(expectedPath);

			CollectionAssert.AreEqual(expectedPath, _unit.GetPathTo(new GridCoords(1, 2)));
		}

		[Test]
		public void GetMovementRange()
		{
			var expectedMovementRange = new List<GridCoords> {new GridCoords(4, 3), new GridCoords(2, 1)};
			_pathfinder.GetMovementRange(_unit).Returns(expectedMovementRange);

			CollectionAssert.AreEqual(expectedMovementRange, _unit.GetMovementRange());
		}

	    [Test]
	    public void GetUnitAction()
	    {
	        var expectedUnitAction0 = new UnitAction();
	        _unitActionStore.GetUnitAction("TestAction0").Returns(expectedUnitAction0);

	        var expectedUnitAction1 = new UnitAction();
	        _unitActionStore.GetUnitAction("TestAction1").Returns(expectedUnitAction1);

	        var unitConfig = new UnitConfig
	        {
	            EquippedActionIds = new List<string> {"TestAction0", "TestAction1"}
	        };
	        _unit.LoadConfig(unitConfig);

	        Assert.AreSame(expectedUnitAction0, _unit.GetUnitAction(0));
	        Assert.AreSame(expectedUnitAction1, _unit.GetUnitAction(1));
	    }

		[Test]
		public void GetDefaultUnitAction()
		{
			var defaultUnitAction = Substitute.For<IUnitAction>();
			_unitActionStore.GetDefaultUnitActions().Returns(new List<IUnitAction> {defaultUnitAction});

			var unitConfig = new UnitConfig
			{
				EquippedActionIds = new List<string> { "TestAction0", "TestAction1" }
			};
			_unit.LoadConfig(unitConfig);

			Assert.AreSame(defaultUnitAction, _unit.GetUnitAction(2));
		}

	    [Test]
        [ExpectedException(typeof(ArgumentException), ExpectedMessage = "No UnitAction with index 3 for 'Test'.")]
	    public void GetInvalidUnitActions()
	    {
            var unitConfig = new UnitConfig
            {
                Name = "Test",
                EquippedActionIds = new List<string> { "TestAction0", "TestAction1" }
            };
			_unitActionStore.GetDefaultUnitActions().Returns(new List<IUnitAction>());
            _unit.LoadConfig(unitConfig);
	        _unit.GetUnitAction(3);
	    }

	    [Test]
	    public void GetEquippedUnitActions()
	    {
            var expectedUnitAction0 = Substitute.For<IUnitAction>();
			_unitActionStore.GetUnitAction("TestAction0").Returns(expectedUnitAction0);

		    var expectedUnitAction1 = Substitute.For<IUnitAction>();
			_unitActionStore.GetUnitAction("TestAction1").Returns(expectedUnitAction1);

		    var defaultUnitActions = new List<IUnitAction> {Substitute.For<IUnitAction>(), Substitute.For<IUnitAction>()};
			_unitActionStore.GetDefaultUnitActions().Returns(defaultUnitActions);

            var unitConfig = new UnitConfig
            {
                EquippedActionIds = new List<string> { "TestAction0", "TestAction1" }
            };
            _unit.LoadConfig(unitConfig);

	        CollectionAssert.AreEqual(new List<IUnitAction> {expectedUnitAction0, expectedUnitAction1, defaultUnitActions[0], defaultUnitActions[1]}, _unit.EquippedUnitActions);
	    }

	    [Test]
	    public void ApplyActionEffect()
	    {
		    var actorStats = Substitute.For<IUnitStats>();

	        var unitActionEffect = Substitute.For<IActionEffect>();
		    unitActionEffect.Delta.Returns(-2);
			unitActionEffect.StatType.Returns(StatType.Health);

	        _unit.ApplyActionEffect(unitActionEffect, actorStats);

			_unit.Stats.Received(1).ApplyModifier(Arg.Is<IStatsModifier>(x => x.StatType == StatType.Health && x.Delta == -2));
	    }


		[Test]
		public void SetFacing()
		{
			_unit.Facing = Facing.Back;
			Assert.AreEqual(Facing.Back, _unit.Facing);
		}
	}
}

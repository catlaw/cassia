﻿using System.Collections.Generic;
using System.Linq;
using Configs;
using Game.Controllers.Units;
using Game.Models.Occupants.Units;
using Game.Models.Occupants.Units.UnitActions;
using NSubstitute;
using NUnit.Framework;
using UnitTest.Common.StateMachine;

namespace UnitTest.Game.Models.Occupants.Units
{
    [TestFixture]
    public class UnitStatsTest
    {
        [Test]
        public void LoadConfig()
        {
            var statsConfig = new UnitStatsConfig
            {
                new StatConfig{StatType = StatType.Health, Value = 5},
                new StatConfig{StatType = StatType.Move, Value = 2},
                new StatConfig{StatType = StatType.Jump, Value = 1}
            };
            var stats = new UnitStats();
            stats.LoadConfig(statsConfig);

            Assert.AreEqual(5, stats.Get(StatType.Health));
            Assert.AreEqual(0, stats.Get(StatType.Power));
            Assert.AreEqual(0, stats.Get(StatType.Defense));
            Assert.AreEqual(2, stats.Get(StatType.Move));
            Assert.AreEqual(1, stats.Get(StatType.Jump));
        }

        [Test]
        public void CreateEmpty()
        {
            var stats = new UnitStats();
            Assert.AreEqual(0, stats.Get(StatType.Health));
            Assert.AreEqual(0, stats.Get(StatType.Power));
            Assert.AreEqual(0, stats.Get(StatType.Defense));
            Assert.AreEqual(0, stats.Get(StatType.Move));
            Assert.AreEqual(0, stats.Get(StatType.Jump));
        }

        [Test]
        public void SetNonRemoveableModifier()
        {
            var statsConfig = new UnitStatsConfig
            {
                new StatConfig {StatType = StatType.Health, Value = 5}
            };
            var stats = new UnitStats();
            stats.LoadConfig(statsConfig);

            var modifier = Substitute.For<IStatsModifier>();
            modifier.StatType.Returns(StatType.Health);
			modifier.Delta.Returns(-1);
			modifier.AbnormalStatus.Returns(AbnormalStatusType.Weaken);
            modifier.IsRemoveable.Returns(false);

            stats.ApplyModifier(modifier);

            Assert.AreEqual(4, stats.Get(StatType.Health));
            CollectionAssert.Contains(stats.AbnormalStatuses, AbnormalStatusType.Weaken);
            CollectionAssert.IsEmpty(stats.Modifiers);
        }

        [Test]
        public void SetRemoveableModifier()
        {
            var statsConfig = new UnitStatsConfig
            {
                new StatConfig {StatType = StatType.Health, Value = 5}
            };
            var stats = new UnitStats();
            stats.LoadConfig(statsConfig);

            var modifier = Substitute.For<IStatsModifier>();
            modifier.StatType.Returns(StatType.Health);
			modifier.Delta.Returns(-1);
			modifier.AbnormalStatus.Returns(AbnormalStatusType.Weaken);
            modifier.IsRemoveable.Returns(true);

            stats.ApplyModifier(modifier);

            Assert.AreEqual(4, stats.Get(StatType.Health));
            CollectionAssert.Contains(stats.AbnormalStatuses, AbnormalStatusType.Weaken);
            Assert.AreEqual(1, stats.Modifiers.Count());
            Assert.AreEqual(modifier, stats.Modifiers.First());
        }

        [Test]
        public void SetNonRemoveableModifierWithUnknownStatType()
        {
            var stats = new UnitStats();

            var modifier = Substitute.For<IStatsModifier>();
            modifier.StatType.Returns(StatType.Health);
			modifier.Delta.Returns(-1);
			modifier.IsRemoveable.Returns(false);

            stats.ApplyModifier(modifier);

            Assert.AreEqual(-1, stats.Get(StatType.Health));
            CollectionAssert.IsEmpty(stats.Modifiers);
        }

        [Test]
        public void SetRemoveableModifierWithUnknownStatType()
        {
            var stats = new UnitStats();

            var modifier = Substitute.For<IStatsModifier>();
            modifier.StatType.Returns(StatType.Health);
			modifier.Delta.Returns(-1);
			modifier.IsRemoveable.Returns(true);

            stats.ApplyModifier(modifier);

            Assert.AreEqual(-1, stats.Get(StatType.Health));
            Assert.AreEqual(1, stats.Modifiers.Count());
            Assert.AreEqual(modifier, stats.Modifiers.First());
        }

        [Test]
        public void ApplyAbnormalStatusTwice()
        {
            var stats = new UnitStats();

            var nonRemoveableModifier = Substitute.For<IStatsModifier>();
            nonRemoveableModifier.AbnormalStatus.Returns(AbnormalStatusType.Weaken);
            nonRemoveableModifier.IsRemoveable.Returns(false);

            var removeableModifier = Substitute.For<IStatsModifier>();
            removeableModifier.AbnormalStatus.Returns(AbnormalStatusType.Weaken);
            removeableModifier.IsRemoveable.Returns(true);

            stats.ApplyModifier(nonRemoveableModifier);
            stats.ApplyModifier(nonRemoveableModifier);
            stats.ApplyModifier(removeableModifier);

            CollectionAssert.AreEqual(new List<AbnormalStatusType> { AbnormalStatusType.Weaken },  stats.AbnormalStatuses);
        }

        [Test]
        public void CombineModifier()
        {
            var stats = new UnitStats();

            var modifier = Substitute.For<IStatsModifier>();
            var unitAction = Substitute.For<IUnitAction>();
            modifier.UnitAction.Returns(unitAction);
            modifier.StatType.Returns(StatType.Health);
            modifier.Delta.Returns(-1);
            modifier.IsRemoveable.Returns(true);
            modifier.IsStackable.Returns(true);

            stats.ApplyModifier(modifier);
            stats.ApplyModifier(modifier);

            Assert.AreEqual(1, stats.Modifiers.Count());
            Assert.AreEqual(StatType.Health, stats.Modifiers.First().StatType);
        }

        [Test]
        public void ReduceDurations()
        {
            var stats = new UnitStats();

            var effect = Substitute.For<IActionEffect>();
            effect.Duration.Returns(2);
            effect.IsRemoveable.Returns(true);
            var modifier = StatsModifier.Create(effect, null, null);

            stats.ApplyModifier(modifier);

            Assert.AreEqual(2, stats.Modifiers.First().Duration);
            stats.ReduceDurations();
            Assert.AreEqual(1, stats.Modifiers.First().Duration);
            stats.ReduceDurations();
            CollectionAssert.IsEmpty(stats.Modifiers);
        }

        [Test]
        public void ReduceDurationForPermanent()
        {
            var stats = new UnitStats();

            var effect = Substitute.For<IActionEffect>();
            effect.Duration.Returns(default(int?));
            effect.IsRemoveable.Returns(true);
            var modifier = StatsModifier.Create(effect, null, null);

            stats.ApplyModifier(modifier);
            stats.ReduceDurations();

            Assert.AreEqual(1, stats.Modifiers.Count());
            Assert.AreEqual(null, stats.Modifiers.First().Duration);
        }

        // TODO remove removeable modifiers
    }
}

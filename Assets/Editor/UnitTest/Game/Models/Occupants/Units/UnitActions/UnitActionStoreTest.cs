﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common.Configurables;
using Common.DataStructures;
using Configs;
using Game.Models.Occupants.Units;
using Game.Models.Occupants.Units.UnitActions;
using NSubstitute;
using NUnit.Framework;

namespace UnitTest.Game.Models.Occupants.Units.UnitActions
{
	[TestFixture]
	public class UnitActionStoreTest
	{
		private IConfig<UnitActionsConfig> _config;
		private IConfigurableStore<IUnitAction, UnitActionConfig> _configurableStore;
		private UnitActionStore _store;

		[SetUp]
		public void SetUp()
		{
			_config = Substitute.For<IConfig<UnitActionsConfig>>();
			_configurableStore = Substitute.For<IConfigurableStore<IUnitAction, UnitActionConfig>>();
			_store = new UnitActionStore(_config, _configurableStore);
		}

		[Test]
		public void GetUnitAction()
		{
			var expectedUnitAction0 = Substitute.For<IUnitAction>();
			_configurableStore.GetById("Test0").Returns(expectedUnitAction0);

			var expectedUnitAction1 = Substitute.For<IUnitAction>();
			_configurableStore.GetById("Test1").Returns(expectedUnitAction1);

			Assert.AreSame(expectedUnitAction0, _store.GetUnitAction("Test0"));
			Assert.AreSame(expectedUnitAction1, _store.GetUnitAction("Test1"));
		}

		[Test]
		public void GetUnitActionWhenLoadingRequired()
		{
			var config = new UnitActionsConfig
			{
				UnitActions = Substitute.For<ILibrary<UnitActionConfig>>()
			};
			_config.Get().Returns(config);

			_configurableStore.GetById("Test").Returns(Substitute.For<IUnitAction>());
			_configurableStore.RequiresLoading.Returns(true);

			_store.GetUnitAction("Test");

			_configurableStore.Received(1).LoadConfigs(config.UnitActions);
		}

		[Test]
		public void GetUnitActionWhenLoadingNotRequired()
		{
			_configurableStore.GetById("Test").Returns(Substitute.For<IUnitAction>());
			_configurableStore.RequiresLoading.Returns(false);

			_store.GetUnitAction("Test");

			_configurableStore.DidNotReceive().LoadConfigs(Arg.Any<ILibrary<UnitActionConfig>>());
		}


		[Test]
		public void GetDefaultUnitActions()
		{
			var config = new UnitActionsConfig
			{
				DefaultUnitActionIds = new List<string>
				{
					"UnitAction0", "UnitAction1"
				}
			};
			_config.Get().Returns(config);
			_configurableStore.RequiresLoading.Returns(true);

			var expectedUnitAction0 = Substitute.For<IUnitAction>();
			_configurableStore.GetById("UnitAction0").Returns(expectedUnitAction0);

			var expectedUnitAction1 = Substitute.For<IUnitAction>();
			_configurableStore.GetById("UnitAction1").Returns(expectedUnitAction1);

			var expectedDefaultUnitActions = new List<IUnitAction> { expectedUnitAction0, expectedUnitAction1 };
			var actualDefaultUnitActions = _store.GetDefaultUnitActions();

			CollectionAssert.AreEqual(expectedDefaultUnitActions, actualDefaultUnitActions);
		}

		[Test]
		public void GetDefaultUnitActionsWhenLoadingRequired()
		{
			var config = new UnitActionsConfig
			{
				UnitActions = Substitute.For<ILibrary<UnitActionConfig>>()
			};
			_config.Get().Returns(config);
			_configurableStore.RequiresLoading.Returns(true);

			_store.GetDefaultUnitActions();

			_configurableStore.Received(1).LoadConfigs(config.UnitActions);
		}

		[Test]
		public void GetDefaultUnitActionsWhenLoadingNotRequired()
		{
			var config = new UnitActionsConfig
			{
				UnitActions = Substitute.For<ILibrary<UnitActionConfig>>()
			};
			_config.Get().Returns(config);
			_configurableStore.RequiresLoading.Returns(false);

			_store.GetDefaultUnitActions();

			_configurableStore.DidNotReceive().LoadConfigs(Arg.Any<ILibrary<UnitActionConfig>>());
		}

		[Test]
		[ExpectedException(typeof(ArgumentNullException))]
		public void GetUnitActionWithNullId()
		{
			_store.GetUnitAction(null);
		}
	}
}
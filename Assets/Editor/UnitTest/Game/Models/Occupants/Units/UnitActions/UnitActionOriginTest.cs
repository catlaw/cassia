﻿using System;
using Common.DataContainers;
using Game.Models.Occupants.Units;
using Game.Models.Occupants.Units.UnitActions;
using NSubstitute;
using NUnit.Framework;

namespace UnitTest.Game.Models.Occupants.Units.UnitActions
{
    [TestFixture]
    public class UnitActionOriginTest
    {
        [Test]
        public void Create()
        {
            var actor = Substitute.For<IUnit>();
            var position = new GridCoords(1, 2);

            var unitActionOrigin = UnitActionOrigin.CreateFromActorAndPosition(actor, position);

            Assert.AreEqual(actor, unitActionOrigin.Actor);
            Assert.AreEqual(position, unitActionOrigin.ActorPosition);
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void CreateWithNullActor()
        {
            UnitActionOrigin.CreateFromActorAndPosition(null, new GridCoords(1, 2));
        }
    }
}

﻿using System.Collections.Generic;
using Common.DataContainers;
using Configs;
using Game.Controllers.Units;
using Game.Controllers.Units.UnitActions;
using Game.Models.Occupants.Units;
using Game.Models.Occupants.Units.UnitActions;
using NSubstitute;
using NUnit.Framework;

namespace UnitTest.Game.Models.Occupants.Units.UnitActions
{
    [TestFixture]
    public class UnitActionTest
    {
	    private UnitAction _unitAction;

	    [SetUp]
	    public void SetUp()
	    {
		    _unitAction = new UnitAction
		    {
				Targeter = Substitute.For<ITargeter>(),
				Effector = Substitute.For<IEffector>()
		    };
	    }

        [Test]
        public void LoadConfig()
        {
            var config = new UnitActionConfig
            {
                Name = "TestAttack",
                Description = "Test Description",
				ViewId = "TestAttackView",
				Target = new TargeterConfig(),
				Effects = new EffectorConfig()
            };

			_unitAction.LoadConfig(config);

	        var expectedTargetRange = new List<GridCoordsOffset>();
	        _unitAction.Targeter.TargetRange.Returns(expectedTargetRange);
	        var expectedAreaOfEffect = new List<GridCoordsOffset>();
	        _unitAction.Targeter.AreaOfEffect.Returns(expectedAreaOfEffect);

            Assert.AreEqual("TestAttack", _unitAction.Name);
            Assert.AreEqual("Test Description", _unitAction.Description);
	        Assert.AreEqual("TestAttackView", _unitAction.ViewId);
	        Assert.AreSame(expectedTargetRange, _unitAction.TargetRange);
	        Assert.AreSame(expectedAreaOfEffect, _unitAction.AreaOfEffect);

			_unitAction.Targeter.Received(1).LoadConfig(config.Target);
			_unitAction.Effector.Received(1).LoadConfig(_unitAction, config.Effects);
        }

	    [Test]
	    public void GetTargetRange()
	    {
		    var expectedTargetRange = new List<GridCoords>();
		    _unitAction.Targeter.GetTargetRange(new GridCoords(1, 2)).Returns(expectedTargetRange);

		    Assert.AreSame(expectedTargetRange, _unitAction.GetTargetRange(new GridCoords(1, 2)));
	    }

	    [Test]
	    public void GetAreaOfEffect()
	    {
		    var expectedAreaOfEffect = new List<GridCoords>();
		    _unitAction.Targeter.GetAreaOfEffect(new GridCoords(2, 2), Facing.Left).Returns(expectedAreaOfEffect);

		    Assert.AreSame(expectedAreaOfEffect, _unitAction.GetAreaOfEffect(new GridCoords(2, 2), Facing.Left));
	    }

	    [Test]
	    public void CanTarget()
	    {
		    _unitAction.Targeter.CanTarget.Returns(true);
		    Assert.IsTrue(_unitAction.CanTarget);
	    }

        [Test]
        public void GetTargetEffectBuilders()
        {
	        var expectedTargetEffectBuilders = new List<IPotentialActionEffectBuilder>();
	        _unitAction.Effector.TargetEffectBuilders.Returns(expectedTargetEffectBuilders);

	        Assert.AreSame(expectedTargetEffectBuilders, _unitAction.TargetEffectBuilders);
        }

	    [Test]
	    public void GetSelfEffectBuilders()
	    {
		    var expectedSelfEffectBuilders = new List<IPotentialActionEffectBuilder>();
		    _unitAction.Effector.SelfEffectBuilders.Returns(expectedSelfEffectBuilders);

			Assert.AreSame(expectedSelfEffectBuilders, _unitAction.SelfEffectBuilders);
	    }

	    [Test]
	    public void HasEffect()
	    {
		    _unitAction.Effector.HasEffect.Returns(true);
		    Assert.IsTrue(_unitAction.HasEffect);
	    }
	}
}

﻿using System;
using System.Collections.Generic;
using Game.Controllers.Units;
using Game.Models.Occupants.Units;
using Game.Models.Occupants.Units.UnitActions;
using NSubstitute;
using NUnit.Framework;

namespace UnitTest.Game.Models.Occupants.Units.UnitActions
{
    [TestFixture]
    public class PotentialActionOutcomeFactoryTest
    {
	    private PotentialActionOutcomeFactory _factory;

	    [SetUp]
	    public void SetUp()
	    {
		    _factory = new PotentialActionOutcomeFactory
		    {
				PotentialActionEffectFactory = Substitute.For<IPotentialActionEffectFactory>()
		    };
	    }

        [Test]
        public void Create()
        {
            var actorStats = Substitute.For<IUnitStats>();
	        var affectedUnit = Substitute.For<IAffectedUnit>();

            var effectBuilders = new List<IPotentialActionEffectBuilder> { Substitute.For<IPotentialActionEffectBuilder>(), Substitute.For<IPotentialActionEffectBuilder>() };
	        var effects = new List<IPotentialActionEffect>
	        {
		        Substitute.For<IPotentialActionEffect>(),
		        Substitute.For<IPotentialActionEffect>()
	        };

	        _factory.PotentialActionEffectFactory.CreateActionEffect(effectBuilders[0], actorStats, affectedUnit.Unit.Stats).Returns(effects[0]);
			_factory.PotentialActionEffectFactory.CreateActionEffect(effectBuilders[1], actorStats, affectedUnit.Unit.Stats).Returns(effects[1]);

			var outcome = _factory.CreateActionOutcome(actorStats, affectedUnit, effectBuilders);

            Assert.AreEqual(affectedUnit, outcome.AffectedUnit);
            CollectionAssert.AreEqual(effectBuilders, effectBuilders);
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void CreateWithNullAffectedUnit()
        {
			_factory.CreateActionOutcome(Substitute.For<IUnitStats>(), null, new List<IPotentialActionEffectBuilder>());
        }

	    [Test]
		[ExpectedException(typeof(ArgumentNullException))]
		public void CreateWithNullActorStats()
	    {
		    _factory.CreateActionOutcome(null, Substitute.For<IAffectedUnit>(), new List<IPotentialActionEffectBuilder>());
	    }

        [Test]
        public void CreateWithNullEffects()
        {
	        var actorStats = Substitute.For<IUnitStats>();
            var affectedUnit = Substitute.For<IAffectedUnit>();

            var outcome = _factory.CreateActionOutcome(actorStats, affectedUnit, null);

            Assert.AreEqual(affectedUnit, outcome.AffectedUnit);
            CollectionAssert.IsEmpty(outcome.Effects);
        }
    }
}

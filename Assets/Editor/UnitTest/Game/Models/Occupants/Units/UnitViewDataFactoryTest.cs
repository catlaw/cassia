﻿using System.Collections.Generic;
using System.Linq;
using Common.DataStructures;
using Configs;
using Game.Models.Occupants.Units;
using Game.Views.Animations;
using Instantiator;
using NSubstitute;
using NUnit.Framework;

namespace UnitTest.Game.Models.Occupants.Units
{
    [TestFixture]
    public class UnitViewDataFactoryTest
    {
        private IDecorationFactory _decorationFactory;
        private UnitViewDataFactory _factory;

        [SetUp]
        public void SetUp()
        {
            _decorationFactory = Substitute.For<IDecorationFactory>();
            _factory = new UnitViewDataFactory(_decorationFactory);
        }

        [Test]
        public void CreateUnitViewData()
        {
            var config = new UnitViewConfig
            {
                Unit = new DecorationConfig(),
                UI = new DecorationConfig(),
                Decorations = new List<DecorationConfig>
                {
                    new DecorationConfig(),
                    new DecorationConfig()
                },
                Animations = Substitute.For<ILibrary<AnimationType, IList<PlayableUnitAnimationConfig>>>(),
                MoveSpeed = 3,
                MaximumWalkHeight = 4
            };

            var data = _factory.CreateUnitViewData(config);

            Assert.AreSame(_decorationFactory.CreateDecoration(config.Unit), data.Unit);
            Assert.AreSame(_decorationFactory.CreateDecoration(config.UI), data.UI);
            Assert.AreSame(_decorationFactory.CreateDecoration(config.Decorations.ElementAt(0)),
               data.Decorations.ElementAt(0));
            Assert.AreSame(_decorationFactory.CreateDecoration(config.Decorations.ElementAt(1)),
               data.Decorations.ElementAt(1));
            Assert.AreSame(config.Animations, data.Animations);
            Assert.AreEqual(config.MoveSpeed, data.MoveSpeed);
            Assert.AreEqual(config.MaximumWalkHeight, data.MaximumWalkHeight);
        }

        // null argument
    }
}
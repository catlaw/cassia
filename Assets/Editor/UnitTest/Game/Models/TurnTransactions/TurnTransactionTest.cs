﻿using Common.DataContainers;
using Game.Controllers.Units;
using Game.Models.Occupants.Units;
using Game.Models.Occupants.Units.UnitActions;
using Game.Models.TurnTransactions;
using NSubstitute;
using NUnit.Framework;

namespace UnitTest.Game.Models.TurnTransactions
{
    [TestFixture]
    public class TurnTransactionTest
    {
        private TurnTransaction _transaction;

        [SetUp]
        public void SetUp()
        {
	        _transaction = new TurnTransaction
	        {
		        StateMachine = Substitute.For<ITurnTransactionStateMachine>(),
		        TurnTransactionOutcomeFactory = Substitute.For<ITurnTransactionOutcomeFactory>()
	        };
        }

        [Test]
        public void Construct()
        {
			Assert.AreSame(EmptyUnit.None, _transaction.Actor);
			Assert.AreSame(EmptyUnitAction.None, _transaction.UnitAction);
			Assert.AreSame(EmptyTurnTransactionOutcome.None, _transaction.Outcome);

			_transaction.PostConstruct();
	        _transaction.StateMachine.Received(1).Init(TransactionState.Nothing);
        }

        [Test]
        public void NormalFlow()
        {
	        var actor = Substitute.For<IUnit>();

			var startPosition = new GridCoords(0, 1);
            _transaction.SelectActor(actor, startPosition);

            var movePosition = new GridCoords(1, 2);
            _transaction.SelectMovePosition(movePosition);

            var unitAction = Substitute.For<IUnitAction>();
            _transaction.SelectUnitAction(unitAction);

	        var facing = Facing.Right;
	        _transaction.SelectFacing(facing);

			var targetPosition = new GridCoords(3, 4);
            var outcome = Substitute.For<ITurnTransactionOutcome>();
            _transaction.TurnTransactionOutcomeFactory.GetOutcome(Arg.Is<UnitActionOrigin>(x => x.Actor == actor && x.ActorPosition == targetPosition), 
                unitAction, targetPosition).Returns(outcome);
            _transaction.SelectTargetPosition(targetPosition);

            Assert.AreEqual(actor, _transaction.Actor);
	        Assert.AreEqual(startPosition, _transaction.StartPosition);
            Assert.AreEqual(movePosition, _transaction.MovePosition);
            Assert.AreEqual(unitAction, _transaction.UnitAction);
	        Assert.AreEqual(facing, _transaction.Facing);
			Assert.AreEqual(targetPosition, _transaction.TargetPosition);
            Assert.AreEqual(outcome, _transaction.Outcome);
        }

        [Test]
        public void IsReady()
        {
            Assert.IsFalse(_transaction.IsReady);

            _transaction.SelectActor(Substitute.For<IUnit>(), new GridCoords(0, 1));
            Assert.IsFalse(_transaction.IsReady);

            _transaction.SelectMovePosition(new GridCoords(1, 2));
            Assert.IsFalse(_transaction.IsReady);

            _transaction.SelectUnitAction(Substitute.For<IUnitAction>());
            Assert.IsFalse(_transaction.IsReady);

            var unitActionOrigin = UnitActionOrigin.CreateFromActorAndPosition(_transaction.Actor, new GridCoords(1, 2));
            _transaction.TurnTransactionOutcomeFactory.GetOutcome(unitActionOrigin, _transaction.UnitAction, new GridCoords(3, 4)).Returns(Substitute.For<ITurnTransactionOutcome>());
            _transaction.SelectTargetPosition(new GridCoords(3, 4));

            _transaction.SelectTargetPosition(new GridCoords(3, 4));
            Assert.IsTrue(_transaction.IsReady);
        }

        [Test]
        public void Clear()
        {
            _transaction.SelectActor(Substitute.For<IUnit>(), new GridCoords(0, 1));
            _transaction.SelectMovePosition(new GridCoords(1, 2));
            _transaction.SelectUnitAction(Substitute.For<IUnitAction>());
            _transaction.SelectTargetPosition(new GridCoords(3, 4));

            _transaction.Clear();

			Assert.AreSame(EmptyUnit.None, _transaction.Actor);
			Assert.AreSame(EmptyUnitAction.None, _transaction.UnitAction);
			Assert.AreSame(EmptyTurnTransactionOutcome.None, _transaction.Outcome);
		}

	    [Test]
	    public void CurrentState()
	    {
		    _transaction.StateMachine.CurrentState.Returns(TransactionState.PreApplyTransaction);
		    var actualState = _transaction.CurrentState;

		    Assert.AreEqual(TransactionState.PreApplyTransaction, actualState);
	    }

		// TODO: the gridcoords position really needs to be a grid position

		// TODO: deselect move position

		// TODO: deselect unit action

		// TODO: deselect target position
	}
}

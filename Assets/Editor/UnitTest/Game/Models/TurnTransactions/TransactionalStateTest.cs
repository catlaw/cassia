﻿using Game.Models.TurnTransactions;
using NSubstitute;
using NUnit.Framework;

namespace UnitTest.Game.Models.TurnTransactions
{
	[TestFixture]
	public class TransactionalStateTest
	{
		private TurnTransactionState _state;

		[SetUp]
		public void SetUp()
		{
			_state = new TurnTransactionState()
			{
				TurnTransaction = Substitute.For<ITurnTransaction>()
			};
		}

		[Test]
		public void GetNothingState()
		{
			_state.TurnTransaction.CurrentState.Returns(global::Game.Controllers.Units.TransactionState.Nothing);
			Assert.IsFalse(_state.HasActorMoved);
		}

		[Test]
		public void GetActorSelectedState()
		{
			_state.TurnTransaction.CurrentState.Returns(global::Game.Controllers.Units.TransactionState.ActorSelected);
			Assert.IsFalse(_state.HasActorMoved);
		}

		[Test]
		public void GetMovingState()
		{
			_state.TurnTransaction.CurrentState.Returns(global::Game.Controllers.Units.TransactionState.Moving);
			Assert.IsTrue(_state.HasActorMoved);
		}

		[Test]
		public void GetPreSelectActionState()
		{
			_state.TurnTransaction.CurrentState.Returns(global::Game.Controllers.Units.TransactionState.PreSelectAction);
			Assert.IsTrue(_state.HasActorMoved);
		}

		[Test]
		public void GetPreSelectTargetState()
		{
			_state.TurnTransaction.CurrentState.Returns(global::Game.Controllers.Units.TransactionState.PreSelectTarget);
			Assert.IsTrue(_state.HasActorMoved);
		}

		[Test]
		public void GetPreApplyTransactionState()
		{
			_state.TurnTransaction.CurrentState.Returns(global::Game.Controllers.Units.TransactionState.PreApplyTransaction);
			Assert.IsTrue(_state.HasActorMoved);
		}
	}
}
﻿using Common.DataContainers;
using Game.Models.Occupants;
using Game.Models.Occupants.Units;
using Game.Models.Stage;
using Game.Models.TurnTransactions;
using NSubstitute;
using NUnit.Framework;

namespace UnitTest.Game.Models.TurnTransactions
{
	[TestFixture]
	public class TransactionalUnitMapTest
	{
		private TransactionalUnitMap _map;

		[SetUp]
		public void SetUp()
		{
			_map = new TransactionalUnitMap
			{
				TurnTransactionState = Substitute.For<ITurnTransactionState>(),
				TurnTransaction = Substitute.For<ITurnTransaction>(),
				Map = Substitute.For<IMap>()
			};
		}

		[Test]
		public void GetUnitInOccupiedSquare()
		{
			var unit = Substitute.For<IUnit>();
			unit.Facing = Facing.Back;
			var square = Substitute.For<ISquare>();
			square.Occupant.Returns(unit);

			_map.Map.GetSquare(new GridCoords(4, 4)).Returns(square);

			var unitState = _map.GetUnitAt(new GridCoords(4, 4));

			Assert.AreSame(unit, unitState.Unit);
			Assert.AreEqual(Facing.Back, unitState.Facing);
		}

		[Test]
		public void GetUnitInUnoccupiedSquare()
		{
			var square = Substitute.For<ISquare>();
			square.Occupant.Returns(EmptyOccupant.None);

			_map.Map.GetSquare(new GridCoords(4, 4)).Returns(square);

			var unitState = _map.GetUnitAt(new GridCoords(4, 4));

			Assert.AreSame(EmptyUnit.None, unitState.Unit);
			Assert.AreEqual(Facing.None, unitState.Facing);
		}

		[Test]
		public void GetActorStartPosition()
		{
			var actor = Substitute.For<IUnit>();
			var square = Substitute.For<ISquare>();
			square.Occupant.Returns(actor);

			_map.Map.GetSquare(new GridCoords(3, 4)).Returns(square);
			_map.TurnTransactionState.HasActorMoved.Returns(true);
			_map.TurnTransaction.StartPosition.Returns(new GridCoords(3, 4));

			var unitState = _map.GetUnitAt(new GridCoords(3, 4));

			Assert.AreSame(EmptyUnit.None, unitState.Unit);
			Assert.AreEqual(Facing.None, unitState.Facing);
		}

		[Test]
		public void GetActorMovePosition()
		{
			var actor = Substitute.For<IUnit>();
			actor.Facing.Returns(Facing.Forward);
			var square = Substitute.For<ISquare>();
			square.Occupant.Returns(EmptyOccupant.None);

			_map.Map.GetSquare(new GridCoords(1, 2)).Returns(square);
			_map.TurnTransactionState.HasActorMoved.Returns(true);
			_map.TurnTransaction.Actor.Returns(actor);
			_map.TurnTransaction.MovePosition.Returns(new GridCoords(1, 2));
			_map.TurnTransaction.Facing.Returns(Facing.Left);

			var unitState = _map.GetUnitAt(new GridCoords(1, 2));

			Assert.AreSame(actor, unitState.Unit);
			Assert.AreEqual(Facing.Left, unitState.Facing);
		}

		[Test]
		public void GetActorStartAndMovePosition()
		{
			var actor = Substitute.For<IUnit>();
			actor.Facing.Returns(Facing.Forward);
			var square = Substitute.For<ISquare>();
			square.Occupant.Returns(EmptyOccupant.None);

			_map.Map.GetSquare(new GridCoords(1, 2)).Returns(square);
			_map.TurnTransactionState.HasActorMoved.Returns(true);
			_map.TurnTransaction.Actor.Returns(actor);
			_map.TurnTransaction.StartPosition.Returns(new GridCoords(1, 2));
			_map.TurnTransaction.MovePosition.Returns(new GridCoords(1, 2));
			_map.TurnTransaction.Facing.Returns(Facing.Left);

			var unitState = _map.GetUnitAt(new GridCoords(1, 2));

			Assert.AreSame(actor, unitState.Unit);
			Assert.AreEqual(Facing.Left, unitState.Facing);
		}

		[Test]
		public void TestUnitTurnTransactionNotMoved()
		{
			var actor = Substitute.For<IUnit>();
			actor.Facing.Returns(Facing.Forward);
			var square = Substitute.For<ISquare>();
			square.Occupant.Returns(actor);

			_map.Map.GetSquare(new GridCoords(3, 4)).Returns(square);
			_map.TurnTransactionState.HasActorMoved.Returns(false);
			_map.TurnTransaction.StartPosition.Returns(new GridCoords(3, 4));

			var unitState = _map.GetUnitAt(new GridCoords(3, 4));

			Assert.AreSame(actor, unitState.Unit);
			Assert.AreEqual(Facing.Forward, unitState.Facing);
		}
	}
}
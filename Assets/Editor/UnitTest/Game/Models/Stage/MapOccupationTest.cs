using System;
using Common.DataContainers;
using Configs;
using Game.Models.Occupants;
using Game.Models.Occupants.Units;
using Game.Models.Stage;
using NSubstitute;
using NUnit.Framework;

namespace UnitTest.Game.Models.Stage
{
	[TestFixture]
	public class MapOccupationTest
	{
		private Map _map;

		[SetUp]
		public void SetUp()
		{
			var levelConfig = Substitute.For<IConfig<LevelConfig>>();
			levelConfig.Get().Returns(new LevelConfig
			{
				MapHeights = new[,]
				{
					{1, 1}
				}
			});

            _map = new Map(levelConfig);
			_map.LoadHeights();
		}

		[Test]
		public void GetOccupantCoords()
		{
			var occupant = Substitute.For<IUnit>();
			_map.SetOccupant(occupant, new GridCoords(0, 1));

			var occupantCoords = _map.GetGridCoords(occupant);

			Assert.AreEqual(new GridCoords(0, 1), occupantCoords);
		}

		[Test]
		[ExpectedException(typeof(ArgumentNullException))]
		public void GetGridCoordsForNull()
		{
			_map.GetGridCoords(null);
		}

		[Test]
		[ExpectedException(typeof(ArgumentException), 
			ExpectedMessage = "Cannot get GridCoords for an occupant that is not on the map.")]
		public void GetGridCoordsForOccupantNotOnMap()
		{
			var occupant = Substitute.For<IUnit>();
			_map.GetGridCoords(occupant);
		}

		[Test]
		public void HasOccupant()
		{
			var occupant = Substitute.For<IUnit>();
			Assert.IsFalse(_map.HasOccupant(occupant));

			_map.SetOccupant(occupant, new GridCoords(0, 0));
			Assert.IsTrue(_map.HasOccupant(occupant));
		}
	}
}
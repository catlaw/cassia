﻿using System;
using Configs;
using Game.Models.Occupants;
using Game.Models.Occupants.Units;
using Game.Models.Stage;
using NSubstitute;
using NUnit.Framework;

namespace UnitTest.Game.Models.Stage
{
	[TestFixture]
	public class SquareTest
	{
		private Square _square;

		[SetUp]
		public void SetUp()
		{
			_square =  Square.CreateWithHeight(1);
		}

		[Test]
		public void Create()
		{
			Assert.AreEqual(1, _square.Height);
			Assert.IsFalse(_square.IsOccupied);
		}

		[Test]
		public void IsAllyPathable()
		{
			var allyUnit = Substitute.For<IUnit>();
			var unit = Substitute.For<IUnit>();
			allyUnit.AllowsPassingBy(unit).Returns(true);

			_square.SetOccupant(allyUnit);

			Assert.IsTrue(_square.IsOccupied);
			Assert.IsTrue(_square.IsPathableBy(unit));
		}

		[Test]
		public void UnaffiliatedCharacterNeverPathable()
		{
			var character = Substitute.For<IUnit>();
			var otherCharacter = Substitute.For<IUnit>();
			_square.SetOccupant(character);

			Assert.IsFalse(_square.IsPathableBy(otherCharacter));
		}

		[Test]
		public void IsNonAllyUnpathable()
		{
			var nonAllyCharacter = Substitute.For<IUnit>();

			_square.SetOccupant(nonAllyCharacter);

			var allyTeam = Affiliation.CreateWithName("Ally");
			var character = Substitute.For<IUnit>();
			character.Affiliation.Returns(allyTeam);

			Assert.IsTrue(_square.IsOccupied);
			Assert.IsFalse(_square.IsPathableBy(character));
		}

		[Test]
		public void SetOccupant()
		{
			var unit = Substitute.For<IUnit>();
			_square.SetOccupant(unit);

			Assert.IsTrue(_square.IsOccupied);
		}

		[Test]
		public void SetOccupantToSelf()
		{
			var unit = Substitute.For<IUnit>();
			_square.SetOccupant(unit);
			_square.SetOccupant(unit);

			Assert.IsTrue(_square.IsOccupied);
		}

		[Test]
		public void RemoveOccupant()
		{
			var unitModel = Substitute.For<IUnit>();
			_square.SetOccupant(unitModel);
			_square.RemoveOccupant();

			Assert.IsFalse(_square.IsOccupied);
		}

		[Test]
		[ExpectedException(typeof(InvalidOperationException), ExpectedMessage = "Cannot set occupant on occupied square.")]
		public void SetOccupantOnOccupied()
		{
			var unit = Substitute.For<IUnit>();
			_square.SetOccupant(unit);

			var unitNew = Substitute.For<IUnit>();
			_square.SetOccupant(unitNew);
		}
	}
}

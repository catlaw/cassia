﻿using System;
using Common.DataContainers;
using Configs;
using Game.Models.Occupants;
using Game.Models.Occupants.Units;
using Game.Models.Stage;
using NSubstitute;
using NUnit.Framework;

namespace UnitTest.Game.Models.Stage
{
	[TestFixture]
	class MapSetOccupantTest
	{
		private Map _map;
		private IUnit _unit;

		[SetUp]
		public void SetUp()
		{
			var levelConfig = Substitute.For<IConfig<LevelConfig>>();
			levelConfig.Get().Returns(new LevelConfig
			{
				MapHeights = new[,]
				{
					{1, 1, 1}
				}
			});
            _map = new Map(levelConfig);
			_map.LoadHeights();

			_unit = Substitute.For<IUnit>();
			_map.SetOccupant(_unit, new GridCoords(0, 0));
		    _map.SetOccupant(EmptyOccupant.Blocking, new GridCoords(0, 2));
		}

		[Test]
		public void SetOccupant()
		{
			_map.SetOccupant(_unit, new GridCoords(0, 0));

			Assert.AreEqual(_unit, _map.GetSquare(new GridCoords(0, 0)).Occupant);
			Assert.AreEqual(EmptyOccupant.None, _map.GetSquare(new GridCoords(0, 1)).Occupant);

			_map.SetOccupant(_unit, new GridCoords(0, 1));

			Assert.AreEqual(EmptyOccupant.None, _map.GetSquare(new GridCoords(0, 0)).Occupant);
			Assert.AreEqual(_unit, _map.GetSquare(new GridCoords(0, 1)).Occupant);
		}

		[Test]
		[ExpectedException(typeof(InvalidOperationException),
			ExpectedMessage = "Cannot set occupant at GridCoords(0, 2) because this square is occupied.")]
		public void SetOccupantAtUnpathableSquare()
		{
			_map.SetOccupant(_unit, new GridCoords(0, 2));
		}

		[Test]
		[ExpectedException(typeof(InvalidOperationException),
			ExpectedMessage = "Cannot set occupant at GridCoords(0, 0) because this square is occupied.")]
		public void SetOccupantAtOccupiedSquare()
		{
			_map.SetOccupant(_unit, new GridCoords(0, 0));

			var characterModelNew = Substitute.For<IUnit>();
			_map.SetOccupant(characterModelNew, new GridCoords(0, 0));
		}

		[Test]
		public void SetOccupantOnOwnSquare()
		{
			_map.SetOccupant(_unit, new GridCoords(0, 0));
			_map.SetOccupant(_unit, new GridCoords(0, 0));

			Assert.AreEqual(_unit, _map.GetSquare(new GridCoords(0, 0)).Occupant);
		}
	}
}

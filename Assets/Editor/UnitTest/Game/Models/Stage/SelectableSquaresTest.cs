﻿using System.Collections.Generic;
using System.Runtime.InteropServices.ComTypes;
using Common.DataContainers;
using Game.Models.Stage;
using NUnit.Framework;

namespace UnitTest.Game.Models.Stage
{
    [TestFixture]
    public class SelectableSquaresTest
    {
        private StoredSquares _storedSquares;

        private interface ITestSquare
        { }

        [SetUp]
        public void SetUp()
        {
            _storedSquares = new StoredSquares();
        }

        [Test]
        public void Select()
        {
            _storedSquares.Set(new List<GridCoords> { new GridCoords(1, 2), new GridCoords(3, 4) });
            CollectionAssert.AreEqual(new List<GridCoords> { new GridCoords(1, 2), new GridCoords(3, 4) }, _storedSquares.Get());
        }

        [Test]
        public void SelectWithOtherSelected()
        {
            _storedSquares.Set(new List<GridCoords> { new GridCoords(1, 2) });
            _storedSquares.Set(new List<GridCoords> { new GridCoords(3, 4) });
            CollectionAssert.AreEqual(new List<GridCoords> { new GridCoords(3, 4) }, _storedSquares.Get());
        }

        [Test]
        public void GetDefault()
        {
            CollectionAssert.AreEqual(new List<GridCoords>(), _storedSquares.Get());
        }

        // TODO add?
    }
}

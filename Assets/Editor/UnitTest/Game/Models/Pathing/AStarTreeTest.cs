﻿using System.Collections.Generic;
using Common.DataContainers;
using Game.Models.Pathing;
using NUnit.Framework;

namespace UnitTest.Game.Models.Pathing
{
	[TestFixture]
	public class AStarTreeTest
	{
		[Test]
		public void Create()
		{
			var tree = AStarTree.CreateWithMaxDepth(10);
			tree.Root = AStarNode.CreateWithValue(new GridCoords(0, 0), 10);
			var child = tree.Root.AddChild(new GridCoords(1, 0), 9);

			Assert.AreEqual(10, tree.Root.Cost);
			CollectionAssert.AreEqual(new List<AStarNode> {child}, tree.Root.Children);
			Assert.AreEqual(10, child.Cost);
			Assert.AreEqual(tree.Root, child.Parent);
		}

		[Test]
		public void GetNextNodeToVisit()
		{
			var tree = AStarTree.CreateWithMaxDepth(2);
			tree.Root = AStarNode.CreateWithValue(new GridCoords(0, 0), 10);
			var childDepth1_0 = tree.Root.AddChild(new GridCoords(0, 0), 5);
			var childDepth1_1 = tree.Root.AddChild(new GridCoords(0, 0), 5);
			childDepth1_1.AddChild(new GridCoords(0, 0), 5);
			childDepth1_1.AddChild(new GridCoords(0, 0), 1).AddChild(new GridCoords(0, 0), 0);
			
			Assert.AreEqual(childDepth1_0, tree.GetNextNode());
		}

		[Test]
		public void GetNextNodeToVisitWithoutRoot()
		{
			var tree = AStarTree.CreateWithMaxDepth(1);
			Assert.AreEqual(AStarNode.None, tree.GetNextNode());
		}

		[Test]
		public void GetNextNodeToVisitForRoot()
		{
			var tree = AStarTree.CreateWithMaxDepth(10);
			tree.Root = AStarNode.CreateWithValue(new GridCoords(0, 0), 10);
			Assert.AreEqual(tree.Root, tree.GetNextNode());
		}

		[Test]
		public void GetNoNextNodeToVisit()
		{
			var tree = AStarTree.CreateWithMaxDepth(0);
			tree.Root = AStarNode.CreateWithValue(new GridCoords(0, 0), 10);
			tree.Root.AddChild(new GridCoords(0, 0), 10);
			Assert.AreEqual(AStarNode.None, tree.GetNextNode());
		}
	}
}
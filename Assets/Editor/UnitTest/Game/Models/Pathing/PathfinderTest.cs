﻿using System;
using System.Collections.Generic;
using Common.DataContainers;
using Configs;
using Game.Models.Occupants;
using Game.Models.Occupants.Units;
using Game.Models.Pathing;
using Game.Models.Stage;
using NSubstitute;
using NUnit.Framework;
using UnitTest.Utils;

namespace UnitTest.Game.Models.Pathing
{
	[TestFixture]
	public class PathfinderTest
	{
		private Pathfinder _pathfinder;
		private IUnit _unit;

		[SetUp]
		public void SetUp()
		{
			var levelConfig = Substitute.For<IConfig<LevelConfig>>();
			levelConfig.Get().Returns(new LevelConfig
			{
				MapHeights = new[,]
				{
					{1, 1, 5, 1},
					{2, 3, 1, 1}
				}
			});

			var map = new Map(levelConfig);
			map.LoadHeights();

			var allies = Affiliation.CreateWithName("Test");

			_unit = Substitute.For<IUnit>();
			_unit.Name.Returns("TestUnit");
			_unit.Move.Returns(5);
			_unit.Jump.Returns(1);
			_unit.Affiliation.Returns(allies);
			_unit.AllowsPassingBy(Arg.Any<IOccupant>()).Returns(true);

			_pathfinder = new Pathfinder {Map = map};
		}

		[Test]
		public void FindPath()
		{
			_pathfinder.Map.SetOccupant(_unit, new GridCoords(0, 0));
			var expected = new List<GridCoords> { new GridCoords(0, 0), new GridCoords(1, 0) };
			var actual = _pathfinder.GetPathTo(_unit, new GridCoords(1, 0));

			CollectionAssert.AreEqual(expected, actual);
		}

		[Test]
		public void FindDistantPath()
		{
			_pathfinder.Map.SetOccupant(_unit, new GridCoords(0, 0));
			var expected = new List<GridCoords> { new GridCoords(0, 0), new GridCoords(1, 0), new GridCoords(1, 1), new GridCoords(1, 2), new GridCoords(1, 3), new GridCoords(0, 3) };
			var actual = _pathfinder.GetPathTo(_unit, new GridCoords(0, 3));

			CollectionAssert.AreEqual(expected, actual);
		}

		[Test]
		public void FindUnpathable()
		{
			_pathfinder.Map.SetOccupant(_unit, new GridCoords(0, 3));
			CollectionAssert.AreEqual(new List<GridCoords>(), _pathfinder.GetPathTo(_unit, new GridCoords(0, 0)));
		}

		[Test]
		[ExpectedException(typeof(ArgumentException), ExpectedMessage = "Cannot find path if unit 'TestUnit' is not on the map.")]
		public void UnitNotOnMap()
		{
			_pathfinder.GetPathTo(_unit, new GridCoords(0, 0));
		}

		[Test]
		[ExpectedException(typeof(IndexOutOfRangeException),
			ExpectedMessage = "Cannot find path from GridCoords(0, 0) to GridCoords(2, 0) because Map is of size (2, 4).")]
		public void FindPathWithInvalidGridCoords()
		{
			_pathfinder.Map.SetOccupant(_unit, new GridCoords(0, 0));
			_pathfinder.GetPathTo(_unit, new GridCoords(2, 0));
		}

		[Test]
		public void FindBlockedPath()
		{
			_pathfinder.Map.SetOccupant(_unit, new GridCoords(0, 0));

			var unit = Substitute.For<IUnit>();
			_pathfinder.Map.SetOccupant(unit, new GridCoords(1, 2));

			CollectionAssert.AreEqual(new List<GridCoords>(), _pathfinder.GetPathTo(_unit, new GridCoords(0, 3)));
		}

		[Test]
		public void FindPathNotBlockedByAlly()
		{
			_pathfinder.Map.SetOccupant(_unit, new GridCoords(0, 0));

			var allyUnit = Substitute.For<IUnit>();
			allyUnit.AllowsPassingBy(_unit).Returns(true);

			_pathfinder.Map.SetOccupant(allyUnit, new GridCoords(1, 2));

			var expected = new List<GridCoords> { new GridCoords(0, 0), new GridCoords(1, 0), new GridCoords(1, 1), new GridCoords(1, 2), new GridCoords(1, 3), new GridCoords(0, 3) };
			var actual = _pathfinder.GetPathTo(_unit, new GridCoords(0, 3));

			CollectionAssert.AreEqual(expected, actual);
		}

		[Test]
		public void FindPathOutOfRange()
		{
			var levelConfig = Substitute.For<IConfig<LevelConfig>>();
			levelConfig.Get().Returns(new LevelConfig
			{
				MapHeights = new[,]
				{
					{1, 1, 1, 1, 1, 1, 1, 1}
				}
			});
			_pathfinder.Map = new Map(levelConfig);
			_pathfinder.Map.LoadHeights();

			_pathfinder.Map.SetOccupant(_unit, new GridCoords(0, 0));

			CollectionAssert.AreNotEqual(new List<GridCoords>(), _pathfinder.GetPathTo(_unit, new GridCoords(0, 5)));
			CollectionAssert.AreEqual(new List<GridCoords>(), _pathfinder.GetPathTo(_unit, new GridCoords(0, 6)));
		}

		[Test]
		public void FindPathToSameSquare()
		{
			_pathfinder.Map.SetOccupant(_unit, new GridCoords(0, 0));
			var actual = _pathfinder.GetPathTo(_unit, new GridCoords(0, 0));

			CollectionAssert.AreEqual(new List<GridCoords>(), actual);
		}

		[Test]
		public void FindWayAroundCliff()
		{
			var levelConfig = Substitute.For<IConfig<LevelConfig>>();
			levelConfig.Get().Returns(new LevelConfig
			{
				MapHeights = new[,]
				{
					{1, 1, 1, 1, 1, 1},
					{1, 1, 1, 1, 3, 4},
					{1, 1, 1, 1, 2, 1}
				}
			});
			_pathfinder.Map = new Map(levelConfig);
			_pathfinder.Map.LoadHeights();

			_pathfinder.Map.SetOccupant(_unit, new GridCoords(2, 5));

			var expected = new List<GridCoords>
			{
				new GridCoords(2, 5),
				new GridCoords(2, 4),
				new GridCoords(1, 4),
				new GridCoords(1, 5)
			};

			CollectionAssert.AreEqual(expected, _pathfinder.GetPathTo(_unit, new GridCoords(1, 5)));
		}

		[Test]
		public void GetMovementRange()
		{
			var levelConfig = Substitute.For<IConfig<LevelConfig>>();
			levelConfig.Get().Returns(new LevelConfig
			{
				MapHeights = new[,]
				{
					{0, 0, 0},
					{0, 0, 0},
					{0, 0, 0}
				}
			});
			_pathfinder.Map = new Map(levelConfig);
			_pathfinder.Map.LoadHeights();

			var unit = Substitute.For<IUnit>();
			unit.Move.Returns(1);

			_pathfinder.Map.SetOccupant(unit, new GridCoords(1, 1));

			var expectedGridCoords = new List<GridCoords>
			{
				new GridCoords(1, 1),
				new GridCoords(1, 0),
				new GridCoords(0, 1),
				new GridCoords(1, 2),
				new GridCoords(2, 1)
			};
			CollectionAssert.AreEquivalent(expectedGridCoords, _pathfinder.GetMovementRange(unit));
		}
	}
}

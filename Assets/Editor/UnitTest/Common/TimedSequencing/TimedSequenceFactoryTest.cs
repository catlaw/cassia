﻿using System.Collections.Generic;
using Common.TimedSequencing;
using NSubstitute;
using NUnit.Framework;
using strange.framework.api;

namespace UnitTest.Common.TimedSequencing
{
	[TestFixture]
	public class TimedSequenceFactoryTest
	{
		private IInstanceProvider _instanceProvider;
		private TimedSequenceFactory _factory;

		[SetUp]
		public void SetUp()
		{
			_instanceProvider = Substitute.For<IInstanceProvider>();
			_factory = new TimedSequenceFactory(_instanceProvider);
		}

		[Test]
		public void GetSequence()
		{
			var expectedSequence = Substitute.For<ITimedSequence>();
			_instanceProvider.GetInstance<ITimedSequence>().Returns(expectedSequence);
			var actualSequence = _factory.GetTimedSequence();

			Assert.AreSame(expectedSequence, actualSequence);
		}
	}
}
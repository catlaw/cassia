﻿using System;
using System.Collections;
using System.Collections.Generic;
using Common.TimedSequencing;
using UnitTest.Utils.CoroutineRunner;

namespace UnitTest.Common.TimedSequencing
{
    // not sure if i still need this!
    public class FakeTimedSequence : ITimedSequence
    {
        private readonly List<PendingAction> _pendingActions;
        private readonly FakeCoroutineRunner _runner;


        public FakeTimedSequence()
        {
            _pendingActions = new List<PendingAction>();
            _runner = FakeCoroutineRunner.CreateWithConstantDeltaTime();
        }

        public float Elapsed { get { return _runner.Time; } }


        public IEnumerable<ITimedSequenceAction> GetActions()
        {
            throw new NotSupportedException(); 
        }

        public ITimedSequence Wait(float seconds)
        {
            _pendingActions.Add(new PendingAction { CoroutineEnumerator = CoWait(seconds)} );
            return this;
        }

        private IEnumerator CoWait(float seconds)
        {
            var elapsed = 0f;
            while (elapsed < seconds)
            {
                elapsed += DeltaTime;
                yield return null;
            }
        }

        public void Run()
        {
            _runner.StartCoroutine(CoRun());
            _runner.Run();
        }

        private IEnumerator CoRun()
        {
            foreach (var pendingAction in _pendingActions)
            {
                if (pendingAction.Action != null)
                    pendingAction.Action();
                else
                    yield return _runner.StartCoroutine(pendingAction.CoroutineEnumerator);
            }
        }

        public ITimedSequence WaitThenDo(float seconds, Action action)
        {
            _pendingActions.Add(new PendingAction { CoroutineEnumerator = CoWait(seconds) });
            _pendingActions.Add(new PendingAction { Action = action });
            return this;
        }

        public ITimedSequence Do(Action action)
        {
            _pendingActions.Add(new PendingAction { Action = action });
            return this;
        }

        public ITimedSequence DoCoroutine(IEnumerator coroutineEnumerator)
        {
            _pendingActions.Add(new PendingAction { CoroutineEnumerator = coroutineEnumerator });
            return this;
        }

        private class PendingAction
        {
            public Action Action { get; set; }
            public IEnumerator CoroutineEnumerator { get; set; }
        }

        public float DeltaTime
        {
            get { return _runner.DeltaTime; }
        }

        public float MaxDeltaTime
        {
            get { return _runner.MaxDeltaTime; }
        }

        public void CleanUp()
        {
			_runner.CleanUp();
        }
    }
}
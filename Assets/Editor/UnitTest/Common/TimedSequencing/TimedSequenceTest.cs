using System;
using System.Collections;
using System.Collections.Generic;
using Common.TimedSequencing;
using NSubstitute;
using NUnit.Framework;

namespace UnitTest.Common.TimedSequencing
{
	[TestFixture]
	public class TimedSequenceTest
	{
		private ITimedSequenceActionFactory _sequenceActionFactory;
		private ITimedSequenceRunner _runner;
		private TimedSequence _sequence;

		[SetUp]
		public void SetUp()
		{
			_sequenceActionFactory = Substitute.For<ITimedSequenceActionFactory>();
			_runner = Substitute.For<ITimedSequenceRunner>();
			_sequence = new TimedSequence(_sequenceActionFactory, _runner);
		}

		[Test]
		public void Do()
		{
			var action = Substitute.For<ITimedSequenceAction>();
			_sequenceActionFactory.GetSequenceAction(TestAction0).Returns(action);

			_sequence.Do(TestAction0);

			var expectedActions = new List<ITimedSequenceAction> { action };
			var actualActions = _sequence.GetActions();

			CollectionAssert.AreEqual(expectedActions, actualActions);
		}

		private void TestAction0() { }

		[Test]
		public void Wait()
		{
			var wait0 = Substitute.For<ITimedSequenceAction>();
			_sequenceActionFactory.GetWait(1.5f).Returns(wait0);

			_sequence.Wait(1.5f);

			var expectedActions = new List<ITimedSequenceAction> { wait0 };
			var actualActions = _sequence.GetActions();

			CollectionAssert.AreEqual(expectedActions, actualActions);
		}

		[Test]
		public void WaitThenDo()
		{
			var action = Substitute.For<ITimedSequenceAction>();
			_sequenceActionFactory.GetSequenceAction(TestAction0, 0.4f).Returns(action);

			_sequence.WaitThenDo(0.4f, TestAction0);

			var expectedActions = new List<ITimedSequenceAction> { action };
			var actualActions = _sequence.GetActions();

			CollectionAssert.AreEqual(expectedActions, actualActions);
		}

		[Test]
		public void DoAndWaitAndWaitThenDo()
		{
			var action0 = Substitute.For<ITimedSequenceAction>();
			_sequenceActionFactory.GetSequenceAction(TestAction0).Returns(action0);

			var action1 = Substitute.For<ITimedSequenceAction>();
			_sequenceActionFactory.GetWait(0.4f).Returns(action1);

			var action2 = Substitute.For<ITimedSequenceAction>();
			_sequenceActionFactory.GetSequenceAction(TestAction0, 0.1f).Returns(action2);

			_sequence.Do(TestAction0).Wait(0.4f).WaitThenDo(0.1f, TestAction0);

			var expectedActions = new List<ITimedSequenceAction>
			{
				action0,
				action1,
				action2
			};
			var actualActions = _sequence.GetActions();

			CollectionAssert.AreEqual(expectedActions, actualActions);
		}

	    [Test]
	    public void DoCoroutine()
	    {
	        var coroutine = CoTest();
	        var action = Substitute.For<ITimedSequenceAction>();
	        _sequenceActionFactory.GetSequenceCoroutine(coroutine).Returns(action);

	        _sequence.DoCoroutine(coroutine);

            var expectedActions = new List<ITimedSequenceAction> { action };
            var actualActions = _sequence.GetActions();

            CollectionAssert.AreEqual(expectedActions, actualActions);
        }

	    private IEnumerator CoTest()
	    {
	        yield break;
	    }

		[Test]
		[ExpectedException(typeof(ArgumentNullException))]
		public void DoNullThrowsException()
		{
			_sequence.Do(null);
		}

		[Test]
		[ExpectedException(typeof(ArgumentNullException))]
		public void WaitThenDoNullThrowsException()
		{
			_sequence.WaitThenDo(0f, null);
		}

	    [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void DoNullCoroutineThrowsException()
	    {
	        _sequence.DoCoroutine(null);
	    }

		[Test]
		public void Run()
		{
			_sequence.Run();

			_runner.Received(1).Run(_sequence);
		}
	}
}
﻿using System;
using System.Collections;
using Common.TimedSequencing;
using NUnit.Framework;

namespace UnitTest.Common.TimedSequencing
{
	[TestFixture]
	public class TimedSequenceActionFactoryTest
	{
		private TimedSequenceActionFactory _factory;

		[SetUp]
		public void SetUp()
		{
			_factory = new TimedSequenceActionFactory();	
		}

		[Test]
		public void GetSequenceAction()
		{
			Action testAction = TestAction;
			var sequenceAction = _factory.GetSequenceAction(testAction);

			Assert.AreEqual(0, sequenceAction.WaitTime);
			Assert.AreSame(testAction, sequenceAction.Action);
		}

		[Test]
		public void GetDelayedSequenceAction()
		{
			Action testAction = TestAction;
			var sequenceAction = _factory.GetSequenceAction(testAction, 0.4f);

			Assert.AreEqual(0.4f, sequenceAction.WaitTime);
			Assert.AreSame(testAction, sequenceAction.Action);
		}

		private void TestAction() { }

		[Test]
		public void GetWait()
		{
			var sequenceAction = _factory.GetWait(0.5f);

			Assert.AreEqual(0.5f, sequenceAction.WaitTime);
			Assert.NotNull(sequenceAction.Action); // can't really test it, but does nothing.
		    Assert.IsNull(sequenceAction.CoroutineEnumerator);
		}

	    [Test]
	    public void GetSequenceCoroutine()
	    {
	        var coroutine = CoTest();
	        var sequenceAction = _factory.GetSequenceCoroutine(coroutine);

	        Assert.AreEqual(0, sequenceAction.WaitTime);
	        Assert.AreSame(coroutine, sequenceAction.CoroutineEnumerator);
            Assert.NotNull(sequenceAction.Action); // can't really test it, but does nothing.
        }

	    private IEnumerator CoTest()
	    {
	        yield break;
	    }
	}
}
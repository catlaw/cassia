﻿using System;
using System.Collections;
using NUnit.Framework;
using UnitTest.Utils;
using UnitTest.Utils.Asserts;
using UnitTest.Utils.CoroutineRunner;

namespace UnitTest.Common.TimedSequencing
{
    [TestFixture]
    public class FakeTimedSequenceTest
    {
        private FakeTimedSequence _sequence;

        [SetUp]
        public void SetUp()
        {
            _sequence = new FakeTimedSequence();
        }

        [TearDown]
        public void TearDown()
        {
            _sequence.CleanUp();
        }

        [Test]
        public void Do()
        {
            var count = 0;
            _sequence.Do(() => { count++; }).Do(() => { count++; });

            Assert.AreEqual(0, count);
            AssertTime.Elapsed(0, _sequence);

            _sequence.Run();

            Assert.AreEqual(2, count);
            AssertTime.Elapsed(0, _sequence);
        }

        [Test]
        public void Wait()
        {
            _sequence.Wait(1.5f).Wait(0.2f);

            AssertTime.Elapsed(0, _sequence);

            _sequence.Run();

            AssertTime.Elapsed(1.7f, _sequence);
        }

        [Test]
        public void WaitThenDo()
        {
            var count = 0;
            _sequence.WaitThenDo(1.5f, () => { count++; }).WaitThenDo(0.2f, () => { count++; });

            Assert.AreEqual(0, count);
            AssertTime.Elapsed(0, _sequence);

            _sequence.Run();

            Assert.AreEqual(2, count);
            AssertTime.Elapsed(1.7f, _sequence);
        }

        [Test]
        public void DoCoroutineThenDo()
        {
            var count = 0;
            var coroutine = CoWaitForSeconds(1.5f, () => { count++; });
            _sequence.DoCoroutine(coroutine);
            _sequence.Do(() => { count++; });

            _sequence.Run();

            Assert.AreEqual(2, count);
            AssertTime.Elapsed(1.5f, _sequence);
        }

        private IEnumerator CoWaitForSeconds(float seconds, Action callback)
        {
            var elapsed = 0f;
            while (elapsed < seconds)
            {
                elapsed += _sequence.DeltaTime;
                yield return null;
            }
            callback();
        }
    }
}
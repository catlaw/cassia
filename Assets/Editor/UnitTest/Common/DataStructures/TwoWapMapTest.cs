﻿using System;
using System.Collections.Generic;
using Common.DataStructures;
using NUnit.Framework;

namespace UnitTest.Common.DataStructures
{
	[TestFixture]
	public class TwoWapMapTest
	{
		private TwoWayMap<int, string> _twoWayMap;

		[SetUp]
		public void SetUp()
		{
			_twoWayMap = new TwoWayMap<int, string>();
		}

		[Test]
		public void GetForward()
		{
			_twoWayMap.Add(1, "one");

			Assert.AreEqual("one", _twoWayMap.GetForward(1));
		}

		[Test]
		public void GetBackward()
		{
			_twoWayMap.Add(1, "one");

			Assert.AreEqual(1, _twoWayMap.GetBackward("one"));
		}

		[Test]
		[ExpectedException(typeof(ArgumentException),
			ExpectedMessage = "Cannot add duplicate forward key '1'.")]
		public void AddDuplicateForwardKey()
		{
			_twoWayMap.Add(1, "one");
			_twoWayMap.Add(1, "One");	
		}

		[Test]
		[ExpectedException(typeof(ArgumentException),
			ExpectedMessage = "Cannot add duplicate backward key 'one'.")]
		public void AddDuplicateBackwardKey()
		{
			_twoWayMap.Add(1, "one");
			_twoWayMap.Add(2, "one");
		}

		[Test]
		[ExpectedException(typeof(KeyNotFoundException),
			ExpectedMessage = "Unknown forward key '1'.")]
		public void GetNonExistantForwardKey()
		{
			_twoWayMap.GetForward(1);
		}

		[Test]
		[ExpectedException(typeof(KeyNotFoundException),
			ExpectedMessage = "Unknown backward key 'one'.")]
		public void GetNonExistantBackwardKey()
		{
			_twoWayMap.GetBackward("one");
		}

		[Test]
		public void ContainsForward()
		{
			_twoWayMap.Add(1, "one");

			Assert.IsTrue(_twoWayMap.ContainsForward(1));
			Assert.IsFalse(_twoWayMap.ContainsForward(2));
		}

		[Test]
		public void ContainsBackward()
		{
			_twoWayMap.Add(1, "one");

			Assert.IsTrue(_twoWayMap.ContainsBackward("one"));
			Assert.IsFalse(_twoWayMap.ContainsBackward("two"));
		}
	}
}
﻿using System;
using System.Collections.Generic;
using Common.DataStructures;
using Newtonsoft.Json.Linq;
using NUnit.Framework;

namespace UnitTest.Common.DataStructures
{
	[TestFixture]
	public class DefaultableLibraryTest
	{
		[Test]
		public void LoadDictionaryAndGet()
		{
			var jToken = new JObject
			{
				{"Default", 1},
				{
					"Values", new JObject
					{
						{ "Two", 2 },
						{ "Three", 3 }
					}
				}
			};
			var library = new DefaultableLibrary<int>(jToken);

			Assert.AreEqual(1, library.GetById("One"));
			Assert.AreEqual(2, library.GetById("Two"));
			Assert.AreEqual(3, library.GetById("Three"));
		}

		[Test]
		[ExpectedException(typeof(ArgumentNullException))]
		public void CreateWithNullThrowsException()
		{
			var library = new DefaultableLibrary<int>(null);
		}
	}
}
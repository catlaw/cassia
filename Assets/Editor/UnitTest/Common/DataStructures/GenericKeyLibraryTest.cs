﻿using System;
using System.Collections.Generic;
using Common.DataStructures;
using Newtonsoft.Json.Linq;
using NUnit.Framework;

namespace UnitTest.Common.DataStructures
{
	[TestFixture]
	public class GenericKeyLibraryTest
	{
		private enum TestKey { One, Two }

		[Test]
		public void CreateFromJTokenAndGet()
		{
			var dictionary = new Dictionary<TestKey, string>
			{
				{TestKey.One, "One"},
				{TestKey.Two, "Two"}
			};
			var jToken = JToken.FromObject(dictionary);

			var library = Library<TestKey, string>.CreateFromJToken(jToken);

			Assert.AreEqual("One", library.GetById(TestKey.One));
			Assert.AreEqual("Two", library.GetById(TestKey.Two));
		}

		[Test]
		public void CreateEmpty()
		{
			var library = Library<TestKey, string>.CreateEmpty();
			CollectionAssert.IsEmpty(library);
		}

		[Test]
		[ExpectedException(typeof(ArgumentNullException))]
		public void CreateFromNullThrowsException()
		{
			Library<TestKey, string>.CreateFromJToken(null);
		}

		[Test]
		[ExpectedException(typeof(ArgumentException), ExpectedMessage = "Id 'Two' not found on 'String' Library.")]
		public void GetUnknownKeyThrowsException()
		{
			var library = Library<TestKey, string>.CreateFromJToken(new JObject());
			library.GetById(TestKey.Two);
		}

		[Test]
		[ExpectedException(typeof(ArgumentNullException))]
		public void GetWithNullIdThrowsException()
		{
			var library = Library<string>.CreateFromJToken(new JObject());
			library.GetById(null);
		}

		[Test]
		public void GetEnumerator()
		{
			var dictionary = new Dictionary<TestKey, string>
			{
				{ TestKey.One, "testValue0" },
				{ TestKey.Two, "testValue1" }
			};

			var library = Library<TestKey, string>.CreateFromJToken(JToken.FromObject(dictionary));

			CollectionAssert.AreEqual(new List<string> { "testValue0", "testValue1" }, library);
		}

		[Test]
		public void CreateFromJTokenAndGetKeyValuePairs()
		{
			var dictionary = new Dictionary<TestKey, string>
			{
				{TestKey.One, "One"},
				{TestKey.Two, "Two"}
			};
			var jToken = JToken.FromObject(dictionary);

			var library = Library<TestKey, string>.CreateFromJToken(jToken);

			CollectionAssert.AreEqual(dictionary, library.GetKeyValuePairs());
		}
	}
}
﻿using System;
using System.Collections.Generic;
using System.Reflection.Emit;
using Common.DataStructures;
using Newtonsoft.Json.Linq;
using NUnit.Framework;

namespace UnitTest.Common.DataStructures
{
	[TestFixture]
	public class LibraryTest
	{
		[Test]
		public void CreateFromJTokenAndGet()
		{
			var dictionary = new Dictionary<string, string>
			{
				{ "test", "testValue" }
			};
			var jToken = JToken.FromObject(dictionary);

			var library = Library<string>.CreateFromJToken(jToken);

			Assert.AreSame("testValue", library.GetById("test"));
		}

		[Test]
		public void CreateEmpty()
		{
			var library = Library<string>.CreateEmpty();
			CollectionAssert.IsEmpty(library);
		}

		[Test]
		[ExpectedException(typeof(ArgumentNullException))]
		public void CreateFromNullThrowsException()
		{
			Library<string>.CreateFromJToken(null);
		}

		[Test]
		[ExpectedException(typeof(ArgumentException), ExpectedMessage = "Id 'unknown' not found on 'String' Library.")]
		public void GetUnknownKeyThrowsException()
		{
			var library = Library<string>.CreateFromJToken(new JObject());
			library.GetById("unknown");
		}

		[Test]
		[ExpectedException(typeof(ArgumentNullException))]
		public void GetWithNullIdThrowsException()
		{
			var library = Library<string>.CreateFromJToken(new JObject());
			library.GetById(null);
		}

		[Test]
		public void GetEnumerator()
		{
			var dictionary = new Dictionary<string, string>
			{
				{ "test0", "testValue0" },
				{ "test1", "testValue1" },
				{ "test2", "testValue2" },
			};

			var library = Library<string>.CreateFromJToken(JToken.FromObject(dictionary));
			
			CollectionAssert.AreEqual(new List<string> {"testValue0", "testValue1", "testValue2"}, library);
		}

		[Test]
		public void ContainsId()
		{
			var dictionary = new Dictionary<string, string>
			{
				{ "test", "testValue" }
			};
			var jToken = JToken.FromObject(dictionary);

			var library = Library<string>.CreateFromJToken(jToken);

			Assert.IsTrue(library.ContainsId("test"));
			Assert.IsFalse(library.ContainsId("notest"));
		}

		[Test]
		public void CreateFromJTokenAndGetKeyValuePairs()
		{
			var dictionary = new Dictionary<string, string>
			{
				{ "test", "testValue" }
			};
			var jToken = JToken.FromObject(dictionary);

			var library = Library<string>.CreateFromJToken(jToken);

			CollectionAssert.AreEqual(dictionary, library.GetKeyValuePairs());
		}
	}
}
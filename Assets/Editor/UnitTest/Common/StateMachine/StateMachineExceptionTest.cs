using System;
using Common.StateMachine;
using NUnit.Framework;
using strange.extensions.signal.impl;

namespace UnitTest.Common.StateMachine
{
	[TestFixture]
	public class StateMachineExceptionTest
	{
		private enum TestState { Stopped, Walking, Running }

		[Test]
		[ExpectedException(typeof(ArgumentException), ExpectedMessage = "Duplicate transition defined for 'Stopped'.")]
		public void DuplicateRuleDefined()
		{
			var stateMachine = new StateMachine<TestState>();
			stateMachine.Init(TestState.Stopped);

			var speedUpSignal = new Signal();
			stateMachine.In(TestState.Stopped).On(speedUpSignal).GoTo(TestState.Walking);
			stateMachine.In(TestState.Stopped).On(speedUpSignal).GoTo(TestState.Running);
			stateMachine.Start();
		}

		[Test]
		[ExpectedException(typeof(InvalidOperationException), ExpectedMessage = "Cannot add rule after state machine has started.")]
		public void AddRuleAfterStart()
		{
			var stateMachine = new StateMachine<TestState>();
			stateMachine.Init(TestState.Stopped);

			stateMachine.Start();

			stateMachine.In(TestState.Stopped).On(new Signal()).GoTo(TestState.Walking);
		}
	}
}
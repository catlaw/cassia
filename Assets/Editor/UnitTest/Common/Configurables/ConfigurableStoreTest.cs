﻿using System;
using System.Collections.Generic;
using Common.Configurables;
using Common.DataStructures;
using Common.Exceptions;
using Configs;
using NSubstitute;
using NUnit.Framework;

namespace UnitTest.Common.Configurables
{
	[TestFixture]
	public class ConfigurableStoreTest
	{
		private IConfigurableFactory<ITestConfigurable, TestConfig> _factory;
		private ConfigurableStore<ITestConfigurable, TestConfig> _store;

		public interface ITestConfigurable : IConfigurable<TestConfig> { }

		public class TestConfig { }


		[SetUp]
		public void SetUp()
		{
			_factory = Substitute.For<IConfigurableFactory<ITestConfigurable, TestConfig>>();
			_store = new ConfigurableStore<ITestConfigurable, TestConfig>(_factory);
		}

		[Test]
		public void LoadAndGetConfigs()
		{
			var configs = Substitute.For<ILibrary<TestConfig>>();
			var testConfigs = new Dictionary<string, TestConfig>
			{
				{ "One", new TestConfig() },
				{ "Two", new TestConfig() }
			};
			configs.GetKeyValuePairs().Returns(testConfigs);

			var expectedFirstConfigurable = Substitute.For<ITestConfigurable>();
			_factory.GetConfiguredInstance(testConfigs["One"]).Returns(expectedFirstConfigurable);

			var expectedSecondConfigurable = Substitute.For<ITestConfigurable>();
			_factory.GetConfiguredInstance(testConfigs["Two"]).Returns(expectedSecondConfigurable);

			_store.LoadConfigs(configs);

			Assert.AreSame(expectedFirstConfigurable, _store.GetById("One"));
			Assert.AreSame(expectedSecondConfigurable, _store.GetById("Two"));
		}


		[Test]
		[ExpectedException(typeof(InvalidOperationException),
			ExpectedMessage = "Cannot get before loading.")]
		public void GetBeforeLoadingThrowsException()
		{
			_store.GetById("unknown");
		}

		[Test]
		[ExpectedException(typeof(ArgumentException), 
			ExpectedMessage = "Could not get ITestConfigurable with id 'unknown'.")]
		public void GetNonExistantIdThrowsException()
		{
			var configs = Substitute.For<ILibrary<TestConfig>>();
			configs.ContainsId("unknown").Returns(false);
			_store.LoadConfigs(configs);
			_store.GetById("unknown");
		}


		[Test]
		[ExpectedException(typeof(ArgumentNullException))]
		public void LoadNullConfigs()
		{
			_store.LoadConfigs(null);
		}

		[Test]
		[ExpectedException(typeof(ArgumentNullException))]
		public void GetWithNullId()
		{
			_store.LoadConfigs(Substitute.For<ILibrary<TestConfig>>());
			_store.GetById(null);
		}

		[Test]
		public void IsLoaded()
		{
			Assert.IsTrue(_store.RequiresLoading);
			_store.LoadConfigs(Substitute.For<ILibrary<TestConfig>>());
			Assert.IsFalse(_store.RequiresLoading);
		}
	}
}
﻿using System;
using Common.Binders;
using Common.Configurables;
using NSubstitute;
using NUnit.Framework;
using strange.framework.api;

namespace UnitTest.Common.Configurables
{
	[TestFixture]
	public class ConfigurableFactoryTest
	{
		private IInstanceProvider _instanceProvider;
		private ConfigurableFactory<ITestConfigurable, TestConfig> _factory;


		[SetUp]
		public void SetUp()
		{
			_instanceProvider = Substitute.For<IInstanceProvider>();
			_factory = new ConfigurableFactory<ITestConfigurable, TestConfig>(_instanceProvider);
		}

		public interface ITestConfigurable : IConfigurable<TestConfig> { }
		public class TestConfig { }

		[Test]
		public void GetConfiguredInstance()
		{
			var expectedConfigurable = Substitute.For<ITestConfigurable>();
			_instanceProvider.GetInstance<ITestConfigurable>().Returns(expectedConfigurable);

			var config = new TestConfig();
			var actualConfigurable = _factory.GetConfiguredInstance(config);

			expectedConfigurable.Received(1).LoadConfig(config);
			Assert.AreSame(expectedConfigurable, actualConfigurable);
		}

		[Test]
		[ExpectedException(typeof(ArgumentNullException))]
		public void GetConfiguredInstanceWithNullConfig()
		{
			_factory.GetConfiguredInstance(null);
		}
	}
}
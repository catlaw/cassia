﻿using System;
using System.Collections.Generic;
using Common.DataContainers;
using Configs;
using NUnit.Framework;
using UnityEngine.Networking;

namespace UnitTest.Common.DataContainers
{
    [TestFixture]
    public class GridCoordsTest
    {
        [Test]
        public void GetOffset()
        {
            var gridCoords = new GridCoords(1, 2);
            var gridCoordsOffset = new GridCoordsOffset(1, 2);

            var expected = new GridCoords(2, 4);
            var actual = gridCoords.GetOffset(gridCoordsOffset);
            Assert.AreEqual(expected, actual);
        }

	    [Test]
	    public void CreateFromList()
	    {
			var intList = new List<int> {3, 5};
		    var result = GridCoords.CreateFromList(intList);

		    Assert.AreEqual(new GridCoords(3, 5), result);
	    }

	    [Test]
		[ExpectedException(typeof(ArgumentException), 
			ExpectedMessage = "Can only create a GridCoords with a list of size 2.  List of size 1 was provided.")]
	    public void CreateFromInvalidSizeList()
	    {
			var intList = new List<int> { 3 };
			GridCoords.CreateFromList(intList);
		}

	    [Test]
		[ExpectedException(typeof(ArgumentNullException))]
	    public void CreateFromNull()
	    {
		    GridCoords.CreateFromList(null);
	    }
    }
}

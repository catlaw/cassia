﻿using Common.DataContainers;
using NUnit.Framework;

namespace UnitTest.Common.DataContainers
{
    [TestFixture]
    public class IntVector3Test
    {
        [Test]
        public void Addition()
        {
            var first = new IntVector3(0, 3, 2);
            var second = new IntVector3(1, -1, 4);

            var expected = new IntVector3(1, 2, 6);
            var actual = first + second;

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void ConvertToString()
        {
            var vector = new IntVector3(1, 2, 3);

            var expected = "IntVector3(1, 2, 3)";
            var actual = vector.ToString();

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void Equality()
        {
            Assert.AreEqual(new IntVector3(1, 2, 3), new IntVector3(1, 2, 3));
            Assert.That(new IntVector3(1, 2, 3).Equals(new IntVector3(1, 2, 3)));
        }
    }
}

﻿using System;
using System.Collections.Generic;
using Common.DataContainers;
using NUnit.Framework;

namespace UnitTest.Common.DataContainers
{
	[TestFixture]
	public class FacingTest
	{
		[Test]
		public void CreateForwardFromList()
		{
			var intList = new List<int> {5, 7};
			var facing = Facing.CreateFromList(intList);

			Assert.AreEqual(Facing.Forward, facing);
		}

		[Test]
		public void CreateBackFromList()
		{
			var intList = new List<int> { 5, -7 };
			var facing = Facing.CreateFromList(intList);

			Assert.AreEqual(Facing.Back, facing);
		}

		[Test]
		public void CreateRightFromList()
		{
			var intList = new List<int> { 5, 1 };
			var facing = Facing.CreateFromList(intList);

			Assert.AreEqual(Facing.Right, facing);
		}

		[Test]
		public void CreateLeftFromList()
		{
			var intList = new List<int> { -2, 1 };
			var facing = Facing.CreateFromList(intList);

			Assert.AreEqual(Facing.Left, facing);
		}

		[Test]
		public void FacingPriorities()
		{
			var forward = Facing.CreateFromList(new List<int> {1, 1});
			var back = Facing.CreateFromList(new List<int> {1, -1});
			var left = Facing.CreateFromList(new List<int> {-1, 0});
			var right = Facing.CreateFromList(new List<int> {1, 0});

			Assert.AreEqual(Facing.Forward, forward);
			Assert.AreEqual(Facing.Back, back);
			Assert.AreEqual(Facing.Left, left);
			Assert.AreEqual(Facing.Right, right);
		}

		[Test]
		[ExpectedException(typeof(ArgumentException), 
			ExpectedMessage = "Can only create a Facing with a list of size 2.  List of size 3 was provided.")]
		public void InvalidListSize()
		{
			var intList = new List<int> {1, 2, 3};
			Facing.CreateFromList(intList);
		}

		[Test]
		[ExpectedException(typeof(ArgumentNullException))]
		public void NullList()
		{
			Facing.CreateFromList(null);
		}

		[Test]
		public void CreateFromDirection()
		{
			var forward = Facing.CreateFromDirection(Direction.Forward);
			var back = Facing.CreateFromDirection(Direction.Back);
			var right = Facing.CreateFromDirection(Direction.Right);
			var left = Facing.CreateFromDirection(Direction.Left);

			Assert.AreEqual(Facing.Forward, forward);
			Assert.AreEqual(Facing.Back, back);
			Assert.AreEqual(Facing.Left, left);
			Assert.AreEqual(Facing.Right, right);
		}
	}
}
﻿using System.Collections.Generic;
using Configs;
using Instantiator.Stage.Models.MeshFactory;
using NSubstitute;
using NUnit.Framework;
using UnityEngine;

namespace UnitTest.Instantiator.Stage.Models.MeshFactory
{
	[TestFixture]
	public class LevelMeshFactoryTest
	{
		private IConfig<LevelConfig> _levelConfig;
		private IConfig<MapViewConfig> _mapViewConfig;
		private LevelMeshFactory _meshFactory;

		[SetUp]
		public void SetUp()
		{
			_levelConfig = Substitute.For<IConfig<LevelConfig>>();
			_mapViewConfig = Substitute.For<IConfig<MapViewConfig>>();
			_meshFactory = new LevelMeshFactory(_mapViewConfig, _levelConfig);
		}

		[Test]
		public void CreateChunk()
		{
            var mapViewConfig = new MapViewConfig
		    {
				ChunkSize = 1,
				ChunkHeight = 0.2f,
		        Squares = new[,]
		        {
		            {
		                new SquareViewConfig
		                {
		                    TopTextures = new string[] {"grass"},
		                    RightTextures = new string[] {"dirt"},
		                    FrontTextures = new string[] {"dirt"}
		                }
		            }
		        },
				TextureTiles = new Dictionary<string, List<Vector2>>
				{
					{
						"dirt",
						new List<Vector2>
						{
							new Vector2(0, 0),
							new Vector2(0, 1),
							new Vector2(0, 2),
							new Vector2(0, 3)
						}
					},

					{
						"grass",
						new List<Vector2>
						{
							new Vector2(1, 0),
							new Vector2(1, 1),
							new Vector2(1, 2),
							new Vector2(1, 3)
						}
					}
				}
			};
			_mapViewConfig.Get().Returns(mapViewConfig);

			var levelConfig = new LevelConfig
			{
				MapHeights = new[,] { {1} }
			};
			_levelConfig.Get().Returns(levelConfig);

			var mesh = _meshFactory.CreateMesh();

			Assert.AreEqual(12, mesh.vertices.Length);
            CollectionAssert.AreEqual(new Vector3[]
            {
                 new Vector3(0.5f, 0.2f, -0.5f),
                 new Vector3(-0.5f, 0.2f, -0.5f),
                 new Vector3(-0.5f, 0.2f, 0.5f),
                 new Vector3(0.5f, 0.2f, 0.5f),

                 new Vector3(0.5f, 0.2f, -0.5f),
                 new Vector3(0.5f, 0.2f, 0.5f),
                 new Vector3(0.5f, 0f, 0.5f),
                 new Vector3(0.5f, 0f, -0.5f),

                 new Vector3(0.5f, 0.2f, 0.5f),
                 new Vector3(-0.5f, 0.2f, 0.5f),
                 new Vector3(-0.5f, 0f, 0.5f),
                 new Vector3(0.5f, 0f, 0.5f)
            }, mesh.vertices);

            CollectionAssert.AreEqual(new int[]
            {
                0, 1, 2, 2, 3, 0,
                4, 5, 6, 6, 7, 4,
                8, 9, 10, 10, 11, 8
            }, mesh.triangles);

            CollectionAssert.AreEqual(new Vector2[]
            {
                new Vector2(1, 0),
                new Vector2(1, 1),
                new Vector2(1, 2),
                new Vector2(1, 3),

                new Vector2(0, 0),
                new Vector2(0, 1),
                new Vector2(0, 2),
                new Vector2(0, 3),

                new Vector2(0, 0),
                new Vector2(0, 1),
                new Vector2(0, 2),
                new Vector2(0, 3)

            }, mesh.uv);
		}

	    [Test]
	    public void CreateTwoChunks()
	    {
            var mapViewConfig = new MapViewConfig
	        {
				ChunkSize = 1,
				ChunkHeight = 0.2f,
				Squares = new[,]
	            {
	                {
	                    new SquareViewConfig
	                    {
	                        TopTextures = new string[] {"grass"},
	                        RightTextures = new string[] {"dirt", "dirt"},
	                        FrontTextures = new string[] {"dirt", "dirt"}
	                    }
	                }
	            },
				TextureTiles = new Dictionary<string, List<Vector2>>
				{
					{
						"dirt",
						new List<Vector2>
						{
							new Vector2(0, 0),
							new Vector2(0, 1),
							new Vector2(0, 2),
							new Vector2(0, 3)
						}
					},

					{
						"grass",
						new List<Vector2>
						{
							new Vector2(1, 0),
							new Vector2(1, 1),
							new Vector2(1, 2),
							new Vector2(1, 3)
						}
					}
				}
			};
		    _mapViewConfig.Get().Returns(mapViewConfig);

			var levelConfig = new LevelConfig
			{
				MapHeights = new[,] { { 2 } }
			};
			_levelConfig.Get().Returns(levelConfig);

			var mesh = _meshFactory.CreateMesh();

            Assert.AreEqual(20, mesh.vertices.Length);
            CollectionAssert.AreEqual(new Vector3[]
            {
                 // 0-top
                 new Vector3(0.5f, 0.4f, -0.5f),
                 new Vector3(-0.5f, 0.4f, -0.5f),
                 new Vector3(-0.5f, 0.4f, 0.5f),
                 new Vector3(0.5f, 0.4f, 0.5f),

                 // 0-right
                 new Vector3(0.5f, 0.4f, -0.5f),
                 new Vector3(0.5f, 0.4f, 0.5f),
                 new Vector3(0.5f, 0.2f, 0.5f),
                 new Vector3(0.5f, 0.2f, -0.5f), 

                 // 1-front
                 new Vector3(0.5f, 0.4f, 0.5f),
                 new Vector3(-0.5f, 0.4f, 0.5f),
                 new Vector3(-0.5f, 0.2f, 0.5f),
                 new Vector3(0.5f, 0.2f, 0.5f),
                 
                 // 1-right
                 new Vector3(0.5f, 0.2f, -0.5f),
                 new Vector3(0.5f, 0.2f, 0.5f),
                 new Vector3(0.5f, 0f, 0.5f),
                 new Vector3(0.5f, 0f, -0.5f),

                 // 1-front
                 new Vector3(0.5f, 0.2f, 0.5f),
                 new Vector3(-0.5f, 0.2f, 0.5f),
                 new Vector3(-0.5f, 0f, 0.5f),
                 new Vector3(0.5f, 0f, 0.5f),
            }, mesh.vertices);
            CollectionAssert.AreEqual(new int[]
            {
                0, 1, 2, 2, 3, 0,
                4, 5, 6, 6, 7, 4,
                8, 9, 10, 10, 11, 8,
                12, 13, 14, 14, 15, 12,
                16, 17, 18, 18, 19, 16
            }, mesh.triangles);
	    }
	}
}

﻿using System;
using System.Collections.Generic;
using Common.DataContainers;
using Configs;
using Instantiator.Stage.Models.MeshModel;
using NUnit.Framework;
using UnityEngine;

namespace UnitTest.Instantiator.Stage.Models.MeshModel
{
	[TestFixture]
	public class ChunkFaceFactoryTest
	{
		private ChunkFaceFactory _factory;
	    private MapViewConfig _mapTextureConfig;

		[SetUp]
		public void SetUp()
		{
		    _mapTextureConfig = new MapViewConfig
			{
		        TextureTiles = new Dictionary<string, List<Vector2>>
		        {
                    {
                        "test",
                        new List<Vector2> { new Vector2(0, 0), new Vector2(1, 1), new Vector2(2, 2), new Vector2(3, 3) }
                    }
		        }
		    };

			_factory = ChunkFaceFactory.Create(_mapTextureConfig);
        }

		[Test]
		public void CreateTopFaceWithUvs()
		{
			var face = _factory.CreateTopFaceAtPosition(new IntVector3(0, 0, 0), "test");

		    var expected = new List<IntVector3>
		    {
                new IntVector3(1, 1, 0),
                new IntVector3(0, 1, 0),
                new IntVector3(0, 1, 1),
                new IntVector3(1, 1, 1),
            };
		    var actual = face.GetCornerPositions();

            CollectionAssert.AreEqual(expected, actual);
            CollectionAssert.AreEqual(_mapTextureConfig.TextureTiles["test"], face.GetUv());
		}

		[Test]
		public void CreateRightFaceWithUvs()
		{
			var face = _factory.CreateRightFaceAtPosition(new IntVector3(0, 0, 0), "test");

            var expected = new List<IntVector3>
            {
                new IntVector3(1, 1, 0),
                new IntVector3(1, 1, 1),
                new IntVector3(1, 0, 1),
                new IntVector3(1, 0, 0),
            };
            var actual = face.GetCornerPositions();

            CollectionAssert.AreEqual(expected, actual);
            CollectionAssert.AreEqual(_mapTextureConfig.TextureTiles["test"], face.GetUv());
        }

		[Test]
		public void CreateFrontFace()
		{
			var face = _factory.CreateFrontFaceAtPosition(new IntVector3(0, 0, 0), "test");

            var expected = new List<IntVector3>
            {
                new IntVector3(1, 1, 1),
                new IntVector3(0, 1, 1),
                new IntVector3(0, 0, 1),
                new IntVector3(1, 0, 1),
            };
            var actual = face.GetCornerPositions();

            CollectionAssert.AreEqual(expected, actual);
            CollectionAssert.AreEqual(_mapTextureConfig.TextureTiles["test"], face.GetUv());
        }

	    [Test]
        [ExpectedException(typeof(ArgumentException), 
            ExpectedMessage = "Could not find texture tile name 'nothing'.")]
	    public void CreateWithNonExistantTextureTile()
	    {
            _factory.CreateTopFaceAtPosition(new IntVector3(0, 0, 0), "nothing");
        }

        [Test]
	    public void CreateWithNullTextureTile()
	    {
			var face =  _factory.CreateTopFaceAtPosition(new IntVector3(0, 0, 0), null);
	        var expected = new List<Vector2> {Vector2.zero, Vector2.zero, Vector2.zero, Vector2.zero};
	        var actual = face.GetUv();

			CollectionAssert.AreEqual(expected, actual);
	    }
	}
}

using System;
using System.Collections.Generic;
using Common.DataContainers;
using Instantiator.Stage.Models.MeshModel;
using NUnit.Framework;
using UnityEngine;

namespace UnitTest.Instantiator.Stage.Models.MeshModel
{
	[TestFixture]
	public class ChunkFaceTest
	{
		[Test]
		public void Create()
		{
			IList<IntVector3> corners = new List<IntVector3>
			{
				new IntVector3(0, 0, 0),
                new IntVector3(1, 1, 1),
                new IntVector3(2, 2, 2),
                new IntVector3(3, 3, 3)
            };

            IList<Vector2> uv = new List<Vector2>
            {
                new Vector2(0, 0),
                new Vector2(1, 1),
                new Vector2(2, 2),
                new Vector2(2, 2)
            };

			var face = ChunkFace.CreateWithCornersAndUv(corners, uv);

		    CollectionAssert.AreEqual(corners, face.GetCornerPositions());
            CollectionAssert.AreEqual(uv, face.GetUv());
		}

		private static IList<IntVector3> CreateFakeCorners()
		{
			return new List<IntVector3>
				{
                    new IntVector3(0, 0, 0),
                    new IntVector3(0, 0, 0),
                    new IntVector3(0, 0, 0),
                    new IntVector3(0, 0, 0)
                };
		}

	    private static IList<Vector2> CreateFakeUv()
	    {
	        return new List<Vector2>
	        {
                new Vector2(0, 0),
                new Vector2(0, 0),
                new Vector2(0, 0),
                new Vector2(0, 0)
	        };
	    }

        [Test]
        [ExpectedException(typeof(ArgumentException),
            ExpectedMessage = "Expected a list of 4 corners, but got 3 instead.")]
        public void CreateWithWrongCornerSizeList()
        {
            var corners = new List<IntVector3>
            {
                new IntVector3(0, 0, 0),
                new IntVector3(1, 1, 1),
                new IntVector3(2, 2, 2)
            };
            ChunkFace.CreateWithCornersAndUv(corners, CreateFakeUv());
        }

        [Test]
        [ExpectedException(typeof(ArgumentException), 
            ExpectedMessage = "Expected a list of 4 uvs, but got 2 instead.")]
	    public void CreateWithWrongUvSizeList()
        {
            var uv = new List<Vector2>
            {
                new Vector2(0, 0),
                new Vector2(1, 1)
            };
            ChunkFace.CreateWithCornersAndUv(CreateFakeCorners(), uv);
        }

		[Test]
		[ExpectedException(typeof(ArgumentNullException))]
		public void CreateWithNullCornersANdUv()
		{
			ChunkFace.CreateWithCornersAndUv(null, null);
		}

	    [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void CreateWithNullUv()
	    {
            var face = ChunkFace.CreateWithCornersAndUv(CreateFakeCorners(), null);
	        var defaultUv = new List<Vector2>
	        {
                new Vector2(0, 0),
                new Vector2(0, 0),
                new Vector2(0, 0),
                new Vector2(0, 0)
            };

            CollectionAssert.AreEqual(defaultUv, face.GetUv());
        }
	}
}
﻿using System;
using Common.DataContainers;
using Instantiator.Stage.Models.HeightMap;
using NUnit.Framework;

namespace UnitTest.Instantiator.Stage.Models.HeightMap
{
    [TestFixture]
    public class HeightMapSquareTest
    {
        [Test]
        public void CreateAndGetTiles()
        {
            var square = HeightMapSquare.CreateAtPosition(1, 2);
            square.CreateTilesForHeight(1);

	        Assert.AreEqual(2, square.TileCount);
            Assert.AreEqual(new IntVector3(1, 0, 2), square.GetTile(0).LeftBack);
            Assert.AreEqual(new IntVector3(1, 1, 2), square.GetTile(1).LeftBack);
        }

	    [Test]
	    public void CreateTileWith0Height()
	    {
			var square = HeightMapSquare.CreateAtPosition(1, 2);
			square.CreateTilesForHeight(0);

			Assert.AreEqual(0, square.TileCount);
		}

        [Test]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void GetOutOfRangeTile()
        {
            var square = HeightMapSquare.CreateAtPosition(0, 0);
            square.CreateTilesForHeight(1);
            square.GetTile(2);
        }

        [Test]
        public void GetTileCount()
        {
            var square = HeightMapSquare.CreateAtPosition(0, 0);
            square.CreateTilesForHeight(5);

            Assert.AreEqual(6, square.TileCount);
        }

	    [Test]
	    public void GetHeight()
	    {
		    var square = HeightMapSquare.CreateAtPosition(0, 0);
		    square.CreateTilesForHeight(5);

			Assert.AreEqual(5, square.Height);
	    }

	    [Test]
	    public void Get0Height()
	    {
			var square = HeightMapSquare.CreateAtPosition(0, 0);
			square.CreateTilesForHeight(0);

			Assert.AreEqual(0, square.Height);
		}
    }
}

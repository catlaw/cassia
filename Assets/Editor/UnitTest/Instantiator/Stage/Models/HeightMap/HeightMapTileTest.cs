﻿using Common.DataContainers;
using Instantiator.Stage.Models.HeightMap;
using NUnit.Framework;

namespace UnitTest.Instantiator.Stage.Models.HeightMap
{
    [TestFixture]
    public class HeightMapTileTest
    {
        [Test]
        public void Create()
        {
            IntVector3 leftBackPoint = new IntVector3(2, 4, 1);
            var tile = HeightMapTile.CreateWithLeftBackPoint(leftBackPoint);

            Assert.AreEqual(new IntVector3(2, 4, 1), tile.LeftBack);
            Assert.AreEqual(new IntVector3(3, 4, 1), tile.RightBack);
            Assert.AreEqual(new IntVector3(2, 4, 2), tile.LeftFront);
            Assert.AreEqual(new IntVector3(3, 4, 2), tile.RightFront);
        }
    }
}

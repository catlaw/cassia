﻿using System;
using Instantiator;
using NSubstitute;
using NUnit.Framework;
using UnitTest.Utils.TestFactories;
using UnityEngine;

namespace UnitTest.Instantiator
{
    [TestFixture]
    public class ResourcesOnlyGameObjectFactoryTest
    {
        private TemporaryGameObjectFactory _goFactory;
        private IResourceLoader _resourceLoader;
        private IGameObjectInstantiator _gameObjectInstantiator;
        private ResourcesOnlyGameObjectFactory _factory;

        [SetUp]
        public void SetUp()
        {
            _goFactory = new TemporaryGameObjectFactory();
            _resourceLoader = Substitute.For<IResourceLoader>();
            _gameObjectInstantiator = Substitute.For<IGameObjectInstantiator>();
            _factory = new ResourcesOnlyGameObjectFactory(_resourceLoader,
                _gameObjectInstantiator);
        }

        [TearDown]
        public void TearDown()
        {
            _goFactory.DestroyAll();
        }

        [Test]
        public void Instantiate()
        {
            var asset = Substitute.For<IAsset>();
            var prefab = _goFactory.CreateGameObject();
            _resourceLoader.Load<GameObject>(asset.Path).Returns(prefab);

            var expectedGameObject = _goFactory.CreateGameObject();
            _gameObjectInstantiator.InstantiatePrefab(prefab).Returns(expectedGameObject);

            var actualGameObject = _factory.Instantiate(asset);

            Assert.AreSame(expectedGameObject, actualGameObject);
        }

        [Test]
        public void Destroy()
        {
            var gameObject = _goFactory.CreateGameObject();
            _factory.Destroy(gameObject);

            _gameObjectInstantiator.Received(1).Destroy(gameObject);
        }

        [Test]
        public void NullArgumentsThrowsExceptions()
        {
            Assert.Throws<ArgumentNullException>(() =>
            {
                _factory.Instantiate(null);
            });

            Assert.Throws<ArgumentNullException>(() =>
            {
                _factory.Destroy(null);
            });
        }
    }
}
﻿using System;
using Instantiator;
using Instantiator.GameObjectPools;
using NSubstitute;
using NUnit.Framework;
using UnitTest.Utils.TestFactories;

namespace UnitTest.Instantiator.GameObjectPools
{
    [TestFixture]
    public class LazyGameObjectPoolTest
    {
        private TemporaryGameObjectFactory _goFactory;
        private IGameObjectFactory _gameObjectFactory;
        private LazyGameObjectPool _pool;

        [SetUp]
        public void SetUp()
        {
            _goFactory = new TemporaryGameObjectFactory();
            _gameObjectFactory = Substitute.For<IGameObjectFactory>();
            _pool = new LazyGameObjectPool(_gameObjectFactory);
        }

        [TearDown]
        public void TearDown()
        {
            _goFactory.DestroyAll();
        }

        private IAsset GetAssetInstance(string bundle, string path)
        {
            var asset = Substitute.For<IAsset>();
            asset.Bundle.Returns(bundle);
            asset.Path.Returns(path);
            return asset;
        }

        [Test]
        public void GetPooled()
        {
            var asset0 = GetAssetInstance("TestBundle", "TestPath");
            var asset1 = GetAssetInstance("TestBundle", "TestPath");

            var expectedGameObject0 = _goFactory.CreateGameObject();
            var expectedGameObject1 = _goFactory.CreateGameObject();

            _gameObjectFactory.Instantiate(asset0).Returns(expectedGameObject0);
            _gameObjectFactory.Instantiate(asset1).Returns(expectedGameObject1);

            var actualGameObject0 = _pool.GetPooled(asset0);
            var actualGameObject1 = _pool.GetPooled(asset1);

            Assert.AreSame(expectedGameObject0, actualGameObject0);
            Assert.AreSame(expectedGameObject1, actualGameObject1);
        }

        [Test]
        public void GetPooledFromReturned()
        {
            var asset0 = GetAssetInstance("TestBundle", "TestPath");
            var asset1 = GetAssetInstance("TestBundle", "TestPath");

            var expectedGameObject = _goFactory.CreateGameObject();

            _gameObjectFactory.Instantiate(asset0).Returns(expectedGameObject);

            var actualGameObject0 = _pool.GetPooled(asset0);
            _pool.ReturnToPool(actualGameObject0);
            var actualGameObject1 = _pool.GetPooled(asset1);

            Assert.AreSame(expectedGameObject, actualGameObject0);
            Assert.AreSame(expectedGameObject, actualGameObject1);
        }

        [Test]
        public void ReturnToPool()
        {
            var asset = GetAssetInstance("TestBundle", "TestPath");
            var expectedGameObject = _goFactory.CreateGameObject();

            _gameObjectFactory.Instantiate(asset).Returns(expectedGameObject);

            var gameObject = _pool.GetPooled(asset);
            Assert.IsTrue(gameObject.activeSelf);

            _pool.ReturnToPool(gameObject);
            Assert.IsFalse(gameObject.activeSelf);

            _pool.GetPooled(asset);
            Assert.IsTrue(gameObject.activeSelf);
        }

        [Test]
        public void ReturnUnknownObjectToPoolThrowsException()
        {
            var gameObject = _goFactory.CreateGameObject();

            var exception = Assert.Throws<ArgumentException>(() =>
            {
                _pool.ReturnToPool(gameObject);
            });

            Assert.AreEqual("Cannot return unborrowed GameObject to pool.", exception.Message);
        }

        [Test]
        public void GetPooledNullArgumentThrowsException()
        {
            Assert.Throws<ArgumentNullException>(() =>
            {
                _pool.GetPooled(null);
            });
        }

        [Test]
        public void ReturnToPoolNullArgumentsThrowsException()
        {
            Assert.Throws<ArgumentNullException>(() =>
            {
                _pool.ReturnToPool(null);
            });
        }
    }
}
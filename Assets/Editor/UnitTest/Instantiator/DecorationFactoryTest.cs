﻿using Configs;
using Game.ConfigActions.GameObjects;
using Instantiator;
using NSubstitute;
using NUnit.Framework;
using UnitTest.Utils.TestFactories;
using UnityEngine;

namespace UnitTest.Instantiator
{
	[TestFixture]
	public class DecorationFactoryTest
	{
		private TemporaryGameObjectFactory _goFactory;
		private DecorationFactory _factory;

		[SetUp]
		public void SetUp()
		{
			_factory = new DecorationFactory();
		}

		[Test]
		public void CreateDecoration()
		{
			var config = new DecorationConfig
			{
				Asset = new AssetConfig(),
				Position = new Vector3(1, 2, 3),
				Rotation = new Vector3(2, 3, 4)
			};

			var decoration = _factory.CreateDecoration(config);

			Assert.AreSame(config.Asset, decoration.Asset);
			Assert.AreEqual(new Vector3(1, 2, 3), decoration.Position);
			Assert.AreEqual(new Vector3(2, 3, 4), decoration.Rotation);
		}

		// TODO create with null config
	}
}
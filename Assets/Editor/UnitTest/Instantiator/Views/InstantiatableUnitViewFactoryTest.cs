﻿using System.Collections.Generic;
using Common.Configurables;
using Common.DataContainers;
using Configs;
using Game.Models.Occupants.Units;
using Instantiator.Units.Views;
using NSubstitute;
using NUnit.Framework;

namespace UnitTest.Instantiator.Views
{
    [TestFixture]
    public class InstantiatableUnitViewFactoryTest
    {
        private IConfigurableFactory<IUnit, UnitConfig> _unitFactory;
        private IUnitViewDataFactory _unitViewDataFactory;
        private InstantiatableUnitViewFactory _factory;

        [SetUp]
        public void SetUp()
        {
            _unitFactory = Substitute.For<IConfigurableFactory<IUnit, UnitConfig>>();
            _unitViewDataFactory = Substitute.For<IUnitViewDataFactory>();

            _factory = new InstantiatableUnitViewFactory(_unitFactory, _unitViewDataFactory);
        }

        [Test]
        public void BuildUnit()
        {
            var levelUnitConfig = new LevelUnitConfig
            {
                Unit = new UnitConfig
                {
                    Name = "Test",
                    Stats = new UnitStatsConfig
                    {
                        new StatConfig {StatType = StatType.Health, Value = 1},
                        new StatConfig {StatType = StatType.Power, Value = 2}
                    },
                    EquippedActionIds = new List<string> {"TestAction"}
                },
                StartPosition = new GridPosition(new GridCoords(1, 2), Facing.Left)
            };

            var expectedUnit = Substitute.For<IUnit>();
            _unitFactory.GetConfiguredInstance(levelUnitConfig.Unit).Returns(expectedUnit);

            var unitViewConfig = new UnitViewConfig();
            var expectedUnitViewData = Substitute.For<IUnitViewData>();
            _unitViewDataFactory.CreateUnitViewData(unitViewConfig).Returns(expectedUnitViewData);

            var buildableUnitView = _factory.Build(levelUnitConfig, unitViewConfig);

            Assert.AreEqual(expectedUnit, buildableUnitView.Unit);
            Assert.AreSame(expectedUnitViewData, buildableUnitView.Data);
            Assert.AreEqual(new GridPosition(new GridCoords(1, 2), Facing.Left), buildableUnitView.StartPosition);
        }
    }
}

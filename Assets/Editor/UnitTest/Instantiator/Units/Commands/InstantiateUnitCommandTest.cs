﻿using Common.DataContainers;
using Configs;
using Game.Models.Occupants.Units;
using Game.Models.Stage;
using Instantiator.Stage.Signals.ViewSignals;
using Instantiator.Units;
using Instantiator.Units.Commands;
using Instantiator.Units.Views;
using NSubstitute;
using NUnit.Framework;

namespace UnitTest.Instantiator.Units.Commands
{
	[TestFixture]
	public class InstantiateUnitCommandTest
	{
		private IMap _map;
		private IInstantiatableUnitViewFactory _instantiatableUnitViewFactory;
		private IInstantiateUnitViewSignal _instantiatableUnitViewSignal;
	    private IUnitViewDataFactory _unitViewDataFactory;
		private IUnitViewMap _unitViewMap;
		private InstantiateUnitCommand _command;

		[SetUp]
		public void SetUp()
		{
			_map = Substitute.For<IMap>();
			_instantiatableUnitViewFactory = Substitute.For<IInstantiatableUnitViewFactory>();
			_instantiatableUnitViewSignal = Substitute.For<IInstantiateUnitViewSignal>();
		    _unitViewDataFactory = Substitute.For<IUnitViewDataFactory>();
			_unitViewMap = Substitute.For<IUnitViewMap>();
		}

		[Test]
		public void BuildUnits()
		{
		    var levelUnitConfig = new LevelUnitConfig
		    {
                StartPosition = new GridPosition(new GridCoords(1, 0), Facing.Back)
		    };
		    var unitViewConfig = new UnitViewConfig();

		    var unitViewData = Substitute.For<IUnitViewData>();
		    _unitViewDataFactory.CreateUnitViewData(unitViewConfig).Returns(unitViewData);

            var buildableUnitView = Substitute.For<IInstantiatableUnitView>();
			var builtUnit = Substitute.For<IUnit>();
            buildableUnitView.Unit.Returns(builtUnit);

			buildableUnitView.StartPosition.Returns(levelUnitConfig.StartPosition);
			_instantiatableUnitViewFactory.Build(levelUnitConfig, unitViewConfig).Returns(buildableUnitView);

			_command = new InstantiateUnitCommand(_map, _instantiatableUnitViewFactory, _unitViewDataFactory,
                _instantiatableUnitViewSignal, _unitViewMap, levelUnitConfig, unitViewConfig);

			_command.Execute();

			builtUnit.Received(1).Facing = Facing.Back;
			_map.Received(1).SetOccupant(buildableUnitView.Unit, new GridCoords(1, 0));
			_instantiatableUnitViewSignal.Received().Dispatch(buildableUnitView, new GridCoords(1, 0));
			_unitViewMap.Received(1).Add(builtUnit, unitViewData);
		}
	}
}

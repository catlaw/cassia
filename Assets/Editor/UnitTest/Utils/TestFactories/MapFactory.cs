﻿using Configs;
using Game.Models.Stage;
using NSubstitute;

namespace UnitTest.Utils.TestFactories
{
	public class MapFactory
	{
		private MapFactory() { }

		public static MapFactory Create()
		{
			return new MapFactory();
		}

		public Map CreateMapWithHeights(int[,] heightMap)
		{
			var levelConfig = Substitute.For<IConfig<LevelConfig>>();
			levelConfig.Get().Returns(new LevelConfig
			{
				MapHeights = heightMap
			});
			return new Map(levelConfig);
		}
	}
}

﻿using System.Collections.Generic;
using UnityEngine;

namespace UnitTest.Utils.TestFactories
{
	public class TemporaryGameObjectFactory
	{
		private readonly List<GameObject> _gameObjects;

		public TemporaryGameObjectFactory()
		{
			_gameObjects = new List<GameObject>();
		}

		public GameObject CreateGameObject()
		{
			var go = new GameObject();
			_gameObjects.Add(go);
			return go;
		}

	    public GameObject CreateGameObject(string name)
	    {
	        var go = CreateGameObject();
	        go.name = name;
	        return go;
	    }

		public void DestroyAll()
		{
			foreach (var go in _gameObjects)
			{
				GameObject.DestroyImmediate(go);
			}

			_gameObjects.Clear();
		}
	}
}
using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

namespace UnitTest.Utils
{
	public class DispatchArgs
	{
		private IEnumerable<object[]> _actualArgs;

		private DispatchArgs() { }

		public static DispatchArgs CreateWithActualArgs(IEnumerable<object[]> actualArgs)
		{
			return new DispatchArgs
			{
				_actualArgs = actualArgs
			};
		}

		public void WithArgs<T>(Predicate<T> predicate)
		{
			foreach (var args in _actualArgs)
			{
				AssertArgCount(1, args);

				var message = "Dispatched args did not match expected.";
				Assert.IsTrue(predicate((T)args[0]), message);
			}
		}

		public void WithArgs<TFirst, TSecond>(Predicate<TFirst> firstPredicate, Predicate<TSecond> secondPredicate)
		{
			foreach (var args in _actualArgs)
			{
				AssertArgCount(2, args);

				var message = "Dispatched args did not match expected.";
				Assert.IsTrue(firstPredicate((TFirst)args[0]), message);
				Assert.IsTrue(secondPredicate((TSecond)args[1]), message);
			}
		}

		private void AssertArgCount(int expectedArgCount, object[] args)
		{
			if (args.Length != expectedArgCount)
			{
				var message = string.Format("Number of arguments did not match. Expected {0}, but got {1} instead.", expectedArgCount, args.Length);
				Assert.Fail(message);
			}
		}

		// TODO with more args
	}
}
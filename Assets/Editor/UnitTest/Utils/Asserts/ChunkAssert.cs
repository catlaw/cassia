﻿using System.Linq;
using Common.DataContainers;
using Instantiator.Stage.Models.MeshModel;
using NUnit.Framework;

namespace UnitTest.Utils.Asserts
{
	public static class ChunkAssert
	{
		public static void IsFirstCornerEqual(IntVector3 expected, ChunkFace face)
		{
			var center = GetFirstCorner(face);
			Assert.AreEqual(expected, center);
		}

		private static IntVector3 GetFirstCorner(ChunkFace face)
		{
			if (face == null) Assert.Fail();

		    return face.GetCornerPositions().First();
		}

		public static void IsPositionEqual(IntVector3 expected, Chunk chunk)
		{
			Assert.AreEqual(expected, chunk.Position);
		}
	}
}

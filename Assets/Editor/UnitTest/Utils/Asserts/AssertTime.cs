﻿using NUnit.Framework;
using UnitTest.Common.TimedSequencing;
using UnitTest.Utils.CoroutineRunner;

namespace UnitTest.Utils.Asserts
{
	public static class AssertTime
	{
		public static void Elapsed(float elapsed, FakeCoroutineRunner coroutineRunner)
		{
			Assert.AreEqual(elapsed, coroutineRunner.Time, coroutineRunner.MaxDeltaTime);
		}

	    public static void Elapsed(float elapsed, FakeTimedSequence timedSequence)
	    {
	        Assert.AreEqual(elapsed, timedSequence.Elapsed, timedSequence.MaxDeltaTime);
	    }
	}
}

﻿using Configs;
using Game.Models.Occupants.Units;
using NUnit.Framework;

namespace UnitTest.Utils.Asserts
{
    public static class StatsAssert
    {
        public static void AreEqual(UnitStatsConfig statsConfig, IUnitStats stats)
        {
            foreach (var statConfig in statsConfig)
            {
                Assert.AreEqual(statConfig.Value, stats.Get(statConfig.StatType));
            }
        }
    }
}

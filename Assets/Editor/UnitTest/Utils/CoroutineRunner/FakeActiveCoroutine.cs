﻿using System.Collections;
using UnityEngine;

namespace UnitTest.Utils.CoroutineRunner
{
    public class FakeActiveCoroutine
    {
        private IEnumerator _enumerator;

        private FakeActiveCoroutine() { }

        public static FakeActiveCoroutine Create(Coroutine coroutine, IEnumerator enumerator)
        {
            FakeActiveCoroutine fakeActiveCoroutine = new FakeActiveCoroutine();
            fakeActiveCoroutine._enumerator = enumerator;
            fakeActiveCoroutine.Coroutine = coroutine;
            fakeActiveCoroutine.HasNext = true;
            fakeActiveCoroutine.IsPaused = fakeActiveCoroutine.IsParent;
	        fakeActiveCoroutine.IsStopped = false;
            fakeActiveCoroutine.IsLinked = false;
            return fakeActiveCoroutine;
        }

        public void MoveNext()
        {
            if (IsPaused || IsStopped)
            {
                return;
            }

            HasNext = _enumerator.MoveNext();

            if (IsParent)
            {
                IsPaused = true;
                IsLinked = false;
            }

            if (!HasNext && Parent != null)
            {
                Parent.Unpause();
            }
        }

	    public void Stop()
	    {
			IsStopped = true;
	    }

        public void Unpause()
        {
            IsPaused = false;
            MoveNext();
        }

        public Coroutine GetSpawnedChildCoroutine()
        {
            return (Coroutine)_enumerator.Current;
        }

        public void LinkToChild(FakeActiveCoroutine child)
        {
            IsLinked = true;
            child.Parent = this;
        }

        public bool IsParent
        {
            get { return _enumerator.Current is Coroutine; }
        }

        public Coroutine Coroutine { get; private set; }
        public bool HasNext { get; private set; }
        public bool IsPaused { get; private set; }
		public bool IsStopped { get; private set; }
        public bool IsLinked { get; private set; }
        public FakeActiveCoroutine Parent { get; private set; }

	    public bool IsActive
	    {
		    get { return !IsPaused && !IsStopped; }
	    }
    }
}

using Common.Util;
using NUnit.Framework;
using UnityEngine;

namespace UnitTest.Utils.CoroutineRunner
{
	[TestFixture]
	public class FakeCoroutineRunnerStopTest
	{
		private GameObject _dummy;
		private FakeCoroutineRunner _coroutineRunner;
		private TestCoroutineFactory _coroutineFactory;

		[SetUp]
		public void Init()
		{
			_dummy = new GameObject();
			CoroutineCreator coroutineCreator = _dummy.AddComponent<CoroutineCreator>();
			_coroutineRunner = FakeCoroutineRunner.CreateWithConstantDeltaTime();
			_coroutineFactory = TestCoroutineFactory.Create();
		}

		[TearDown]
		public void CleanUp()
		{
			GameObject.DestroyImmediate(_dummy);
		}

		[Test]
		public void StopCoroutine()
		{
			TestDummy dummy = TestDummy.CreateWithStartingValue(0);
			var coroutine = _coroutineRunner.StartCoroutine(_coroutineFactory.IncreaseValue(dummy, 10));
			_coroutineRunner.StopCoroutine(coroutine);
			_coroutineRunner.Run();

			Assert.AreEqual(1, dummy.Value);
		}

		[Test]
		public void StopStoppedCoroutine()
		{
			TestDummy dummy = TestDummy.CreateWithStartingValue(0);
			var coroutine = _coroutineRunner.StartCoroutine(_coroutineFactory.IncreaseValue(dummy, 10));
			_coroutineRunner.StopCoroutine(coroutine);
			_coroutineRunner.StopCoroutine(coroutine);
			_coroutineRunner.Run();

			Assert.AreEqual(1, dummy.Value);
		}

		// TODO: the coroutine has a nested coroutine, but the parent coroutine is stopped
	}
}
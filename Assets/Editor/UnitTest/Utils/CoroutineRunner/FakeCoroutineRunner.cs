﻿using System;
using System.Collections;
using System.Collections.Generic;
using Common.Util;
using UnityEngine;

namespace UnitTest.Utils.CoroutineRunner
{
    public class FakeCoroutineRunner : ICoroutineRunner
    {
		private GameObject _coroutineRunnerHolder;

		private CoroutineCreator _coroutineCreator;
        private FakeActiveCoroutineLocator _locator;
	    private Func<float> _getNextDeltaTime;

        private FakeCoroutineRunner() { }

        public static FakeCoroutineRunner CreateWithConstantDeltaTime(float deltaTime = 0.1f)
        {
            var coroutineRunnerHolder = new GameObject("CoroutineRunner");

            FakeCoroutineRunner coroutineRunner = new FakeCoroutineRunner
	        {
                _coroutineRunnerHolder = coroutineRunnerHolder,
                _coroutineCreator = coroutineRunnerHolder.AddComponent<CoroutineCreator>(),
		        _locator = FakeActiveCoroutineLocator.Create(),
		        _getNextDeltaTime = () => deltaTime,
				DeltaTime = deltaTime,
				MinDeltaTime = deltaTime,
				MaxDeltaTime = deltaTime
	        };
	        return coroutineRunner;
        }

		public static FakeCoroutineRunner CreateWithRandomDeltaTime(DeltaTimeParams deltaTimeParams)
		{
            var coroutineRunnerHolder = new GameObject("CoroutineRunner");

            FakeCoroutineRunner coroutineRunner = new FakeCoroutineRunner
			{
                _coroutineRunnerHolder = coroutineRunnerHolder,
                _coroutineCreator = coroutineRunnerHolder.AddComponent<CoroutineCreator>(),
				_locator = FakeActiveCoroutineLocator.Create(),
				MinDeltaTime = deltaTimeParams.MinDeltaTime,
				MaxDeltaTime = deltaTimeParams.MaxDeltaTime
			};
			UnityEngine.Random.seed = deltaTimeParams.Seed;
			coroutineRunner._getNextDeltaTime =
				() => UnityEngine.Random.Range(deltaTimeParams.MinDeltaTime, deltaTimeParams.MaxDeltaTime);
			coroutineRunner.DeltaTime = coroutineRunner._getNextDeltaTime();
			return coroutineRunner;
		}

		public void CleanUp()
		{
			GameObject.DestroyImmediate(_coroutineRunnerHolder);
		}

		public int ElapsedFrames { get; private set; }

		public float MinDeltaTime { get; private set; }

		public float MaxDeltaTime { get; private set; }

	    public float DeltaTime { get; private set; }

        public float Time { get; private set; }

	    public void SetSeed(int seed)
	    {
		    UnityEngine.Random.seed = seed;
		    DeltaTime = _getNextDeltaTime();
	    }

        public Coroutine StartCoroutine(IEnumerator enumerator)
        {
            Coroutine coroutine = _coroutineCreator.StartCoroutine(enumerator);
            _locator.Add(coroutine, enumerator);
            return coroutine;
        }

		public void StopCoroutine(Coroutine coroutine)
		{
			var activeCoroutine = _locator.GetCoroutine(coroutine);
			activeCoroutine.Stop();
		}

		public void StopCoroutine(IEnumerator enumerator)
		{
			_coroutineCreator.StopCoroutine(enumerator);
		}

		public void Run()
        {
            while (_locator.HasActiveCoroutines)
            {
                RunOneFrame();

                if (ElapsedFrames > 500)
                {
                    throw new TimeoutException("Over 500 frames detected! Stopping.");
                }
            }
        }

        public void RunOneFrame()
        {
            ++ElapsedFrames;
	        Time += DeltaTime;
	        DeltaTime = _getNextDeltaTime();

            LinkCoroutines();
            ExecuteCoroutines();
        }

        private void LinkCoroutines()
        {
            IList<FakeActiveCoroutine> unlinkedParentCoroutines = _locator.GetUnlinkedParentCoroutines();
            for (int i = 0; i < unlinkedParentCoroutines.Count; ++i)
            {
                FakeActiveCoroutine parent = unlinkedParentCoroutines[i];
                FakeActiveCoroutine child = _locator.GetCoroutine(parent.GetSpawnedChildCoroutine());
                parent.LinkToChild(child);
            }
        }

        private void ExecuteCoroutines()
        {
            IList<FakeActiveCoroutine> activeCoroutines = _locator.GetActiveCoroutines();
            for (int i = 0; i < activeCoroutines.Count; ++i)
            {
                FakeActiveCoroutine activeCoroutine = activeCoroutines[i];
                activeCoroutine.MoveNext();
            }
        }

        public void RunForTime(float time)
        {
	        var elapsedTime = 0f;
            while (elapsedTime < time)
            {
                RunOneFrame();
	            elapsedTime += DeltaTime;
            }
        }

	    public void RunUntilTime(float time)
	    {
			while (Time < time)
			{
				RunOneFrame();
			}
		}
    }

	public struct DeltaTimeParams
	{
		public int Seed { get; set; }
		public float MinDeltaTime { get; set; }
		public float MaxDeltaTime { get; set; }
	}
}

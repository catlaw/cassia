﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace UnitTest.Utils.CoroutineRunner
{
    public class FakeActiveCoroutineLocator
    {
        private List<FakeActiveCoroutine> _coroutines;

        private FakeActiveCoroutineLocator() { }

        public static FakeActiveCoroutineLocator Create()
        {
            FakeActiveCoroutineLocator locator = new FakeActiveCoroutineLocator();
            locator._coroutines = new List<FakeActiveCoroutine>();
            return locator;
        }

        public void Add(Coroutine coroutine, IEnumerator enumerator)
        {
            FakeActiveCoroutine fakeActiveCoroutine = FakeActiveCoroutine.Create(coroutine, enumerator);
            _coroutines.Add(fakeActiveCoroutine);
        }

        public IList<FakeActiveCoroutine> GetActiveCoroutines()
        {
            return _coroutines.Where(x => x.HasNext && x.IsActive).ToList();
        }

        public IList<FakeActiveCoroutine> GetUnlinkedParentCoroutines()
        {
            return _coroutines.Where(x => x.IsParent && !x.IsLinked).ToList();
        }

        public bool HasActiveCoroutines
        {
            get { return GetActiveCoroutines().Any(); }
        }

        public FakeActiveCoroutine GetCoroutine(Coroutine coroutine)
        {
            return _coroutines.First(x => x.Coroutine == coroutine);
        }
    }
}

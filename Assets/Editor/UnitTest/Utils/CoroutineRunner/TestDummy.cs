﻿namespace UnitTest.Utils.CoroutineRunner
{
    public class TestDummy
    {
        private TestDummy() { }

        public static TestDummy CreateWithStartingValue(int startingValue)
        {
            TestDummy dummy = new TestDummy();
            dummy.Value = startingValue;
            return dummy;
        }

        public int Value { get; set; }
    }
}

﻿using System;
using System.Collections;
using Common.Util;
using NUnit.Framework;
using UnityEngine;

namespace UnitTest.Utils.CoroutineRunner
{
    [TestFixture]
    public class FakeCoroutineRunnerTest
    {
        private GameObject _dummy;
        private FakeCoroutineRunner _coroutineRunner;
	    private TestCoroutineFactory _coroutineFactory;

        [SetUp]
        public void Init()
        {
            _dummy = new GameObject();
            CoroutineCreator coroutineCreator = _dummy.AddComponent<CoroutineCreator>();
            _coroutineRunner = FakeCoroutineRunner.CreateWithConstantDeltaTime();
			_coroutineFactory = TestCoroutineFactory.Create();
        }

        [TearDown]
        public void CleanUp()
        {
            GameObject.DestroyImmediate(_dummy);
        }

        [Test]
        public void YieldBreak()
        {
            _coroutineRunner.StartCoroutine(_coroutineFactory.YieldBreak());
            _coroutineRunner.Run();

            Assert.AreEqual(1, _coroutineRunner.ElapsedFrames);
        }

        [Test]
        public void YieldReturnNull()
        {
            _coroutineRunner.StartCoroutine(_coroutineFactory.YieldReturnNull());
            _coroutineRunner.Run();

            Assert.AreEqual(1, _coroutineRunner.ElapsedFrames);
        }

        [Test]
        public void StartCoroutine()
        {
            TestDummy dummy = TestDummy.CreateWithStartingValue(0);
            _coroutineRunner.StartCoroutine(_coroutineFactory.IncreaseValue(dummy, 10));
            _coroutineRunner.Run();

            Assert.AreEqual(10, dummy.Value);
            Assert.AreEqual(10, _coroutineRunner.ElapsedFrames);
        }

        [Test]
        public void RunSequentialCoroutinesTwice()
        {
            TestDummy dummy = TestDummy.CreateWithStartingValue(0);
            _coroutineRunner.StartCoroutine(coSequentialIncreaseValue(dummy, 1, 2));
            _coroutineRunner.Run();

            Assert.AreEqual(2, dummy.Value);
            Assert.AreEqual(2, _coroutineRunner.ElapsedFrames);
        }

        private IEnumerator coSequentialIncreaseValue(TestDummy dummy, int frames, int times)
        {
            for (int i = 0; i < times; ++i)
            {
                yield return _coroutineRunner.StartCoroutine(_coroutineFactory.IncreaseValue(dummy, frames));
            }
        }

        [Test]
        public void RunSequentialCoroutinesManyTimes()
        {
            TestDummy dummy = TestDummy.CreateWithStartingValue(0);
            _coroutineRunner.StartCoroutine(coSequentialIncreaseValue(dummy, 5, 4));
            _coroutineRunner.Run();

            Assert.AreEqual(20, dummy.Value);
            Assert.AreEqual(20, _coroutineRunner.ElapsedFrames);
        }

        [Test]
        public void RunSequentialCoroutinesToZero()
        {
            TestDummy dummy = TestDummy.CreateWithStartingValue(0);
            _coroutineRunner.StartCoroutine(coSequentialIncreaseValue(dummy, 0, 3));
            _coroutineRunner.Run();

            Assert.AreEqual(0, dummy.Value);
            Assert.AreEqual(3, _coroutineRunner.ElapsedFrames);
        }

        [Test]
        public void RunConcurrentCoroutines()
        {
            TestDummy dummy = TestDummy.CreateWithStartingValue(0);
            _coroutineRunner.StartCoroutine(_coroutineFactory.IncreaseValue(dummy, 9));
            _coroutineRunner.StartCoroutine(_coroutineFactory.IncreaseValue(dummy, 9));
            _coroutineRunner.Run();

            Assert.AreEqual(18, dummy.Value);
            Assert.AreEqual(9, _coroutineRunner.ElapsedFrames);
        }

        [Test]
        public void RunNestedConcurrentCoroutines()
        {
            TestDummy dummy = TestDummy.CreateWithStartingValue(0);
            _coroutineRunner.StartCoroutine(coNestedConcurrentIncreaseValue(dummy, 5));
            _coroutineRunner.Run();

            Assert.AreEqual(10, dummy.Value);
            Assert.AreEqual(5, _coroutineRunner.ElapsedFrames);
        }

        private IEnumerator coNestedConcurrentIncreaseValue(TestDummy dummy, int frames)
        {
            _coroutineRunner.StartCoroutine(_coroutineFactory.IncreaseValue(dummy, frames));
            _coroutineRunner.StartCoroutine(_coroutineFactory.IncreaseValue(dummy, frames));
            yield break;
        }
        
        [Test]
        public void RunWeirdCoroutine()
        {
            _coroutineRunner.StartCoroutine(coWeirdCoroutine());
            _coroutineRunner.Run();

            Assert.AreEqual(3, _coroutineRunner.ElapsedFrames);
        }

        private IEnumerator coWeirdCoroutine()
        {
            yield return _coroutineRunner.StartCoroutine(_coroutineFactory.YieldReturnNull());
            yield return _coroutineRunner.StartCoroutine(_coroutineFactory.YieldBreak());
            yield return null;
        }

        private IEnumerator coTwoSequenceYieldReturnNull()
        {
            yield return _coroutineRunner.StartCoroutine(_coroutineFactory.YieldReturnNull());
            yield return _coroutineRunner.StartCoroutine(_coroutineFactory.YieldReturnNull());
        }

        [Test]
        public void RunComplexCoroutine()
        {
            TestDummy dummy = TestDummy.CreateWithStartingValue(0);

            _coroutineRunner.StartCoroutine(coComplexCoroutine(dummy));
            _coroutineRunner.Run();

            Assert.AreEqual(56, dummy.Value);
            Assert.AreEqual(27, _coroutineRunner.ElapsedFrames);
        }

        private IEnumerator coComplexCoroutine(TestDummy dummy)
        {
            yield return _coroutineRunner.StartCoroutine(_coroutineFactory.YieldReturnNull());
            yield return _coroutineRunner.StartCoroutine(coNestedConcurrentIncreaseValue(dummy, 5));
            yield return _coroutineRunner.StartCoroutine(_coroutineFactory.IncreaseValue(dummy, 3));
            _coroutineRunner.StartCoroutine(_coroutineFactory.IncreaseValue(dummy, 4));
            yield return _coroutineRunner.StartCoroutine(coNestedConcurrentIncreaseValue(dummy, 7));
            _coroutineRunner.StartCoroutine(_coroutineFactory.IncreaseValue(dummy, 10));
            yield return _coroutineRunner.StartCoroutine(coSequentialIncreaseValue(dummy, 0, 3));
            yield return _coroutineRunner.StartCoroutine(_coroutineFactory.YieldBreak());
            yield return null;
            yield return _coroutineRunner.StartCoroutine(_coroutineFactory.YieldBreak());
            yield return _coroutineRunner.StartCoroutine(coSequentialIncreaseValue(dummy, 5, 3));
        }

        [Test]
        [ExpectedException(typeof(TimeoutException), ExpectedMessage = "Over 500 frames detected! Stopping.")]
        public void RunInfiniteCoroutine()
        {
            _coroutineRunner.StartCoroutine(coInfinite());
            _coroutineRunner.Run();
        }

        private IEnumerator coInfinite()
        {
            while (true)
            {
                yield return null;
            }
        }

        [Test]
        public void RunWithNoCoroutines()
        {
            _coroutineRunner.Run();
        }
    }
}

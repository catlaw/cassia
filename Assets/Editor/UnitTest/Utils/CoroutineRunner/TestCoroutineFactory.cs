﻿using System.Collections;

namespace UnitTest.Utils.CoroutineRunner
{
	public class TestCoroutineFactory
	{
		private TestCoroutineFactory() { }

		public static TestCoroutineFactory Create()
		{
			return new TestCoroutineFactory();
		}

		public IEnumerator YieldBreak()
		{
			yield break;
		}

		public IEnumerator YieldReturnNull()
		{
			yield return null;
		}

		public IEnumerator IncreaseValue(TestDummy dummy, int frames)
		{
			for (int i = 0; i < frames; ++i)
			{
				++dummy.Value;
				yield return null;
			}
		}
	}
}
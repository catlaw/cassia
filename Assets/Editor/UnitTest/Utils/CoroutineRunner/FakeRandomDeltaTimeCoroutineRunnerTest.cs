﻿using System.Collections;
using Common.Util;
using NUnit.Framework;

namespace UnitTest.Utils.CoroutineRunner
{
	[TestFixture]
	public class FakeRandomDeltaTimeCoroutineRunnerTest
	{
		private FakeCoroutineRunner _coroutineRunner;

		[SetUp]
		public void Init()
		{
			UnityEngine.GameObject dummy = new UnityEngine.GameObject();
		}

		[TearDown]
		public void CleanUp()
		{
			_coroutineRunner.CleanUp();
		}

		[Test]
		public void GetDeltaTime([Range(0, 10, 1)] int seed)
		{
			CreateCoroutineRunnerWithSeed(seed);

			var dummy = new Dummy();
			_coroutineRunner.StartCoroutine(coLogDeltaTime(dummy));
			_coroutineRunner.Run();

			Assert.AreEqual(_coroutineRunner.Time, dummy.ElapsedTime);
		}

		private void CreateCoroutineRunnerWithSeed(int seed)
		{
			var deltaTimeParams = new DeltaTimeParams
			{
				Seed = seed,
				MinDeltaTime = 0,
				MaxDeltaTime = 10
			};
			_coroutineRunner = FakeCoroutineRunner.CreateWithRandomDeltaTime(deltaTimeParams);
		}

		private IEnumerator coLogDeltaTime(Dummy dummy)
		{
			dummy.ElapsedTime = _coroutineRunner.DeltaTime;
			yield return null;
		}

		private class Dummy
		{
			public float ElapsedTime { get; set; }
		}

		[Test]
		public void RunWithRandomDeltaTime([Range(0, 10, 1)] int seed)
		{
			CreateCoroutineRunnerWithSeed(seed);

			_coroutineRunner.StartCoroutine(coRunForFrames(10));
			_coroutineRunner.Run();

			Assert.AreEqual(10, _coroutineRunner.ElapsedFrames);
			Assert.AreNotEqual(10, _coroutineRunner.Time);
		}

		private IEnumerator coRunForFrames(int frameCount)
		{
			for (int i = 0; i < frameCount; ++i)
			{
				yield return null;
			}
		}
	}
}

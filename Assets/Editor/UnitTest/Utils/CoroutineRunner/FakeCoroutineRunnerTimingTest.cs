﻿using System.Collections;
using Common.Util;
using NUnit.Framework;
using UnitTest.Utils.Asserts;
using UnityEngine;

namespace UnitTest.Utils.CoroutineRunner
{
    [TestFixture]
    public class FakeCoroutineRunnerTimingTest
    {
        private GameObject _dummy;
        private FakeCoroutineRunner _coroutineRunner;

        [SetUp]
        public void Init()
        {
            _dummy = new GameObject();
            _coroutineRunner = FakeCoroutineRunner.CreateWithConstantDeltaTime(0.23f);
        }

        [TearDown]
        public void CleanUp()
        {
            GameObject.DestroyImmediate(_dummy);
        }

        [Test]
        public void WaitForFrames()
        {
            _coroutineRunner.StartCoroutine(coWaitForFrames(5));
            _coroutineRunner.Run();

            Assert.AreEqual(5 * _coroutineRunner.DeltaTime, _coroutineRunner.Time);
        }

        private IEnumerator coWaitForFrames(int frames)
        {
            for (var i = 0; i < frames; ++i)
            {
                yield return null;
            }
        }

        [Test]
        public void RunOneFrame()
        {
            var dummy = TestDummy.CreateWithStartingValue(0);

            _coroutineRunner.StartCoroutine(coInfiniteIncrement(dummy));
            _coroutineRunner.RunOneFrame();

            Assert.AreEqual(_coroutineRunner.DeltaTime, _coroutineRunner.Time);
        }

        private IEnumerator coInfiniteIncrement(TestDummy dummy)
        {
            while (true)
            {
                ++dummy.Value;
                yield return null;
            }
        }

        [Test]
        public void RunOneFrameWithNoCoroutines()
        {
            _coroutineRunner.RunOneFrame();
        }


        [Test]
        public void RunForTime()
        {
            var dummy = TestDummy.CreateWithStartingValue(0);

            _coroutineRunner.StartCoroutine(coInfiniteIncrement(dummy));
            _coroutineRunner.RunForTime(6);
			AssertTime.Elapsed(6, _coroutineRunner);
		}

        [Test]
        public void RunForTimeWithNoCoroutines()
        {
            _coroutineRunner.RunForTime(7);
        }

	    [Test]
	    public void RunForTimeTwice()
	    {
		    var dummy = TestDummy.CreateWithStartingValue(0);

		    _coroutineRunner.StartCoroutine(coInfiniteIncrement(dummy));
			_coroutineRunner.RunForTime(2);
			_coroutineRunner.RunForTime(3);
			AssertTime.Elapsed(5 + _coroutineRunner.DeltaTime, _coroutineRunner);
	    }

	    [Test]
	    public void RunUntilTime()
	    {
			var dummy = TestDummy.CreateWithStartingValue(0);

			_coroutineRunner.StartCoroutine(coInfiniteIncrement(dummy));
			_coroutineRunner.RunUntilTime(6);
			AssertTime.Elapsed(6, _coroutineRunner);

		    _coroutineRunner.RunUntilTime(7);
			AssertTime.Elapsed(7, _coroutineRunner);
		}
    }
}

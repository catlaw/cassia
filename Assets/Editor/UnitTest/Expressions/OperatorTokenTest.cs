﻿using System;
using Game.Expressions;
using Game.Expressions.Operators;
using NSubstitute;
using NUnit.Framework;

namespace UnitTest.Expressions
{
	[TestFixture]
	public class OperatorTokenTest
	{
		private IExpressionOperator _operator;
		private OperatorToken _token;

		[SetUp]
		public void SetUp()
		{
			_operator = Substitute.For<IExpressionOperator>();
			_token = new OperatorToken(_operator);
		}

		[Test]
		[ExpectedException(typeof(InvalidOperationException),
			ExpectedMessage = "Cannot get value for OperatorToken.")]
		public void GetValueThrowsException()
		{
			_token.GetValue();
		}

		[Test]
		public void Apply()
		{
			var operand0 = Substitute.For<IExpressionToken>();
			operand0.GetValue().Returns(3);

			var operand1 = Substitute.For<IExpressionToken>();
			operand1.GetValue().Returns(7);

			var result = Substitute.For<IExpressionToken>();
			var expression = Substitute.For<IExpressionResolver>();
			expression.Pop().Returns(operand0, operand1);
			_operator.Operate(typeof(int), 3, 7).Returns(result);

			_token.Apply(typeof(int), expression);

			expression.Received(2).Pop();
			expression.Received(1).Push(result);
		}

		[Test]
		[ExpectedException(typeof(ArgumentNullException))]
		public void ApplyWithNull()
		{
			_token.Apply(typeof(int), null);
		}
	}
}
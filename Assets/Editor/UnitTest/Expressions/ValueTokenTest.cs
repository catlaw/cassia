﻿using System;
using Game.Expressions;
using NSubstitute;
using NUnit.Framework;

namespace UnitTest.Expressions
{
	[TestFixture]
	public class ValueTokenTest
	{
		[Test]
		public void GetValue()
		{
			var token = new ValueToken(3);
			
			Assert.AreEqual(3, token.GetValue());
		}

		[Test]
		public void Apply()
		{
			var expression = Substitute.For<IExpressionResolver>();
			var token = new ValueToken(3);

			token.Apply(typeof(int), expression);

			expression.Received(1).Push(token);
		}

		[Test]
		[ExpectedException(typeof(ArgumentNullException))]
		public void ApplyWithNullThrowsException()
		{
			var token = new ValueToken(3);
			token.Apply(typeof(int), null);
		}
	}
}
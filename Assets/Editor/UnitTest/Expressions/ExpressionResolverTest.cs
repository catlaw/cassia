﻿using System;
using Game.Expressions;
using NSubstitute;
using NUnit.Framework;

namespace UnitTest.Expressions
{
	[TestFixture]
	public class ExpressionResolverTest
	{
		private ExpressionResolver _resolver;

		[SetUp]
		public void SetUp()
		{
			_resolver = new ExpressionResolver();
		}

		[Test]
		public void PushAndPopToken()
		{
			var expectedToken = Substitute.For<IExpressionToken>();
			_resolver.Push(expectedToken);

			var actualToken = _resolver.Pop();

			Assert.AreSame(expectedToken, actualToken);
		}

		[Test]
		[ExpectedException(typeof(InvalidOperationException),
			ExpectedMessage = "Cannot pop token when stack empty.")]
		public void PopTokenWhenStackEmpty()
		{
			_resolver.Pop();
		}

		[Test]
		[ExpectedException(typeof(ArgumentNullException))]
		public void PushNullToken()
		{
			_resolver.Push(null);
		}

		[Test]
		public void Resolve()
		{
			var expectedToken = Substitute.For<IExpressionToken>();
			expectedToken.GetValue().Returns(5);

			_resolver.Push(expectedToken);

			Assert.AreEqual(5, _resolver.Resolve());
		}

		[Test]
		[ExpectedException(typeof(InvalidOperationException),
			ExpectedMessage = "Cannot resolve expression when token stack empty.")]
		public void ResolveWhenStackEmptyThrowsException()
		{
			_resolver.Resolve();
		}

		[Test]
		[ExpectedException(typeof(InvalidOperationException),
			ExpectedMessage = "Cannot resolve expression when token stack has more than one token.")]
		public void ResolveWhenStackGreaterThanOneThrowsException()
		{
			_resolver.Push(Substitute.For<IExpressionToken>());
			_resolver.Push(Substitute.For<IExpressionToken>());

			_resolver.Resolve();
		}
	}
}
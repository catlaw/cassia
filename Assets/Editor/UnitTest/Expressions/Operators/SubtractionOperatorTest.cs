﻿using System;
using Game.Expressions.Operators;
using NUnit.Framework;
using UnityEngine;

namespace UnitTest.Expressions.Operators
{
    public class SubtractionOperatorTest
    {
        private SubtractionOperator _operator;

        [SetUp]
        public void SetUp()
        {
            _operator = new SubtractionOperator();
        }

        [Test]
        public void OperateWithInt()
        {
            var result = _operator.Operate(typeof(int), 1, 2);
            Assert.AreEqual(-1, result.GetValue());
        }

        [Test]
        public void OperateWithFloat()
        {
            var result = _operator.Operate(typeof(float), 3.5f, 2.4f);
            Assert.AreEqual(1.1f, (float)result.GetValue(), 0.005f);
        }

        [Test]
        public void OperateWithVector3()
        {
            var result = _operator.Operate(typeof(Vector3), new Vector3(1, 2, 3),
                new Vector3(2, 3, 4));
            Assert.AreEqual(new Vector3(-1, -1, -1), result.GetValue());
        }

        [Test]
        [ExpectedException(typeof(NotSupportedException),
            ExpectedMessage = "Subtraction operator not supported for type 'String'.")]
        public void OperateWithUnsupportedValue()
        {
            _operator.Operate(typeof(string), "Hello", "World");
        }

        [Test]
        [ExpectedException(typeof(ArgumentException),
            ExpectedMessage = "Failed to operate subtraction of type 'Single' with 'one' and 'two'.")]
        public void OperateWithMismatchedType()
        {
            _operator.Operate(typeof(float), "one", "two");
        }
    }
}
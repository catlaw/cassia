﻿using System;
using Game.Expressions.Operators;
using NUnit.Framework;
using UnityEngine;

namespace UnitTest.Expressions.Operators
{
	[TestFixture]
	public class AdditionOperatorTest
	{
		private AdditionOperator _operator;

		[SetUp]
		public void SetUp()
		{
			_operator = new AdditionOperator();
		}

		[Test]
		public void OperateWithInt()
		{
			var result = _operator.Operate(typeof(int), 1, 2);
			Assert.AreEqual(3, result.GetValue());
		}

		[Test]
		public void OperateWithFloat()
		{
			var result = _operator.Operate(typeof(float), 1.5f, 2.4f);
			Assert.AreEqual(3.9f, result.GetValue());
		}

		[Test]
		public void OperateWithVector3()
		{
			var result = _operator.Operate(typeof(Vector3), new Vector3(1, 2, 3),
				new Vector3(2, 3, 4));
			Assert.AreEqual(new Vector3(3, 5, 7), result.GetValue());
		}

		[Test]
		[ExpectedException(typeof(NotSupportedException),
			ExpectedMessage = "Addition operator not supported for type 'String'.")]
		public void OperateWithUnsupportedValue()
		{
			_operator.Operate(typeof(string), "Hello", "World");
		}

		[Test]
		[ExpectedException(typeof(ArgumentException),
			ExpectedMessage = "Failed to operate addition of type 'Single' with 'one' and 'two'.")]
		public void OperateWithMismatchedType()
		{
			_operator.Operate(typeof(float), "one", "two");
		}
	}
}
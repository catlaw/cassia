﻿using System;
using Configs;
using NUnit.Framework;

namespace UnitTest.Configs
{
	[TestFixture]
	public class ConfigTest
	{
		private Config<TestConfig> _config;

		private class TestConfig
		{
			
		}

		[SetUp]
		public void SetUp()
		{
			_config = new Config<TestConfig>();
		}

		[Test]
		public void LoadAndGet()
		{
			var config = new TestConfig();
			_config.LoadConfig(config);

			Assert.AreSame(config, _config.Get());
		}

		[Test]
		[ExpectedException(typeof(InvalidOperationException), ExpectedMessage = "Cannot get config 'TestConfig' before loading it.")]
		public void GetBeforeLoadingThrowsException()
		{
			_config.Get();
		}

		[Test]
		[ExpectedException(typeof(ArgumentNullException))]
		public void LoadNullConfigThrowsException()
		{
			_config.LoadConfig(null);
		}
	}
}
﻿using Common.Signals;
using strange.extensions.signal.impl;

namespace Game.Signals.ViewSignals
{
	public class DisplayActionsSignal : Signal<bool>, IDisplayActionsSignal
	{
		
	}

	public interface IDisplayActionsSignal : ISignal<bool>
	{
		
	}
}
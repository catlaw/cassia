﻿using Common.Signals;
using strange.extensions.signal.impl;
using UnityEngine;

namespace Game.Signals.ViewSignals
{
    public class HighlightViewSignal : Signal<bool, Color>, IHighlightViewSignal
    {
        
    }

    public interface IHighlightViewSignal : ISignal<bool, Color>
    {
        
    }
}
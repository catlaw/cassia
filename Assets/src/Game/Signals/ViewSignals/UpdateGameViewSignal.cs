﻿using Common.Signals;
using strange.extensions.signal.impl;

namespace Game.Signals.ViewSignals
{
	public class UpdateGameViewSignal : Signal, IUpdateGameViewSignal
	{
	}

	public interface IUpdateGameViewSignal : ISignal
	{
	}
}
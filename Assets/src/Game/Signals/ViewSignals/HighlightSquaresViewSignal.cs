﻿using System.Collections.Generic;
using Common.DataContainers;
using Common.Signals;
using strange.extensions.signal.impl;
using Game.Models.Stage;

namespace Game.Signals.ViewSignals
{
	public class HighlightSquaresViewSignal : Signal<IEnumerable<GridCoords>>, IHighlightSquaresViewSignal
	{
	}

	public interface IHighlightSquaresViewSignal : ISignal<IEnumerable<GridCoords>>
	{
	}
}
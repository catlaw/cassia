﻿using Common.Signals;
using Game.Views.Animations;
using strange.extensions.signal.impl;

namespace Game.Signals.ViewSignals
{
	public class TriggerUnitAnimationViewSignal : Signal<IPlayableUnitAnimation>, ITriggerUnitAnimationViewSignal
	{
	}

	public interface ITriggerUnitAnimationViewSignal : ISignal<IPlayableUnitAnimation>
	{
	}
}
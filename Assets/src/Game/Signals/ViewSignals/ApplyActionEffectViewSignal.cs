﻿using Common.Signals;
using Game.Models.Occupants.Units.UnitActions;
using strange.extensions.signal.impl;

namespace Game.Signals.ViewSignals
{
	public class ApplyActionEffectViewSignal : Signal<IActionEffect>, IApplyActionEffectViewSignal
	{
	}

	public interface IApplyActionEffectViewSignal : ISignal<IActionEffect>
	{
	}
}
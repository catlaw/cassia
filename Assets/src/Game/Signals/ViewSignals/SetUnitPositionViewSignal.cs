﻿using Common.DataContainers;
using Common.Signals;
using strange.extensions.signal.impl;

namespace Game.Signals.ViewSignals
{
	public class SetUnitPositionViewSignal : Signal<GridPosition>, ISetUnitPositionViewSignal
	{
	}

	public interface ISetUnitPositionViewSignal : ISignal<GridPosition>
	{
	}
}
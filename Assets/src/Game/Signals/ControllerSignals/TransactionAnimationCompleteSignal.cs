﻿using Common.Signals;
using strange.extensions.signal.impl;

namespace Game.Signals.ControllerSignals
{
	public class TransactionAnimationCompleteSignal : Signal, ITransactionAnimationCompleteSignal
	{
	}

	public interface ITransactionAnimationCompleteSignal : ISignal
	{
	}
}
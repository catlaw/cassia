﻿using System.Collections.Generic;
using Common.DataContainers;
using Common.Signals;
using strange.extensions.signal.impl;

namespace Game.Signals.CommandSignals
{
    public class SetAreaOfEffectSignal : Signal<IEnumerable<GridCoords>>, ISetAreaOfEffectSignal
    {
        
    }

    public interface ISetAreaOfEffectSignal : ISignal<IEnumerable<GridCoords>>
    {
        
    }
}
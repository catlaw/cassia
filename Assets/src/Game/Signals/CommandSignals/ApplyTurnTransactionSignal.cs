﻿using Common.Signals;
using strange.extensions.signal.impl;

namespace Game.Signals.CommandSignals
{
	public class ApplyTurnTransactionSignal : Signal, IApplyTurnTransactionSignal
	{
	}

	public interface IApplyTurnTransactionSignal : ISignal
	{
	}
}
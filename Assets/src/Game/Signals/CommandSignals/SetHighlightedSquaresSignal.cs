﻿using System.Collections.Generic;
using Common.DataContainers;
using Common.Signals;
using strange.extensions.signal.impl;

namespace Game.Signals.CommandSignals
{
	public class SetHighlightedSquaresSignal : Signal<IEnumerable<GridCoords>>, ISetHighlightedSquaresSignal
	{
	}

	public interface ISetHighlightedSquaresSignal : ISignal<IEnumerable<GridCoords>>
	{
	}
}
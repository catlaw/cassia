﻿using Common.DataContainers;
using Game.Controllers.Units;
using Game.Models.Occupants.Units;
using Game.Models.Occupants.Units.UnitActions;

namespace Game.Models.TurnTransactions
{
	public interface ITurnTransaction
	{
		IUnit Actor { get; }
		GridCoords MovePosition { get; }
		ITurnTransactionOutcome Outcome { get; }
		IUnitAction UnitAction { get; }
		bool IsReady { get; }
		TransactionState CurrentState { get; }
		GridCoords StartPosition { get; }
		Facing Facing { get; }
		GridCoords TargetPosition { get; }
		void Clear();
		void SelectMovePosition(GridCoords movePosition);
		void SelectTargetPosition(GridCoords targetPosition);
		void SelectFacing(Facing facing);
		void SelectUnitAction(IUnitAction unitAction);
		void SelectActor(IUnit unit, GridCoords startPosition);
	}
}
﻿using Common.DataContainers;
using Game.Models.Occupants.Units;

namespace Game.Models.TurnTransactions
{
	public interface ITransactionalUnitState
	{
		Facing Facing { get; }
		IUnit Unit { get; }
	}
}
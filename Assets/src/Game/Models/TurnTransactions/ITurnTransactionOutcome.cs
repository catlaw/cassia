﻿using System.Collections.Generic;
using Common.DataContainers;
using Game.Models.Occupants.Units.UnitActions;

namespace Game.Models.TurnTransactions
{
	public interface ITurnTransactionOutcome
	{
		IEnumerable<GridCoords> AreaOfEffect { get; }
		IEnumerable<IPotentialActionOutcome> PotentialActionOutcomes { get; }
		IEnumerable<IActionOutcome> ActionOutcomes { get; }
	}
}
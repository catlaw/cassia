﻿using System.Collections.Generic;
using Common.DataContainers;
using Game.Models.Occupants.Units.UnitActions;

namespace Game.Models.TurnTransactions
{
	public class TurnTransactionOutcome : ITurnTransactionOutcome
	{
		public IEnumerable<GridCoords> AreaOfEffect { get; set; }
		public IEnumerable<IPotentialActionOutcome> PotentialActionOutcomes { get; set; }
		public IEnumerable<IActionOutcome> ActionOutcomes { get; set; }
	}
}
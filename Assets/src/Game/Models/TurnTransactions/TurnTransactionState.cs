﻿using System.Collections.Generic;
using Game.Controllers.Units;

namespace Game.Models.TurnTransactions
{
	public class TurnTransactionState : ITurnTransactionState
	{
		private readonly HashSet<TransactionState> _movedStates;

		public TurnTransactionState()
		{
			_movedStates = new HashSet<TransactionState>
			{
				TransactionState.Moving,
				TransactionState.PreSelectAction,
				TransactionState.PreSelectTarget,
				TransactionState.PreApplyTransaction
			};
		}

		[Inject]
		public ITurnTransaction TurnTransaction { get; set; }

		public bool HasActorMoved
		{
			get { return _movedStates.Contains(TurnTransaction.CurrentState); }
		}
	}
}
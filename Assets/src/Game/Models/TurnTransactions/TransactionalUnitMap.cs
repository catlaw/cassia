﻿using Common.DataContainers;
using Game.Models.Occupants.Units;
using Game.Models.Stage;

namespace Game.Models.TurnTransactions
{
	public class TransactionalUnitMap : ITransactionalUnitMap
	{
		[Inject]
		public ITurnTransaction TurnTransaction { get; set; }

		[Inject]
		public IMap Map { get; set; }

		[Inject]
		public ITurnTransactionState TurnTransactionState { get; set; }

		public ITransactionalUnitState GetUnitAt(GridCoords position)
		{
			if (TurnTransactionState.HasActorMoved)
			{
				if (TurnTransaction.MovePosition == position)
				{
					return new TransactionalUnitState
					{
						Unit = TurnTransaction.Actor,
						Facing = TurnTransaction.Facing
					};
				}

				if (TurnTransaction.StartPosition == position)
				{
					return new TransactionalUnitState
					{
						Unit = EmptyUnit.None,
						Facing = Facing.None
					};
				}
			}

			return GetUnitFromMap(position);
		}

		private ITransactionalUnitState GetUnitFromMap(GridCoords position)
		{
			var square = Map.GetSquare(position);
			var occupant = square.Occupant;

			var unit = occupant is IUnit ? occupant as IUnit : EmptyUnit.None;
			return new TransactionalUnitState
			{
				Unit = unit,
				Facing = unit.Facing
			};
		}
	}
}
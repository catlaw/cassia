using Common.DataContainers;
using Game.Models.Occupants.Units;

namespace Game.Models.TurnTransactions
{
	public class TransactionalUnitState : ITransactionalUnitState
	{
		public IUnit Unit { get; set; }
		public Facing Facing { get; set; }
	}
}
﻿using Common.DataContainers;
using Game.Models.Occupants.Units.UnitActions;

namespace Game.Models.TurnTransactions
{
	public interface ITurnTransactionOutcomeFactory
	{
		ITurnTransactionOutcome GetOutcome(IUnitActionOrigin unitActionOrigin, IUnitAction unitAction,
			GridCoords targetPosition);
	}
}
﻿using Common.DataContainers;
using Game.Controllers.Units;
using Game.Models.Occupants.Units;
using Game.Models.Occupants.Units.UnitActions;

namespace Game.Models.TurnTransactions
{
	public class TurnTransaction : ITurnTransaction
	{
		public TurnTransaction()
		{
			Reset();
		}

		[Inject]
		public ITurnTransactionOutcomeFactory TurnTransactionOutcomeFactory { get; set; }

		[Inject]
		public ITurnTransactionStateMachine StateMachine { get; set; }

		public IUnit Actor { get; private set; }

		public GridCoords StartPosition { get; private set; }

		public GridCoords MovePosition { get; private set; }

		public GridCoords TargetPosition { get; private set; }

		public IUnitAction UnitAction { get; private set; }

		public Facing Facing { get; private set; }

		public ITurnTransactionOutcome Outcome { get; private set; }

		public bool IsReady
		{
			get { return Outcome != EmptyTurnTransactionOutcome.None; }
		}

		public TransactionState CurrentState
		{
			get { return StateMachine.CurrentState; }
		}

		public void SelectActor(IUnit unit, GridCoords startPosition)
		{
			Actor = unit;
			StartPosition = startPosition;
		}

		public void SelectMovePosition(GridCoords movePosition)
		{
			MovePosition = movePosition;
		}

		public void SelectUnitAction(IUnitAction unitAction)
		{
			UnitAction = unitAction;
		}

		public void SelectTargetPosition(GridCoords targetPosition)
		{
			TargetPosition = targetPosition;
			var unitActionOrigin = UnitActionOrigin.CreateFromActorAndPosition(Actor, targetPosition);
			Outcome = TurnTransactionOutcomeFactory.GetOutcome(unitActionOrigin, UnitAction, targetPosition);
		}

		public void SelectFacing(Facing facing)
		{
			Facing = facing;
		}

		public void Clear()
		{
			Reset();
		}

		[PostConstruct]
		public void PostConstruct()
		{
			StateMachine.Init(TransactionState.Nothing);
		}

		private void Reset()
		{
			Actor = EmptyUnit.None;
			UnitAction = EmptyUnitAction.None;
			Outcome = EmptyTurnTransactionOutcome.None;
		}
	}
}
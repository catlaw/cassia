﻿using Common.DataContainers;

namespace Game.Models.TurnTransactions
{
	public interface ITransactionalUnitMap
	{
		ITransactionalUnitState GetUnitAt(GridCoords position);
	}
}
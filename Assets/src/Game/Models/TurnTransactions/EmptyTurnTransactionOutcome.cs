﻿using System.Collections.Generic;
using Common.DataContainers;
using Game.Models.Occupants.Units.UnitActions;

namespace Game.Models.TurnTransactions
{
	public class EmptyTurnTransactionOutcome : ITurnTransactionOutcome
	{
		public static readonly ITurnTransactionOutcome None = new EmptyTurnTransactionOutcome();

		public IEnumerable<GridCoords> AreaOfEffect
		{
			get { return new List<GridCoords>(); }
		}

		public IEnumerable<IPotentialActionOutcome> PotentialActionOutcomes
		{
			get { return new List<IPotentialActionOutcome>(); }
		}

		public IEnumerable<IActionOutcome> ActionOutcomes
		{
			get { return new List<IActionOutcome>(); }
		}
	}
}
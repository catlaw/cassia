﻿namespace Game.Models.TurnTransactions
{
	public interface ITurnTransactionState
	{
		bool HasActorMoved { get; }
		ITurnTransaction TurnTransaction { get; set; }
	}
}
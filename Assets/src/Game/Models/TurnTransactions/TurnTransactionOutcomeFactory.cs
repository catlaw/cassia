﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common.DataContainers;
using Game.Controllers;
using Game.Controllers.Units;
using Game.Models.Occupants.Units.UnitActions;

namespace Game.Models.TurnTransactions
{
	public class TurnTransactionOutcomeFactory : ITurnTransactionOutcomeFactory
	{
		private readonly IActionOutcomeFactory _actionOutcomeFactory;
		private readonly IAffectedActorFactory _affectedActorFactory;
		private readonly IAffectedUnitsLookup _affectedUnitsLookup;
		private readonly IDirectionCalculator _directionCalculator;
		private readonly IPotentialActionOutcomeFactory _potentialActionOutcomeFactory;

		public TurnTransactionOutcomeFactory(IDirectionCalculator directionCalculator,
			IAffectedActorFactory affectedActorFactory,
			IAffectedUnitsLookup affectedUnitsLookup,
			IPotentialActionOutcomeFactory potentialActionOutcomeFactory,
			IActionOutcomeFactory actionOutcomeFactory)
		{
			_directionCalculator = directionCalculator;
			_affectedActorFactory = affectedActorFactory;
			_affectedUnitsLookup = affectedUnitsLookup;
			_potentialActionOutcomeFactory = potentialActionOutcomeFactory;
			_actionOutcomeFactory = actionOutcomeFactory;
		}

		public ITurnTransactionOutcome GetOutcome(IUnitActionOrigin unitActionOrigin, IUnitAction unitAction,
			GridCoords targetPosition)
		{
			var actor = unitActionOrigin.Actor;

			var potentialOutcomes = new List<IPotentialActionOutcome>();
			var selfEffects = unitAction.SelfEffectBuilders;

			if (selfEffects.Any())
			{
				potentialOutcomes.Add(_potentialActionOutcomeFactory
					.CreateActionOutcome(actor.Stats, _affectedActorFactory.CreateAffectedActor(), selfEffects));
			}

			var direction = _directionCalculator.GetFacing(unitActionOrigin.ActorPosition, targetPosition);
			var areaOfEffect = unitAction.GetAreaOfEffect(targetPosition, direction).ToList();
			var affectedUnits = _affectedUnitsLookup.GetUnits(areaOfEffect);
			var targetEffects = unitAction.TargetEffectBuilders;

			if (targetEffects.Any())
			{
				foreach (var affectedUnit in affectedUnits)
				{
					potentialOutcomes.Add(_potentialActionOutcomeFactory.CreateActionOutcome(actor.Stats, affectedUnit, targetEffects));
				}
			}

			return new TurnTransactionOutcome
			{
				AreaOfEffect = areaOfEffect,
				PotentialActionOutcomes = potentialOutcomes,
				ActionOutcomes = potentialOutcomes.Select(x => _actionOutcomeFactory.GetActionOutcome(x))
			};
		}
	}
}
﻿using System.Collections.Generic;
using Common.DataContainers;
using Game.Models.Occupants;

namespace Game.Models.Stage
{
	public interface IMap
	{
		int Depth { get; }
		int Width { get; }

		IEnumerable<GridCoords> GetAdjacentGridCoords(GridCoords gridCoords);
		bool HasOccupant(IOccupant occupant);
		GridCoords GetGridCoords(IOccupant occupant);
		ISquare GetSquare(GridCoords gridCoords);
		bool IsWithinLevel(GridCoords gridCoords);
		void SetOccupant(IOccupant occupant, GridCoords gridCoords);
		void LoadHeights();
	}
}
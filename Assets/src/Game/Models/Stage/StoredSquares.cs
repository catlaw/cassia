﻿using System.Collections.Generic;
using System.Linq;
using Common.DataContainers;

namespace Game.Models.Stage
{
	public class StoredSquares : ISelectableSquares, IAreaOfEffectSquares
	{
		private List<GridCoords> _selectedSquares;

		public StoredSquares()
		{
			_selectedSquares = new List<GridCoords>();
		}

		public void Set(IEnumerable<GridCoords> squares)
		{
			_selectedSquares = squares.ToList();
		}

		public IEnumerable<GridCoords> Get()
		{
			return _selectedSquares;
		}
	}
}
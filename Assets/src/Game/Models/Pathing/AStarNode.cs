using System.Collections.Generic;
using Common.DataContainers;

namespace Game.Models.Pathing
{
	public class AStarNode
	{
		public static readonly AStarNode None = AStarNoneNode.Create();

		protected int _manhattanDistance;

		protected AStarNode()
		{
		}

		public int Cost
		{
			get { return Depth + _manhattanDistance; }
		}

		public virtual int Depth
		{
			get { return Parent.Depth + 1; }
		}

		public GridCoords GridCoords { get; protected set; }
		public AStarNode Parent { get; set; }
		public List<AStarNode> Children { get; protected set; }

		public static AStarNode CreateWithValue(GridCoords gridCoords, int manhattanDistance)
		{
			return new AStarNode
			{
				GridCoords = gridCoords,
				_manhattanDistance = manhattanDistance,
				Children = new List<AStarNode>(),
				Parent = None
			};
		}

		public AStarNode AddChild(GridCoords childCoords, int manhattanDistance)
		{
			var child = CreateWithValue(childCoords, manhattanDistance);
			child.Parent = this;
			Children.Add(child);
			return child;
		}

		public IEnumerable<AStarNode> GetPathFromRoot()
		{
			var pathFromRoot = new List<AStarNode>();
			var currentNode = this;
			while (currentNode.Depth >= 0)
			{
				pathFromRoot.Add(currentNode);
				currentNode = currentNode.Parent;
			}

			pathFromRoot.Reverse();
			return pathFromRoot;
		}

		public override string ToString()
		{
			return string.Format("AStarNode: {0}, Cost: {1}", GridCoords, Cost);
		}
	}

	public class AStarNoneNode : AStarNode
	{
		protected AStarNoneNode()
		{
		}

		public override int Depth
		{
			get { return -1; }
		}

		public static AStarNoneNode Create()
		{
			return new AStarNoneNode
			{
				GridCoords = new GridCoords(0, 0),
				_manhattanDistance = int.MaxValue,
				Children = new List<AStarNode>()
			};
		}
	}
}
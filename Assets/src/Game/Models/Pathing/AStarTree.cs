using System.Collections.Generic;
using System.Linq;

namespace Game.Models.Pathing
{
	public class AStarTree
	{
		private int _maxDepth;

		public AStarNode Root { get; set; }

		public static AStarTree CreateWithMaxDepth(int maxDepth)
		{
			return new AStarTree
			{
				_maxDepth = maxDepth,
				Root = AStarNode.None
			};
		}

		public AStarNode GetNextNode()
		{
			var visitableLeafNodes = GetVisitableLeafNodes(Root);

			if (!visitableLeafNodes.Any())
				return AStarNode.None;

			return visitableLeafNodes.OrderBy(x => x.Cost).First();
		}

		private IList<AStarNode> GetVisitableLeafNodes(AStarNode node)
		{
			var leafNodes = new List<AStarNode>();

			if (node.Depth > _maxDepth)
			{
				return leafNodes;
			}

			if (!node.Children.Any())
			{
				leafNodes.Add(node);
				return leafNodes;
			}

			foreach (var childNode in node.Children)
			{
				leafNodes.AddRange(GetVisitableLeafNodes(childNode));
			}

			return leafNodes;
		}
	}
}
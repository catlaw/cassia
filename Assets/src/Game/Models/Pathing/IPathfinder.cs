﻿using System.Collections.Generic;
using Common.DataContainers;
using Game.Models.Occupants.Units;

namespace Game.Models.Pathing
{
	public interface IPathfinder
	{
		IEnumerable<GridCoords> GetPathTo(IUnit unit, GridCoords targetCoords);
		IEnumerable<GridCoords> GetMovementRange(IUnit unit);
	}
}
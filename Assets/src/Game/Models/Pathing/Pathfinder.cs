﻿using System;
using System.Collections.Generic;
using Common.DataContainers;
using Game.Models.Occupants.Units;
using Game.Models.Stage;

namespace Game.Models.Pathing
{
	public class Pathfinder : IPathfinder
	{
		[Inject]
		public IMap Map { get; set; }

		public IEnumerable<GridCoords> GetPathTo(IUnit unit, GridCoords targetCoords)
		{
			if (!Map.HasOccupant(unit))
			{
				var message = string.Format("Cannot find path if unit '{0}' is not on the map.", unit.Name);
				throw new ArgumentException(message);
			}

			var pathSeeker = AStarAlgorithm.CreateForCharacterAndMap(unit, Map);
			var unitCoords = Map.GetGridCoords(unit);
			return pathSeeker.GetPathBetween(unitCoords, targetCoords);
		}

		public IEnumerable<GridCoords> GetMovementRange(IUnit unit)
		{
			var movementRangeCalculator = MovementRangeCalculator.CreateForMapAndUnit(Map, unit);
			return movementRangeCalculator.GetMovementRange();
		}
	}
}
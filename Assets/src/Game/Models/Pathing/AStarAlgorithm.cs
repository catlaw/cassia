﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common.DataContainers;
using Game.Models.Occupants.Units;
using Game.Models.Stage;

namespace Game.Models.Pathing
{
	internal class AStarAlgorithm
	{
		private IMap _map;
		private IUnit _unit;

		private AStarAlgorithm()
		{
		}

		public static AStarAlgorithm CreateForCharacterAndMap(IUnit unit, IMap map)
		{
			return new AStarAlgorithm
			{
				_unit = unit,
				_map = map
			};
		}

		public IEnumerable<GridCoords> GetPathBetween(GridCoords from, GridCoords to)
		{
			AssertWithinLevel(from, to);

			if (from == to) return new List<GridCoords>();

			var tree = AStarTree.CreateWithMaxDepth(_unit.Move);
			tree.Root = AStarNode.CreateWithValue(from, GetManhattanDistance(from, to));

			while (tree.GetNextNode() != AStarNode.None)
			{
				var nextNode = tree.GetNextNode();
				if (nextNode.GridCoords == to)
				{
					return nextNode.GetPathFromRoot().Select(x => x.GridCoords);
				}

				foreach (var adjacentCoords in _map.GetAdjacentGridCoords(nextNode.GridCoords))
				{
					if (GetHeightDifference(nextNode.GridCoords, adjacentCoords) > _unit.Jump
					    || !_map.GetSquare(adjacentCoords).IsPathableBy(_unit)) continue;

					nextNode.AddChild(adjacentCoords, GetManhattanDistance(adjacentCoords, to));
				}
			}

			return new List<GridCoords>();
		}


		private void AssertWithinLevel(GridCoords from, GridCoords to)
		{
			if (!_map.IsWithinLevel(from) || !_map.IsWithinLevel(to))
			{
				var message = string.Format("Cannot find path from {0} to {1} because Map is of size ({2}, {3}).", from, to,
					_map.Depth, _map.Width);
				throw new IndexOutOfRangeException(message);
			}
		}

		private int GetManhattanDistance(GridCoords from, GridCoords to)
		{
			return Math.Abs(from.Row - to.Row) + Math.Abs(from.Column - to.Column);
		}

		private int GetHeightDifference(GridCoords from, GridCoords to)
		{
			var fromSquare = _map.GetSquare(from);
			var toSquare = _map.GetSquare(to);

			return toSquare.Height - fromSquare.Height;
		}
	}
}
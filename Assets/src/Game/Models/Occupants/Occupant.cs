﻿using Common.DataContainers;
using Game.Signals.ViewSignals;

namespace Game.Models.Occupants
{
	public abstract class Occupant : IOccupant
	{
		protected Occupant()
		{
			Affiliation = Affiliation.None;
			HighlightViewSignal = new HighlightViewSignal();
		}

		public Facing Facing { get; set; }
		public Affiliation Affiliation { get; set; }

		public virtual bool AllowsPassingBy(IOccupant other)
		{
			return Affiliation.IsFriendlyWith(other.Affiliation);
		}

	    public IHighlightViewSignal HighlightViewSignal { get; protected set; }

	    public virtual bool IsSelectable
		{
			get { return false; }
		}
	}
}
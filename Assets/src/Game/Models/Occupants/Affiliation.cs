﻿namespace Game.Models.Occupants
{
	public class Affiliation
	{
		public static readonly NoneAffiliation None = new NoneAffiliation();

		protected Affiliation()
		{
		}

		public string Name { get; private set; }

		public static Affiliation CreateWithName(string name)
		{
			return new Affiliation
			{
				Name = name
			};
		}

		// TODO: this needs to be tested or something sometime
		public virtual bool IsFriendlyWith(Affiliation other)
		{
			// TODO: just returns true if the affiliation is the same for now
			return this == other;
		}
	}

	public class NoneAffiliation : Affiliation
	{
		public override bool IsFriendlyWith(Affiliation other)
		{
			return false;
		}
	}
}
﻿using System.Collections.Generic;
using Game.Controllers.Units;

namespace Game.Models.Occupants.Units.UnitActions
{
	public interface IActionOutcome
	{
		IAffectedUnit AffectedUnit { get; }
		IEnumerable<IActionEffect> Effects { get; }
	}
}
﻿using System;
using System.Collections.Generic;
using Common.DataContainers;
using Configs;

namespace Game.Models.Occupants.Units.UnitActions
{
	public class EmptyUnitAction : IUnitAction
	{
		public static readonly IUnitAction None = new EmptyUnitAction();

		public string Description
		{
			get { return ""; }
		}

		public IEnumerable<GridCoordsOffset> AreaOfEffect
		{
			get { return new List<GridCoordsOffset>(); }
		}

		public string ViewId
		{
			get { return null; }
		}

		public string Name
		{
			get { return "None"; }
		}

		public IEnumerable<GridCoordsOffset> TargetRange
		{
			get { return new List<GridCoordsOffset>(); }
		}

		public bool CanTarget
		{
			get { return false; }
		}

		public IEnumerable<IPotentialActionEffectBuilder> SelfEffectBuilders
		{
			get { return new List<IPotentialActionEffectBuilder>(); }
		}

		public IEnumerable<IPotentialActionEffectBuilder> TargetEffectBuilders
		{
			get { return new List<IPotentialActionEffectBuilder>(); }
		}

		public bool HasEffect
		{
			get { return false; }
		}

		public IEnumerable<GridCoords> GetTargetRange(GridCoords target)
		{
			return new List<GridCoords>();
		}

		public IEnumerable<GridCoords> GetAreaOfEffect(GridCoords target, Facing direction)
		{
			return new List<GridCoords>();
		}

		public void LoadConfig(UnitActionConfig config)
		{
			throw new NotImplementedException("Empty Unit Action does not implement LoadConfig.");
		}

		public Type GetConfigType()
		{
			throw new NotImplementedException("Empty Unit Action does not implement GetConfigType.");
		}
	}
}
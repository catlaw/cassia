﻿using System.Collections.Generic;

namespace Game.Models.Occupants.Units.UnitActions
{
	public interface IEffectable
	{
		IEnumerable<IPotentialActionEffectBuilder> SelfEffectBuilders { get; }
		IEnumerable<IPotentialActionEffectBuilder> TargetEffectBuilders { get; }
		bool HasEffect { get; }
	}
}
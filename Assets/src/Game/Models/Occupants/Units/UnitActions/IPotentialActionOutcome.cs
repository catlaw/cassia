﻿using System.Collections.Generic;
using Game.Controllers.Units;

namespace Game.Models.Occupants.Units.UnitActions
{
	public interface IPotentialActionOutcome
	{
		IAffectedUnit AffectedUnit { get; }
		IEnumerable<IPotentialActionEffect> Effects { get; }
	}
}
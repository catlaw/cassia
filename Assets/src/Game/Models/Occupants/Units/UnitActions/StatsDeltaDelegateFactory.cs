﻿using System;
using Configs;
using UnityEngine;

namespace Game.Models.Occupants.Units.UnitActions
{
	public class StatsDeltaDelegateFactory : IStatsDeltaDelegateFactory
	{
		public StatsDeltaDelegate GetStatsDeltaDelegate(StatsMultiplierConfig actorStatsMultiplier,
			StatsMultiplierConfig targetStatsMultiplier)
		{
			return (actorStats, targetStats) =>
			{
				var actorDelta = GetDelta(actorStatsMultiplier, actorStats);
				var targetDelta = GetDelta(targetStatsMultiplier, targetStats);

				// the target cannot prevent more damage than is dealt
				if (HasDifferentSigns(actorDelta, targetDelta))
				{
					if (Mathf.Abs(targetDelta) > Mathf.Abs(actorDelta)) return 0;
				}

				return (int) Math.Round(actorDelta + targetDelta);
			};
		}

		private float GetDelta(StatsMultiplierConfig multiplierConfig, IUnitStats stats)
		{
			if (multiplierConfig == null) return 0;

			var statType = multiplierConfig.StatType;
			var multiplier = multiplierConfig.Value;
			return stats.Get(statType)*multiplier;
		}

		private bool HasDifferentSigns(float first, float second)
		{
			return (first < 0 && second > 0) || (first > 0 && second < 0);
		}
	}
}
namespace Game.Models.Occupants.Units.UnitActions
{
	public delegate int StatsDeltaDelegate(IUnitStats actorStats, IUnitStats targetStats);
}
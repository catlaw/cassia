﻿namespace Game.Models.Occupants.Units.UnitActions
{
	public class PotentialActionEffect : IPotentialActionEffect
	{
		public IUnitAction UnitAction { get; private set; }
		public float Accuracy { get; private set; }
		public AbnormalStatusType? AbnormalStatus { get; private set; }
		public int? Duration { get; private set; }
		public StatType StatType { get; private set; }
		public bool IsStackable { get; private set; }
		public bool IsRemoveable { get; private set; }
		public int Delta { get; private set; }

		public static PotentialActionEffect CreateWithStats(IPotentialActionEffectBuilder effectBuilder, IUnitStats actorStats,
			IUnitStats affectedUnitStats)
		{
			return new PotentialActionEffect
			{
				UnitAction = effectBuilder.UnitAction,
				Accuracy = effectBuilder.Accuracy,
				AbnormalStatus = effectBuilder.AbnormalStatus,
				Duration = effectBuilder.Duration,
				StatType = effectBuilder.StatType,
				IsStackable = effectBuilder.IsRemoveable,
				IsRemoveable = effectBuilder.IsRemoveable,
				Delta = effectBuilder.GetDelta(actorStats, affectedUnitStats)
			};
		}
	}
}
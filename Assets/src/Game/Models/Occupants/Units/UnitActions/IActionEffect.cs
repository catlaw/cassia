﻿namespace Game.Models.Occupants.Units.UnitActions
{
	public interface IActionEffect
	{
		AbnormalStatusType? AbnormalStatus { get; }
		int Delta { get; }
		bool IsRemoveable { get; }
		bool IsStackable { get; }
		StatType StatType { get; }
		IUnitAction UnitAction { get; }
		int? Duration { get; }
	}
}
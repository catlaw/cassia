﻿namespace Game.Models.Occupants.Units.UnitActions
{
	public enum AbnormalStatusType
	{
		Weaken,
		Blind
	}
}
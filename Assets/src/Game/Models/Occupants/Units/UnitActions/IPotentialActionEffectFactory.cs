﻿namespace Game.Models.Occupants.Units.UnitActions
{
	public interface IPotentialActionEffectFactory
	{
		IPotentialActionEffect CreateActionEffect(IPotentialActionEffectBuilder effectBuilder, IUnitStats actorStats,
			IUnitStats affectedUnitStats);
	}
}
namespace Game.Models.Occupants.Units.UnitActions
{
	public class PotentialActionEffectFactory : IPotentialActionEffectFactory
	{
		public IPotentialActionEffect CreateActionEffect(IPotentialActionEffectBuilder effectBuilder, IUnitStats actorStats,
			IUnitStats affectedUnitStats)
		{
			return PotentialActionEffect.CreateWithStats(effectBuilder, actorStats, affectedUnitStats);
		}
	}
}
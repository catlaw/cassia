﻿using System.Collections.Generic;
using Configs;

namespace Game.Models.Occupants.Units.UnitActions
{
	public class ActionEffectBuildersFactory : IActionEffectBuildersFactory
	{
		private readonly IStatsDeltaDelegateFactory _deltaDelegateFactory;

		public ActionEffectBuildersFactory(IStatsDeltaDelegateFactory deltaDelegateFactory)
		{
			_deltaDelegateFactory = deltaDelegateFactory;
		}

		public IEnumerable<IPotentialActionEffectBuilder> GetEffectBuilders(IUnitAction unitAction,
			IEnumerable<ActionEffectConfig> effectConfigs)
		{
			var unitActionEffects = new List<IPotentialActionEffectBuilder>();

			foreach (var effectConfig in effectConfigs)
			{
				var unitActionEffect = PotentialActionEffectBuilder.Create();

				unitActionEffect.UnitAction = unitAction;
				unitActionEffect.Accuracy = effectConfig.Accuracy;
				unitActionEffect.AbnormalStatus = effectConfig.AbnormalStatus;
				unitActionEffect.Duration = effectConfig.Duration;
				unitActionEffect.IsStackable = effectConfig.IsStackable;
				unitActionEffect.IsRemoveable = effectConfig.IsRemoveable;
				unitActionEffect.StatType = effectConfig.TargetStatType;
				unitActionEffect.GetDelta = _deltaDelegateFactory
					.GetStatsDeltaDelegate(effectConfig.ActorMultiplier, effectConfig.TargetMultiplier);
				unitActionEffects.Add(unitActionEffect);
			}

			return unitActionEffects;
		}
	}
}
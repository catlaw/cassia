namespace Game.Models.Occupants.Units.UnitActions
{
	public class PotentialActionEffectBuilder : IPotentialActionEffectBuilder
	{
		private PotentialActionEffectBuilder()
		{
		}

		public IUnitAction UnitAction { get; set; }
		public float Accuracy { get; set; }
		public AbnormalStatusType? AbnormalStatus { get; set; }
		public StatType StatType { get; set; }
		public int? Duration { get; set; }
		public bool IsStackable { get; set; }
		public bool IsRemoveable { get; set; }
		public StatsDeltaDelegate GetDelta { get; set; }

		public static PotentialActionEffectBuilder Create()
		{
			return new PotentialActionEffectBuilder();
		}
	}
}
﻿using System.Collections.Generic;
using Game.Controllers.Units;

namespace Game.Models.Occupants.Units.UnitActions
{
	public class PotentialActionOutcome : IPotentialActionOutcome
	{
		public IAffectedUnit AffectedUnit { get; set; }
		public IEnumerable<IPotentialActionEffect> Effects { get; set; }
	}
}
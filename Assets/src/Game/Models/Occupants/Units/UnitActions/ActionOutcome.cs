﻿using System.Collections.Generic;
using Game.Controllers.Units;

namespace Game.Models.Occupants.Units.UnitActions
{
	public class ActionOutcome : IActionOutcome
	{
		public IAffectedUnit AffectedUnit { get; set; }
		public IEnumerable<IActionEffect> Effects { get; set; }
	}
}
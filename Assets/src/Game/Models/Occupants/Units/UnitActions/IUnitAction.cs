﻿using Common.Configurables;
using Configs;

namespace Game.Models.Occupants.Units.UnitActions
{
	public interface IUnitAction : IEffectable, ITargetable, IConfigurable<UnitActionConfig>
	{
		string ViewId { get; }
		string Name { get; }
		string Description { get; }
	}
}
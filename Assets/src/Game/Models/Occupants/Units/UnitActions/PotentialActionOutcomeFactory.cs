﻿using System;
using System.Collections.Generic;
using System.Linq;
using Game.Controllers.Units;

namespace Game.Models.Occupants.Units.UnitActions
{
	public class PotentialActionOutcomeFactory : IPotentialActionOutcomeFactory
	{
		[Inject]
		public IPotentialActionEffectFactory PotentialActionEffectFactory { get; set; }

		public IPotentialActionOutcome CreateActionOutcome(IUnitStats actorStats, IAffectedUnit affectedUnit, IEnumerable<IPotentialActionEffectBuilder> effectBuilders)
		{
			if (actorStats == null)
				throw new ArgumentNullException("actorStats");

			if (affectedUnit == null)
				throw new ArgumentNullException("affectedUnit");

			effectBuilders = effectBuilders ?? new List<IPotentialActionEffectBuilder>();
			var effects =
				effectBuilders.Select(x => PotentialActionEffectFactory.CreateActionEffect(x, actorStats, affectedUnit.Unit.Stats));
			return new PotentialActionOutcome
			{
				AffectedUnit = affectedUnit,
				Effects = effects
			};
		}
	}
}
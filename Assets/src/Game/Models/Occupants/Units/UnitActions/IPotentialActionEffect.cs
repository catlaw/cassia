namespace Game.Models.Occupants.Units.UnitActions
{
	public interface IPotentialActionEffect : IActionEffect
	{
		float Accuracy { get; }
	}
}
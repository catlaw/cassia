﻿using System.Collections.Generic;
using Common.DataContainers;

namespace Game.Models.Occupants.Units.UnitActions
{
	public interface ITargetable
	{
		IEnumerable<GridCoordsOffset> AreaOfEffect { get; }
		IEnumerable<GridCoordsOffset> TargetRange { get; }
		bool CanTarget { get; }
		IEnumerable<GridCoords> GetTargetRange(GridCoords target);
		IEnumerable<GridCoords> GetAreaOfEffect(GridCoords target, Facing direction);
	}
}
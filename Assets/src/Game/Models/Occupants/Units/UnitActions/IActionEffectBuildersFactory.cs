﻿using System.Collections.Generic;
using Configs;

namespace Game.Models.Occupants.Units.UnitActions
{
	public interface IActionEffectBuildersFactory
	{
		IEnumerable<IPotentialActionEffectBuilder> GetEffectBuilders(IUnitAction unitAction,
			IEnumerable<ActionEffectConfig> effectConfigs);
	}
}
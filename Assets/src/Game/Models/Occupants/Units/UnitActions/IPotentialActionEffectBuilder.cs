﻿namespace Game.Models.Occupants.Units.UnitActions
{
	public interface IPotentialActionEffectBuilder
	{
		IUnitAction UnitAction { get; }
		AbnormalStatusType? AbnormalStatus { get; }
		float Accuracy { get; }
		int? Duration { get; }
		StatType StatType { get; }
		StatsDeltaDelegate GetDelta { get; }
		bool IsStackable { get; }
		bool IsRemoveable { get; }
	}
}
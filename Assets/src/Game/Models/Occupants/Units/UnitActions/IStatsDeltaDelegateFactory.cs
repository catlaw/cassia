﻿using Configs;

namespace Game.Models.Occupants.Units.UnitActions
{
	public interface IStatsDeltaDelegateFactory
	{
		StatsDeltaDelegate GetStatsDeltaDelegate(StatsMultiplierConfig actorStatsMultiplier,
			StatsMultiplierConfig targetStatsMultiplier);
	}
}
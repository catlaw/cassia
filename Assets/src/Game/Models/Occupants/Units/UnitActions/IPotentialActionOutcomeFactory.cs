﻿using System.Collections.Generic;
using Game.Controllers.Units;

namespace Game.Models.Occupants.Units.UnitActions
{
	public interface IPotentialActionOutcomeFactory
	{
		IPotentialActionOutcome CreateActionOutcome(IUnitStats actorStats, IAffectedUnit affectedUnit, IEnumerable<IPotentialActionEffectBuilder> effectBuilders);
	}
}
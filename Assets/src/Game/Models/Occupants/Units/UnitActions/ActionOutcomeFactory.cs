﻿using System;
using System.Linq;
using Common.Util;
using Game.Controllers;

namespace Game.Models.Occupants.Units.UnitActions
{
	public class ActionOutcomeFactory : IActionOutcomeFactory
	{
		private readonly IRandomNumberGenerator _randomNumberGenerator;

		public ActionOutcomeFactory(IRandomNumberGenerator randomNumberGenerator)
		{
			_randomNumberGenerator = randomNumberGenerator;
		}

		public IActionOutcome GetActionOutcome(IPotentialActionOutcome potentialActionOutcome)
		{
			if (potentialActionOutcome == null) throw new ArgumentNullException("potentialActionOutcome");

			return new ActionOutcome
			{
				AffectedUnit = potentialActionOutcome.AffectedUnit,
				Effects = potentialActionOutcome.Effects
					.Where(x => x.Accuracy > _randomNumberGenerator.GetValue())
					.Cast<IActionEffect>()
			};
		}
	}
}
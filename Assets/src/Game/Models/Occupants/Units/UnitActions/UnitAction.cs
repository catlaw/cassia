﻿using System.Collections.Generic;
using Common.DataContainers;
using Configs;
using Game.Controllers.Units.UnitActions;

namespace Game.Models.Occupants.Units.UnitActions
{
	public class UnitAction : IUnitAction
	{
		[Inject]
		public ITargeter Targeter { get; set; }

		[Inject]
		public IEffector Effector { get; set; }

		public void LoadConfig(UnitActionConfig config)
		{
			var unitActionConfig = config;
			Name = unitActionConfig.Name;
			Description = unitActionConfig.Description;
			ViewId = unitActionConfig.ViewId;

			Targeter.LoadConfig(unitActionConfig.Target);
			Effector.LoadConfig(this, unitActionConfig.Effects);
		}

		public string ViewId { get; private set; }
		public string Name { get; private set; }
		public string Description { get; private set; }

		public IEnumerable<GridCoordsOffset> TargetRange
		{
			get { return Targeter.TargetRange; }
		}

		public IEnumerable<GridCoordsOffset> AreaOfEffect
		{
			get { return Targeter.AreaOfEffect; }
		}

		public IEnumerable<GridCoords> GetTargetRange(GridCoords target)
		{
			return Targeter.GetTargetRange(target);
		}

		public IEnumerable<GridCoords> GetAreaOfEffect(GridCoords target, Facing direction)
		{
			return Targeter.GetAreaOfEffect(target, direction);
		}

		public bool CanTarget
		{
			get { return Targeter.CanTarget; }
		}

		public IEnumerable<IPotentialActionEffectBuilder> TargetEffectBuilders
		{
			get { return Effector.TargetEffectBuilders; }
		}

		public IEnumerable<IPotentialActionEffectBuilder> SelfEffectBuilders
		{
			get { return Effector.SelfEffectBuilders; }
		}

		public bool HasEffect
		{
			get { return Effector.HasEffect; }
		}
	}
}
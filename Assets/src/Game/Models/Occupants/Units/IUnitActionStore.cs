﻿using System.Collections.Generic;
using Game.Models.Occupants.Units.UnitActions;

namespace Game.Models.Occupants.Units
{
	public interface IUnitActionStore
	{
		IEnumerable<IUnitAction> GetDefaultUnitActions();
		IUnitAction GetUnitAction(string unitActionId);
	}
}
﻿using System;
using Game.Models.Occupants.Units.UnitActions;

namespace Game.Models.Occupants.Units
{
	public class StatsModifier : IStatsModifier
	{
		private StatsModifier()
		{
		}

		public IUnitAction UnitAction { get; private set; }
		public StatType StatType { get; private set; }
		public int Delta { get; private set; }
		public int? Duration { get; private set; }
		public AbnormalStatusType? AbnormalStatus { get; private set; }
		public bool IsStackable { get; private set; }
		public bool IsRemoveable { get; set; }

		public void Combine(IStatsModifier modifier)
		{
			if (StatType != modifier.StatType)
			{
				var message = string.Format("Cannot combine StatsModifiers with type '{0}' with one of type '{1}'.", StatType,
					modifier.StatType);
				throw new ArgumentException(message);
			}

			Delta = IsStackable ? modifier.Delta + Delta : modifier.Delta;
			Duration = modifier.Duration;
		}

		public void ReduceDuration()
		{
			Duration--;
		}

		public static StatsModifier Create(IActionEffect actionEffect, IUnitStats actorStats, IUnitStats targetStats)
		{
			return new StatsModifier
			{
				UnitAction = actionEffect.UnitAction,
				StatType = actionEffect.StatType,
				Delta = actionEffect.Delta,
				Duration = actionEffect.Duration,
				AbnormalStatus = actionEffect.AbnormalStatus,
				IsStackable = actionEffect.IsStackable,
				IsRemoveable = actionEffect.IsRemoveable
			};
		}
	}
}
﻿namespace Game.Models.Occupants.Units
{
	public interface IUnitViewMap
	{
		void Add(IUnit unit, IUnitViewData viewData);
		IUnitViewData GetView(IUnit unit);
	}
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common.Configurables;
using Configs;
using Game.Models.Occupants.Units.UnitActions;

namespace Game.Models.Occupants.Units
{
	public class UnitActionStore : IUnitActionStore
	{
		private readonly IConfig<UnitActionsConfig> _config;
		private readonly IConfigurableStore<IUnitAction, UnitActionConfig> _configurableStore;
		private readonly List<string> _defaultUnitActionIds;

		public UnitActionStore(IConfig<UnitActionsConfig> config,
			IConfigurableStore<IUnitAction, UnitActionConfig> configurableStore)
		{
			_config = config;
			_configurableStore = configurableStore;
			_defaultUnitActionIds = new List<string>();
		}

		public IUnitAction GetUnitAction(string unitActionId)
		{
			if (unitActionId == null) throw new ArgumentNullException("unitActionId");
			if (_configurableStore.RequiresLoading) LoadConfig();

			return _configurableStore.GetById(unitActionId);
		}

		public IEnumerable<IUnitAction> GetDefaultUnitActions()
		{
			if (_configurableStore.RequiresLoading) LoadConfig();

			return _defaultUnitActionIds.Select(id => _configurableStore.GetById(id));
		}

		private void LoadConfig()
		{
			_defaultUnitActionIds.Clear();

			var config = _config.Get();
			_defaultUnitActionIds.AddRange(config.DefaultUnitActionIds);
			_configurableStore.LoadConfigs(config.UnitActions);
		}
	}
}
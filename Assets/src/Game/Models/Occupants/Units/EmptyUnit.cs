using System;
using System.Collections.Generic;
using Common.DataContainers;
using Configs;
using Game.Models.Occupants.Units.UnitActions;
using Game.Signals.ViewSignals;

namespace Game.Models.Occupants.Units
{
	public static class EmptyUnit
	{
		public static readonly IUnit None = new NullUnit();

		public class NullUnit : IUnit
		{
		    private readonly IHighlightViewSignal _highlightViewSignal = new HighlightViewSignal();
			private readonly IDisplayActionsSignal _displayActionsSignal = new DisplayActionsSignal();

		    public string Id
			{
				get { return "Null"; }
			}

			public IMoveUnitViewSignal MoveUnitViewSignal
			{
				get { throw new NotImplementedException(); }
			}

			public ISetUnitPositionViewSignal SetUnitPositionViewSignal
			{
				get { throw new NotImplementedException(); }
			}

			public ITriggerUnitAnimationViewSignal TriggerUnitAnimationViewSignal
			{
				get { throw new NotImplementedException(); }
			}

			public IApplyActionEffectViewSignal ApplyActionEffectViewSignal
			{
				get { throw new NotImplementedException(); }
			}

			public IDisplayActionsSignal DisplayActionsSignal
			{
				get { return _displayActionsSignal; }
			}

			public IHighlightViewSignal HighlightViewSignal
		    {
		        get { return _highlightViewSignal; }
		    }

		    public int Jump
			{
				get { return 0; }
			}

			public int Move
			{
				get { return 0; }
			}

			public IUnitStats Stats
			{
				get { return new UnitStats(); }
			}

			public IList<GridCoords> GetPathTo(GridCoords targetCoords)
			{
				throw new NotImplementedException("Cannot get path for NullUnit.");
			}

			public IList<GridCoords> GetMovementRange()
			{
				return new List<GridCoords>();
			}

			public IUnitAction GetUnitAction(int index)
			{
				throw new NotImplementedException("Cannot get UnitAction for NullUnit.");
			}

			public IEnumerable<IUnitAction> EquippedUnitActions
			{
				get { return new List<IUnitAction>(); }
			}

			public Facing Facing { get; set; }

			public void ApplyActionEffect(IActionEffect actionEffect, IUnitStats actorStats)
			{
				throw new NotImplementedException("Cannot apply unitActionEffectBuilder for NullUnit");
			}

			public string Name
			{
				get { return "Null"; }
				set { }
			}

			public void LoadConfig(UnitConfig config)
			{
				throw new NotImplementedException("Cannot load config for NullUnit.");
			}

			public Affiliation Affiliation { get; set; }

			public bool AllowsPassingBy(IOccupant other)
			{
				return true;
			}

		    public bool IsSelectable
			{
				get { return false; }
			}

			public void Select()
			{
				throw new NotImplementedException("Cannot select NullUnit.");
			}

			public Type GetConfigType()
			{
				throw new NotImplementedException("Cannot GetConfigType for NullUnit");
			}

			public void LoadConfig(object config)
			{
				throw new NotImplementedException("Cannot LoadCOnfig for NullUnit");
			}
		}
	}
}
﻿using System.Collections.Generic;
using Common.Configurables;
using Common.DataContainers;
using Configs;
using Game.Models.Occupants.Units.UnitActions;
using Game.Signals.ViewSignals;

namespace Game.Models.Occupants.Units
{
	public interface IUnit : IOccupant, IConfigurable<UnitConfig>
	{
		IMoveUnitViewSignal MoveUnitViewSignal { get; }
		ISetUnitPositionViewSignal SetUnitPositionViewSignal { get; }
		ITriggerUnitAnimationViewSignal TriggerUnitAnimationViewSignal { get; }
		IApplyActionEffectViewSignal ApplyActionEffectViewSignal { get; }
		IDisplayActionsSignal DisplayActionsSignal { get; }
		string Name { get; }
		int Jump { get; }
		int Move { get; }
		IUnitStats Stats { get; }
		IEnumerable<IUnitAction> EquippedUnitActions { get; }
		Facing Facing { get; set; }
	    IList<GridCoords> GetPathTo(GridCoords targetCoords);
		IList<GridCoords> GetMovementRange();
		IUnitAction GetUnitAction(int index);
		void ApplyActionEffect(IActionEffect actionEffect, IUnitStats actorStats);
	}
}
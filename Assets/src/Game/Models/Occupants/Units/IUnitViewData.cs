﻿using System.Collections.Generic;
using Common.DataStructures;
using Configs;
using Game.Views.Animations;
using Instantiator;

namespace Game.Models.Occupants.Units
{
	public interface IUnitViewData
	{
        IDecoration Unit { get; }
        IDecoration UI { get; }
        IEnumerable<IDecoration> Decorations { get; }
        ILibrary<AnimationType, IList<PlayableUnitAnimationConfig>> Animations { get; }
        float MoveSpeed { get; }
        int MaximumWalkHeight { get; }
    }
}
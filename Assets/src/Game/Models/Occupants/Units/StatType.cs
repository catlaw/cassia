﻿namespace Game.Models.Occupants.Units
{
	public enum StatType
	{
		MaxHealth,
		Health,
		Power,
		Defense,
		Move,
		Jump
	}
}
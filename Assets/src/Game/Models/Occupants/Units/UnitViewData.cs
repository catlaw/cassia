﻿using System.Collections.Generic;
using Common.DataStructures;
using Configs;
using Game.Views.Animations;
using Instantiator;

namespace Game.Models.Occupants.Units
{
    public class UnitViewData : IUnitViewData
    {
        public IDecoration Unit { get; set; }
        public IDecoration UI { get; set; }
        public IEnumerable<IDecoration> Decorations { get; set; }
        public ILibrary<AnimationType, IList<PlayableUnitAnimationConfig>> Animations { get; set; }
        public float MoveSpeed { get; set; }
        public int MaximumWalkHeight { get; set; }
    }
}
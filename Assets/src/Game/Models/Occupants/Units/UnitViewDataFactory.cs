﻿using System.Linq;
using Configs;
using Instantiator;

namespace Game.Models.Occupants.Units
{
    public class UnitViewDataFactory : IUnitViewDataFactory
    {
        private readonly IDecorationFactory _decorationFactory;

        public UnitViewDataFactory(IDecorationFactory decorationFactory)
        {
            _decorationFactory = decorationFactory;
        }

        public IUnitViewData CreateUnitViewData(UnitViewConfig config)
        {
            return new UnitViewData
            {
                Unit = _decorationFactory.CreateDecoration(config.Unit),
                UI = _decorationFactory.CreateDecoration(config.UI),
                Decorations = config.Decorations.Select(x => _decorationFactory.CreateDecoration(x)),
                Animations = config.Animations,
                MoveSpeed = config.MoveSpeed,
                MaximumWalkHeight = config.MaximumWalkHeight
            };
        }
    }
}
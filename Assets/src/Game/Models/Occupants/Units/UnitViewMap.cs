﻿using System;
using System.Collections.Generic;
using Common.DataStructures;

namespace Game.Models.Occupants.Units
{
	public class UnitViewMap : IUnitViewMap
	{
		private readonly TwoWayMap<IUnit, IUnitViewData> _map;

		public UnitViewMap()
		{
			_map = new TwoWayMap<IUnit, IUnitViewData>();
		}

		public void Add(IUnit unit, IUnitViewData viewData)
		{
			if (_map.ContainsForward(unit))
			{
				var message = string.Format("Cannot add unit '{0}' twice.", unit.Name);
				throw new ArgumentException(message);
			}

			if (_map.ContainsBackward(viewData))
			{
				var existingUnit = _map.GetBackward(viewData);
				var message = string.Format("Cannot assign same view to both '{0}' and '{1}'.",
					existingUnit.Name, unit.Name);
				throw new ArgumentException(message);
			}

			_map.Add(unit, viewData);
		}

		public IUnitViewData GetView(IUnit unit)
		{
			if (!_map.ContainsForward(unit))
			{
				var message = string.Format("No view found for '{0}'.", unit.Name);
				throw new KeyNotFoundException(message);
			}

			return _map.GetForward(unit);
		}

		public IUnit GetUnit(IUnitViewData unitView)
		{
			if (!_map.ContainsBackward(unitView))
			{
				var message = "No unit found for a unit view.";
				throw new KeyNotFoundException(message);
			}

			return _map.GetBackward(unitView);
		}
	}
}
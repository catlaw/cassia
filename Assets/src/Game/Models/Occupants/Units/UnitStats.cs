﻿using System.Collections.Generic;
using System.Linq;
using Configs;
using Game.Models.Occupants.Units.UnitActions;

namespace Game.Models.Occupants.Units
{
	public class UnitStats : IUnitStats
	{
		private readonly HashSet<AbnormalStatusType> _abnormalStatuses;
		private List<IStatsModifier> _modifiers;
		private UnitStatsContainer _stats;

		public UnitStats()
		{
			_stats = UnitStatsContainer.CreateEmpty();
			_modifiers = new List<IStatsModifier>();
			_abnormalStatuses = new HashSet<AbnormalStatusType>();
		}

		public IEnumerable<IStatsModifier> Modifiers
		{
			get { return _modifiers; }
		}

		public IEnumerable<AbnormalStatusType> AbnormalStatuses
		{
			get
			{
				var abnormalStatuses = new HashSet<AbnormalStatusType>();

				var abnormalStatusModifiers = _modifiers.Where(x => x.AbnormalStatus.HasValue);

				foreach (
					var statusModifier in abnormalStatusModifiers.Where(statusModifier => statusModifier.AbnormalStatus.HasValue))
				{
					abnormalStatuses.Add(statusModifier.AbnormalStatus.Value);
				}

				foreach (var abnormalStatus in _abnormalStatuses)
				{
					abnormalStatuses.Add(abnormalStatus);
				}

				return abnormalStatuses;
			}
		}

		public void LoadConfig(UnitStatsConfig config)
		{
			_stats = UnitStatsContainer.CreateFromConfig(config);
		}

		public int Get(StatType statType)
		{
			var stat = _stats.Get(statType);
			var statModifiers = Modifiers.Where(x => x.StatType == statType);
			stat += statModifiers.Sum(statModifier => statModifier.Delta);

			return stat;
		}

		public void ApplyModifier(IStatsModifier modifier)
		{
			if (modifier.IsRemoveable)
			{
				var existingModifier = _modifiers.FirstOrDefault(x => x.UnitAction == modifier.UnitAction);
				if (existingModifier != null)
				{
					existingModifier.Combine(modifier);
				}
				else
				{
					_modifiers.Add(modifier);
				}
			}
			else
			{
				_stats.SetDelta(modifier.StatType, modifier.Delta);
				if (modifier.AbnormalStatus.HasValue) _abnormalStatuses.Add(modifier.AbnormalStatus.Value);
			}
		}

		public void ReduceDurations()
		{
			foreach (var modifier in _modifiers)
			{
				modifier.ReduceDuration();
			}

			_modifiers = _modifiers.Where(x => !x.Duration.HasValue || x.Duration > 0).ToList();
		}
	}
}
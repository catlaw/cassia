﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common.DataContainers;
using Configs;
using Game.Models.Occupants.Units.UnitActions;
using Game.Models.Pathing;
using Game.Signals.ViewSignals;

namespace Game.Models.Occupants.Units
{
	public class Unit : Occupant, IUnit
	{
		private readonly IPathfinder _pathfinder;
		private readonly IUnitActionStore _unitActionStore;
		private UnitConfig _config;
		private List<IUnitAction> _equippedUnitActions;

		public Unit(IPathfinder pathfinder,
			IUnitActionStore unitActionStore,
			IUnitStats unitStats,
			IMoveUnitViewSignal moveUnitViewSignal,
			ISetUnitPositionViewSignal setUnitPositionViewSignal,
			ITriggerUnitAnimationViewSignal triggerUnitAnimationViewSignal,
			IApplyActionEffectViewSignal applyActionEffectViewSignal,
            IHighlightViewSignal highlightViewSignal,
			IDisplayActionsSignal displayActionsSignal)
		{
			_pathfinder = pathfinder;
			_unitActionStore = unitActionStore;
			Stats = unitStats;
			MoveUnitViewSignal = moveUnitViewSignal;
			SetUnitPositionViewSignal = setUnitPositionViewSignal;
			TriggerUnitAnimationViewSignal = triggerUnitAnimationViewSignal;
			ApplyActionEffectViewSignal = applyActionEffectViewSignal;
			HighlightViewSignal = highlightViewSignal;
			DisplayActionsSignal = displayActionsSignal;
		}

		public IUnitStats Stats { get; private set; }

		public IMoveUnitViewSignal MoveUnitViewSignal { get; private set; }
		public ISetUnitPositionViewSignal SetUnitPositionViewSignal { get; private set; }
		public ITriggerUnitAnimationViewSignal TriggerUnitAnimationViewSignal { get; private set; }
		public IApplyActionEffectViewSignal ApplyActionEffectViewSignal { get; private set; }
		public IDisplayActionsSignal DisplayActionsSignal { get; private set; }

		public void LoadConfig(UnitConfig config)
		{
			_config = config;

			_equippedUnitActions = new List<IUnitAction>();
			foreach (var unitActionId in _config.EquippedActionIds)
			{
				_equippedUnitActions.Add(_unitActionStore.GetUnitAction(unitActionId));
			}
			_equippedUnitActions.AddRange(_unitActionStore.GetDefaultUnitActions());

			Stats.LoadConfig(_config.Stats);
		}

		public string Name
		{
			get { return _config != null ? _config.Name : "Unnamed"; }
		}

		public int Move
		{
			get { return Stats.Get(StatType.Move); }
		}

		public int Jump
		{
			get { return Stats.Get(StatType.Jump); }
		}

		public override bool IsSelectable
		{
			get { return true; }
		}

		public IEnumerable<IUnitAction> EquippedUnitActions
		{
			get { return _equippedUnitActions; }
		}


		public IList<GridCoords> GetPathTo(GridCoords targetCoords)
		{
			return _pathfinder.GetPathTo(this, targetCoords).ToList();
		}

		public IList<GridCoords> GetMovementRange()
		{
			return _pathfinder.GetMovementRange(this).ToList();
		}

		public IUnitAction GetUnitAction(int unitActionIndex)
		{
			if (unitActionIndex < 0 || unitActionIndex >= _equippedUnitActions.Count)
			{
				var message = string.Format("No UnitAction with index {0} for '{1}'.", unitActionIndex, Name);
				throw new ArgumentException(message);
			}

			return _equippedUnitActions[unitActionIndex];
		}

		public void ApplyActionEffect(IActionEffect actionEffect, IUnitStats actorStats)
		{
			var modifier = StatsModifier.Create(actionEffect, actorStats, Stats);
			Stats.ApplyModifier(modifier);
		}

		public Type GetConfigType()
		{
			return typeof(UnitConfig);
		}
	}
}
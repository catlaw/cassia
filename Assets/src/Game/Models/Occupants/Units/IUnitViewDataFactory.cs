﻿using Configs;

namespace Game.Models.Occupants.Units
{
    public interface IUnitViewDataFactory
    {
        IUnitViewData CreateUnitViewData(UnitViewConfig config);
    }
}
﻿using Configs;

namespace Game.Models.Occupants.Units
{
	public interface IUnitStats
	{
		int Get(StatType statType);
		void ApplyModifier(IStatsModifier modifier);
		void LoadConfig(UnitStatsConfig config);
	}
}
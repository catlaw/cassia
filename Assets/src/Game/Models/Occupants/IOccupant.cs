﻿using Game.Signals.ViewSignals;

namespace Game.Models.Occupants
{
	public interface IOccupant
	{
		Affiliation Affiliation { get; set; }
		bool IsSelectable { get; }
		bool AllowsPassingBy(IOccupant other);
        IHighlightViewSignal HighlightViewSignal { get; }
	}
}
﻿using System;
using System.Collections.Generic;
using Game.ConfigActions.Methods;

namespace Game.ConfigActions
{
	public class ConfigActionSet : IConfigActionSet
	{
		private readonly Dictionary<UnitActionEventType, IConfigAction> _sequences;

		public ConfigActionSet()
		{
			_sequences = new Dictionary<UnitActionEventType, IConfigAction>();
		}

		public void Add(UnitActionEventType unitActionEventType, IConfigAction configAction)
		{
			if (_sequences.ContainsKey(unitActionEventType))
			{
				var message = string.Format("ConfigAction with type '{0}' already exists.", unitActionEventType);
				throw new ArgumentException(message);
			}

			_sequences.Add(unitActionEventType, configAction);
		}

		public IConfigAction GetConfigAction(UnitActionEventType unitActionEventType)
		{
			if (!_sequences.ContainsKey(unitActionEventType))
				return new EmptyConfigAction();

			return _sequences[unitActionEventType];
		}
	}
}
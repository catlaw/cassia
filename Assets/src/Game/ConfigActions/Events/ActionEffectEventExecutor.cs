﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Common.Util;
using Game.ConfigActions.Methods;
using Game.ConfigActions.Params;
using Game.Models.Occupants.Units.UnitActions;
using Game.Views.Animations;
using UnityEngine;

namespace Game.ConfigActions.Events
{
    public class ActionEffectEventExecutor : IActionEffectEventExecutor
    {
        private readonly IActionEffectConfigActions _actionEffectConfigActions;
        private readonly IActionEffectedUnitFactory _actionEffectedUnitFactory;
        private readonly IActionEffectEventFactory _actionEffectEventFactory;
        private readonly IConfigActionExecutor _configActionExecutor;
        private readonly ICoroutineRunner _coroutineRunner;
        private readonly IParallelCoroutineRunner _parallelCoroutineRunner;

        public ActionEffectEventExecutor(ICoroutineRunner coroutineRunner, 
            IParallelCoroutineRunner parallelCoroutineRunner, 
            IConfigActionExecutor configActionExecutor, 
            IActionEffectedUnitFactory actionEffectedUnitFactory, 
            IActionEffectEventFactory actionEffectEventFactory,
            IActionEffectConfigActions actionEffectConfigActions)
        {
            _coroutineRunner = coroutineRunner;
            _parallelCoroutineRunner = parallelCoroutineRunner;
            _configActionExecutor = configActionExecutor;
            _actionEffectedUnitFactory = actionEffectedUnitFactory;
            _actionEffectEventFactory = actionEffectEventFactory;
            _actionEffectConfigActions = actionEffectConfigActions;
        }

        public Coroutine Execute(ActionEffectEventType actionEffectEventType)
        {
            return _coroutineRunner.StartCoroutine(CoExecute(actionEffectEventType));
        }

        private IEnumerator CoExecute(ActionEffectEventType actionEffectEventType)
        {
            var configAction = _actionEffectConfigActions.Get(actionEffectEventType);

            foreach (var actionEffectUnits in _actionEffectedUnitFactory.GetActionEffectedUnits())
            {
                var iEnumerators = actionEffectUnits.Select(x => CoExecuteEvent(configAction, x)).ToList();
                yield return _parallelCoroutineRunner.RunInParallel(iEnumerators);
            }
        }

        private IEnumerator CoExecuteEvent(IConfigAction configAction, IActionEffectedUnit actionEffectedUnit)
        {
            actionEffectedUnit.AffectedUnit.Unit.ApplyActionEffectViewSignal.Dispatch(actionEffectedUnit.ActionEffect);
            var gameEvent = _actionEffectEventFactory.CreateEvent(actionEffectedUnit.AffectedUnit,
                actionEffectedUnit.ActionEffect);
            yield return _coroutineRunner.StartCoroutine(_configActionExecutor.CoExecute(configAction, gameEvent));
        }
    }
}
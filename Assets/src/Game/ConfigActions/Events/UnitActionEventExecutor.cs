﻿using System;
using Common.Util;
using Game.ConfigActions.Methods;
using Game.ConfigActions.Params;
using Game.Views.Animations;
using UnityEngine;

namespace Game.ConfigActions.Events
{
	public class UnitActionEventExecutor : IUnitActionEventExecutor
	{
		private readonly ICoroutineRunner _coroutineRunner;
		private readonly IConfigActionExecutor _configActionExecutor;
		private readonly IUnitActionEventFactory _eventFactory;
		private readonly IUnitAnimationFactory _unitAnimationFactory;

		public UnitActionEventExecutor(IUnitAnimationFactory unitAnimationFactory, 
			IConfigActionExecutor configActionExecutor,
			IUnitActionEventFactory eventFactory, 
			ICoroutineRunner coroutineRunner)
		{
			_unitAnimationFactory = unitAnimationFactory;
			_configActionExecutor = configActionExecutor;
			_eventFactory = eventFactory;
			_coroutineRunner = coroutineRunner;
		}

		public Coroutine Execute(UnitActionEventType unitActionEventType)
		{
			var actionSets = _unitAnimationFactory.GetActorUnitAnimation().ConfigActionSet;
			var configAction = actionSets.GetConfigAction(unitActionEventType);

			return _coroutineRunner.StartCoroutine(_configActionExecutor.CoExecute(configAction, _eventFactory.CreateEvent()));
		}
	}
}
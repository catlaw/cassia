﻿using Game.ConfigActions.Methods;
using UnityEngine;

namespace Game.ConfigActions.Events
{
    public interface IActionEffectEventExecutor
    {
        Coroutine Execute(ActionEffectEventType actionEffectEventType);
    }
}
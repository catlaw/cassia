﻿using Game.ConfigActions.Methods;
using UnityEngine;

namespace Game.ConfigActions.Events
{
	public interface IUnitActionEventExecutor
	{
		Coroutine Execute(UnitActionEventType unitActionEventType);
	}
}
﻿using UnityEngine;

namespace Game.ConfigActions.Tweening
{
	public class DoDelegateFactory : IDoDelegateFactory
	{
		public IDOGetter<Vector3> GetPositionGetter(Transform transform)
		{
			return new DOGetterWrapper<Vector3>(() => transform.position);
		}

		public IDOSetter<Vector3> GetPositionSetter(Transform transform)
		{
			return new DOSetterWrapper<Vector3>(x => transform.position = x);
		}
	}
}
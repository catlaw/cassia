﻿using DG.Tweening.Core;

namespace Game.ConfigActions.Tweening
{
	public class DOSetterWrapper<T> : IDOSetter<T>
	{
		private readonly DOSetter<T> _setter;

		public DOSetterWrapper(DOSetter<T> setter)
		{
			_setter = setter;
		}

		public DOSetter<T> Get()
		{
			return _setter;
		}
	}
}
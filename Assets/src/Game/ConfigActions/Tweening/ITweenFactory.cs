﻿using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

namespace Game.ConfigActions.Tweening
{
	public interface ITweenFactory
	{
		ITween GetTween(IDOGetter<Vector3> getter, IDOSetter<Vector3> setter, Vector3 endValue, float duration);

	    ITween GetPathTween(Transform transform, IEnumerable<Vector3> path,
	        float duration, PathType pathType);
	}
}
﻿using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using UnityEngine;

namespace Game.ConfigActions.Tweening
{
	public class TweenFactory : ITweenFactory
	{
		public ITween GetTween(IDOGetter<Vector3> getter, IDOSetter<Vector3> setter,
            Vector3 endValue, float duration)
		{
			var tween = DOTween.To(getter.Get(), setter.Get(), endValue, duration);
			return new TweenWrapper(tween);
		}

	    public ITween GetPathTween(Transform transform, IEnumerable<Vector3> path,
            float duration, PathType pathType)
	    {
	        var tween = transform.DOPath(path.ToArray(), duration, pathType);
            return new TweenWrapper(tween);
	    }
	}
}
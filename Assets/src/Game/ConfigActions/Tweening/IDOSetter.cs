﻿using DG.Tweening.Core;

namespace Game.ConfigActions.Tweening
{
	public interface IDOSetter<T>
	{
		DOSetter<T> Get();
	}
}
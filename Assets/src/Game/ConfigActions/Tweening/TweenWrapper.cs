﻿using DG.Tweening;
using UnityEngine;

namespace Game.ConfigActions.Tweening
{
	public class TweenWrapper : ITween
	{
		private readonly Tween _tween;

		public TweenWrapper(Tween tween)
		{
			_tween = tween;
		}

		public ITween SetEase(Ease ease)
		{
			_tween.SetEase(ease);
			return this;
		}

	    public float PathLength()
	    {
	        return _tween.PathLength();
	    }

	    public YieldInstruction WaitForCompletion()
		{
			return _tween.WaitForCompletion();
		}
	}
}
using DG.Tweening.Core;

namespace Game.ConfigActions.Tweening
{
	public interface IDOGetter<T>
	{
		DOGetter<T> Get();
	}
}
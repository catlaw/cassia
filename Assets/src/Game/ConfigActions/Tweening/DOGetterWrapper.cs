﻿using DG.Tweening.Core;

namespace Game.ConfigActions.Tweening
{
	public class DOGetterWrapper<T> : IDOGetter<T>
	{
		private readonly DOGetter<T> _getter;

		public DOGetterWrapper(DOGetter<T> getter)
		{
			_getter = getter;
		}

		public DOGetter<T> Get()
		{
			return _getter;
		}
	}
}
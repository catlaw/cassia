﻿using UnityEngine;

namespace Game.ConfigActions.Tweening
{
	public interface IDoDelegateFactory
	{
		IDOGetter<Vector3> GetPositionGetter(Transform transform);
		IDOSetter<Vector3> GetPositionSetter(Transform transform);
	}
}
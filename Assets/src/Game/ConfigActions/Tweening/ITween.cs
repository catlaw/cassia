﻿using DG.Tweening;
using UnityEngine;

namespace Game.ConfigActions.Tweening
{
	public interface ITween
	{
		ITween SetEase(Ease ease);
	    float PathLength();
		YieldInstruction WaitForCompletion();
	}
}
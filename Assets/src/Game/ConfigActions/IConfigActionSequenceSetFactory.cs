﻿using System.Collections.Generic;
using Configs;
using Game.ConfigActions.Methods;

namespace Game.ConfigActions
{
	public interface IConfigActionSequenceSetFactory
	{
		IConfigActionSet CreateConfigActionSequenceSet(
			IDictionary<UnitActionEventType, ConfigActionSequenceConfig> configActions);

	    IConfigActionSet CreateEmpty();
	}
}
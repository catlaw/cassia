﻿using System.Collections;
using System.Collections.Generic;
using Game.ConfigActions.Params;

namespace Game.ConfigActions
{
	public interface IConfigAction
	{
		IEnumerable<IConfigAction> Next { get; }
		IEnumerator CoExecute(IGameEvent gameEvent);
		bool IsInstant(IGameEvent gameEvent);
	}
}
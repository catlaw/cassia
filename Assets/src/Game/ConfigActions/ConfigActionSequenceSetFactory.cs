﻿using System.Collections.Generic;
using Configs;
using Game.ConfigActions.Methods;
using strange.framework.api;

namespace Game.ConfigActions
{
	public class ConfigActionSequenceSetFactory : IConfigActionSequenceSetFactory
	{
		private readonly IInstanceProvider _instanceProvider;
		private readonly IConfigActionSequenceFactory _sequenceFactory;

		public ConfigActionSequenceSetFactory(IInstanceProvider instanceProvider,
			IConfigActionSequenceFactory sequenceFactory)
		{
			_instanceProvider = instanceProvider;
			_sequenceFactory = sequenceFactory;
		}

		public IConfigActionSet CreateConfigActionSequenceSet(
			IDictionary<UnitActionEventType, ConfigActionSequenceConfig> configActions)
		{
			var set = _instanceProvider.GetInstance<IConfigActionSet>();
			foreach (var kvp in configActions)
			{
				set.Add(kvp.Key, _sequenceFactory.CreateConfigActionSequence(kvp.Value));
			}

			return set;
		}

	    public IConfigActionSet CreateEmpty()
	    {
	        return _instanceProvider.GetInstance<IConfigActionSet>();
        }
	}
}
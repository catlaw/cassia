﻿using System.Collections;
using Game.ConfigActions.Params;

namespace Game.ConfigActions
{
	public interface IConfigActionExecutor
	{
		IEnumerator CoExecute(IConfigAction configAction, IGameEvent gameEvent);
	}
}
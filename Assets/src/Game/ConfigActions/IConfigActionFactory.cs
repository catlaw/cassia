﻿using System.Collections.Generic;
using Game.ConfigActions.Methods;
using Game.ConfigActions.Params;

namespace Game.ConfigActions
{
	public interface IConfigActionFactory
	{
		IConfigAction CreateConfigAction(IConfigActionMethod method, IMethodParamsFactory methodParamsFactory,
			IEnumerable<IConfigAction> next);
	}
}
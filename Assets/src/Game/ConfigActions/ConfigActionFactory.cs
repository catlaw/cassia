﻿using System.Collections.Generic;
using Game.ConfigActions.Methods;
using Game.ConfigActions.Params;

namespace Game.ConfigActions
{
	public class ConfigActionFactory : IConfigActionFactory
	{
		public IConfigAction CreateConfigAction(
			IConfigActionMethod method,
			IMethodParamsFactory methodParamsFactory,
			IEnumerable<IConfigAction> next)
		{
			return new ConfigAction(method, methodParamsFactory, next);
		}
	}
}
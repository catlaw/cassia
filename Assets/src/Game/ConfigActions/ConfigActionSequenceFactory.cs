﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common.Exceptions;
using Configs;
using Game.ConfigActions.Methods;
using strange.extensions.injector.api;

namespace Game.ConfigActions
{
	public class ConfigActionSequenceFactory : IConfigActionSequenceFactory
	{
		private readonly IConfigActionFactory _configActionFactory;
		private readonly IInjectionBinder _injectionBinder;

		public ConfigActionSequenceFactory(IInjectionBinder injectionBinder,
			IConfigActionFactory configActionFactory)
		{
			_injectionBinder = injectionBinder;
			_configActionFactory = configActionFactory;
		}

		public IConfigAction CreateConfigActionSequence(ConfigActionSequenceConfig config)
		{
			if (config == null) throw new ArgumentNullException("config");

			return CreateConfigAction(config.Root, config.Actions);
		}

		private IConfigAction CreateConfigAction(string id, IDictionary<string, ConfigActionConfig> configs)
		{
			if (!configs.ContainsKey(id))
			{
				var message = string.Format("Could not find ConfigAction with id '{0}'.", id);
				throw new InvalidConfigException(message);
			}

			var config = configs[id];
			var next = config.Next ?? new List<string>();
			var nextConfigActions = next.Select(x => CreateConfigAction(x, configs)).ToList();
			var method = _injectionBinder.GetInstance<IConfigActionMethod>(config.Method);
			return _configActionFactory.CreateConfigAction(method,
				config.MethodParamsFactory, nextConfigActions);
		}
	}
}
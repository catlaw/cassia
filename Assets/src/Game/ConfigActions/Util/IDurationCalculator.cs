﻿using System.Collections.Generic;
using UnityEngine;

namespace Game.ConfigActions.Util
{
	public interface IDurationCalculator
	{
		float GetDuration(Vector3 origin, Vector3 destination, float speed);
	    float GetDuration(IEnumerable<Vector3> path, float speed);
		float GetUnitMoveDuration(int squaresToMove, float speed);
	}
}
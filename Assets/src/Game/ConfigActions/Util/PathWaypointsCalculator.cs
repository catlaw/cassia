﻿using System;
using System.Collections.Generic;
using System.Linq;
using Game.ConfigActions.Methods;
using UnityEngine;

namespace Game.ConfigActions.Util
{
    public class PathWaypointsCalculator : IPathWaypointsCalculator
    {
        private readonly IParametricPointCalculator _parametricPointCalculator;

        public PathWaypointsCalculator(IParametricPointCalculator parametricPointCalculator)
        {
            _parametricPointCalculator = parametricPointCalculator;
        }

        public IEnumerable<Vector3> GetPathWaypoints(Vector3 origin, Vector3 destination, 
            IEnumerable<ParametricPoint> parametricPoints)
        {
            if (parametricPoints == null) throw new ArgumentNullException("parametricPoints");

            var midwayPoints = parametricPoints.Select(x => _parametricPointCalculator
                .GetParametricPoint(x.Parameter, x.Offset, origin, destination));

            return midwayPoints.Concat(new List<Vector3> { destination });
        }
    }
}
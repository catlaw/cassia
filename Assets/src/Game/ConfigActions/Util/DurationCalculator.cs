﻿using System;
using System.Collections.Generic;
using System.Linq;
using Configs;
using UnityEngine;

namespace Game.ConfigActions.Util
{
	public class DurationCalculator : IDurationCalculator
	{
		private readonly IConfig<MapViewConfig> _mapViewConfig;

		public DurationCalculator(IConfig<MapViewConfig> mapViewConfig)
		{
			_mapViewConfig = mapViewConfig;
		}

		public float GetDuration(Vector3 origin, Vector3 destination, float speed)
		{
			if (speed <= 0)
			{
				var message = "Cannot get duration with 0 or less speed.";
				throw new ArgumentException(message);
			}

			var difference = origin - destination;
			var distance = difference.magnitude;

			return distance/speed;
		}

        public float GetDuration(IEnumerable<Vector3> path, float speed)
        {
            if (speed <= 0)
            {
                var message = "Cannot get duration with 0 or less speed.";
                throw new ArgumentException(message);
            }

            if (!path.Any())
            {
                var message = "Cannot get duration for empty path.";
                throw new ArgumentException(message);
            }

            var pathQueue = new Queue<Vector3>(path);

            var pathLength = 0f;
            var currentPosition = pathQueue.Dequeue();
            while (pathQueue.Any())
            {
                var newPosition = pathQueue.Dequeue();
                pathLength += (newPosition - currentPosition).magnitude;
                currentPosition = newPosition;
            }

            return pathLength/speed;
        }

		public float GetUnitMoveDuration(int squaresToMove, float speed)
		{
			if (speed <= 0)
			{
				var message = "Cannot get duration with 0 or less speed.";
				throw new ArgumentException(message);
			}

			return squaresToMove*_mapViewConfig.Get().ChunkSize/speed;
		}
	}
}
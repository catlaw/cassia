﻿using UnityEngine;

namespace Game.ConfigActions.Util
{
	public class ParametricPointCalculator : IParametricPointCalculator
	{
		public Vector3 GetParametricPoint(float parameter, Vector3 offset, 
			Vector3 origin, Vector3 destination)
		{
			var displacement = destination - origin;
			var pointOnPath = origin + displacement*parameter;
			return pointOnPath + offset;
		}
	}
}
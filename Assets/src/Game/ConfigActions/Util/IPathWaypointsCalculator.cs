﻿using System.Collections.Generic;
using Game.ConfigActions.Methods;
using UnityEngine;

namespace Game.ConfigActions.Util
{
    public interface IPathWaypointsCalculator
    {
        IEnumerable<Vector3> GetPathWaypoints(Vector3 origin, Vector3 destination, 
            IEnumerable<ParametricPoint> parametricPoints);
    }
}
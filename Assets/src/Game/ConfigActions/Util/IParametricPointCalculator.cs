﻿using UnityEngine;

namespace Game.ConfigActions.Util
{
	public interface IParametricPointCalculator
	{
		Vector3 GetParametricPoint(float parameter, Vector3 offset,
			Vector3 origin, Vector3 destination);
	}
}
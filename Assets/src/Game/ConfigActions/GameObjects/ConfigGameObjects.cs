﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Game.ConfigActions.GameObjects
{
	public class ConfigGameObjects : IConfigGameObjects
	{
		private readonly Dictionary<IGameObjectId, GameObject> _gameObjectsWithIds;
		private readonly List<GameObject> _gameObjectsWithoutIds;

		public ConfigGameObjects()
		{
			_gameObjectsWithoutIds = new List<GameObject>();
			_gameObjectsWithIds = new Dictionary<IGameObjectId, GameObject>();
		}

		public void AddGameObject(GameObject gameObject)
		{
			if (gameObject == null) throw new ArgumentNullException("gameObject");

			_gameObjectsWithoutIds.Add(gameObject);
		}

		public void AddGameObject(IGameObjectId gameObjectId, GameObject gameObject)
		{
			if (gameObjectId == null) throw new ArgumentNullException("gameObjectId");
			if (gameObject == null) throw new ArgumentNullException("gameObject");

            _gameObjectsWithIds.Add(gameObjectId, gameObject);
		}

		public GameObject GetGameObject(IGameObjectId gameObjectId)
		{
			if (gameObjectId == null) throw new ArgumentNullException("gameObjectId");

            if (!_gameObjectsWithIds.ContainsKey(gameObjectId))
            {
                var message = string.Format("Could not get GameObject with unknown GameObjectId '{0}'.", gameObjectId.Id);
                throw new ArgumentException(message);
            }

            return _gameObjectsWithIds[gameObjectId];
        }

		public GameObject RemoveGameObject(IGameObjectId gameObjectId)
		{
			if (gameObjectId == null) throw new ArgumentNullException("gameObjectId");

            if (!_gameObjectsWithIds.ContainsKey(gameObjectId))
            {
                var message = string.Format("Could not remove GameObject with unknown GameObjectId '{0}'.", gameObjectId.Id);
                throw new ArgumentException(message);
            }

            var gameObject = _gameObjectsWithIds[gameObjectId];
            _gameObjectsWithIds.Remove(gameObjectId);

            return gameObject;
        }

		public bool ContainsId(IGameObjectId gameObjectId)
		{
			if (gameObjectId == null) throw new ArgumentNullException("gameObjectId");

            return _gameObjectsWithIds.ContainsKey(gameObjectId);
		}

		public IEnumerable<GameObject> RemoveAll()
		{
			var gameObjectsWithIds = new List<GameObject>(_gameObjectsWithIds.Values);
			var gameObjectsWithoutIds = new List<GameObject>(_gameObjectsWithoutIds);

			_gameObjectsWithIds.Clear();
			_gameObjectsWithoutIds.Clear();

			return gameObjectsWithIds.Concat(gameObjectsWithoutIds);
		}
	}
}
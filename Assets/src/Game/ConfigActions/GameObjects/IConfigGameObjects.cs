﻿using System.Collections.Generic;
using UnityEngine;

namespace Game.ConfigActions.GameObjects
{
	public interface IConfigGameObjects
	{
		void AddGameObject(GameObject gameObject);
		void AddGameObject(IGameObjectId gameObjectId, GameObject gameObject);
		bool ContainsId(IGameObjectId gameObjectId);
		GameObject GetGameObject(IGameObjectId gameObjectId);
		IEnumerable<GameObject> RemoveAll();
		GameObject RemoveGameObject(IGameObjectId gameObjectId);
	}
}
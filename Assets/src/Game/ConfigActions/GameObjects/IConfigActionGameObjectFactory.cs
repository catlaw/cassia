﻿using Instantiator;
using UnityEngine;

namespace Game.ConfigActions.GameObjects
{
	public interface IConfigActionGameObjectFactory
	{
		GameObject Instantiate(IAsset asset);
		GameObject Instantiate(IGameObjectId id, IAsset asset);
		void DestroyGameObject(IGameObjectId id);
		void DestroyAll();
	}
}
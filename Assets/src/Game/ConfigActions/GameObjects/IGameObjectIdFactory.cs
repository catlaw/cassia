﻿using Game.ConfigActions.Params;

namespace Game.ConfigActions.GameObjects
{
    public interface IGameObjectIdFactory
    {
        IGameObjectId CreateId(IGameEvent gameEvent, string gameObjectId);
    }
}
﻿using Game.ConfigActions.Params;

namespace Game.ConfigActions.GameObjects
{
    public interface IGameObjectId
    {
        string Id { get; }
        IGameEvent GameEvent { get; }
    }
}
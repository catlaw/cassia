﻿using System;
using Game.ConfigActions.Params;

namespace Game.ConfigActions.GameObjects
{
    public class GameObjectIdFactory : IGameObjectIdFactory
    {
        public IGameObjectId CreateId(IGameEvent gameEvent, string gameObjectId)
        {
            if (gameEvent == null) throw new ArgumentNullException("gameEvent");
            if (gameObjectId == null) throw new ArgumentNullException("gameObjectId");

            return new GameObjectId
            {
                GameEvent = gameEvent,
                Id = gameObjectId
            };
        }
    }
}
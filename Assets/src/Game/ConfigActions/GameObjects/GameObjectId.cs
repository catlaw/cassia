﻿using Game.ConfigActions.Params;

namespace Game.ConfigActions.GameObjects
{
    public class GameObjectId : IGameObjectId
    {
        public string Id { get; set; }
        public IGameEvent GameEvent { get; set; }

        protected bool Equals(GameObjectId other)
        {
            return string.Equals(Id, other.Id) && GameEvent.Equals(other.GameEvent);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((GameObjectId) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (Id.GetHashCode()*397) ^ GameEvent.GetHashCode();
            }
        }

        public static bool operator ==(GameObjectId left, GameObjectId right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(GameObjectId left, GameObjectId right)
        {
            return !Equals(left, right);
        }
    }
}
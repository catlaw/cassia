﻿using System;
using Instantiator;
using UnityEngine;

namespace Game.ConfigActions.GameObjects
{
    public class ConfigActionGameObjectFactory : IConfigActionGameObjectFactory
	{
		private readonly IConfigGameObjects _gameObjects;
		private readonly IGameObjectFactory _gameObjectFactory;

		public ConfigActionGameObjectFactory(IConfigGameObjects gameObjects,
            IGameObjectFactory gameObjectFactory)
		{
			_gameObjects = gameObjects;
			_gameObjectFactory = gameObjectFactory;
		}

		public GameObject Instantiate(IAsset asset)
		{
			if (asset == null) throw new ArgumentNullException("asset");

			var gameObject = _gameObjectFactory.Instantiate(asset);
			_gameObjects.AddGameObject(gameObject);

			return gameObject;
		}

		public GameObject Instantiate(IGameObjectId id, IAsset asset)
		{
			if (id == null) throw new ArgumentNullException("id");
			if (asset == null) throw new ArgumentNullException("asset");

			var gameObject = _gameObjectFactory.Instantiate(asset);
			_gameObjects.AddGameObject(id, gameObject);

			return gameObject;
		}

		public void DestroyGameObject(IGameObjectId id)
		{
			if (id == null) throw new ArgumentNullException("id");

			var gameObject = _gameObjects.RemoveGameObject(id);
            _gameObjectFactory.Destroy(gameObject);
		}

		public void DestroyAll()
		{
			var gameObjects = _gameObjects.RemoveAll();
			foreach (var gameObject in gameObjects)
			{
                _gameObjectFactory.Destroy(gameObject);
			}
		}
	}
}
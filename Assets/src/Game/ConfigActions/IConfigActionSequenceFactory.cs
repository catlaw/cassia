﻿using Configs;

namespace Game.ConfigActions
{
	public interface IConfigActionSequenceFactory
	{
		IConfigAction CreateConfigActionSequence(ConfigActionSequenceConfig config);
	}
}
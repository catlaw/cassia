﻿using System;

namespace Game.ConfigActions.Params
{
	// not tested
	public class EventPropertyGetter : IEventPropertyGetter
	{
		private readonly Type _type;
		private readonly IGameEvent _gameEvent;

		public EventPropertyGetter(IGameEvent gameEvent)
		{
			_gameEvent = gameEvent;
			_type = gameEvent.GetType();
		}

		public object GetPropertyValue(string propertyName)
		{
			var property = _type.GetProperty(propertyName);

			if (property == null)
			{
				var message = string.Format("Cannot find property '{0}' on type '{1}'.",
					propertyName, _type.Name);
				throw new ArgumentException(message);
			}

			return property.GetValue(_gameEvent, null);
		}

	    public IGameEvent GetEvent()
	    {
	        return _gameEvent;
	    }
	}
}
﻿using System;
using System.Collections.Generic;
using Game.Expressions;
using Newtonsoft.Json.Linq;

namespace Game.ConfigActions.Params
{
    public interface IExpressionTokenizer
    {
        IEnumerable<IExpressionToken> GetTokens(Type type, IJEnumerable<JToken> jTokens);
    }
}
﻿using System;
using Newtonsoft.Json.Linq;

namespace Game.ConfigActions.Params
{
    /// <summary>
    /// This only works on top level for now... don't need to overkill.
    /// </summary>
	public interface IMethodParamParser
	{
		object Parse(JToken jToken, Type type);
	}
}
﻿using System.Collections.Generic;

namespace Game.ConfigActions.Params
{
	public interface IActionEffectedUnitFactory
	{
		IEnumerable<IEnumerable<IActionEffectedUnit>> GetActionEffectedUnits();
	}
}
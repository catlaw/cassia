﻿namespace Game.ConfigActions.Params
{
    public interface IMethodParamParserFactory
    {
        IMethodParamParser CreateMethodParamParser(IGameEvent gameEvent);
    }
}
﻿using System;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Game.ConfigActions.Params
{
	public class MethodParamsFactory : IMethodParamsFactory
	{
		private readonly JObject _methodParamsJObject;
	    private readonly IMethodParamParserFactory _methodParamParserFactory;

		public MethodParamsFactory(JObject methodParamsJObject,
            IMethodParamParserFactory methodParamParserFactory)
		{
			if (methodParamsJObject == null) throw new ArgumentNullException("methodParamsJObject");
            if (methodParamParserFactory == null) throw new ArgumentNullException("methodParamParserFactory");

			_methodParamsJObject = methodParamsJObject;
		    _methodParamParserFactory = methodParamParserFactory;
		}
		
		public TMethodParams CreateMethodParams<TMethodParams>(IGameEvent gameEvent)
			where TMethodParams : new()
		{
			if (gameEvent == null) throw new ArgumentNullException("gameEvent");

			var methodParams = new TMethodParams();
            var methodParamParser = _methodParamParserFactory.CreateMethodParamParser(gameEvent);


            foreach (var jProperty in _methodParamsJObject.Properties())
			{
                var propertyName = jProperty.Name;
                var methodParamsProperty = typeof(TMethodParams).GetProperty(propertyName);

				if (methodParamsProperty == null)
				{
					var message = string.Format("Cannot create '{0}' with unknown property '{1}'.",
						typeof(TMethodParams).Name, propertyName);
					throw new ArgumentException(message);
				}

                var propertyValue = methodParamParser.Parse(jProperty.Value, methodParamsProperty.PropertyType);

				try
				{
				    if (!methodParamsProperty.PropertyType.IsInstanceOfType(propertyValue))
				    {
				        var convertedValue = Convert.ChangeType(propertyValue, methodParamsProperty.PropertyType);
                        methodParamsProperty.SetValue(methodParams, convertedValue, null);
				    }
				    else
				    {
					    methodParamsProperty.SetValue(methodParams, propertyValue, null);
                    }
                }
				catch (ArgumentException)
				{
					var message = string.Format("Cannot create '{0}' because value '{1}' could not be assigned to '{2}'.",
						typeof(TMethodParams).Name, propertyValue, propertyName);
					throw new ArgumentException(message);
				}
			}

			return methodParams;
		}
	}
}
﻿namespace Game.ConfigActions.Params
{
	public interface IUnitActionEventFactory
	{
		IGameEvent CreateEvent();
	}
}
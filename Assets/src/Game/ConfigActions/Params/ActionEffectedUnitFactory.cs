﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Game.Controllers.Units;
using Game.Models.Occupants.Units.UnitActions;
using Game.Models.TurnTransactions;

namespace Game.ConfigActions.Params
{
    public class ActionEffectedUnitFactory : IActionEffectedUnitFactory
	{
		private readonly ITurnTransaction _turnTransaction;

		public ActionEffectedUnitFactory(ITurnTransaction turnTransaction)
		{
			_turnTransaction = turnTransaction;
		}

		public IEnumerable<IEnumerable<IActionEffectedUnit>> GetActionEffectedUnits()
		{
			var result = new List<IEnumerable<IActionEffectedUnit>>();

		    var actionEffectQueues = _turnTransaction.Outcome.ActionOutcomes
		        .Select(x => new ActionEffectQueue
		        {
		            AffectedUnit = x.AffectedUnit,
                    ActionEffects = new Queue<IActionEffect>(x.Effects)
		        })
                .ToList();

            while (actionEffectQueues.Any(x => x.ActionEffects.Any()))
		    {
                var units = new List<IActionEffectedUnit>();
                foreach (var actionEffectQueue in actionEffectQueues)
                {
                    if (!actionEffectQueue.ActionEffects.Any()) continue;

                    var actionEffect = actionEffectQueue.ActionEffects.Dequeue();
                    var unit = new ActionEffectedUnit
                    {
                        AffectedUnit = actionEffectQueue.AffectedUnit,
                        ActionEffect = actionEffect
                    };
                    units.Add(unit);
                }

                result.Add(units);
            }

			return result;
		}

	    private class ActionEffectQueue
	    {
	        public IAffectedUnit AffectedUnit { get; set; }
            public Queue<IActionEffect> ActionEffects { get; set; }
	    }
	}
}
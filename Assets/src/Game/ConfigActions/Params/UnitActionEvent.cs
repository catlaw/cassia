using Game.Models.Occupants.Units;
using UnityEngine;

namespace Game.ConfigActions.Params
{
	public class UnitActionEvent : IGameEvent
	{
        public IUnit Actor { get; set; }
		public Vector3 ActorPosition { get; set; }
		public Vector3 TargetPosition { get; set; }
	}
}
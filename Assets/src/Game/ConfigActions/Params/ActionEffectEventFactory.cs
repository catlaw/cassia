﻿using System;
using System.Collections.Generic;
using System.Linq;
using Game.Controllers;
using Game.Controllers.Units;
using Game.Models.Occupants.Units.UnitActions;
using Game.Models.TurnTransactions;

namespace Game.ConfigActions.Params
{
	public class ActionEffectEventFactory : IActionEffectEventFactory
	{
		private readonly IWorldGridCoordsConverter _converter;

		public ActionEffectEventFactory(IWorldGridCoordsConverter converter)
		{
			_converter = converter;
		}

		public IGameEvent CreateEvent(IAffectedUnit affectedUnit, IActionEffect actionEffect)
		{
			if (affectedUnit == null) throw new ArgumentNullException("affectedUnit");
			if (actionEffect == null) throw new ArgumentNullException("actionEffect");

			return new ActionEffectEvent
			{
                AffectedUnit = affectedUnit.Unit,
				TargetPosition = _converter.GetWorldPosition(affectedUnit.GridCoords),
				StatType = actionEffect.StatType,
				Delta = actionEffect.Delta
			};
		}
	}
}
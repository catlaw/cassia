﻿namespace Game.ConfigActions.Params
{
	public interface IEventPropertyGetter
	{
		object GetPropertyValue(string propertyName);
	    IGameEvent GetEvent();
	}
}
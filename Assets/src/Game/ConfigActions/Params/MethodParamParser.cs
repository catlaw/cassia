﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Game.ConfigActions.GameObjects;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Game.ConfigActions.Params
{
    public class MethodParamParser : IMethodParamParser
	{
		private readonly IMethodParamsExpressionSolver _solver;
        private readonly IEventParamsParser _eventParamsParser;

		public MethodParamParser(IMethodParamsExpressionSolver solver,
            IEventParamsParser eventParamsParser)
		{
			_solver = solver;
		    _eventParamsParser = eventParamsParser;
		}

		public object Parse(JToken jToken, Type type)
		{
            if (jToken == null) throw new ArgumentNullException("jToken");
            if (type == null) throw new ArgumentNullException("type");

		    if (_eventParamsParser.IsVariable(jToken))
		        return _eventParamsParser.ParseVariable(jToken);

		    if (type == typeof(IGameObjectId))
		        return _eventParamsParser.ParseConfigGameObjectId(jToken);

		    if (jToken.Type == JTokenType.Array)
		    {
		        if (typeof(IEnumerable).IsAssignableFrom(type) && type.IsGenericType)
		        {
                    var genericType = type.GetGenericArguments().First();
                    var listType = typeof(List<>);
                    var genericListType = listType.MakeGenericType(genericType);

                    var listInstance = (IList)Activator.CreateInstance(genericListType);

                    foreach (var element in jToken.Children())
                    {
                        listInstance.Add(Parse(element, genericType));
                    }

                    return listInstance;
                }
		        else
		        {
                    return _solver.Solve(type, jToken.Children());
                }
		        
		    }

		    var reader = jToken.CreateReader();
            var serializer = new JsonSerializer();

		    try
		    {
		        return serializer.Deserialize(reader, type);
		    }
		    catch (JsonSerializationException)
		    {
		        var message = string.Format("Error parsing Method Param: Could not convert '{0}' to '{1}'.",
                    jToken, type.Name);
		        throw new ArgumentException(message);
		    }
		}
    }
}
﻿using Game.Controllers;
using Game.Models.TurnTransactions;

namespace Game.ConfigActions.Params
{
	public class UnitActionEventFactory : IUnitActionEventFactory
	{
		private readonly IWorldGridCoordsConverter _gridCoordsConverter;
		private readonly ITurnTransaction _turnTransaction;

		public UnitActionEventFactory(ITurnTransaction turnTransaction,
			IWorldGridCoordsConverter gridCoordsConverter)
		{
			_turnTransaction = turnTransaction;
			_gridCoordsConverter = gridCoordsConverter;
		}

		public IGameEvent CreateEvent()
		{
			var actorGridCoords = _turnTransaction.MovePosition;
			var actorPosition = _gridCoordsConverter.GetWorldPosition(actorGridCoords);

			var targetGridCoords = _turnTransaction.TargetPosition;
			var targetPosition = _gridCoordsConverter.GetWorldPosition(targetGridCoords);

			return new UnitActionEvent
			{
                Actor = _turnTransaction.Actor,
				ActorPosition = actorPosition,
				TargetPosition = targetPosition
			};
		}
	}
}
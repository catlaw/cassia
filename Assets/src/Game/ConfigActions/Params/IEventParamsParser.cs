﻿using Game.ConfigActions.GameObjects;
using Newtonsoft.Json.Linq;

namespace Game.ConfigActions.Params
{
	public interface IEventParamsParser
	{
		object ParseVariable(JToken jToken);
	    bool IsVariable(JToken jToken);
	    IGameObjectId ParseConfigGameObjectId(JToken jToken);
	}
}
﻿using Game.ConfigActions.GameObjects;
using Game.Expressions;
using Game.Expressions.Operators;

namespace Game.ConfigActions.Params
{
    public class MethodParamParserFactory : IMethodParamParserFactory
    {
        // no DI available here!
        public IMethodParamParser CreateMethodParamParser(IGameEvent gameEvent)
        {
            var eventParamsPropertyGetter = new EventPropertyGetter(gameEvent);
            var configGameObjectIdFactory = new GameObjectIdFactory();
            var eventParamsReplacer = new EventParamsParser(eventParamsPropertyGetter, configGameObjectIdFactory);
            var primitiveParser = new PrimitiveTokenParser(eventParamsReplacer);
            var operatorTokenMap = new OperatorTokenMap();
            var tokenizer = new ShittyExpressionTokenizer(operatorTokenMap, primitiveParser);
            var resolverFactory = new ExpressionResolverFactory();
            var solver = new MethodParamsExpressionSolver(tokenizer, resolverFactory);
            return new MethodParamParser(solver, eventParamsReplacer);
        }
    }
}
﻿using System;
using Newtonsoft.Json.Linq;

namespace Game.ConfigActions.Params
{
	public interface IMethodParamsExpressionSolver
	{
		object Solve(Type type, IJEnumerable<JToken> jTokens);
	}
}
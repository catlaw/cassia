﻿using Game.Controllers.Units;
using Game.Models.Occupants.Units.UnitActions;

namespace Game.ConfigActions.Params
{
	public interface IActionEffectEventFactory
	{
		IGameEvent CreateEvent(IAffectedUnit affectedUnit, IActionEffect actionEffect);
	}
}
﻿using Game.Controllers.Units;
using Game.Models.Occupants.Units;
using Game.Models.Occupants.Units.UnitActions;

namespace Game.ConfigActions.Params
{
    public class ActionEffectedUnit : IActionEffectedUnit
    {
        public IAffectedUnit AffectedUnit { get; set; }
        public IActionEffect ActionEffect { get; set; }
    }
}
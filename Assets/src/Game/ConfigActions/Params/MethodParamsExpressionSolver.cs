﻿using System;
using Game.Expressions;
using Newtonsoft.Json.Linq;
using strange.framework.api;

namespace Game.ConfigActions.Params
{
	public class MethodParamsExpressionSolver : IMethodParamsExpressionSolver
	{
		private readonly IExpressionTokenizer _tokenizer;
	    private readonly IExpressionResolverFactory _resolverFactory;

		public MethodParamsExpressionSolver(IExpressionTokenizer tokenizer,
            IExpressionResolverFactory resolverFactory)
		{
			_tokenizer = tokenizer;
		    _resolverFactory = resolverFactory;
		}

		public object Solve(Type type, IJEnumerable<JToken> jTokens)
		{
			if (type == null) throw new ArgumentNullException("type");
			if (jTokens == null) throw new ArgumentNullException("jTokens");

			var tokens = _tokenizer.GetTokens(type, jTokens);
		    var resolver = _resolverFactory.CreateExpressionResolver();

			foreach (var token in tokens)
			{
				token.Apply(type, resolver);
			}

			return resolver.Resolve();
		}
	}
}
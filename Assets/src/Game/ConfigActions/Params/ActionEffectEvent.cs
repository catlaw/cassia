﻿using Game.Models.Occupants.Units;
using UnityEngine;

namespace Game.ConfigActions.Params
{
	public class ActionEffectEvent : IGameEvent
	{
	    public IUnit AffectedUnit { get; set; }
		public Vector3 TargetPosition { get; set; }
		public StatType StatType { get; set; }
		public int Delta { get; set; }
	}
}
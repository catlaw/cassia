﻿using System;
using System.Collections.Generic;
using System.Linq;
using Game.Expressions;
using Game.Expressions.Operators;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using UnityEngine;

namespace Game.ConfigActions.Params
{
    // heh this only works in the form A or A + B. don't need more than this for now.
	public class ShittyExpressionTokenizer : IExpressionTokenizer
    {
		private readonly IOperatorTokenMap _operatorTokenMap;
		private readonly IPrimitiveTokenParser _primitiveTokenParser;

		public ShittyExpressionTokenizer(IOperatorTokenMap operatorTokenMap,
			IPrimitiveTokenParser primitiveTokenParser)
		{
			_operatorTokenMap = operatorTokenMap;
			_primitiveTokenParser = primitiveTokenParser;
		}

		public IEnumerable<IExpressionToken> GetTokens(Type type, IJEnumerable<JToken> jTokens)
		{
		    if (!type.IsPrimitive && type != typeof(string) && type != typeof(Vector3))
		    {
		        var typeException = string.Format("Cannot get tokens for type '{0}'.", type.Name);
                throw new ArgumentException(typeException);
		    }

		    if (jTokens.Count() == 1)
		    {
			    var operand0 = GetTokenValue(type, jTokens.ElementAt(0));

                return new List<IExpressionToken>
                {
                    new ValueToken(operand0)
                };
            }

		    if (jTokens.Count() == 3)
		    {
                var operand0 = GetTokenValue(type, jTokens.ElementAt(0));
				var operand1 = GetTokenValue(type, jTokens.ElementAt(2));
				var operatorString = GetTokenValue(typeof(string), jTokens.ElementAt(1));

				return new List<IExpressionToken>
                {
                    new ValueToken(operand0),
                    new ValueToken(operand1),
                    _operatorTokenMap.GetOperator((string) operatorString)
                };
            }

		    var arraySizeException = "Expression array is of wrong size: Expecting 1 or 3. Shit!";
            throw new ArgumentException(arraySizeException);
		}

		private object GetTokenValue(Type type, JToken token)
		{
			return type == typeof(Vector3) && token.Type == JTokenType.Object ?
				GetVector3TokenValue(token) :
				_primitiveTokenParser.Parse(type, token);
		}

		private object GetVector3TokenValue(JToken token)
		{
			var x = GetTokenValue(typeof(float), token["x"]);
			var y = GetTokenValue(typeof(float), token["y"]);
			var z = GetTokenValue(typeof(float), token["z"]);

			return new Vector3
			(
				Convert.ToSingle(x),
				Convert.ToSingle(y),
				Convert.ToSingle(z)
			);
		}
    }
}
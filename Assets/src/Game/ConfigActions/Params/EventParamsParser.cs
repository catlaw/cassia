﻿using System;
using System.Linq;
using Game.ConfigActions.GameObjects;
using Newtonsoft.Json.Linq;

namespace Game.ConfigActions.Params
{
	public class EventParamsParser : IEventParamsParser
	{
        private readonly IEventPropertyGetter _propertyGetter;
	    private readonly IGameObjectIdFactory _gameObjectIdFactory;

        public EventParamsParser(
            IEventPropertyGetter propertyGetter, 
            IGameObjectIdFactory gameObjectIdFactory)
        {
            _propertyGetter = propertyGetter;
            _gameObjectIdFactory = gameObjectIdFactory;
        }

	    public object ParseVariable(JToken jToken)
		{
			if (jToken == null) throw new ArgumentNullException("jToken");

			return _propertyGetter.GetPropertyValue(jToken.Value<string>().Substring(1));
		}

	    public bool IsVariable(JToken jToken)
	    {
            if (jToken == null) throw new ArgumentNullException("jToken");

	        return jToken.Type == JTokenType.String && jToken.Value<string>().First() == '@';
	    }

	    public IGameObjectId ParseConfigGameObjectId(JToken jToken)
	    {
            if (jToken == null) throw new ArgumentNullException("jToken");

	        var id = jToken.Value<string>();
	        var gameEvent = _propertyGetter.GetEvent();
	        return _gameObjectIdFactory.CreateId(gameEvent, id);
	    }
	}
}
﻿namespace Game.ConfigActions.Params
{
	public interface IMethodParamsFactory
	{
		TMethodParams CreateMethodParams<TMethodParams>(IGameEvent gameEvent)
			where TMethodParams : new();
	}
}
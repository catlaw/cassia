﻿using System;
using Newtonsoft.Json.Linq;

namespace Game.ConfigActions.Params
{
	public interface IPrimitiveTokenParser
	{
		object Parse(Type type, JToken jToken);
	}
}
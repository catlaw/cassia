﻿using Game.Controllers.Units;
using Game.Models.Occupants.Units;
using Game.Models.Occupants.Units.UnitActions;

namespace Game.ConfigActions.Params
{
    public interface IActionEffectedUnit
    {
        IActionEffect ActionEffect { get; }
        IAffectedUnit AffectedUnit { get; }
    }
}
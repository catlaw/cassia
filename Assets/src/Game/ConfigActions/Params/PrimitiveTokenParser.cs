﻿using System;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Game.ConfigActions.Params
{
	public class PrimitiveTokenParser : IPrimitiveTokenParser
	{
		private readonly IEventParamsParser _eventParamsParser;

		public PrimitiveTokenParser(IEventParamsParser eventParamsParser)
		{
			_eventParamsParser = eventParamsParser;
		}

		public object Parse(Type type, JToken jToken)
		{
            if (type == null) throw new ArgumentNullException("type");
		    if (jToken == null) return Activator.CreateInstance(type);

			if (!type.IsValueType && type != typeof(string))
			{
				var message = string.Format("Could not parse token: Type '{0}' is not a supported type.",
					type.Name);
				throw new ArgumentException(message);
			}

            if (_eventParamsParser.IsVariable(jToken))
                return _eventParamsParser.ParseVariable(jToken);

            var reader = jToken.CreateReader();
			var serializer = new JsonSerializer();

			try
			{
				return serializer.Deserialize(reader, type);
			}
			catch (JsonSerializationException)
			{
				var message = string.Format("Could not parse token: Failed to convert '{0}' to type '{1}'.",
					jToken, type.Name);
				throw new ArgumentException(message);
			}
		}
	}
}
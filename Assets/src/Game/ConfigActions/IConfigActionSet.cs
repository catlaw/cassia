﻿using Game.ConfigActions.Methods;

namespace Game.ConfigActions
{
	public interface IConfigActionSet
	{
		void Add(UnitActionEventType unitActionEventType, IConfigAction configAction);
		IConfigAction GetConfigAction(UnitActionEventType unitActionEventType);
	}
}
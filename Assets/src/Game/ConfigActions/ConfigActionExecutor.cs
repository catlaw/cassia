﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Common.Util;
using Game.ConfigActions.Params;
using UnityEngine;

namespace Game.ConfigActions
{
	public class ConfigActionExecutor : IConfigActionExecutor
	{
		private readonly ICoroutineRunner _coroutineRunner;

		public ConfigActionExecutor(ICoroutineRunner coroutineRunner)
		{
			_coroutineRunner = coroutineRunner;
		}

		public IEnumerator CoExecute(IConfigAction configAction, IGameEvent gameEvent)
		{
			if (configAction == null) throw new ArgumentNullException("configAction");
			if (gameEvent == null) throw new ArgumentNullException("gameEvent");

			if (configAction.IsInstant(gameEvent))
				_coroutineRunner.StartCoroutine(configAction.CoExecute(gameEvent));
			else
				yield return _coroutineRunner.StartCoroutine(configAction.CoExecute(gameEvent));

			if (!configAction.Next.Any()) yield break;

			var observers = configAction.Next
				.Select(x => new CoroutineObserver(_coroutineRunner, CoExecute(x, gameEvent))).ToList();

			while (observers.Any(x => x.IsRunning))
			{
				yield return null;
			}
		}

		private class CoroutineObserver
		{
			private readonly ICoroutineRunner _runner;

			public CoroutineObserver(ICoroutineRunner runner, IEnumerator iEnumerator)
			{
				_runner = runner;
				IsRunning = true;
				_runner.StartCoroutine(CoObserve(iEnumerator));
			}

			public bool IsRunning { get; private set; }

			private IEnumerator CoObserve(IEnumerator iEnumerator)
			{
				yield return _runner.StartCoroutine(iEnumerator);
				IsRunning = false;
			}
		}
	}
}
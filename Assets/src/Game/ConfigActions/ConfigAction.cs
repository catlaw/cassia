﻿using System.Collections;
using System.Collections.Generic;
using Game.ConfigActions.Methods;
using Game.ConfigActions.Params;

namespace Game.ConfigActions
{
	public class ConfigAction : IConfigAction
	{
		private readonly IConfigActionMethod _method;
		private readonly IMethodParamsFactory _methodParamsFactory;

		public ConfigAction(IConfigActionMethod method, IMethodParamsFactory methodParamsFactory,
			IEnumerable<IConfigAction> next)
		{
			_method = method;
			_methodParamsFactory = methodParamsFactory;
			Next = next;
		}

		public IEnumerator CoExecute(IGameEvent gameEvent)
		{
			return _method.CoExecute(gameEvent, _methodParamsFactory);
		}

		public bool IsInstant(IGameEvent gameEvent)
		{
			return _method.IsInstant(gameEvent, _methodParamsFactory);
		}

		public IEnumerable<IConfigAction> Next { get; private set; }
	}
}
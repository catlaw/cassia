﻿namespace Game.ConfigActions.Methods
{
	public enum ConfigActionMethodType
	{
		Create,
		MoveTo,
	    MovePath,
	    Wait,
	    Destroy,
	    PlayUnitAnimation,
	    SetText
	}
}
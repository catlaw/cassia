﻿using System.Collections;
using Common.Util;

namespace Game.ConfigActions.Methods
{
    public class WaitMethod : Method<WaitParams>
    {
        private readonly ICoroutineRunner _runner;

        public WaitMethod(ICoroutineRunner runner)
        {
            _runner = runner;
        }

        public override IEnumerator CoExecute(WaitParams methodParams)
        {
            var elapsed = 0f;
            while (elapsed < methodParams.Duration)
            {
                elapsed += _runner.DeltaTime;
                yield return null;
            }
        }

        public override bool IsInstant(WaitParams methodParams)
        {
            return methodParams.Duration <= 0;
        }
    }

    public class WaitParams
    {
        public float Duration { get; set; }
    }
}
using System.Collections;
using Game.ConfigActions.Params;

namespace Game.ConfigActions.Methods
{
	public abstract class Method<TMethodParams> : IConfigActionMethod where TMethodParams : new()
	{
		public IEnumerator CoExecute(IGameEvent gameEvent, IMethodParamsFactory methodParamsFactory)
		{
			var methodParams = methodParamsFactory.CreateMethodParams<TMethodParams>(gameEvent);
			return CoExecute(methodParams);
		}

		public bool IsInstant(IGameEvent gameEvent, IMethodParamsFactory methodParamsFactory)
		{
			var methodParams = methodParamsFactory.CreateMethodParams<TMethodParams>(gameEvent);
			return IsInstant(methodParams);
		}

		// these are public for testing. make it better later
		public abstract IEnumerator CoExecute(TMethodParams methodParams);
		public abstract bool IsInstant(TMethodParams methodParams);
	}
}
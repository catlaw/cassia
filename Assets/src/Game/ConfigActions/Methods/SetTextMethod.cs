﻿using System.Collections;
using Common.Util;
using Game.ConfigActions.GameObjects;
using TMPro;

namespace Game.ConfigActions.Methods
{
    public class SetTextMethod : Method<SetTextParams>
    {
        private readonly IConfigGameObjects _configGameObjects;
        private readonly ILoggyLog _loggyLog;

        public SetTextMethod(
            IConfigGameObjects configGameObjects, 
            ILoggyLog loggyLog)
        {
            _configGameObjects = configGameObjects;
            _loggyLog = loggyLog;
        }

        public override IEnumerator CoExecute(SetTextParams methodParams)
        {
            var gameObject = _configGameObjects.GetGameObject(methodParams.GameObjectId);
            var target = gameObject.transform.Find(methodParams.GameObjectPath);
            var textMeshPro = target.GetComponent<TextMeshPro>();

            if (textMeshPro == null)
            {
                var message = string.Format("Cannot SetText on GameObject with GameObjectId '{0}'" +
                                            " because no TextMeshPro component found.", methodParams.GameObjectId.Id);
                _loggyLog.LogError(message);
                yield break;
            }

            textMeshPro.text = methodParams.Text;
        }

        public override bool IsInstant(SetTextParams methodParams)
        {
            return true;
        }
    }

    public class SetTextParams
    {
        public IGameObjectId GameObjectId { get; set; }
        public string GameObjectPath { get; set; }
        public string Text { get; set; }
    }
}
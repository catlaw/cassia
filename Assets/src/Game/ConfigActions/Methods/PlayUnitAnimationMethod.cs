﻿using System.Collections;
using Common.Util;
using Game.Models.Occupants.Units;
using Game.Views.Animations;

namespace Game.ConfigActions.Methods
{
    public class PlayUnitAnimationMethod : Method<PlayUnitAnimationParams>
    {
        private readonly IPlayableUnitAnimationSelector _animationSelector;
        private readonly ICoroutineRunner _runner;

        public PlayUnitAnimationMethod(
            IPlayableUnitAnimationSelector animationSelector,
            ICoroutineRunner runner)
        {
            _animationSelector = animationSelector;
            _runner = runner;
        }

        public override IEnumerator CoExecute(PlayUnitAnimationParams methodParams)
        {
            var playableAnimation = _animationSelector.GetAnimation(methodParams.Unit,
                methodParams.AnimationType, methodParams.PowerTier);
            methodParams.Unit.TriggerUnitAnimationViewSignal.Dispatch(playableAnimation);

            var elapsed = 0f;
            while (elapsed <= playableAnimation.HitDelay)
            {
                elapsed += _runner.DeltaTime;
                yield return null;
            }
        }

        public override bool IsInstant(PlayUnitAnimationParams methodParams)
        {
            var playableAnimation = _animationSelector.GetAnimation(methodParams.Unit,
                methodParams.AnimationType, methodParams.PowerTier);
            return playableAnimation.HitDelay <= 0;
        }
    }

    public class PlayUnitAnimationParams
    {
        public IUnit Unit { get; set; }
        public AnimationType AnimationType { get; set; }
        public int PowerTier { get; set; }
    }
}
﻿using System.Collections;
using Game.ConfigActions.Params;

namespace Game.ConfigActions.Methods
{
	public interface IConfigActionMethod
	{
		IEnumerator CoExecute(IGameEvent gameEvent, IMethodParamsFactory methodParamsFactory);
		bool IsInstant(IGameEvent gameEvent, IMethodParamsFactory methodParamsFactory);
	}
}
﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using Game.ConfigActions.GameObjects;
using Game.ConfigActions.Tweening;
using Game.ConfigActions.Util;
using UnityEngine;

namespace Game.ConfigActions.Methods
{
	public class MovePathMethod : Method<MovePathParams>
	{
        private readonly IConfigGameObjects _configGameObjects;
        private readonly IDurationCalculator _durationCalculator;
        private readonly IPathWaypointsCalculator _pathWaypointsCalculator;
        private readonly ITweenFactory _tweenFactory;

        public MovePathMethod(IConfigGameObjects configGameObjects,
            IPathWaypointsCalculator pathWaypointsCalculator,
            IDurationCalculator durationCalculator,
            ITweenFactory tweenFactory)
        {
            _configGameObjects = configGameObjects;
            _pathWaypointsCalculator = pathWaypointsCalculator;
            _durationCalculator = durationCalculator;
            _tweenFactory = tweenFactory;
        }

        public override IEnumerator CoExecute(MovePathParams methodParams)
        {
            var target = _configGameObjects.GetGameObject(methodParams.Id);
            var origin = target.transform.position;
            var path = _pathWaypointsCalculator.GetPathWaypoints(origin,
                methodParams.Position, methodParams.ParametricWaypoints);
            var duration = _durationCalculator.GetDuration(new List<Vector3> {origin}.Concat(path),
                methodParams.Speed);

            var tween = _tweenFactory.GetPathTween(target.transform, path, duration, 
                methodParams.PathType);
            tween.SetEase(methodParams.Ease);

            yield return tween.WaitForCompletion();
        }

		public override bool IsInstant(MovePathParams methodParams)
		{
		    return false;
		}
	}

	public class MovePathParams
	{
	    public MovePathParams()
	    {
	        PathType = PathType.CatmullRom;
	    }

		public IGameObjectId Id { get; set; }
		public IEnumerable<ParametricPoint> ParametricWaypoints { get; set; }
		public float Speed { get; set; }
		public Ease Ease { get; set; }
        public PathType PathType { get; set; }
	    public Vector3 Position { get; set; }
	}

    public class ParametricPoint
    {
        public float Parameter { get; set; }
        public Vector3 Offset { get; set; }
    }
}
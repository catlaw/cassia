﻿namespace Game.ConfigActions.Methods
{
	public enum UnitActionEventType
	{
		PreHit,
		Hit
	}
}
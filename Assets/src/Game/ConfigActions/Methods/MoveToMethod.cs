﻿using System;
using System.Collections;
using DG.Tweening;
using Game.ConfigActions.GameObjects;
using Game.ConfigActions.Tweening;
using Game.ConfigActions.Util;
using UnityEngine;

namespace Game.ConfigActions.Methods
{
	public class MoveToMethod : Method<MoveToParams>
	{
		private readonly IConfigGameObjects _configGameObjects;
		private readonly IDoDelegateFactory _delegateFactory;
		private readonly IDurationCalculator _durationCalculator;
		private readonly ITweenFactory _tweenFactory;

		public MoveToMethod(ITweenFactory tweenFactory, 
			IDoDelegateFactory delegateFactory, 
			IConfigGameObjects configGameObjects, 
			IDurationCalculator durationCalculator)
		{
			_tweenFactory = tweenFactory;
			_delegateFactory = delegateFactory;
			_configGameObjects = configGameObjects;
			_durationCalculator = durationCalculator;
		}

		public override IEnumerator CoExecute(MoveToParams methodParams)
		{
			var gameObject = _configGameObjects.GetGameObject(methodParams.GameObjectId);
			var origin = gameObject.transform.position;
			var destination = methodParams.Position;

			var duration = _durationCalculator.GetDuration(origin, destination, methodParams.Speed);

			var getter = _delegateFactory.GetPositionGetter(gameObject.transform);
			var setter = _delegateFactory.GetPositionSetter(gameObject.transform);
			var tween = _tweenFactory.GetTween(getter, setter, destination, duration);

			tween.SetEase(methodParams.Ease);
			yield return tween.WaitForCompletion();
		}

		public override bool IsInstant(MoveToParams methodParams)
		{
			return false;
		}
	}

	public class MoveToParams
	{
		public IGameObjectId GameObjectId { get; set; }
		public Vector3 Position { get; set; }
		public float Speed { get; set; }
		public Ease Ease { get; set; }
	}
}
﻿using System.Collections;
using Configs;
using Game.ConfigActions.GameObjects;
using Instantiator;
using UnityEngine;

namespace Game.ConfigActions.Methods
{
	public class CreateMethod : Method<CreateParams>
	{
		private readonly IConfigActionGameObjectFactory _gameObjectFactory;

		public CreateMethod(IConfigActionGameObjectFactory gameObjectFactory)
		{
			_gameObjectFactory = gameObjectFactory;
		}

		public override IEnumerator CoExecute(CreateParams methodParams)
		{
			var go = methodParams.GameObjectId != null
				? _gameObjectFactory.Instantiate(methodParams.GameObjectId, methodParams.Asset)
				: _gameObjectFactory.Instantiate(methodParams.Asset);
			go.transform.position = methodParams.Position + methodParams.Offset;
			go.transform.localScale = methodParams.Scale;

			yield break;
		}

		public override bool IsInstant(CreateParams methodParams)
		{
			return true;
		}
	}

	public class CreateParams
	{
		public CreateParams()
		{
			Scale = new Vector3(1, 1, 1);
		}

		public IGameObjectId GameObjectId { get; set; }
		public AssetConfig Asset { get; set; }
		public Vector3 Position { get; set; }
		public Vector3 Offset { get; set; }
		public Vector3 Scale { get; set; }
	}
}
﻿using System.Collections;
using System.Collections.Generic;
using Game.ConfigActions.Params;

namespace Game.ConfigActions
{
	public class EmptyConfigAction : IConfigAction
	{
		public IEnumerator CoExecute(IGameEvent gameEvent)
		{
			yield break;
		}

		public bool IsInstant(IGameEvent gameEvent)
		{
			return true;
		}

		public IEnumerable<IConfigAction> Next
		{
			get { return new List<IConfigAction>(); }
		}
	}
}
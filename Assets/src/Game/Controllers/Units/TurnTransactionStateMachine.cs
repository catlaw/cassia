﻿using System.Collections.Generic;
using System.Linq;
using Common.DataContainers;
using Common.StateMachine;
using Game.Inputs;
using Game.Inputs.EventSignals;
using Game.Models.Stage;
using Game.Models.TurnTransactions;
using Game.Signals.CommandSignals;
using Game.Signals.ControllerSignals;
using Game.Signals.ViewSignals;

namespace Game.Controllers.Units
{
	public class TurnTransactionStateMachine : ITurnTransactionStateMachine
	{
		private StateMachine<TransactionState> _stateMachine;

		[Inject]
		public IUnitEventSignal UnitEventSignal { get; set; }

		[Inject]
		public IMapEventSignal MapEventSignal { get; set; }

		[Inject]
		public IMoveUnitViewCompleteSignal MoveUnitViewCompleteSignal { get; set; }

		[Inject]
		public IButtonEventSignal ButtonEventSignal { get; set; }

		[Inject]
		public IApplyTurnTransactionSignal ApplyTurnTransactionSignal { get; set; }

		[Inject]
		public ITransactionAnimationCompleteSignal TransactionAnimationCompleteSignal { get; set; }

		[Inject]
		public ITurnTransaction TurnTransaction { get; set; }

		[Inject]
		public IUpdateGameViewSignal UpdateGameViewSignal { get; set; }

		[Inject]
		public IDirectionCalculator DirectionCalculator { get; set; }

		[Inject]
		public ISetHighlightedSquaresSignal SetHighlightedSquaresSignal { get; set; }

        [Inject]
        public ISetAreaOfEffectSignal SetAreaOfEffectSignal { get; set; }

		[Inject]
		public ISelectableSquares SelectableSquares { get; set; }

		public TransactionState CurrentState
		{
			get { return _stateMachine.CurrentState; }
		}

		public void Init(TransactionState initialState)
		{
			_stateMachine = new StateMachine<TransactionState>();
			_stateMachine.Init(initialState);

			InitializeNothingState();
			InitializeActorSelectedState();
			InitializeActorMovingState();
			InitializePreSelectUnitActionState();
			InitializePreSelectTargetState();
			InitializePreApplyTransactionState();
			InitializePlayTransactionAnimationState();

			_stateMachine.Start();
		}

		private void InitializeNothingState()
		{
			_stateMachine.In(TransactionState.Nothing)
				.On(UnitEventSignal)
				.GoTo(TransactionState.ActorSelected)
				.Execute((actor, actorPosition) =>
				{
					TurnTransaction.SelectActor(actor, actorPosition);
					SetHighlightedSquaresSignal.Dispatch(actor.GetMovementRange());

					UpdateGameViewSignal.Dispatch();
				});
		}

		private void InitializeActorSelectedState()
		{
			_stateMachine.In(TransactionState.ActorSelected).On(MapEventSignal)
				.If(movePosition => SelectableSquares.Get().Contains(movePosition)).GoTo(TransactionState.Moving).Execute(
					movePosition =>
					{
						TurnTransaction.SelectMovePosition(movePosition);
						var path = TurnTransaction.Actor.GetPathTo(movePosition);
						TurnTransaction.Actor.MoveUnitViewSignal.Dispatch(path);
						SetHighlightedSquaresSignal.Dispatch(new List<GridCoords>());

						UpdateGameViewSignal.Dispatch();
					})
				.GoTo(TransactionState.Nothing).Execute(
					x =>
					{
						SetHighlightedSquaresSignal.Dispatch(new List<GridCoords>());

						UpdateGameViewSignal.Dispatch();
					});

			_stateMachine.In(TransactionState.ActorSelected).On(UnitEventSignal)
				.If((actor, actorPosition) => TurnTransaction.Actor == actor)
				.GoTo(TransactionState.PreSelectAction).Execute((actor, actorPosition) =>
				{
					TurnTransaction.SelectMovePosition(actorPosition);
					SetHighlightedSquaresSignal.Dispatch(new List<GridCoords>());

					UpdateGameViewSignal.Dispatch();
				})
				.GoTo(TransactionState.ActorSelected).Execute((actor, actorPosition) =>
				{
					TurnTransaction.SelectActor(actor, actorPosition);
					SetHighlightedSquaresSignal.Dispatch(actor.GetMovementRange());

					UpdateGameViewSignal.Dispatch();
				});
		}

		private void InitializeActorMovingState()
		{
			_stateMachine.In(TransactionState.Moving).On(MoveUnitViewCompleteSignal)
				.If(actor => TurnTransaction.Actor == actor).GoTo(TransactionState.PreSelectAction)
				.Execute(actor => { UpdateGameViewSignal.Dispatch(); });

			// TODO can also press unit
			_stateMachine.In(TransactionState.Moving).On(MapEventSignal)
				.GoTo(TransactionState.PreSelectAction)
				.Execute(position =>
				{
					var facing = DirectionCalculator.GetFacing(TurnTransaction.StartPosition, TurnTransaction.MovePosition);
					var finalPosition = new GridPosition(TurnTransaction.MovePosition, facing);

					TurnTransaction.Actor.SetUnitPositionViewSignal.Dispatch(finalPosition);
					UpdateGameViewSignal.Dispatch();
				});
		}

		private void InitializePreSelectUnitActionState()
		{
			_stateMachine.In(TransactionState.PreSelectAction)
				.On(ButtonEventSignal)
				.If((buttonType, index) => buttonType == ButtonType.UnitAction)
				.GoTo(TransactionState.PreSelectTarget)
				.Execute(
					(buttonType, index) =>
					{
						var unitAction = TurnTransaction.Actor.GetUnitAction(index);
						TurnTransaction.SelectUnitAction(unitAction);
						SetHighlightedSquaresSignal.Dispatch(unitAction.GetTargetRange(TurnTransaction.MovePosition));
						UpdateGameViewSignal.Dispatch();
					});

			_stateMachine.In(TransactionState.PreSelectAction).On(MapEventSignal).GoTo(TransactionState.ActorSelected).Execute(
				position =>
				{
					var actor = TurnTransaction.Actor;
					var startPosition = new GridPosition(TurnTransaction.StartPosition, actor.Facing);

					actor.SetUnitPositionViewSignal.Dispatch(startPosition);
					SetHighlightedSquaresSignal.Dispatch(actor.GetMovementRange());
					UpdateGameViewSignal.Dispatch();
				});

			// TODO if you click another unit, its also cancel?
		}

		private void InitializePreSelectTargetState()
		{
			// TODO dont let you go into there if you dont do anything but you should be allowed to wait
			// TODO i need a MAP SELECTABLE bool in the targeter i think.
			_stateMachine.In(TransactionState.PreSelectTarget).On(UnitEventSignal)
				.If((targetUnit, targetPosition) => SelectableSquares.Get().Contains(targetPosition))
				.GoTo(TransactionState.PreApplyTransaction)
				.Execute(
					(targetUnit, targetPosition) =>
					{
						// TODO: i need to disable the colliders of all the units not within range
						SelectTargetPosition(targetPosition);
					}).GoTo(TransactionState.PreSelectAction).Execute(
						(actor, actorPosition) =>
						{
							SetHighlightedSquaresSignal.Dispatch(new List<GridCoords>());
							UpdateGameViewSignal.Dispatch();
						});

			// TODO: for now, only wait lets you target map lol. not true
			_stateMachine.In(TransactionState.PreSelectTarget).On(MapEventSignal)
				.If(targetPosition => SelectableSquares.Get().Contains(targetPosition) && !TurnTransaction.UnitAction.HasEffect)
				.GoTo(TransactionState.PreApplyTransaction)
				.Execute(SelectTargetPosition)
				.If(targetPosition => !SelectableSquares.Get().Contains(targetPosition))
				.GoTo(TransactionState.PreSelectAction)
				.Execute(
					position =>
					{
                        SetHighlightedSquaresSignal.Dispatch(new List<GridCoords>());
						UpdateGameViewSignal.Dispatch();
					});
		}

		private void SelectTargetPosition(GridCoords targetPosition)
		{
			TurnTransaction.SelectTargetPosition(targetPosition);
			var movePosition = TurnTransaction.MovePosition;
			var facing = DirectionCalculator.GetFacing(movePosition, targetPosition);
			TurnTransaction.SelectFacing(facing);

			TurnTransaction.Actor.SetUnitPositionViewSignal.Dispatch(new GridPosition(movePosition, facing));
            SetAreaOfEffectSignal.Dispatch(TurnTransaction.Outcome.AreaOfEffect); 
			UpdateGameViewSignal.Dispatch();
		}

		private void InitializePreApplyTransactionState()
		{
			_stateMachine.In(TransactionState.PreApplyTransaction).On(ButtonEventSignal)
				.If((buttonType, index) => buttonType == ButtonType.ApplyTransaction)
				.GoTo(TransactionState.PlayTransactionAnimation).Execute(
					(buttonType, index) =>
					{
						ApplyTurnTransactionSignal.Dispatch();
                        SetAreaOfEffectSignal.Dispatch(new List<GridCoords>());
                        SetHighlightedSquaresSignal.Dispatch(new List<GridCoords>());
						UpdateGameViewSignal.Dispatch();
					});

			// TODO also trigger unit signal
			_stateMachine.In(TransactionState.PreApplyTransaction)
				.On(MapEventSignal)
				.GoTo(TransactionState.PreSelectTarget)
				.Execute(
					position =>
					{
						var unitAction = TurnTransaction.UnitAction;
						var actorPosition = TurnTransaction.MovePosition;

                        SetAreaOfEffectSignal.Dispatch(new List<GridCoords>());
                        SetHighlightedSquaresSignal.Dispatch(unitAction.GetTargetRange(actorPosition));
						UpdateGameViewSignal.Dispatch();
					});
		}

		private void InitializePlayTransactionAnimationState()
		{
			_stateMachine.In(TransactionState.PlayTransactionAnimation)
				.On(TransactionAnimationCompleteSignal)
				.GoTo(TransactionState.Nothing)
				.Execute(
				    () =>
				    {
				        TurnTransaction.Clear();
                        UpdateGameViewSignal.Dispatch();
				    });
		}
	}
}
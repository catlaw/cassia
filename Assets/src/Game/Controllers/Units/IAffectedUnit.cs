﻿using Common.DataContainers;
using Game.Models.Occupants.Units;

namespace Game.Controllers.Units
{
    public interface IAffectedUnit
    {
        IUnit Unit { get; }
        GridCoords GridCoords { get; }
    }
}
﻿namespace Game.Controllers.Units
{
	public interface IAffectedActorFactory
	{
		IAffectedUnit CreateAffectedActor();
	}
}
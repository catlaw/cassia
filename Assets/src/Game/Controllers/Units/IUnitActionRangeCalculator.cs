﻿using System.Collections.Generic;
using Common.DataContainers;

namespace Game.Controllers.Units
{
	public interface IUnitActionRangeCalculator
	{
		IEnumerable<GridCoords> GetAreaOfEffect(GridCoords target, Facing direction,
			IEnumerable<GridCoordsOffset> directionalOffsets);

		IEnumerable<GridCoords> GetRotatedRange(GridCoords gridCoords, IEnumerable<GridCoordsOffset> directionalOffsets);
	}
}
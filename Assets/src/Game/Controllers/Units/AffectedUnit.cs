using Common.DataContainers;
using Game.Models.Occupants.Units;

namespace Game.Controllers.Units
{
    public class AffectedUnit : IAffectedUnit
    {
        public IUnit Unit { get; set; }
        public GridCoords GridCoords { get; set; }
    }
}
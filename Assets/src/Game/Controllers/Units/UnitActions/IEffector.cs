﻿using System.Collections.Generic;
using Configs;
using Game.Models.Occupants.Units.UnitActions;

namespace Game.Controllers.Units.UnitActions
{
	public interface IEffector
	{
		bool HasEffect { get; }
		IEnumerable<IPotentialActionEffectBuilder> SelfEffectBuilders { get; }
		IEnumerable<IPotentialActionEffectBuilder> TargetEffectBuilders { get; }

		void LoadConfig(IUnitAction unitAction, EffectorConfig config);
	}
}
﻿using System.Collections.Generic;
using Common.DataContainers;
using Configs;

namespace Game.Controllers.Units.UnitActions
{
	public interface ITargeter
	{
		bool CanTarget { get; }
		IEnumerable<GridCoordsOffset> TargetRange { get; }
		IEnumerable<GridCoordsOffset> AreaOfEffect { get; }
		void LoadConfig(TargeterConfig config);
		IEnumerable<GridCoords> GetAreaOfEffect(GridCoords targetPosition, Facing facing);
		IEnumerable<GridCoords> GetTargetRange(GridCoords targetPosition);
	}
}
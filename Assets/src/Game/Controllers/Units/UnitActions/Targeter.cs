﻿using System.Collections.Generic;
using Common.DataContainers;
using Configs;

namespace Game.Controllers.Units.UnitActions
{
	public class Targeter : ITargeter
	{
		private TargeterConfig _config;

		[Inject]
		public IUnitActionRangeCalculator RangeCalculator { get; set; }

		public void LoadConfig(TargeterConfig config)
		{
			_config = config;
		}

		public IEnumerable<GridCoordsOffset> TargetRange
		{
			get { return _config.TargetRange; }
		}

		public IEnumerable<GridCoordsOffset> AreaOfEffect
		{
			get { return _config.AreaOfEffect; }
		}

		public bool CanTarget
		{
			get { return _config != null; }
		}

		public IEnumerable<GridCoords> GetTargetRange(GridCoords targetPosition)
		{
			return RangeCalculator.GetRotatedRange(targetPosition, TargetRange);
		}

		public IEnumerable<GridCoords> GetAreaOfEffect(GridCoords targetPosition, Facing facing)
		{
			return RangeCalculator.GetAreaOfEffect(targetPosition, facing, AreaOfEffect);
		}
	}
}
﻿using System;
using System.Collections.Generic;
using Configs;
using Game.Models.Occupants.Units.UnitActions;

namespace Game.Controllers.Units.UnitActions
{
	public class Effector : IEffector
	{
		private EffectorConfig _config;
		private IUnitAction _unitAction;

		[Inject]
		public IActionEffectBuildersFactory EffectBuildersFactory { get; set; }

		public void LoadConfig(IUnitAction unitAction, EffectorConfig config)
		{
			if (unitAction == null) throw new ArgumentNullException("unitAction");

			_unitAction = unitAction;
			_config = config;
		}

		public IEnumerable<IPotentialActionEffectBuilder> TargetEffectBuilders
		{
			get
			{
				if (!HasEffect) return new List<IPotentialActionEffectBuilder>();
				return EffectBuildersFactory.GetEffectBuilders(_unitAction, _config.TargetEffects);
			}
		}

		public IEnumerable<IPotentialActionEffectBuilder> SelfEffectBuilders
		{
			get
			{
				if (!HasEffect) return new List<IPotentialActionEffectBuilder>();
				return EffectBuildersFactory.GetEffectBuilders(_unitAction, _config.ActorEffects);
			}
		}

		public bool HasEffect
		{
			get { return _config != null; }
		}
	}
}
﻿namespace Game.Controllers.Units
{
	public enum TransactionState
	{
		Nothing,
		ActorSelected,
		Moving,
		PreSelectAction,
		PreSelectTarget,
		PreApplyTransaction,
		PlayTransactionAnimation
	}
}
﻿namespace Game.Controllers.Units
{
	public interface ITurnTransactionStateMachine
	{
		TransactionState CurrentState { get; }
		void Init(TransactionState initialState);
	}
}
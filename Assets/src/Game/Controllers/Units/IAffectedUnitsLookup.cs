﻿using System.Collections.Generic;
using Common.DataContainers;
using Game.Models.Occupants.Units;

namespace Game.Controllers.Units
{
	public interface IAffectedUnitsLookup
	{
		IEnumerable<IAffectedUnit> GetUnits(IEnumerable<GridCoords> area);
	}
}
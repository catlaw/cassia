﻿using System;
using Game.Models.TurnTransactions;

namespace Game.Controllers.Units
{
	public class AffectedActorFactory : IAffectedActorFactory
	{
		private readonly ITurnTransaction _turnTransaction;

		public AffectedActorFactory(ITurnTransaction turnTransaction)
		{
			_turnTransaction = turnTransaction;
		}

		public IAffectedUnit CreateAffectedActor()
		{
			return new AffectedUnit
			{
				Unit = _turnTransaction.Actor,
				GridCoords = _turnTransaction.MovePosition
			};
		}
	}
}
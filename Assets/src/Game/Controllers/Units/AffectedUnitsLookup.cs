﻿using System.Collections.Generic;
using Common.DataContainers;
using Game.Models.Occupants.Units;
using Game.Models.TurnTransactions;

namespace Game.Controllers.Units
{
	public class AffectedUnitsLookup : IAffectedUnitsLookup
	{
		[Inject]
		public ITransactionalUnitMap TransactionalUnitMap { get; set; }

		public IEnumerable<IAffectedUnit> GetUnits(IEnumerable<GridCoords> area)
		{
			var affectedUnits = new List<IAffectedUnit>();

			foreach (var gridCoords in area)
			{
				var unitState = TransactionalUnitMap.GetUnitAt(gridCoords);
				if (unitState.Unit != EmptyUnit.None)
				{
				    var affectedUnit = new AffectedUnit
				    {
				        Unit = unitState.Unit,
				        GridCoords = gridCoords
				    };
					affectedUnits.Add(affectedUnit);
				}
			}

			return affectedUnits;
		}
	}
}
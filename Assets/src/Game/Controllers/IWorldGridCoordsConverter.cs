﻿using Common.DataContainers;
using UnityEngine;

namespace Game.Controllers
{
	public interface IWorldGridCoordsConverter
	{
		Vector3 GetClosestWorldPositionCenter(Vector3 worldPosition);
		GridCoords GetGridCoords(Vector3 worldPosition);
		Vector3 GetWorldPosition(GridCoords gridCoords);
		Quaternion GetRotation(Facing facing);
	}
}
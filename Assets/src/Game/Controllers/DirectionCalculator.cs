﻿using Common.DataContainers;
using UnityEngine;

namespace Game.Controllers
{
	public class DirectionCalculator : IDirectionCalculator
	{
		public Facing GetFacing(GridCoords from, GridCoords to)
		{
			var rowDifference = to.Row - from.Row;
			var columnDifference = to.Column - from.Column;

			if (Mathf.Abs(rowDifference) > Mathf.Abs(columnDifference))
			{
				return rowDifference >= 0 ? Facing.Right : Facing.Left;
			}
			return columnDifference >= 0 ? Facing.Forward : Facing.Back;
		}
	}
}
﻿using Game.Models.Occupants.Units.UnitActions;

namespace Game.Controllers
{
	public interface IActionOutcomeFactory
	{
		IActionOutcome GetActionOutcome(IPotentialActionOutcome potentialActionOutcome);
	}
}
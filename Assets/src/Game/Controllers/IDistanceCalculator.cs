﻿using Common.DataContainers;

namespace Game.Controllers
{
	public interface IDistanceCalculator
	{
		float GetDistanceBetween(GridCoords first, GridCoords second);
	}
}
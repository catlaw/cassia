﻿using System;
using Common.DataContainers;
using Configs;
using Instantiator.Stage.Models.MeshFactory;
using UnityEngine;

namespace Game.Controllers
{
	public class WorldGridCoordsConverter : IWorldGridCoordsConverter
	{
		private readonly IConfig<LevelConfig> _levelConfig;
		private readonly IConfig<MapViewConfig> _mapViewConfig;

		public WorldGridCoordsConverter(IConfig<LevelConfig> levelConfig, IConfig<MapViewConfig> mapViewConfig)
		{
			_levelConfig = levelConfig;
			_mapViewConfig = mapViewConfig;
		}

		private float Width
		{
			get { return _levelConfig.Get().MapHeights.GetLength(0); }
		}

		private float Depth
		{
			get { return _levelConfig.Get().MapHeights.GetLength(1); }
		}

		public GridCoords GetGridCoords(Vector3 worldPosition)
		{
			var mapOffset = GetMapOffset(_levelConfig.Get().MapHeights, _mapViewConfig.Get());
			var chunkSize = _mapViewConfig.Get().ChunkSize;

			var maxRow = Width - 1;
			var row = Mathf.Clamp((worldPosition.x - mapOffset.x)/chunkSize, 0, maxRow);

			var maxColumn = Depth - 1;
			var column = Mathf.Clamp((worldPosition.z - mapOffset.z)/chunkSize, 0, maxColumn);

			return new GridCoords(Mathf.FloorToInt(row), Mathf.FloorToInt(column));
		}

		public Vector3 GetWorldPosition(GridCoords gridCoords)
		{
			if (gridCoords.Row < 0 || gridCoords.Row >= Width || gridCoords.Column < 0 || gridCoords.Column >= Depth)
			{
				var message = string.Format("Cannot get world position for {0}.  Map is of size ({1}, {2}).",
					gridCoords, Width, Depth);
				throw new ArgumentException(message);
			}

			var mapOffset = GetMapOffset(_levelConfig.Get().MapHeights, _mapViewConfig.Get());
			var chunkSize = _mapViewConfig.Get().ChunkSize;
			var chunkHeight = _mapViewConfig.Get().ChunkHeight;
			var heights = _levelConfig.Get().MapHeights;

			var x = gridCoords.Row*chunkSize + mapOffset.x + chunkSize/2f;
			var y = heights[gridCoords.Row, gridCoords.Column]*chunkHeight;
			var z = gridCoords.Column*chunkSize + mapOffset.z + chunkSize/2f;

			return new Vector3(x, y, z);
		}

		public Vector3 GetClosestWorldPositionCenter(Vector3 worldPosition)
		{
			var gridCoords = GetGridCoords(worldPosition);
			return GetWorldPosition(gridCoords);
		}

		public Quaternion GetRotation(Facing facing)
		{
			if (facing == Facing.Forward)
				return Quaternion.Euler(0, 0, 0);
			if (facing == Facing.Back)
				return Quaternion.Euler(0, 180, 0);
			if (facing == Facing.Left)
				return Quaternion.Euler(0, 270, 0);
			if (facing == Facing.Right)
				return Quaternion.Euler(0, 90, 0);

			var message = string.Format("Rotation for {0} is not defined.", facing);
			throw new NotSupportedException(message);
		}

		private Vector3 GetMapOffset(int[,] heights, MapViewConfig mapViewConfig)
		{
			var centerOffsetCalculator = CenterOffsetCalculator.CreateWithDimensions(mapViewConfig.ChunkSize);
			return centerOffsetCalculator.GetCenterOffset(heights);
		}
	}
}
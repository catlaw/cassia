﻿using Common.DataContainers;
using UnityEngine;

namespace Game.Controllers
{
	public class DistanceCalculator : IDistanceCalculator
	{
		public float GetDistanceBetween(GridCoords first, GridCoords second)
		{
			var rowDifference = Mathf.Abs(first.Row - second.Row);
			var columnDifference = Mathf.Abs(first.Column - second.Column);

			return Mathf.Sqrt(Mathf.Pow(rowDifference, 2) + Mathf.Pow(columnDifference, 2));
		}
	}
}
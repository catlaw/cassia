﻿using Game.Models.Occupants.Units;
using Game.Views.MonoBehaviours.UI;
using UnityEngine;
using UnityEngine.UI;

namespace Game.Views.GameUI
{
	public class UnitOverviewUI : MonoBehaviour
	{
		public StatsUI StatsUI;
		public Text UnitNameText;

		public void ShowUnitOverview(IUnit unit)
		{
			UnitNameText.gameObject.SetActive(true);
			StatsUI.gameObject.SetActive(true);

			UnitNameText.text = unit.Name;
			StatsUI.SetStats(unit.Stats);
		}

		public void Hide()
		{
			UnitNameText.gameObject.SetActive(false);
			StatsUI.gameObject.SetActive(false);
		}
	}
}
﻿using System;
using Game.Views.UI;
using UnityEngine;
using UnityEngine.UI;

namespace Game.Views.GameUI.MonoBehaviours
{
	[RequireComponent(typeof(TappableButton))]
	public class UnitActionButton : MonoBehaviour
	{
		private Action<int> _onTap;
		private TappableButton _tappableButton;
		public Text ButtonText;

		public int Index { get; set; }

		public string Text
		{
			get { return ButtonText.text; }
			set { ButtonText.text = value; }
		}

		public void Start()
		{
			_tappableButton = GetComponent<TappableButton>();
			_tappableButton.ButtonTapSignal.AddListener(OnTap);
		}

		public void OnTap(Action<int> callback)
		{
			_onTap = callback;
		}

		private void OnTap()
		{
			_onTap = _onTap ?? (x => { });
			_onTap(Index);
		}
	}
}
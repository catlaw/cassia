﻿using System.Linq;
using Common.DataContainers;
using Game.Controllers.Units;
using Game.Inputs;
using Game.Inputs.EventSignals;
using Game.Inputs.TapSignals;
using Game.Models.Occupants.Units;
using Game.Models.TurnTransactions;
using Game.Signals.ViewSignals;
using strange.extensions.mediation.impl;

namespace Game.Views.GameUI
{
	public class GameUIMediator : Mediator
	{
		[Inject]
		public GameUIView View { get; set; }

		[Inject]
		public ITurnTransaction TurnTransaction { get; set; }

		[Inject]
		public IButtonTapSignal ButtonTapSignal { get; set; }

		[Inject]
		public IUpdateGameViewSignal UpdateGameSignal { get; set; }

		[Inject]
		public IUnitEventSignal UnitEventSignal { get; set; }

		public override void OnRegister()
		{
			UpdateGameSignal.AddListener(UpdateUI);
			UnitEventSignal.AddListener(TriggerUnit);

			View.UnitActionButtonClicked
				.AddListener(index => { ButtonTapSignal.Dispatch(ButtonType.UnitAction, index); });
			View.ApplyButtonClicked
				.AddListener(() => { ButtonTapSignal.Dispatch(ButtonType.ApplyTransaction, 0); });

			View.DisplayNothing();
		}

		private void UpdateUI()
		{
			switch (TurnTransaction.CurrentState)
			{
				case TransactionState.ActorSelected:
					View.DisplayUnitOverview(TurnTransaction.Actor);
					TurnTransaction.Actor.DisplayActionsSignal.Dispatch(false);
					break;

				case TransactionState.Moving:
					View.DisplayUnitOverview(TurnTransaction.Actor);
					TurnTransaction.Actor.DisplayActionsSignal.Dispatch(false);
					break;

				case TransactionState.PreSelectAction:
					TurnTransaction.Actor.DisplayActionsSignal.Dispatch(true);
					break;

				case TransactionState.PreSelectTarget:
					View.DisplayUnitOverview(TurnTransaction.Actor);
					TurnTransaction.Actor.DisplayActionsSignal.Dispatch(false);
					break;

				case TransactionState.PreApplyTransaction:
					TurnTransaction.Actor.DisplayActionsSignal.Dispatch(false);
					if (TurnTransaction.Outcome.PotentialActionOutcomes.Any())
					{
						var firstOutcome = TurnTransaction.Outcome.PotentialActionOutcomes.First();
						View.DisplayPotentialActionEffects(firstOutcome.Effects);
					}
					else
					{
						View.DisplayNoUnitActionEffects();
					}
					break;

				default:
					TurnTransaction.Actor.DisplayActionsSignal.Dispatch(false);
					View.DisplayNothing();
					break;
			}
		}

		private void TriggerUnit(IUnit unit, GridCoords position)
		{
			if (TurnTransaction.CurrentState != TransactionState.PreApplyTransaction) return;

			var selectedAffectedUnit = TurnTransaction.Outcome.PotentialActionOutcomes.FirstOrDefault(x => x.AffectedUnit == unit);
			if (selectedAffectedUnit != null)
			{
				View.DisplayPotentialActionEffects(selectedAffectedUnit.Effects);
			}
		}
	}
}
﻿using System.Collections.Generic;
using System.Linq;
using Game.Models.Occupants.Units;
using Game.Models.Occupants.Units.UnitActions;
using Game.Views.MonoBehaviours.UI;
using strange.extensions.mediation.impl;
using strange.extensions.signal.impl;
using UnityEngine;

namespace Game.Views.GameUI
{
	public class GameUIView : View
	{
		public Signal ApplyButtonClicked = new Signal();

		public ConfirmTransactionUI ConfirmTransaction;
		public Signal<int> UnitActionButtonClicked = new Signal<int>();
		public UnitActionButtonsUI UnitActionButtons;
		public UnitOverviewUI UnitOverview;

		protected override void Start()
		{
			ConfirmTransaction.ApplyButton.ButtonTapSignal.AddListener(ApplyButtonClicked.Dispatch);
			//UnitActionButtons.UnitActionButtonClicked.AddListener(UnitActionButtonClicked.Dispatch);
		}

		public void DisplayNothing()
		{
			UnitOverview.Hide();
			//UnitActionButtons.Hide();
			ConfirmTransaction.Hide();
		}

		public void DisplayUnitOverview(IUnit unit)
		{
			UnitOverview.ShowUnitOverview(unit);
			//UnitActionButtons.Hide();
			ConfirmTransaction.Hide();
		}

		public void DisplayUnitActions(IUnit unit, IList<string> unitActionNames)
		{
			UnitOverview.ShowUnitOverview(unit);
			//UnitActionButtons.ShowUnitActions(unitActionNames);
			ConfirmTransaction.Hide();
		}

		public void DisplayPotentialActionEffects(IEnumerable<IPotentialActionEffect> effects)
		{
			UnitOverview.Hide();
			//UnitActionButtons.Hide();

			var firstEffect = effects.First();
			ConfirmTransaction.Show(firstEffect.Delta, Mathf.FloorToInt(firstEffect.Accuracy*100));
		}

		public void DisplayNoUnitActionEffects()
		{
			UnitOverview.Hide();
			//UnitActionButtons.Hide();

			ConfirmTransaction.ShowNoEffect();
		}
	}
}
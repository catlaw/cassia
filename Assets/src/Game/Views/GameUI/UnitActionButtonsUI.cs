using System;
using System.Collections.Generic;
using System.Linq;
using Game.Views.GameUI.MonoBehaviours;
using strange.extensions.signal.impl;
using UnityEngine;

namespace Game.Views.GameUI
{
	public class UnitActionButtonsUI : MonoBehaviour
	{
		public Signal<int> UnitActionButtonClicked = new Signal<int>();
		public List<UnitActionButton> UnitActionButtons;

		public void Start()
		{
			for (var i = 0; i < UnitActionButtons.Count; ++i)
			{
				UnitActionButtons[i].Index = i;
				UnitActionButtons[i].OnTap(UnitActionButtonClicked.Dispatch);
			}
		}

		public void ShowUnitActions(IEnumerable<string> unitActionNames)
		{
			var unitActions = unitActionNames.ToList();
			if (unitActions.Count > UnitActionButtons.Count)
			{
				var message = string.Format("Can only display {0} or fewer UnitActions.", UnitActionButtons.Count);
				throw new ArgumentException(message);
			}

			for (var i = 0; i < UnitActionButtons.Count; ++i)
			{
				if (unitActions.Count > i)
				{
					UnitActionButtons[i].gameObject.SetActive(true);
					UnitActionButtons[i].Text = unitActions[i];
				}
				else
				{
					UnitActionButtons[i].gameObject.SetActive(false);
				}
			}
		}

		public void Hide()
		{
			foreach (var unitActionButton in UnitActionButtons)
			{
				unitActionButton.gameObject.SetActive(false);
			}
		}
	}
}
﻿using System.Collections.Generic;
using strange.extensions.mediation.impl;
using UnityEngine;
using Vectrosity;

namespace Game.Views
{
	public class SquareHighlightsView : View
	{
		private readonly List<GameObject> _highlights = new List<GameObject>();

		public void HighlightMoveSquares(IEnumerable<Vector3> positions)
		{
			foreach (var position in positions)
			{
				var highlight = CreateMoveHighlight(position);
				_highlights.Add(highlight);
			}
		}

		private GameObject CreateMoveHighlight(Vector3 position)
		{
            // TODO this should be in a config somewhere
		    var highlightPrefab = Resources.Load<GameObject>("Prefabs/MoveHighlight");
		    var highlight = GameObject.Instantiate(highlightPrefab);

		    highlight.transform.localRotation = Quaternion.Euler(90, 0, 0);
		    highlight.transform.position = position + new Vector3(0, 0.01f, 0);
            return highlight;
		}

	    public void HighlightAttackSquares(IEnumerable<Vector3> positions)
	    {
            foreach (var position in positions)
            {
                var highlight = CreateAttackHighlight(position);
                _highlights.Add(highlight);
            }
        }

        private GameObject CreateAttackHighlight(Vector3 position)
        {
            // TODO this should be in a config somewhere
            var highlightPrefab = Resources.Load<GameObject>("Prefabs/AttackHighlight");
            var highlight = GameObject.Instantiate(highlightPrefab);

            highlight.transform.localRotation = Quaternion.Euler(90, 0, 0);
            highlight.transform.position = position + new Vector3(0, 0.01f, 0);
            return highlight;
        }

        public void UnhighlightAllSquares()
		{
			foreach (var highlight in _highlights)
			{
				Destroy(highlight);
			}
		}
	}
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common.DataContainers;
using Game.Controllers;
using Game.Controllers.Units;
using Game.Models.Stage;
using Game.Models.TurnTransactions;
using Game.Signals.ViewSignals;
using strange.extensions.mediation.impl;
using UnityEngine;

namespace Game.Views
{
	public class SquareHighlightsMediator : Mediator
	{
		[Inject]
		public SquareHighlightsView View { get; set; }

		[Inject]
		public IWorldGridCoordsConverter WorldGridCoordsConverter { get; set; }

		[Inject]
		public IHighlightSquaresViewSignal HighlightSquaresViewSignal { get; set; }

        [Inject]
        public ITurnTransaction TurnTransaction { get; set; }

		public override void OnRegister()
		{
			HighlightSquaresViewSignal.AddListener(HighlightSquares);
		}

		private void HighlightSquares(IEnumerable<GridCoords> squares)
		{
		    var selectedSquares = squares.Select(x => WorldGridCoordsConverter.GetWorldPosition(x));
			View.UnhighlightAllSquares();

		    switch (TurnTransaction.CurrentState)
		    {
                case TransactionState.ActorSelected:
                    View.HighlightMoveSquares(selectedSquares);
		            break;

                case TransactionState.PreSelectTarget:
                case TransactionState.PreApplyTransaction:
                    View.HighlightAttackSquares(selectedSquares);
		            break;
		    }
		}
	}
}
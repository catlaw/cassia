using UnityEngine;

namespace Game.Views
{
	public class Tap
	{
		private Tap()
		{
		}

		public Vector3 Position { get; set; }

		public static Tap CreateFromRaycastHit(RaycastHit hit)
		{
			return new Tap
			{
				Position = hit.point
			};
		}
	}
}
﻿using Game.Models.Occupants.Units;
using UnityEngine;
using UnityEngine.UI;

namespace Game.Views.MonoBehaviours.UI
{
	public class StatsUI : MonoBehaviour
	{
		public Text HealthText, PowerText, DefenseText;

		public void SetStats(IUnitStats stats)
		{
			HealthText.text = string.Format("Health: {0}/{1}", stats.Get(StatType.Health),
				stats.Get(StatType.MaxHealth));
			PowerText.text = string.Format("Power: {0}", stats.Get(StatType.Power));
			DefenseText.text = string.Format("Health: {0}", stats.Get(StatType.Defense));
		}
	}
}
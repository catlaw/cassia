﻿using Game.Views.UI;
using UnityEngine;

namespace Game.Views.MonoBehaviours.UI
{
	public class ConfirmTransactionUI : MonoBehaviour
	{
		public TappableButton ApplyButton;
		public OutcomeOverviewUI OutcomeOverview;

		public void Show(int damage, int accuracy)
		{
			ApplyButton.gameObject.SetActive(true);

			OutcomeOverview.Show(damage, accuracy);
		}

		public void ShowNoEffect()
		{
			ApplyButton.gameObject.SetActive(true);

			OutcomeOverview.ShowNoEffect();
		}

		public void Hide()
		{
			ApplyButton.gameObject.SetActive(false);

			OutcomeOverview.Hide();
		}
	}
}
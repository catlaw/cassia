﻿using UnityEngine;
using UnityEngine.UI;

namespace Game.Views.MonoBehaviours.UI
{
	public class OutcomeOverviewUI : MonoBehaviour
	{
		public GameObject Background;
		public Text Damage, Accuracy, NoEffect;

		public void Show(int damage, int accuracy)
		{
			Damage.gameObject.SetActive(true);
			Accuracy.gameObject.SetActive(true);
			NoEffect.gameObject.SetActive(false);
			Background.SetActive(true);

			Damage.text = string.Format("Damage: {0}", damage);
			Accuracy.text = string.Format("Accuracy: {0}%", accuracy);
		}

		public void ShowNoEffect()
		{
			Damage.gameObject.SetActive(false);
			Accuracy.gameObject.SetActive(false);
			NoEffect.gameObject.SetActive(true);
			Background.SetActive(true);
		}

		public void Hide()
		{
			Damage.gameObject.SetActive(false);
			Accuracy.gameObject.SetActive(false);
			NoEffect.gameObject.SetActive(false);
			Background.SetActive(false);
		}
	}
}
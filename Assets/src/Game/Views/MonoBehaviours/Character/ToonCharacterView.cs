using Game.Views.Animations;
using UnityEngine;

namespace Game.Views.MonoBehaviours.Character
{
	public class ToonCharacterView : CharacterView
	{
		public Animator Animator;

		public override void PlayAnimation(IPlayableUnitAnimation playableAnimation)
		{
			Animator.SetTrigger(playableAnimation.TriggerName);
		}
	}
}
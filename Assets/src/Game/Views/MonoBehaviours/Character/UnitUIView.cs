﻿using UnityEngine;
using UnityEngine.UI;

namespace Game.Views.MonoBehaviours.Character
{
	public class UnitUIView : MonoBehaviour
	{
		public RectTransform HealthFill;

		public float Health
		{
			get { return HealthFill.localScale.x; }
			set { HealthFill.localScale = new Vector3(value > 0 ? value : 0, 1, 1); }
		}
	}
}
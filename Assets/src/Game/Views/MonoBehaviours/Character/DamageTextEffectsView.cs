﻿using System.Collections;
using Instantiator.GameObjectPools;
using strange.extensions.mediation.impl;
using UnityEngine;
using UnityEngine.UI;

namespace Game.Views.MonoBehaviours.Character
{
	public class DamageTextEffectsView : View
	{
		public Text DamageText;

		public YieldInstruction ShowDamage(int damage)
		{
			return StartCoroutine(CoShowDamage(damage));
		}

		private IEnumerator CoShowDamage(int damage)
		{
			DamageText.text = damage.ToString();
			yield return new WaitForSeconds(1);
		}
	}
}
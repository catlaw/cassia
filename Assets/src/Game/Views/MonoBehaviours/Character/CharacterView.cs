﻿using Game.Views.Animations;
using UnityEngine;

namespace Game.Views.MonoBehaviours.Character
{
	public abstract class CharacterView : MonoBehaviour
	{
		public abstract void PlayAnimation(IPlayableUnitAnimation playableAnimation);
	}
}
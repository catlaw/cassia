﻿using System.Collections.Generic;
using Configs;
using Game.ConfigActions.Methods;
using Game.Views.Animations;

namespace Game.Views
{
	public interface IUnitActionView
	{
		IDictionary<UnitActionEventType, ConfigActionSequenceConfig> ConfigActions { get; }
	}
}
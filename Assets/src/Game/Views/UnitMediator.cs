﻿using System.Collections.Generic;
using System.Linq;
using Common.DataContainers;
using Game.Controllers;
using Game.Inputs;
using Game.Inputs.TapSignals;
using Game.Models.Occupants.Units;
using Game.Models.Occupants.Units.UnitActions;
using Game.Signals.ControllerSignals;
using Game.Views.Animations;
using Game.Views.PathPoints;
using strange.extensions.mediation.impl;
using UnityEngine;

namespace Game.Views
{
    public class UnitMediator : Mediator
	{
		[Inject]
		public UnitView View { get; set; }

		[Inject]
		public IWorldGridCoordsConverter WorldGridCoordsConverter { get; set; }

		[Inject]
		public IPathPointsFactory PathPointsFactory { get; set; }

		[Inject]
		public IMoveUnitViewCompleteSignal MoveUnitViewCompleteSignal { get; set; }

        [Inject]
        public IPlayableUnitAnimationSelector PlayableUnitAnimationSelector { get; set; }

        [Inject]
		public IUnitTapSignal UnitTapSignal { get; set; }

		[Inject]
		public IButtonTapSignal ButtonTapSignal { get; set; }

		private IUnit Unit
		{
			get { return View.Unit; }
		}

		public override void OnRegister()
		{
			Unit.MoveUnitViewSignal.AddListener(MoveUnit);
			Unit.SetUnitPositionViewSignal.AddListener(SetUnitPosition);
			Unit.TriggerUnitAnimationViewSignal.AddListener(TriggerAnimation);
			Unit.ApplyActionEffectViewSignal.AddListener(ApplyActionEffect);
            Unit.HighlightViewSignal.AddListener(View.ShowHighlighted);
			Unit.DisplayActionsSignal.AddListener(View.DisplayActions);

		    var walkingAnimation = PlayableUnitAnimationSelector.GetAnimation(Unit, AnimationType.Walk, 0);
            View.WalkingAnimation = walkingAnimation;

            View.TapSignal.AddListener(OnClick);
			View.StopMoveSignal.AddListener(OnStopMove);
			View.UnitActionTapSignal.AddListener(OnUnitActionButtonTap);
		}

		private void MoveUnit(IEnumerable<GridCoords> path)
		{
			var pathPoints = PathPointsFactory.GetPathPoints(View.ViewData, path);
			View.MoveAlongPath(pathPoints);
		}

		private void SetUnitPosition(GridPosition gridPosition)
		{
			var worldPosition = WorldGridCoordsConverter.GetWorldPosition(gridPosition.GridCoords);
			var worldRotation = WorldGridCoordsConverter.GetRotation(gridPosition.Facing);
			View.SetTo(worldPosition, worldRotation);
		}

		private void OnClick(Vector3 position)
		{
			UnitTapSignal.Dispatch(Unit, position);
		}

		private void OnStopMove()
		{
			MoveUnitViewCompleteSignal.Dispatch(Unit);
		}

		private void TriggerAnimation(IPlayableUnitAnimation playableAnimation)
		{
			View.PlayAnimation(playableAnimation);
		}

		private void ApplyActionEffect(IActionEffect actionEffect)
		{
			switch (actionEffect.StatType)
			{
				case StatType.Health:
					View.ChangeHealth(actionEffect.Delta);
					break;
			}
		}

	    private void OnUnitActionButtonTap(int index)
	    {
		    ButtonTapSignal.Dispatch(ButtonType.UnitAction, index);
	    }
	}
}
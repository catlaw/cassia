﻿using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using UnityEngine;

namespace Game.Views
{
    public class GameObjectHighlighter
    {
        private readonly List<Renderer> _renderers;
        private readonly List<Tween> _tweens;

        public GameObjectHighlighter(GameObject gameObject)
        {
            _renderers = gameObject.GetComponentsInChildren<Renderer>().ToList();
            _tweens = new List<Tween>();
        }

        public void ShowHighlighted(bool highlight, Color color)
        {
            if (highlight) ShowHighlighted(color);
            else ShowUnhighlighted();
        }

        private void ShowHighlighted(Color color)
        {
            if (_tweens.Any()) return;

            foreach (var renderer in _renderers)
            {
                if (!renderer.material.HasProperty("_Color")) continue;

                var tween = renderer.material.DOColor(color, 1f)
                    .SetEase(Ease.Linear)
                    .SetLoops(-1, LoopType.Yoyo);

                _tweens.Add(tween);
            }
        }

        private void ShowUnhighlighted()
        {
            foreach (var tween in _tweens)
            {
                tween.Rewind();
            }
            _tweens.Clear();
        }
    }
}
﻿namespace Game.Views
{
	public interface IUnitActionViewsStore
	{
		IUnitActionView GetByViewId(string viewId);
	}
}
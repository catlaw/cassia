﻿using UnityEngine;

namespace Game.Views.PathPoints
{
    public interface IPathPoint
    {
        bool Jump { get; }
        Vector3 Position { get; }
        Vector3 LookPosition { get; }
        float Duration { get; }
    }
}
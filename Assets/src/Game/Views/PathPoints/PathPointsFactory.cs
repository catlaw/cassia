﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common.DataContainers;
using Game.ConfigActions.Util;
using Game.Controllers;
using Game.Models.Occupants.Units;
using Game.Models.Stage;
using UnityEngine;

namespace Game.Views.PathPoints
{
	public class PathPointsFactory : IPathPointsFactory
	{
		private readonly IDurationCalculator _durationCalculator;
		private readonly IMap _map;
		private readonly IWorldGridCoordsConverter _worldGridCoordsConverter;

		public PathPointsFactory(IWorldGridCoordsConverter worldGridCoordsConverter,
			IDurationCalculator durationCalculator,
			IMap map)
		{
			_worldGridCoordsConverter = worldGridCoordsConverter;
			_durationCalculator = durationCalculator;
			_map = map;
		}

		public IEnumerable<IPathPoint> GetPathPoints(IUnitViewData unitView, IEnumerable<GridCoords> path)
		{
			if (unitView == null) throw new ArgumentNullException("unitView");
			if (path == null) throw new ArgumentNullException("path");


			var gridCoordsPath = path.ToList();

			if (!gridCoordsPath.Any())
			{
				var message = "Cannot get path points for empty path.";
				throw new ArgumentException(message);
			}

			var pathPoints = new List<IPathPoint>();

			var startingPosition = gridCoordsPath.First();
			var previousWorldPosition = _worldGridCoordsConverter.GetWorldPosition(startingPosition);
			var previousHeight = _map.GetSquare(startingPosition).Height;

			foreach (var position in gridCoordsPath.Skip(1))
			{
				var worldPosition = _worldGridCoordsConverter.GetWorldPosition(position);
				var height = _map.GetSquare(position).Height;

				var pathPoint = new PathPoint
				{
					Position = worldPosition,
					LookPosition = new Vector3(worldPosition.x, previousWorldPosition.y, worldPosition.z),
					Duration = _durationCalculator.GetUnitMoveDuration(1, unitView.MoveSpeed),
					Jump = Mathf.Abs(height - previousHeight) > unitView.MaximumWalkHeight
				};

				pathPoints.Add(pathPoint);

				previousWorldPosition = worldPosition;
				previousHeight = height;
			}

			return pathPoints;
		}
	}
}
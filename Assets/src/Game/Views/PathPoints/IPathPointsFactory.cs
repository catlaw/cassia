﻿using System.Collections.Generic;
using Common.DataContainers;
using Game.Models.Occupants.Units;

namespace Game.Views.PathPoints
{
	public interface IPathPointsFactory
	{
		IEnumerable<IPathPoint> GetPathPoints(IUnitViewData unitView, IEnumerable<GridCoords> path);
	}
}
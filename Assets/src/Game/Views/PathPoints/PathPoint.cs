﻿using UnityEngine;

namespace Game.Views.PathPoints
{
    public class PathPoint : IPathPoint
    {
        protected bool Equals(PathPoint other)
        {
            return Position.Equals(other.Position) && LookPosition.Equals(other.LookPosition) &&
                   Duration.Equals(other.Duration) && Jump == other.Jump;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((PathPoint) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = Position.GetHashCode();
                hashCode = (hashCode*397) ^ LookPosition.GetHashCode();
                hashCode = (hashCode*397) ^ Duration.GetHashCode();
                hashCode = (hashCode*397) ^ Jump.GetHashCode();
                return hashCode;
            }
        }

        public static bool operator ==(PathPoint left, PathPoint right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(PathPoint left, PathPoint right)
        {
            return !Equals(left, right);
        }

        public Vector3 Position { get; set; }
        public Vector3 LookPosition { get; set; }
        public float Duration { get; set; }
        public bool Jump { get; set; }
    }
}
﻿using Game.Models.Occupants.Units;

namespace Game.Views.Animations
{
	public interface IPlayableUnitAnimationSelector
	{
		IPlayableUnitAnimation GetAnimation(IUnit unit, AnimationType animationType, int powerTier);
	}
}
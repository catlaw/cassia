﻿namespace Game.Views.Animations
{
	public enum AnimationType
	{
		None,
		Walk,
		Hit,
		Dodge,
		Melee,
		Cast
	}
}
﻿using Game.ConfigActions;
using Game.ConfigActions.Methods;

namespace Game.Views.Animations
{
    public interface IActionEffectConfigActions
    {
        IConfigAction Get(ActionEffectEventType actionEffectEventType);
    }
}
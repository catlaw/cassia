﻿using Configs;
using Game.ConfigActions;
using Game.ConfigActions.Methods;

namespace Game.Views.Animations
{
    public class ActionEffectConfigActions : IActionEffectConfigActions
    {
        private readonly IConfig<AnimationsConfig> _animationsConfig;
        private readonly IConfigActionSequenceFactory _sequenceFactory;

        public ActionEffectConfigActions(IConfig<AnimationsConfig> animationsConfig,
            IConfigActionSequenceFactory sequenceFactory)
        {
            _animationsConfig = animationsConfig;
            _sequenceFactory = sequenceFactory;
        }

        public IConfigAction Get(ActionEffectEventType actionEffectEventType)
        {
            var common = _animationsConfig.Get().ActionEffects;
            if (!common.ContainsKey(actionEffectEventType)) return new EmptyConfigAction();

            var config = common[actionEffectEventType];
            return _sequenceFactory.CreateConfigActionSequence(config);
        }
    }
}
﻿namespace Game.Views.Animations
{
	public enum AnimationPoint
	{
		Start,
		BackSwing,
		Hit,
		End
	}
}
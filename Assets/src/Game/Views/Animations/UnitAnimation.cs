﻿using Game.ConfigActions;
using Game.Models.Occupants.Units;
using Game.Models.Occupants.Units.UnitActions;

namespace Game.Views.Animations
{
	public class UnitAnimation : IUnitAnimation
	{
		public IUnit Unit { get; set; }
		public IConfigActionSet ConfigActionSet { get; set; }
		public IActionEffect ActionEffect { get; set; }

		protected bool Equals(UnitAnimation other)
		{
			return Equals(Unit, other.Unit) && Equals(ConfigActionSet, other.ConfigActionSet) &&
			       Equals(ActionEffect, other.ActionEffect);
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			if (obj.GetType() != GetType()) return false;
			return Equals((UnitAnimation) obj);
		}

		public override int GetHashCode()
		{
			unchecked
			{
				var hashCode = Unit != null ? Unit.GetHashCode() : 0;
				hashCode = (hashCode*397) ^ (ConfigActionSet != null ? ConfigActionSet.GetHashCode() : 0);
				hashCode = (hashCode*397) ^ (ActionEffect != null ? ActionEffect.GetHashCode() : 0);
				return hashCode;
			}
		}

		public static bool operator ==(UnitAnimation left, UnitAnimation right)
		{
			return Equals(left, right);
		}

		public static bool operator !=(UnitAnimation left, UnitAnimation right)
		{
			return !Equals(left, right);
		}
	}
}
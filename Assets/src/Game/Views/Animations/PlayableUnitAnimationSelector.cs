﻿using Configs;
using Game.Models.Occupants.Units;
using UnityEngine;

namespace Game.Views.Animations
{
	public class PlayableUnitAnimationSelector : IPlayableUnitAnimationSelector
	{
		private readonly IUnitViewMap _unitViewMap;

		public PlayableUnitAnimationSelector(IUnitViewMap unitViewMap)
		{
			_unitViewMap = unitViewMap;
		}

		public IPlayableUnitAnimation GetAnimation(IUnit unit, AnimationType animationType, int powerTier)
		{
			if (animationType == AnimationType.None) return new NonePlayableUnitAnimation();

			var unitView = _unitViewMap.GetView(unit);

			var unitAnimations = unitView.Animations.GetById(animationType);
			var powerIndex = Mathf.Clamp(powerTier, 0, unitAnimations.Count - 1);
			return unitAnimations[powerIndex];
		}
	}
}
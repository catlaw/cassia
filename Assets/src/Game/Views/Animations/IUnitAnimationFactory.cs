﻿using System.Collections.Generic;

namespace Game.Views.Animations
{
	public interface IUnitAnimationFactory
	{
		IUnitAnimation GetActorUnitAnimation();
	}
}
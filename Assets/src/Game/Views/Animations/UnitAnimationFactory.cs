﻿using System;
using System.Collections.Generic;
using Game.Models.TurnTransactions;

namespace Game.Views.Animations
{
	public class UnitAnimationFactory : IUnitAnimationFactory
	{
		private readonly ITurnTransaction _turnTransaction;
		private readonly IConfigActionSets _configActionSets;

		public UnitAnimationFactory(ITurnTransaction turnTransaction,
			IConfigActionSets configActionSets)
		{
			_turnTransaction = turnTransaction;
			_configActionSets = configActionSets;
		}

		public IUnitAnimation GetActorUnitAnimation()
		{
			if (!_turnTransaction.IsReady)
			{
				var message = "Cannot get actor UnitAnimation if TurnTransaction is not ready.";
				throw new InvalidOperationException(message);
			}

			return new UnitAnimation
			{
				Unit = _turnTransaction.Actor,
				ConfigActionSet = _configActionSets.GetConfigActionSet(_turnTransaction.UnitAction)
			};
		}
	}
}
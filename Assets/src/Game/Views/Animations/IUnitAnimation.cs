﻿using Game.ConfigActions;
using Game.Models.Occupants.Units;
using Game.Models.Occupants.Units.UnitActions;

namespace Game.Views.Animations
{
	public interface IUnitAnimation
	{
		IUnit Unit { get; }
		IConfigActionSet ConfigActionSet { get; }
		IActionEffect ActionEffect { get; }
	}
}
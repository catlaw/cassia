﻿namespace Game.Views.Animations
{
	public interface IPlayableUnitAnimation
	{
		string TriggerName { get; }
		float HitDelay { get; }
	}
}
﻿using Game.ConfigActions;
using Game.Models.Occupants.Units.UnitActions;

namespace Game.Views.Animations
{
	public interface IConfigActionSets
	{
		IConfigActionSet GetConfigActionSet(IUnitAction unitAction);
	}
}
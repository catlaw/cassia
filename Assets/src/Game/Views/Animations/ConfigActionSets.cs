﻿using System;
using System.Diagnostics;
using Game.ConfigActions;
using Game.Models.Occupants.Units.UnitActions;

namespace Game.Views.Animations
{
	public class ConfigActionSets : IConfigActionSets
	{
		private readonly IConfigActionSequenceSetFactory _sequenceSetFactory;
		private readonly IUnitActionViewsStore _unitActionViewsStore;

		public ConfigActionSets(IUnitActionViewsStore unitActionViewsStore,
			IConfigActionSequenceSetFactory sequenceSetFactory)
		{
			_unitActionViewsStore = unitActionViewsStore;
			_sequenceSetFactory = sequenceSetFactory;
		}

		public IConfigActionSet GetConfigActionSet(IUnitAction unitAction)
		{
			if (unitAction == null) throw new ArgumentNullException("unitAction");

		    if (unitAction.ViewId == null) return _sequenceSetFactory.CreateEmpty();

			var unitActionView = _unitActionViewsStore.GetByViewId(unitAction.ViewId);
		    return _sequenceSetFactory.CreateConfigActionSequenceSet(unitActionView.ConfigActions);
		}
	}
}
﻿using Game.Controllers;
using Game.Inputs.TapSignals;
using strange.extensions.mediation.impl;
using UnityEngine;

namespace Game.Views
{
	public class MapMediator : Mediator
	{
		[Inject]
		public MapView View { get; set; }

		[Inject]
		public IMapTapSignal MapTapSignal { get; set; }

		public override void OnRegister()
		{
			View.ClickSignal.AddListener(OnClick);
		}

		public void OnClick(Vector3 position)
		{
			MapTapSignal.Dispatch(position);
		}
	}
}
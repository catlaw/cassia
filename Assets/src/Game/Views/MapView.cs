﻿using strange.extensions.mediation.impl;
using strange.extensions.signal.impl;
using UnityEngine;

namespace Game.Views
{
	public class MapView : View, ITappable
	{
		public Signal<Vector3> ClickSignal = new Signal<Vector3>();

		public void OnTap(Tap tap)
		{
			ClickSignal.Dispatch(tap.Position);
		}
	}
}
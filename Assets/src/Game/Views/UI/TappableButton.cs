﻿using strange.extensions.signal.impl;
using UnityEngine;
using UnityEngine.UI;

namespace Game.Views.UI
{
	[RequireComponent(typeof(Image))]
	[RequireComponent(typeof(BoxCollider))]
	public class TappableButton : MonoBehaviour, ITappable
	{
		private BoxCollider _collider;

		private Image _image;
		public Signal ButtonTapSignal = new Signal();

		public void OnTap(Tap tap)
		{
			ButtonTapSignal.Dispatch();
		}

		private void Awake()
		{
			_image = GetComponent<Image>();
			_collider = GetComponent<BoxCollider>();

			var rect = _image.rectTransform.rect;
			_collider.size = new Vector3(rect.width, rect.height, 1);
		}
	}
}
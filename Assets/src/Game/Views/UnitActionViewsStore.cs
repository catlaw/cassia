﻿using Configs;

namespace Game.Views
{
	public class UnitActionViewsStore : IUnitActionViewsStore
	{
		private readonly IConfig<AnimationsConfig> _config;

		public UnitActionViewsStore(IConfig<AnimationsConfig> config)
		{
			_config = config;
		}

		public IUnitActionView GetByViewId(string viewId)
		{
			return _config.Get().UnitActions.GetById(viewId);
		}
	}
}
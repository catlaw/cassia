﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Configs;
using DG.Tweening;
using Game.Models.Occupants.Units;
using Game.Views.Animations;
using Game.Views.GameUI;
using Game.Views.MonoBehaviours.Character;
using Game.Views.PathPoints;
using Instantiator.GameObjectPools;
using strange.extensions.mediation.impl;
using strange.extensions.signal.impl;
using UnityEngine;

namespace Game.Views
{
	public class UnitView : View, ITappable
	{
		private CharacterView _characterView;
		private UnitUIView _unitUIView;
	    private GameObjectHighlighter _highlighter;

		public Signal<Vector3> TapSignal = new Signal<Vector3>();
		public Signal StopMoveSignal = new Signal();
		public Signal<int> UnitActionTapSignal = new Signal<int>();

		public IUnit Unit { get; private set; }
		public IUnitViewData ViewData { get; private set; }

        public IPlayableUnitAnimation WalkingAnimation { get; set; }
	    private Coroutine _walkingCoroutine;
		private Tween _walkingTween;

		public void OnTap(Tap tap)
		{
			TapSignal.Dispatch(transform.position);
		}

		public void Ready()
		{
			_characterView = GetComponentInChildren<CharacterView>();
			_unitUIView = GetComponentInChildren<UnitUIView>();
            _highlighter = new GameObjectHighlighter(gameObject);

            UpdateHealth();
		}

		private void UpdateHealth()
		{
			var maxHealth = (float) Unit.Stats.Get(StatType.MaxHealth);
			var currentHealth = (float) Unit.Stats.Get(StatType.Health);
			_unitUIView.Health = currentHealth/maxHealth;
		}

		public void SetUnit(IUnit unit)
		{
			Unit = unit;
		}

		public void SetViewConfig(IUnitViewData viewData)
		{
		    ViewData = viewData;
		}

		public void MoveAlongPath(IEnumerable<IPathPoint> path)
		{
            PlayAnimation(WalkingAnimation);
			_walkingCoroutine = StartCoroutine(CoMoveAlongPath(path));
		}

		public void SetTo(Vector3 position, Quaternion rotation)
		{
			StopWalking();

			transform.position = position;
			_characterView.transform.localRotation = rotation;
		}

		// this is kind of janky but whatever
		private void StopWalking()
		{
			if (_walkingCoroutine != null)
			{
				StopCoroutine(_walkingCoroutine);
				PlayAnimation(WalkingAnimation);
				_walkingTween.Kill(true);

				_walkingCoroutine = null;
			}
		}

		private IEnumerator CoMoveAlongPath(IEnumerable<IPathPoint> pathPoints)
		{
            _characterView.PlayAnimation(WalkingAnimation);

            foreach (var pathPoint in pathPoints)
            {
	            _characterView.transform.DOLookAt(pathPoint.LookPosition, 0.15f);

				if (!pathPoint.Jump)
				{
					_walkingTween = transform.DOMove(pathPoint.Position, pathPoint.Duration)
						.SetEase(Ease.Linear);
				}
				else
				{
					_walkingTween = transform.DOJump(pathPoint.Position, 0.75f, 1, pathPoint.Duration)
						.SetEase(Ease.Linear);
				}

	            yield return _walkingTween.WaitForCompletion();
            }

			StopMoveSignal.Dispatch();
			StopWalking();
		}

		public void PlayAnimation(IPlayableUnitAnimation playableAnimation)
		{
			_characterView.PlayAnimation(playableAnimation);
		}

        // TODO remove this later
		public void ChangeHealth(int healthDelta)
		{
			UpdateHealth();
		}

	    public void ShowHighlighted(bool highlight, Color color)
	    {
	        _highlighter.ShowHighlighted(highlight, color);
	    }

        [Inject]
        public IGameObjectPool GameObjectPool { get; set; }

		private UnitActionButtonsUI _buttons;
		public void DisplayActions(bool shouldDisplay)
		{
			if (shouldDisplay)
			{
				// TODO this should be in a config
                // TODO actually this should be a config action hehe
			    var asset = new AssetConfig {Path = "Prefabs/UnitActionButtonsUI"};
				_buttons = GameObjectPool.GetPooled(asset).GetComponent<UnitActionButtonsUI>();
				_buttons.transform.SetParent(transform);
				_buttons.transform.localPosition = Vector3.zero;

				var unitActions = Unit.EquippedUnitActions.Select(x => x.Name);
				_buttons.ShowUnitActions(unitActions);

				_buttons.UnitActionButtonClicked.AddListener(index =>
				{
					UnitActionTapSignal.Dispatch(index);
				});
			}
			else
			{
			    if (_buttons != null)
			    {
                    GameObjectPool.ReturnToPool(_buttons.gameObject);
			        _buttons = null;
			    }
			}
		}
	}
}
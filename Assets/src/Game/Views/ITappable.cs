﻿namespace Game.Views
{
	public interface ITappable
	{
		void OnTap(Tap tap);
	}
}
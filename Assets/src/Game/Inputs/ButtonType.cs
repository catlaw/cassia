﻿namespace Game.Inputs
{
	public enum ButtonType
	{
		None,
		ApplyTransaction,
		UnitAction
	}
}
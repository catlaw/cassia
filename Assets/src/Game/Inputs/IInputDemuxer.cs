﻿namespace Game.Inputs
{
	public interface IInputDemuxer
	{
		void FlushEvents();
		void Init();
	}
}
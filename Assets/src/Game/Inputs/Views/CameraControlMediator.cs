﻿using Common.DataContainers;
using Game.Controllers;
using Game.Controllers.Units;
using Game.Models.TurnTransactions;
using Game.Signals.ViewSignals;
using strange.extensions.mediation.impl;

namespace Game.Inputs.Views
{
    public class CameraControlMediator : Mediator
    {
        [Inject]
        public CameraControlView View { get; set; }

        [Inject]
        public IUpdateGameViewSignal UpdateGameViewSignal { get; set; }

        [Inject]
        public ITurnTransaction TurnTransaction { get; set; }

        [Inject]
        public IWorldGridCoordsConverter WorldGridCoordsConverter { get; set; }

        public override void OnRegister()
        {
            UpdateGameViewSignal.AddListener(OnGameViewUpdate);
        }

        private void OnGameViewUpdate()
        {
            switch (TurnTransaction.CurrentState)
            {
                case TransactionState.ActorSelected:
                    View.EnableDragging(true);
                    CenterOn(TurnTransaction.StartPosition);
                    View.ZoomOut();
                    break;

                case TransactionState.Moving:
                    View.EnableDragging(false);
                    View.ZoomOut();
                    break;

                case TransactionState.PreSelectAction:
                    View.EnableDragging(true);
                    CenterOn(TurnTransaction.MovePosition);
                    View.ZoomIn();
                    break;

                case TransactionState.PreApplyTransaction:
                    View.EnableDragging(true);
                    CenterOn(TurnTransaction.TargetPosition);
                    View.ZoomOut();
                    break;

                case TransactionState.PlayTransactionAnimation:
                    View.EnableDragging(false);
                    CenterOn(TurnTransaction.TargetPosition);
                    View.ZoomIn();
                    break;

                default:
                    View.EnableDragging(true);
                    View.ZoomOut();
                    break;
            }
        }

        private void CenterOn(GridCoords gridCoords)
        {
            var position = WorldGridCoordsConverter.GetWorldPosition(gridCoords);
            View.CenterOn(position);
        }
    }
}
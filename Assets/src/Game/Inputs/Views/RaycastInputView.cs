﻿using Game.Views;
using strange.extensions.mediation.impl;
using UnityEngine;

namespace Game.Inputs.Views
{
    [RequireComponent(typeof(Camera))]
	public class RaycastInputView : View
	{
		private Camera _camera;
		private ITappable _currentTappable;
        private float _mouseDownTime;

        protected override void Start()
		{
			_camera = GetComponent<Camera>();
		}

		private void Update()
		{
            if (UnityEngine.Input.GetMouseButton(0))
                _mouseDownTime += Time.deltaTime;

			// fire everything for now
			// or not
            if (UnityEngine.Input.GetMouseButtonDown(0))
            {
                var ray = _camera.ScreenPointToRay(UnityEngine.Input.mousePosition);

	            var hits = Physics.RaycastAll(ray, float.MaxValue);
	            foreach (var hit in hits)
	            {
					_currentTappable = hit.collider.GetComponent<ITappable>();
		            break;
	            }
            }
            else if (UnityEngine.Input.GetMouseButtonUp(0))
            {
                var ray = _camera.ScreenPointToRay(UnityEngine.Input.mousePosition);

				var hits = Physics.RaycastAll(ray, float.MaxValue);
	            foreach (var hit in hits)
	            {
					var tappable = hit.collider.GetComponent<ITappable>();

					if (tappable == null)
					{
						throw new MissingComponentException("Monobehaviour that extends ITappable not found on clicked object.");
					}

					if (tappable == _currentTappable && _mouseDownTime < 0.2f)
					{
						tappable.OnTap(Tap.CreateFromRaycastHit(hit));
					}
		            break;
	            }

                _mouseDownTime = 0;
            }
		}


	}
}
﻿using strange.extensions.mediation.impl;

namespace Game.Inputs.Views
{
	public class InputTapDemuxerView : View
	{
		[Inject]
		public IInputDemuxer InputDemuxer { get; set; }

		public void Update()
		{
			InputDemuxer.FlushEvents();
		}
	}
}
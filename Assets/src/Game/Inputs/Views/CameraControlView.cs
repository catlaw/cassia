using System.Collections;
using DG.Tweening;
using strange.extensions.mediation.impl;
using UnityEngine;

namespace Game.Inputs.Views
{
    [RequireComponent(typeof(Camera))]
    public class CameraControlView : View
    {
        public Camera[] ZoomableCameras;

        private Camera _camera;
        private bool _isDragEnabled;
        private bool _canDrag;
        private bool _isDragging;
        private Vector3 _mouseDownPosition;
        private Plane _groundPlane;
        Vector3 _groundCamOffset;

        private void Awake()
        {
            _camera = GetComponent<Camera>();
            _groundPlane = new Plane(Vector3.up, Vector3.zero);
            _isDragEnabled = true;
            _canDrag = true;
            var groundPos = GetWorldPositionAtViewportPoint(0.5f, 0.5f);
            _groundCamOffset = Camera.main.transform.position - groundPos;
        }

        private void Update()
        {
            if (!_isDragEnabled || !_canDrag) return;

            if (UnityEngine.Input.GetMouseButton(0))
            {
                float HitDist;
                var MouseRay = _camera.ScreenPointToRay(UnityEngine.Input.mousePosition);

                if (_isDragging)
                {
                    _groundPlane.Raycast(MouseRay, out HitDist);
                    var CurClickPos = MouseRay.GetPoint(HitDist);
                    _camera.transform.position += _mouseDownPosition - CurClickPos;
                }
                else if (UnityEngine.Input.GetMouseButtonDown(0))
                {
                    _groundPlane.Raycast(MouseRay, out HitDist);
                    _mouseDownPosition = MouseRay.GetPoint(HitDist);
                    _isDragging = true;
                }
            }
            else
                _isDragging = false;
        }


        private Vector3 GetWorldPositionAtViewportPoint(float vx, float vy)
        {
            var worldRay = _camera.ViewportPointToRay(new Vector3(vx, vy, 0));
            float distanceToGround;
            _groundPlane.Raycast(worldRay, out distanceToGround);
            return worldRay.GetPoint(distanceToGround);
        }

        public void EnableDragging(bool isEnabled)
        {
            _isDragEnabled = isEnabled;
        }

        public void CenterOn(Vector3 position)
        {
            var targetPosition = position + _groundCamOffset;
            StartCoroutine(CoCenterOn(targetPosition));
        }

        private IEnumerator CoCenterOn(Vector3 targetPosition)
        {
            _canDrag = false;

            var tween = Camera.main.transform.DOMove(targetPosition, 0.25f);
            yield return tween.WaitForCompletion();

            _canDrag = true;
        }

        public void ZoomIn()
        {
            foreach (var zoomableCamera in ZoomableCameras)
            {
                ZoomIn(zoomableCamera);
            }
        }

        private void ZoomIn(Camera zoomableCamera)
        {
            DOTween.To(
                () => zoomableCamera.orthographicSize,
                x => zoomableCamera.orthographicSize = x,
                4, 0.3f);
        }

        public void ZoomOut()
        {
            foreach (var zoomableCamera in ZoomableCameras)
            {
                ZoomOut(zoomableCamera);
            }
        }

        private void ZoomOut(Camera zoomableCamera)
        {
            DOTween.To(
                () => zoomableCamera.orthographicSize,
                x => zoomableCamera.orthographicSize = x,
                5, 0.3f);
        }
    }
}
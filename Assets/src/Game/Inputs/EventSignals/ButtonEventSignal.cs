﻿using Common.Signals;
using strange.extensions.signal.impl;

namespace Game.Inputs.EventSignals
{
	public class ButtonEventSignal : Signal<ButtonType, int>, IButtonEventSignal
	{
	}

	public interface IButtonEventSignal : ISignal<ButtonType, int>
	{
	}
}
﻿using Common.DataContainers;
using Common.Signals;
using strange.extensions.signal.impl;

namespace Game.Inputs.EventSignals
{
	public class MapEventSignal : Signal<GridCoords>, IMapEventSignal
	{
	}

	public interface IMapEventSignal : ISignal<GridCoords>
	{
	}
}
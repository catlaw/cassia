﻿using Common.DataContainers;
using Common.Signals;
using Game.Models.Occupants.Units;
using strange.extensions.signal.impl;

namespace Game.Inputs.EventSignals
{
	public class UnitEventSignal : Signal<IUnit, GridCoords>, IUnitEventSignal
	{
	}

	public interface IUnitEventSignal : ISignal<IUnit, GridCoords>
	{
	}
}
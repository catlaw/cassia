﻿using Common.DataContainers;
using Game.Models.Occupants.Units;

namespace Game.Inputs
{
	public interface IInputEventHandler
	{
		void DispatchButtonEvent(ButtonType buttonType, int index);
		void DispatchMapEvent(GridCoords position);
		void DispatchUnitEvent(IUnit unit, GridCoords position);
	}
}
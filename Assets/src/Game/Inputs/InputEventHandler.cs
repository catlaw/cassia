﻿using Common.DataContainers;
using Game.Inputs.EventSignals;
using Game.Models.Occupants.Units;

namespace Game.Inputs
{
	public class InputEventHandler : IInputEventHandler
	{
		private readonly IButtonEventSignal _buttonEventSignal;
		private readonly IMapEventSignal _mapEventSignal;
		private readonly IUnitEventSignal _unitEventSignal;

		public InputEventHandler(IMapEventSignal mapEventSignal,
			IUnitEventSignal unitEventSignal, IButtonEventSignal
				buttonEventSignal)
		{
			_mapEventSignal = mapEventSignal;
			_unitEventSignal = unitEventSignal;
			_buttonEventSignal = buttonEventSignal;
		}

		public void DispatchButtonEvent(ButtonType buttonType, int index)
		{
			_buttonEventSignal.Dispatch(buttonType, index);
		}

		public void DispatchMapEvent(GridCoords position)
		{
			_mapEventSignal.Dispatch(position);
		}

		public void DispatchUnitEvent(IUnit unit, GridCoords position)
		{
			_unitEventSignal.Dispatch(unit, position);
		}
	}
}
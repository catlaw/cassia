﻿using Common.Signals;
using strange.extensions.signal.impl;
using UnityEngine;

namespace Game.Inputs.TapSignals
{
	public class MapTapSignal : Signal<Vector3>, IMapTapSignal
	{
	}

	public interface IMapTapSignal : ISignal<Vector3>
	{
	}
}
﻿using Common.Signals;
using strange.extensions.signal.impl;

namespace Game.Inputs.TapSignals
{
	public class ButtonTapSignal : Signal<ButtonType, int>, IButtonTapSignal
	{
	}

	public interface IButtonTapSignal : ISignal<ButtonType, int>
	{
	}
}
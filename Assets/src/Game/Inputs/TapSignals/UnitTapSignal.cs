﻿using Common.Signals;
using Game.Models.Occupants.Units;
using strange.extensions.signal.impl;
using UnityEngine;

namespace Game.Inputs.TapSignals
{
	public class UnitTapSignal : Signal<IUnit, Vector3>, IUnitTapSignal
	{
	}

	public interface IUnitTapSignal : ISignal<IUnit, Vector3>
	{
	}
}
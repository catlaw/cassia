﻿using System;
using System.Collections.Generic;
using System.Linq;
using Game.Controllers;
using Game.Inputs.TapSignals;
using Game.Models.Occupants.Units;
using UnityEngine;

namespace Game.Inputs
{
	public class InputDemuxer : IInputDemuxer
	{
		private readonly List<Action> _buttonEvents;
		private readonly IButtonTapSignal _buttonEventSignal;
	    private readonly IWorldGridCoordsConverter _gridCoordsConverter;
		private readonly IInputEventHandler _inputEventHandler;
		private readonly IMapTapSignal _mapEventSignal;
		private readonly List<Action> _otherEvents;
		private readonly IUnitTapSignal _unitEventSignal;

		public InputDemuxer(
            IWorldGridCoordsConverter gridCoordsConverter,
            IInputEventHandler inputEventHandler,
			IButtonTapSignal buttonEventSignal,
			IMapTapSignal mapEventSignal,
			IUnitTapSignal unitEventSignal)
		{
		    _gridCoordsConverter = gridCoordsConverter;
			_inputEventHandler = inputEventHandler;
			_buttonEventSignal = buttonEventSignal;
			_mapEventSignal = mapEventSignal;
			_unitEventSignal = unitEventSignal;

			_buttonEvents = new List<Action>();
			_otherEvents = new List<Action>();
		}

		public void Init()
		{
			_buttonEventSignal.AddListener(OnButtonTap);
			_mapEventSignal.AddListener(OnMapTap);
			_unitEventSignal.AddListener(OnUnitTap);
		}

		public void FlushEvents()
		{
			if (_buttonEvents.Any())
				_buttonEvents.First()();
			else if (_otherEvents.Any())
				_otherEvents.First()();

			_buttonEvents.Clear();
			_otherEvents.Clear();
		}

		private void OnButtonTap(ButtonType buttonType, int index)
		{
			_buttonEvents.Add(() => { _inputEventHandler.DispatchButtonEvent(buttonType, index); });
		}

		private void OnMapTap(Vector3 position)
		{
		    var gridCoords = _gridCoordsConverter.GetGridCoords(position);
			_otherEvents.Add(() => { _inputEventHandler.DispatchMapEvent(gridCoords); });
		}

		private void OnUnitTap(IUnit unit, Vector3 position)
		{
            var gridCoords = _gridCoordsConverter.GetGridCoords(position);
            _otherEvents.Add(() => { _inputEventHandler.DispatchUnitEvent(unit, gridCoords); });
		}
	}
}
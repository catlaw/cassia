﻿using strange.extensions.context.impl;

namespace Game.Contexts
{
	public class GameContextView : ContextView
	{
		private void Awake()
		{
			context = new GameContext(this);
			context.Start();
		}
	}
}
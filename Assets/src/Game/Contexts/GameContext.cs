﻿using Common.Configurables;
using Common.TimedSequencing;
using Common.Util;
using Configs;
using DG.Tweening;
using Game.Commands;
using Game.ConfigActions;
using Game.ConfigActions.Events;
using Game.ConfigActions.GameObjects;
using Game.ConfigActions.Methods;
using Game.ConfigActions.Params;
using Game.ConfigActions.Tweening;
using Game.ConfigActions.Util;
using Game.Controllers;
using Game.Controllers.Units;
using Game.Controllers.Units.UnitActions;
using Game.Inputs;
using Game.Inputs.EventSignals;
using Game.Inputs.TapSignals;
using Game.Inputs.Views;
using Game.Models.Occupants.Units;
using Game.Models.Occupants.Units.UnitActions;
using Game.Models.Pathing;
using Game.Models.Stage;
using Game.Models.TurnTransactions;
using Game.Signals.CommandSignals;
using Game.Signals.ControllerSignals;
using Game.Signals.ViewSignals;
using Game.Views;
using Game.Views.Animations;
using Game.Views.GameUI;
using Game.Views.PathPoints;
using Instantiator;
using Instantiator.GameObjectPools;
using Instantiator.Stage.Models.MeshFactory;
using Instantiator.Stage.Signals.CommandSignals;
using Instantiator.Stage.Signals.ViewSignals;
using Instantiator.Stage.Views;
using Instantiator.Units.Commands;
using Instantiator.Units.Views;
using Main.Contexts;
using UnityEngine;

namespace Game.Contexts
{
	public class GameContext : SignalContext
	{
		public GameContext(MonoBehaviour view) : base(view)
		{
		}

		protected override void mapBindings()
		{
			injectionBinder.Bind<ICoroutineRunner>().To<CoroutineRunner>().ToSingleton();
		    injectionBinder.Bind<ILoggyLog>().To<LoggyLog>();
		    injectionBinder.Bind<IParallelCoroutineRunner>().To<ParallelCoroutineRunner>();
			injectionBinder.Bind<IRandomNumberGenerator>().To<RandomNumberGenerator>();
			injectionBinder.Bind<ITimedSequenceFactory>().To<TimedSequenceFactory>();
			injectionBinder.Bind<ITimedSequence>().To<TimedSequence>();
			injectionBinder.Bind<ITimedSequenceActionFactory>().To<TimedSequenceActionFactory>();
			injectionBinder.Bind<ITimedSequenceRunner>().To<TimedSequenceRunner>();

			injectionBinder.Bind<IPathfinder>().To<Pathfinder>();
			injectionBinder.Bind<IUnitStats>().To<UnitStats>();
			injectionBinder.Bind<IStatsDeltaDelegateFactory>().To<StatsDeltaDelegateFactory>();
			injectionBinder.Bind<ITurnTransactionStateMachine>().To<TurnTransactionStateMachine>();
			injectionBinder.Bind<ITurnTransaction>().To<TurnTransaction>().ToSingleton();
			injectionBinder.Bind<ITurnTransactionState>().To<TurnTransactionState>().ToSingleton();
			injectionBinder.Bind<IAffectedActorFactory>().To<AffectedActorFactory>();
			injectionBinder.Bind<IAffectedUnitsLookup>().To<AffectedUnitsLookup>();
			injectionBinder.Bind<ITransactionalUnitMap>().To<TransactionalUnitMap>();

			injectionBinder.Bind<IPotentialActionEffectFactory>().To<PotentialActionEffectFactory>().ToSingleton();
			injectionBinder.Bind<IPotentialActionOutcomeFactory>().To<PotentialActionOutcomeFactory>();
			injectionBinder.Bind<IUnitActionRangeCalculator>().To<UnitActionRangeCalculator>();
			injectionBinder.Bind<IActionEffectBuildersFactory>().To<ActionEffectBuildersFactory>();
			injectionBinder.Bind<IDirectionCalculator>().To<DirectionCalculator>();
			injectionBinder.Bind<IDistanceCalculator>().To<DistanceCalculator>();
			injectionBinder.Bind<ITurnTransactionOutcomeFactory>().To<TurnTransactionOutcomeFactory>();
			injectionBinder.Bind<ITargeter>().To<Targeter>();
			injectionBinder.Bind<IEffector>().To<Effector>();
			injectionBinder.Bind<IActionOutcomeFactory>().To<ActionOutcomeFactory>();

			injectionBinder.Bind<ILevelMeshFactory>().To<LevelMeshFactory>();
			injectionBinder.Bind<IMap>().To<Map>().ToSingleton();
			injectionBinder.Bind<IWorldGridCoordsConverter>().To<WorldGridCoordsConverter>().ToSingleton();
			injectionBinder.Bind<ISelectableSquares>().To<StoredSquares>().ToSingleton();
		    injectionBinder.Bind<IAreaOfEffectSquares>().To<StoredSquares>().ToSingleton();

			injectionBinder.Bind<IUnitActionViewsStore>().To<UnitActionViewsStore>();
			injectionBinder.Bind<IConfigActionSets>().To<ConfigActionSets>();
			injectionBinder.Bind<IUnitAnimationFactory>().To<UnitAnimationFactory>();
			injectionBinder.Bind<IPlayableUnitAnimationSelector>().To<PlayableUnitAnimationSelector>();

			injectionBinder.Bind<IUnitActionEventFactory>().To<UnitActionEventFactory>();
			injectionBinder.Bind<IActionEffectEventFactory>().To<ActionEffectEventFactory>();
			injectionBinder.Bind<IActionEffectedUnitFactory>().To<ActionEffectedUnitFactory>();

			injectionBinder.Bind<IUnitActionEventExecutor>().To<UnitActionEventExecutor>();
		    injectionBinder.Bind<IActionEffectEventExecutor>().To<ActionEffectEventExecutor>();

			injectionBinder.Bind<IConfigActionSequenceFactory>().To<ConfigActionSequenceFactory>();
			injectionBinder.Bind<IConfigActionFactory>().To<ConfigActionFactory>();
			injectionBinder.Bind<IConfigActionExecutor>().To<ConfigActionExecutor>();
			injectionBinder.Bind<IConfigActionSequenceSetFactory>().To<ConfigActionSequenceSetFactory>();
			injectionBinder.Bind<IConfigActionSet>().To<ConfigActionSet>();
		    injectionBinder.Bind<IActionEffectConfigActions>().To<ActionEffectConfigActions>();

            injectionBinder.Bind<IResourceLoader>().To<ResourceLoader>();
			injectionBinder.Bind<IGameObjectInstantiator>().To<GameObjectInstantiator>();
		    injectionBinder.Bind<IDecorationFactory>().To<DecorationFactory>();
		    injectionBinder.Bind<IGameObjectFactory>().To<ResourcesOnlyGameObjectFactory>();
		    injectionBinder.Bind<IGameObjectPool>().To<LazyGameObjectPool>().ToSingleton();

		    injectionBinder.Bind<IGameObjectIdFactory>().To<GameObjectIdFactory>();
            injectionBinder.Bind<IConfigGameObjects>().To<ConfigGameObjects>().ToSingleton();
		    injectionBinder.Bind<IUnitViewDataFactory>().To<UnitViewDataFactory>();
			injectionBinder.Bind<IConfigActionGameObjectFactory>().To<ConfigActionGameObjectFactory>();
			injectionBinder.Bind<IDurationCalculator>().To<DurationCalculator>();
			injectionBinder.Bind<IParametricPointCalculator>().To<ParametricPointCalculator>();
		    injectionBinder.Bind<IPathWaypointsCalculator>().To<PathWaypointsCalculator>();
			injectionBinder.Bind<IPathPointsFactory>().To<PathPointsFactory>();
			injectionBinder.Bind<ITweenFactory>().To<TweenFactory>();
			injectionBinder.Bind<IDoDelegateFactory>().To<DoDelegateFactory>();

			injectionBinder.Bind<IInputDemuxer>().To<InputDemuxer>().ToSingleton();
			injectionBinder.Bind<IInputEventHandler>().To<InputEventHandler>();

			BindFactories();
			BindInputSignals();
			BindViewSignals();
			BindCommandSignals();
			BindViews();
			BindConfigs();
			BindAnimationMethods();
		}

		private void BindFactories()
		{
			injectionBinder.Bind<IUnit>().To<Unit>();
			injectionBinder.Bind<IConfigurableFactory<IUnit, UnitConfig>>().To<ConfigurableFactory<IUnit, UnitConfig>>();
			injectionBinder.Bind<IInstantiatableUnitViewFactory>().To<InstantiatableUnitViewFactory>();
			injectionBinder.Bind<IUnitViewMap>().To<UnitViewMap>().ToSingleton();

			injectionBinder.Bind<IUnitAction>().To<UnitAction>();
			injectionBinder.Bind<IConfigurableFactory<IUnitAction, UnitActionConfig>>()
				.To<ConfigurableFactory<IUnitAction, UnitActionConfig>>();
			injectionBinder.Bind<IConfigurableStore<IUnitAction, UnitActionConfig>>()
				.To<ConfigurableStore<IUnitAction, UnitActionConfig>>()
				.ToSingleton();
			injectionBinder.Bind<IUnitActionStore>().To<UnitActionStore>().ToSingleton();
		}

		private void BindInputSignals()
		{
			injectionBinder.Bind<IMapTapSignal>().To<MapTapSignal>().ToSingleton();
			injectionBinder.Bind<IUnitTapSignal>().To<UnitTapSignal>().ToSingleton();
			injectionBinder.Bind<IButtonTapSignal>().To<ButtonTapSignal>().ToSingleton();

			injectionBinder.Bind<IUnitEventSignal>().To<UnitEventSignal>().ToSingleton();
			injectionBinder.Bind<IMapEventSignal>().To<MapEventSignal>().ToSingleton();
			injectionBinder.Bind<IButtonEventSignal>().To<ButtonEventSignal>().ToSingleton();

			injectionBinder.Bind<IMoveUnitViewCompleteSignal>().To<MoveUnitViewCompleteSignal>().ToSingleton();
			injectionBinder.Bind<ITransactionAnimationCompleteSignal>().To<TransactionAnimationCompleteSignal>().ToSingleton();
		}

		private void BindViewSignals()
		{
			injectionBinder.Bind<IInstantiateUnitViewSignal>().To<InstantiateUnitViewSignal>().ToSingleton();
			injectionBinder.Bind<IInstantiateMapViewSignal>().To<InstantiateMapViewSignal>().ToSingleton();
			injectionBinder.Bind<IUpdateGameViewSignal>().To<UpdateGameViewSignal>().ToSingleton();
			injectionBinder.Bind<IHighlightSquaresViewSignal>().To<HighlightSquaresViewSignal>().ToSingleton();

			// these are per unit, not singletons
			injectionBinder.Bind<IMoveUnitViewSignal>().To<MoveUnitViewSignal>();
			injectionBinder.Bind<ISetUnitPositionViewSignal>().To<SetUnitPositionViewSignal>();
			injectionBinder.Bind<ITriggerUnitAnimationViewSignal>().To<TriggerUnitAnimationViewSignal>();
			injectionBinder.Bind<IApplyActionEffectViewSignal>().To<ApplyActionEffectViewSignal>();
		    injectionBinder.Bind<IHighlightViewSignal>().To<HighlightViewSignal>();
			injectionBinder.Bind<IDisplayActionsSignal>().To<DisplayActionsSignal>();
		}

		private void BindCommandSignals()
		{
			injectionBinder.Bind<IInstantiateUnitSignal>().To<InstantiateUnitSignal>().ToSingleton();
			injectionBinder.Bind<IApplyTurnTransactionSignal>().To<ApplyTurnTransactionSignal>().ToSingleton();
			injectionBinder.Bind<ISetHighlightedSquaresSignal>().To<SetHighlightedSquaresSignal>().ToSingleton();
		    injectionBinder.Bind<ISetAreaOfEffectSignal>().To<SetAreaOfEffectSignal>().ToSingleton();

			commandBinder.Bind<IInstantiateUnitSignal>().To<InstantiateUnitCommand>();
			commandBinder.Bind<IApplyTurnTransactionSignal>()
				.To<ApplyTurnTransactionCommand>()
				.To<PlayTransactionAnimationCommand>()
				.InSequence();
			commandBinder.Bind<ISetHighlightedSquaresSignal>().To<SetSelectableSquaresCommand>();
		    commandBinder.Bind<ISetAreaOfEffectSignal>().To<SetAreaOfEffectCommand>();
		}

		private void BindViews()
		{
			mediationBinder.Bind<GameInstantiatorView>().To<GameInstantiatorMediator>();
			mediationBinder.Bind<MapView>().To<MapMediator>();
			mediationBinder.Bind<SquareHighlightsView>().To<SquareHighlightsMediator>();
			mediationBinder.Bind<UnitView>().To<UnitMediator>();
			mediationBinder.Bind<GameUIView>().To<GameUIMediator>();
		    mediationBinder.Bind<CameraControlView>().To<CameraControlMediator>();
		}

		private void BindConfigs()
		{
			injectionBinder.Bind<IConfig<MapViewConfig>>().To<Config<MapViewConfig>>().ToSingleton();
			injectionBinder.Bind<IConfig<LevelConfig>>().To<Config<LevelConfig>>().ToSingleton();
			injectionBinder.Bind<IConfig<UnitViewsConfig>>().To<Config<UnitViewsConfig>>().ToSingleton();
			injectionBinder.Bind<IConfig<UnitActionsConfig>>().To<Config<UnitActionsConfig>>().ToSingleton();
			injectionBinder.Bind<IConfig<AnimationsConfig>>().To<Config<AnimationsConfig>>().ToSingleton();
		}

		private void BindAnimationMethods()
		{
			injectionBinder.Bind<IConfigActionMethod>().To<CreateMethod>().ToName(ConfigActionMethodType.Create);
			injectionBinder.Bind<IConfigActionMethod>().To<MoveToMethod>().ToName(ConfigActionMethodType.MoveTo);
		    injectionBinder.Bind<IConfigActionMethod>().To<MovePathMethod>().ToName(ConfigActionMethodType.MovePath);
		    injectionBinder.Bind<IConfigActionMethod>().To<WaitMethod>().ToName(ConfigActionMethodType.Wait);
            injectionBinder.Bind<IConfigActionMethod>().To<PlayUnitAnimationMethod>().ToName(ConfigActionMethodType.PlayUnitAnimation);
		    injectionBinder.Bind<IConfigActionMethod>().To<SetTextMethod>().ToName(ConfigActionMethodType.SetText);
		}

		public override void Launch()
		{
			DOTween.Init();

			injectionBinder.GetInstance<IInputDemuxer>().Init();

			commandBinder.Bind<GameStartupSignal>().To<GameStartupCommand>().Once();
			injectionBinder.GetInstance<GameStartupSignal>().Dispatch();
		}
	}
}
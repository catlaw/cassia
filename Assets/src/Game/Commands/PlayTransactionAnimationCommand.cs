﻿using System;
using System.Collections;
using System.Collections.Generic;
using Common.Util;
using Game.ConfigActions;
using Game.ConfigActions.Events;
using Game.ConfigActions.GameObjects;
using Game.ConfigActions.Methods;
using Game.ConfigActions.Params;
using Game.Models.TurnTransactions;
using Game.Signals.ControllerSignals;
using Game.Views.Animations;
using Instantiator;
using strange.extensions.command.impl;
using UnityEngine;

namespace Game.Commands
{
	public class PlayTransactionAnimationCommand : Command
	{
        private readonly IActionEffectEventExecutor _actionEffectEventExecutor;
	    private readonly IParallelCoroutineRunner _parallelCoroutineRunner;
        private readonly ICoroutineRunner _coroutineRunner;
        private readonly IUnitActionEventExecutor _unitActionEventExecutor;
	    private readonly IConfigActionGameObjectFactory _gameObjectFactory;
	    private readonly ITransactionAnimationCompleteSignal _animationCompleteSignal;

        public PlayTransactionAnimationCommand(
            ICoroutineRunner coroutineRunner, 
            IParallelCoroutineRunner parallelCoroutineRunner,
            IUnitActionEventExecutor unitActionEventExecutor,
            IActionEffectEventExecutor actionEffectEventExecutor, 
            IConfigActionGameObjectFactory gameObjectFactory, 
            ITransactionAnimationCompleteSignal animationCompleteSignal)
        {
            _coroutineRunner = coroutineRunner;
            _parallelCoroutineRunner = parallelCoroutineRunner;
            _unitActionEventExecutor = unitActionEventExecutor;
            _actionEffectEventExecutor = actionEffectEventExecutor;
            _gameObjectFactory = gameObjectFactory;
            _animationCompleteSignal = animationCompleteSignal;
        }

        public override void Execute()
		{
			_coroutineRunner.StartCoroutine(CoExecute());
		}

	    private IEnumerator CoExecute()
	    {
	        yield return _unitActionEventExecutor.Execute(UnitActionEventType.PreHit);

	        yield return _parallelCoroutineRunner.RunInParallel(new List<Coroutine>
	        {
	            _actionEffectEventExecutor.Execute(ActionEffectEventType.Damage),
	            _unitActionEventExecutor.Execute(UnitActionEventType.Hit)
	        });

            _gameObjectFactory.DestroyAll();
            _animationCompleteSignal.Dispatch();
	    }
	}
}
﻿using Common.DataContainers;
using Game.Models.TurnTransactions;
using strange.extensions.command.impl;

namespace Game.Commands
{
	public class SelectUnitActionTargetCommand : Command
	{
		[Inject]
		public ITurnTransaction TurnTransaction { get; set; }

		[Inject]
		public GridCoords TargetPosition { get; set; }

		public override void Execute()
		{
			TurnTransaction.SelectTargetPosition(TargetPosition);
		}
	}
}
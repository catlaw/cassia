﻿using System;
using System.Collections.Generic;
using Common.DataContainers;
using Game.Models.Stage;
using Game.Signals.ViewSignals;
using strange.extensions.command.impl;

namespace Game.Commands
{
	public class SetSelectableSquaresCommand : Command
	{
        private readonly IHighlightSquaresViewSignal _highlightSquaresViewSignal;
        private readonly ISelectableSquares _selectableSquares;
	    private readonly IEnumerable<GridCoords> _positions;

        public SetSelectableSquaresCommand( 
            IHighlightSquaresViewSignal highlightSquaresViewSignal,
            ISelectableSquares selectableSquares,
            IEnumerable<GridCoords> positions)
        {
            if (positions == null) throw new ArgumentNullException("positions");

            _highlightSquaresViewSignal = highlightSquaresViewSignal;
            _selectableSquares = selectableSquares;
            _positions = positions;
        }

		public override void Execute()
		{
            _selectableSquares.Set(_positions);
            _highlightSquaresViewSignal.Dispatch(_positions);
		}
	}
}
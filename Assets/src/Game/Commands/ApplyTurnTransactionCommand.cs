﻿using System;
using Game.Models.Stage;
using Game.Models.TurnTransactions;
using strange.extensions.command.impl;

namespace Game.Commands
{
	public class ApplyTurnTransactionCommand : Command
	{
		private readonly IMap _map;
		private readonly ITurnTransaction _turnTransaction;

		public ApplyTurnTransactionCommand(ITurnTransaction turnTransaction,
			IMap map)
		{
			_turnTransaction = turnTransaction;
			_map = map;
		}

		public override void Execute()
		{
			if (!_turnTransaction.IsReady)
			{
				var message = "Cannot apply UnitTurnTransaction if it is not ready.";
				throw new ArgumentException(message);
			}

			_map.SetOccupant(_turnTransaction.Actor, _turnTransaction.MovePosition);
			_turnTransaction.Actor.Facing = _turnTransaction.Facing;

			foreach (var outcome in _turnTransaction.Outcome.ActionOutcomes)
			{
				foreach (var effect in outcome.Effects)
				{
					outcome.AffectedUnit.Unit.ApplyActionEffect(effect, _turnTransaction.Actor.Stats);
				}
			}
		}
	}
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common.DataContainers;
using Game.Models.Occupants;
using Game.Models.Stage;
using Game.Models.TurnTransactions;
using Game.Signals.ViewSignals;
using strange.extensions.command.impl;
using UnityEngine;

namespace Game.Commands
{
    public class SetAreaOfEffectCommand : Command
    {
        private readonly IEnumerable<GridCoords> _areaOfEffect;
        private readonly IHighlightSquaresViewSignal _highlightSquaresViewSignal;
        private readonly IMap _map;
        private readonly ITurnTransaction _turnTransaction;
        private readonly IAreaOfEffectSquares _areaOfEffectSquares;

        public SetAreaOfEffectCommand(
            IEnumerable<GridCoords> areaOfEffect,
            IHighlightSquaresViewSignal highlightSquaresViewSignal, 
            IMap map,
            ITurnTransaction turnTransaction,
            IAreaOfEffectSquares areaOfEffectSquares)
        {
            _areaOfEffect = areaOfEffect;
            _highlightSquaresViewSignal = highlightSquaresViewSignal;
            _map = map;
            _turnTransaction = turnTransaction;
            _areaOfEffectSquares = areaOfEffectSquares;
        }

        public override void Execute()
        {
            var previousHighlightedSquares = _areaOfEffectSquares.Get().ToList();
            var squaresToUnhighlight = previousHighlightedSquares.Except(_areaOfEffect).ToList();
            var squaresToHighlight = _areaOfEffect.Except(previousHighlightedSquares).ToList();

            foreach (var squareToUnhighlight in squaresToUnhighlight)
            {
                if (squareToUnhighlight == _turnTransaction.MovePosition)
                {
                    Unhighlight(_turnTransaction.Actor);
                    continue;
                }

                var square = _map.GetSquare(squareToUnhighlight);
                if (square.Occupant != _turnTransaction.Actor)
                {
                    Unhighlight(square.Occupant);
                }
            }

            foreach (var squareToHighlight in squaresToHighlight)
            {
                if (squareToHighlight == _turnTransaction.MovePosition)
                {
                    Highlight(_turnTransaction.Actor);
                    continue;
                }

                var square = _map.GetSquare(squareToHighlight);
                if (square.Occupant != _turnTransaction.Actor)
                {
                    Highlight(square.Occupant);
                }
            }

            _highlightSquaresViewSignal.Dispatch(_areaOfEffect);
            _areaOfEffectSquares.Set(_areaOfEffect);
        }

        private void Highlight(IOccupant occupant)
        {
            occupant.HighlightViewSignal.Dispatch(true, Color.red);
        }

        private void Unhighlight(IOccupant occupant)
        {
            occupant.HighlightViewSignal.Dispatch(false, Color.white);
        }
    }
}
﻿using Configs;
using Game.Models.Occupants.Units;
using Game.Models.Stage;
using Instantiator.Stage.Models.MeshFactory;
using Instantiator.Stage.Signals.CommandSignals;
using Instantiator.Stage.Signals.ViewSignals;
using strange.extensions.command.impl;
using strange.framework.api;

namespace Game.Commands
{
	public class GameStartupCommand : Command
	{
		[Inject]
		public IInstantiateMapViewSignal InstantiateMapViewSignal { get; set; }

		[Inject]
		public IInstantiateUnitSignal InstantiateUnitSignal { get; set; }

		[Inject]
		public IUnitActionStore UnitActionStore { get; set; }

		[Inject]
		public IInstanceProvider InstanceProvider { get; set; }

		[Inject]
		public IMap Map { get; set; }

		public override void Execute()
		{
			// TODO: i need to try loading these with an artificial delay to simulate loading from online
			var mapViewConfigLoader = ConfigLoader.CreateForPath("Configs/MapView/");
			var mapViewConfig = mapViewConfigLoader.LoadConfig<MapViewConfig>("map_view_test");
			InstanceProvider.GetInstance<IConfig<MapViewConfig>>().LoadConfig(mapViewConfig);

			var levelConfigLoader = ConfigLoader.CreateForPath("Configs/Level/");
			var levelConfig = levelConfigLoader.LoadConfig<LevelConfig>("level_test");
			InstanceProvider.GetInstance<IConfig<LevelConfig>>().LoadConfig(levelConfig);

			var unitViewsConfigLoader = ConfigLoader.CreateForPath("Configs/UnitView/");
			var unitViewsConfig = unitViewsConfigLoader.LoadConfig<UnitViewsConfig>("unit_views_test");
			InstanceProvider.GetInstance<IConfig<UnitViewsConfig>>().LoadConfig(unitViewsConfig);

			var unitActionsConfigLoader = ConfigLoader.CreateForPath("Configs/UnitAction/");
			var unitActionsConfig = unitActionsConfigLoader.LoadConfig<UnitActionsConfig>("unit_actions_test");
			InstanceProvider.GetInstance<IConfig<UnitActionsConfig>>().LoadConfig(unitActionsConfig);

			var unitActionsViewConfigLoader = ConfigLoader.CreateForPath("Configs/Animations/");
			var unitActionViewsConfig = unitActionsViewConfigLoader.LoadConfig<AnimationsConfig>("animations_test");
			InstanceProvider.GetInstance<IConfig<AnimationsConfig>>().LoadConfig(unitActionViewsConfig);

			var levelMeshFactory = InstanceProvider.GetInstance<ILevelMeshFactory>();
			var mesh = levelMeshFactory.CreateMesh();

			InstantiateMapViewSignal.Dispatch(mesh);

			Map.LoadHeights();

			foreach (var levelUnitConfig in levelConfig.Units)
			{
				// TODO: should probably verify this in some other command
				var unitViewConfig = unitViewsConfig.UnitViews.GetById(levelUnitConfig.UnitViewId);
				InstantiateUnitSignal.Dispatch(levelUnitConfig, unitViewConfig);
			}
		}
	}
}
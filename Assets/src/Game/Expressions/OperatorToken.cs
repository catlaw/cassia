﻿using System;
using Game.Expressions.Operators;

namespace Game.Expressions
{
	public class OperatorToken : IExpressionToken
	{
		private readonly IExpressionOperator _expressionOperator;

		public OperatorToken(IExpressionOperator expressionOperator)
		{
			_expressionOperator = expressionOperator;
		}

		public object GetValue()
		{
			var message = string.Format("Cannot get value for {0}.", 
				typeof(OperatorToken).Name);
			throw new InvalidOperationException(message);
		}

		public void Apply(Type type, IExpressionResolver expression)
		{
			if (expression == null) throw new ArgumentNullException("expression");

			var operand0 = expression.Pop().GetValue();
			var operand1 = expression.Pop().GetValue();

			var result = _expressionOperator.Operate(type, operand0, operand1);
			expression.Push(result);
		}
	}
}
﻿using System;
using UnityEngine;

namespace Game.Expressions.Operators
{
	// not tested
	public class OperatorTokenMap : IOperatorTokenMap
	{
		public IExpressionToken GetOperator(string operatorString)
		{
			switch (operatorString)
			{
				case "+":
					return new OperatorToken(new AdditionOperator());

                case "-":
                    return new OperatorToken(new SubtractionOperator());
			}

		    var message = string.Format("Operator '{0}' is not supported.", operatorString);
			throw new NotSupportedException(message);
		}
	}
}
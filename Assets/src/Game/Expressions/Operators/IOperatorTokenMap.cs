﻿using System;
using UnityEngine;

namespace Game.Expressions.Operators
{
	public interface IOperatorTokenMap
	{
		IExpressionToken GetOperator(string operatorString);
	}
}
﻿using System;

namespace Game.Expressions.Operators
{
	public interface IExpressionOperator
	{
		IExpressionToken Operate(Type type, object operand0, object operand1);
	}
}
﻿using System;
using UnityEngine;

namespace Game.Expressions.Operators
{
    public class SubtractionOperator : IExpressionOperator
    {
        public IExpressionToken Operate(Type type, object operand0, object operand1)
        {
            try
            {
                if (type == typeof(int))
                {
                    var intResult = Convert.ToInt32(operand0) - Convert.ToInt32(operand1);
                    var result = Convert.ChangeType(intResult, type);
                    return new ValueToken(result);
                }

                if (type == typeof(float))
                {
                    var floatResult = Convert.ToSingle(operand0) - Convert.ToSingle(operand1);
                    var result = Convert.ChangeType(floatResult, type);
                    return new ValueToken(result);
                }

                if (type == typeof(Vector3))
                {
                    var vector0 = (Vector3)Convert.ChangeType(operand0, type);
                    var vector1 = (Vector3)Convert.ChangeType(operand1, type);
                    var result = Convert.ChangeType(vector0 - vector1, type);
                    return new ValueToken(result);
                }
            }
            catch (Exception)
            {
                var argumentMessage = string.Format("Failed to operate subtraction of type '{0}' with '{1}' and '{2}'.",
                    type.Name, operand0, operand1);
                throw new ArgumentException(argumentMessage);
            }


            var NotSupportedMessage = string.Format("Subtraction operator not supported for type '{0}'.", type.Name);
            throw new NotSupportedException(NotSupportedMessage);
        }
    }
}
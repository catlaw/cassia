﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Game.Expressions
{
	public class ExpressionResolver : IExpressionResolver
	{
		private readonly Stack<IExpressionToken> _tokenStack;

		public ExpressionResolver()
		{
			_tokenStack = new Stack<IExpressionToken>();
		}
		
		public void Push(IExpressionToken expectedToken)
		{
			if (expectedToken == null) throw new ArgumentNullException();

			_tokenStack.Push(expectedToken);
		}

		public IExpressionToken Pop()
		{
			if (!_tokenStack.Any())
			{
				var message = "Cannot pop token when stack empty.";
				throw new InvalidOperationException(message);
			}

			return _tokenStack.Pop();
		}

		public object Resolve()
		{
			if (!_tokenStack.Any())
			{
				var message = "Cannot resolve expression when token stack empty.";
				throw new InvalidOperationException(message);
			}

			var result =  _tokenStack.Pop().GetValue();

			if (_tokenStack.Any())
			{
				var message = "Cannot resolve expression when token stack has more than one token.";
				throw new InvalidOperationException(message);
			}

			return result;
		}
	}
}
﻿namespace Game.Expressions
{
    public class ExpressionResolverFactory : IExpressionResolverFactory
    {
        public IExpressionResolver CreateExpressionResolver()
        {
            return new ExpressionResolver();
        }
    }
}
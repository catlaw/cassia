﻿namespace Game.Expressions
{
    public interface IExpressionResolverFactory
    {
        IExpressionResolver CreateExpressionResolver();
    }
}
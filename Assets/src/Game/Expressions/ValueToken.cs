﻿using System;

namespace Game.Expressions
{
	public class ValueToken : IExpressionToken
	{
		private readonly object _value;

		public ValueToken(object value)
		{
			_value = value;
		}

		public object GetValue()
		{
			return _value;
		}

		public void Apply(Type type, IExpressionResolver expression)
		{
			if (expression == null) throw new ArgumentNullException("expression");

			expression.Push(this);
		}
	}
}
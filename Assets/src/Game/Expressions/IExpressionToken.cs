using System;

namespace Game.Expressions
{
	public interface IExpressionToken
	{
		object GetValue();
		void Apply(Type type, IExpressionResolver expression);
	}
}
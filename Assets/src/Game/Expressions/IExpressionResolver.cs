﻿namespace Game.Expressions
{
	public interface IExpressionResolver
	{
		IExpressionToken Pop();
		void Push(IExpressionToken expectedToken);
		object Resolve();
	}
}
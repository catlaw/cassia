﻿using System;
using System.Collections.Generic;

namespace Common.DataContainers
{
	public struct Facing
	{
		public static readonly Facing Left = new Facing(0, -1);
		public static readonly Facing Right = new Facing(0, 1);
		public static readonly Facing Back = new Facing(-1, 0);
		public static readonly Facing Forward = new Facing(1, 0);
		public static readonly Facing None = new Facing(0, 0);

		private Facing(int row, int column)
		{
			Row = row;
			Column = column;
		}

		public int Row { get; private set; }
		public int Column { get; private set; }

		public override string ToString()
		{
			return string.Format("Facing({0}, {1})", Row, Column);
		}

		public bool Equals(Facing other)
		{
			return Row == other.Row && Column == other.Column;
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			return obj is Facing && Equals((Facing) obj);
		}

		public static bool operator ==(Facing current, Facing other)
		{
			return current.Equals(other);
		}

		public static bool operator !=(Facing current, Facing other)
		{
			return !(current == other);
		}

		public override int GetHashCode()
		{
			unchecked
			{
				return (Row*397) ^ Column;
			}
		}

		public static Facing CreateFromList(IList<int> ints)
		{
			if (ints == null)
			{
				throw new ArgumentNullException("ints");
			}

			if (ints.Count != 2)
			{
				var message = string.Format("Can only create a Facing with a list of size 2.  List of size {0} was provided.",
					ints.Count);
				throw new ArgumentException(message);
			}

			var xMagnitude = Math.Abs(ints[0]);
			var zMagnitude = Math.Abs(ints[1]);

			if (zMagnitude >= xMagnitude)
			{
				return ints[1] > 0 ? Forward : Back;
			}
			return ints[0] > 0 ? Right : Left;
		}

		public static Facing CreateFromDirection(Direction direction)
		{
			switch (direction)
			{
				case Direction.Forward:
					return Forward;
				case Direction.Back:
					return Back;
				case Direction.Left:
					return Left;
				case Direction.Right:
					return Right;
			}

			throw new ArgumentException("Wtf?");
		}
	}
}
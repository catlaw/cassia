﻿using strange.framework.impl;

namespace Common.Binders
{
	public class ConfigurableBinding : Binding, IConfigurableBinding
	{
		public ConfigurableBinding()
		{
		}

		public ConfigurableBinding(Binder.BindingResolver resolver) : base(resolver)
		{
		}

		public new IConfigurableBinding Bind<T>()
		{
			return base.Bind<T>() as IConfigurableBinding;
		}

		public new IConfigurableBinding To<T>()
		{
			return base.To<T>() as IConfigurableBinding;
		}
	}
}
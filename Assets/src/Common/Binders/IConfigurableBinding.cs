﻿using strange.framework.api;

namespace Common.Binders
{
	public interface IConfigurableBinding : IBinding
	{
		new IConfigurableBinding Bind<T>();
		new IConfigurableBinding To<T>();
	}
}
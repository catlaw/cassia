using System;
using strange.extensions.signal.impl;

namespace Common.StateMachine
{
	// super awkward. rewrite
	public class SignalListener
	{
		private Action _listener;
		private Signal _signal;

		protected SignalListener()
		{
		}

		public static SignalListener Create(Signal signal, Action callback)
		{
			signal.AddListener(callback);
			return new SignalListener
			{
				_signal = signal,
				_listener = callback
			};
		}

		public virtual void CleanUp()
		{
			_signal.RemoveListener(_listener);
		}

		public override bool Equals(object obj)
		{
			if (obj == null) return false;

			var other = obj as SignalListener;
			if (other == null) return false;

			return GetComparerSignal() == other.GetComparerSignal();
		}

		public bool Equals(SignalListener other)
		{
			if (other == null) return false;

			return GetComparerSignal() == other.GetComparerSignal();
		}

		public override int GetHashCode()
		{
			return _signal.GetHashCode();
		}

		protected virtual BaseSignal GetComparerSignal()
		{
			return _signal;
		}
	}

	public class SignalListener<T0> : SignalListener
	{
		private Action<T0> _listener;
		private Signal<T0> _signal;

		public static SignalListener Create(Signal<T0> signal, Predicate<T0> predicate, Action callback)
		{
			var listener = GetListener(predicate, callback);
			signal.AddListener(listener);
			return new SignalListener<T0>
			{
				_signal = signal,
				_listener = listener
			};
		}

		public override void CleanUp()
		{
			_signal.RemoveListener(_listener);
		}

		private static Action<T0> GetListener(Predicate<T0> predicate, Action callback)
		{
			return x =>
			{
				if (!predicate(x)) return;
				callback();
			};
		}

		protected override BaseSignal GetComparerSignal()
		{
			return _signal;
		}
	}
}
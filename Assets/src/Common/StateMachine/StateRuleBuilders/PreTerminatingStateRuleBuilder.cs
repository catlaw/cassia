using System;
using System.Collections.Generic;
using System.Linq;

namespace Common.StateMachine.StateRuleBuilders
{
	public class PreTerminatingStateRuleBuilder<TKey, T0> : StateRuleBuilder<TKey>
	{
		private PreTerminatingStateRuleBuilder()
		{
		}

		public static PreTerminatingStateRuleBuilder<TKey, T0> Create(List<StateMachine<TKey>.BuildRuleDelegate> buildActions)
		{
			return new PreTerminatingStateRuleBuilder<TKey, T0>
			{
				_buildActions = buildActions
			};
		}

		public TerminatingStateRuleBuilder<TKey> Execute(Action<T0> action)
		{
			_buildActions.Add(rule =>
			{
				((StateRule<TKey, T0>) rule).ConditionalTransitions.Last().TransitionAction = action;
				return rule;
			});
			return TerminatingStateRuleBuilder<TKey>.Create();
		}
	}

	public class PreTerminatingStateRuleBuilder<TKey, T0, T1> : StateRuleBuilder<TKey>
	{
		private PreTerminatingStateRuleBuilder()
		{
		}

		public static PreTerminatingStateRuleBuilder<TKey, T0, T1> Create(
			List<StateMachine<TKey>.BuildRuleDelegate> buildActions)
		{
			return new PreTerminatingStateRuleBuilder<TKey, T0, T1>
			{
				_buildActions = buildActions
			};
		}

		public TerminatingStateRuleBuilder<TKey> Execute(Action<T0, T1> action)
		{
			_buildActions.Add(rule =>
			{
				((StateRule<TKey, T0, T1>) rule).ConditionalTransitions.Last().TransitionAction = action;
				return rule;
			});
			return TerminatingStateRuleBuilder<TKey>.Create();
		}
	}
}
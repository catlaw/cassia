﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common.StateMachine.StateRuleBuilders;

namespace Common.StateMachine
{
	public class State<TKey>
	{
		private List<StateRuleBuilder<TKey>> _ruleBuilders;
		private List<StateRule<TKey>> _rules;
		private TKey _stateKey;
		private Action<TKey> _transitionCallback;

		private State()
		{
		}

		public static State<TKey> CreateWithTransitionCallback(TKey StateKey, Action<TKey> transitionCallback)
		{
			return new State<TKey>
			{
				_stateKey = StateKey,
				_ruleBuilders = new List<StateRuleBuilder<TKey>>(),
				_rules = new List<StateRule<TKey>>(),
				_transitionCallback = transitionCallback
			};
		}

		public void AddRuleBuilder(StateRuleBuilder<TKey> stateRuleBuilder)
		{
			_ruleBuilders.Add(stateRuleBuilder);
		}

		public void BuildRules()
		{
			foreach (var ruleBuilder in _ruleBuilders)
			{
				var rule = ruleBuilder.Build();
				rule.SetTransitionCallback(_transitionCallback);

				if (_rules.Any(x => x.IsTransition && x.Signal == rule.Signal))
				{
					var message = string.Format("Duplicate transition defined for '{0}'.", _stateKey);
					throw new ArgumentException(message);
				}

				_rules.Add(rule);
			}
		}

		public void Enter()
		{
			foreach (var rule in _rules)
			{
				rule.EntryAction();
				rule.StartListening();
			}
		}

		public void Exit()
		{
			foreach (var rule in _rules)
			{
				rule.ExitAction();
				rule.StopListening();
			}
		}
	}
}
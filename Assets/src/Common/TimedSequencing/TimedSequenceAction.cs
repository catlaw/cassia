﻿using System;
using System.Collections;

namespace Common.TimedSequencing
{
	public class TimedSequenceAction : ITimedSequenceAction
	{
		public Action Action { get; set; }
		public float WaitTime { get; set; }
		public IEnumerator CoroutineEnumerator { get; set; }
	}
}
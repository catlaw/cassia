using System;
using System.Collections;
using System.Collections.Generic;

namespace Common.TimedSequencing
{
	public class TimedSequence : ITimedSequence
	{
		private readonly List<ITimedSequenceAction> _actions;
		private readonly ITimedSequenceRunner _runner;
		private readonly ITimedSequenceActionFactory _sequenceActionFactory;

		public TimedSequence(ITimedSequenceActionFactory sequenceActionFactory,
			ITimedSequenceRunner runner)
		{
			_sequenceActionFactory = sequenceActionFactory;
			_runner = runner;
			_actions = new List<ITimedSequenceAction>();
		}

		public IEnumerable<ITimedSequenceAction> GetActions()
		{
			return new List<ITimedSequenceAction>(_actions);
		}

		public ITimedSequence Do(Action action)
		{
			if (action == null) throw new ArgumentNullException("action");

			_actions.Add(_sequenceActionFactory.GetSequenceAction(action));
			return this;
		}

		public ITimedSequence Wait(float seconds)
		{
			_actions.Add(_sequenceActionFactory.GetWait(seconds));
			return this;
		}

		public ITimedSequence WaitThenDo(float seconds, Action action)
		{
			if (action == null) throw new ArgumentNullException("action");

			_actions.Add(_sequenceActionFactory.GetSequenceAction(action, seconds));
			return this;
		}

		public ITimedSequence DoCoroutine(IEnumerator coroutineEnumerator)
		{
			if (coroutineEnumerator == null) throw new ArgumentNullException("coroutineEnumerator");

			_actions.Add(_sequenceActionFactory.GetSequenceCoroutine(coroutineEnumerator));
			return this;
		}

		public void Run()
		{
			_runner.Run(this);
		}
	}
}
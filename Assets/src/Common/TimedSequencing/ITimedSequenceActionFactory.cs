﻿using System;
using System.Collections;

namespace Common.TimedSequencing
{
	public interface ITimedSequenceActionFactory
	{
		ITimedSequenceAction GetSequenceAction(Action action, float delay = 0);
		ITimedSequenceAction GetWait(float time);
		ITimedSequenceAction GetSequenceCoroutine(IEnumerator coroutineEnumerator);
	}
}
﻿namespace Common.TimedSequencing
{
	public interface ITimedSequenceRunner
	{
		void Run(ITimedSequence sequence);
	}
}
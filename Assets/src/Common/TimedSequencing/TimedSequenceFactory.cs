﻿using strange.framework.api;

namespace Common.TimedSequencing
{
	public class TimedSequenceFactory : ITimedSequenceFactory
	{
		private readonly IInstanceProvider _instanceProvider;

		public TimedSequenceFactory(IInstanceProvider instanceProvider)
		{
			_instanceProvider = instanceProvider;
		}

		public ITimedSequence GetTimedSequence()
		{
			return _instanceProvider.GetInstance<ITimedSequence>();
		}
	}
}
﻿namespace Common.TimedSequencing
{
	public interface ITimedSequenceFactory
	{
		ITimedSequence GetTimedSequence();
	}
}
using System;
using System.Collections;
using System.Collections.Generic;

namespace Common.TimedSequencing
{
	public interface ITimedSequence
	{
		IEnumerable<ITimedSequenceAction> GetActions();
		ITimedSequence Do(Action action);
		ITimedSequence Wait(float seconds);
		ITimedSequence WaitThenDo(float seconds, Action action);
		ITimedSequence DoCoroutine(IEnumerator coroutineEnumerator);
		void Run();
	}
}
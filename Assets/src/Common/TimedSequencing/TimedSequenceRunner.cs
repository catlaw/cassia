﻿using System.Collections;
using Common.Util;
using UnityEngine;

namespace Common.TimedSequencing
{
	// not tested
	public class TimedSequenceRunner : ITimedSequenceRunner
	{
		private readonly ICoroutineRunner _coroutineRunner;

		public TimedSequenceRunner(ICoroutineRunner coroutineRunner)
		{
			_coroutineRunner = coroutineRunner;
		}

		public void Run(ITimedSequence sequence)
		{
			_coroutineRunner.StartCoroutine(CoRun(sequence));
		}

		private IEnumerator CoRun(ITimedSequence sequence)
		{
			foreach (var action in sequence.GetActions())
			{
				if (action.WaitTime > 0)
					yield return new WaitForSeconds(action.WaitTime);
				if (action.CoroutineEnumerator != null)
					yield return _coroutineRunner.StartCoroutine(action.CoroutineEnumerator);
				action.Action();
			}
		}
	}
}
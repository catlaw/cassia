using System;
using System.Collections;

namespace Common.TimedSequencing
{
	public interface ITimedSequenceAction
	{
		Action Action { get; }
		float WaitTime { get; }
		IEnumerator CoroutineEnumerator { get; set; }
	}
}
﻿using System;
using System.Collections;

namespace Common.TimedSequencing
{
	public class TimedSequenceActionFactory : ITimedSequenceActionFactory
	{
		public ITimedSequenceAction GetSequenceAction(Action action, float delay = 0)
		{
			return new TimedSequenceAction
			{
				Action = action,
				WaitTime = delay
			};
		}

		public ITimedSequenceAction GetWait(float time)
		{
			return new TimedSequenceAction
			{
				Action = () => { },
				WaitTime = time
			};
		}

		public ITimedSequenceAction GetSequenceCoroutine(IEnumerator coroutineEnumerator)
		{
			return new TimedSequenceAction
			{
				Action = () => { },
				CoroutineEnumerator = coroutineEnumerator
			};
		}
	}
}
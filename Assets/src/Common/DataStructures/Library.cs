﻿using System;
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;

namespace Common.DataStructures
{
	public class Library<TValue> : IEnumerable<TValue>, ILibrary<TValue>
	{
		private Dictionary<string, TValue> _dictionary;

		private Library()
		{
		}

		public IEnumerator<TValue> GetEnumerator()
		{
			return _dictionary.Values.GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return _dictionary.Values.GetEnumerator();
		}

		public TValue GetById(string id)
		{
			if (id == null) throw new ArgumentNullException("id");

			if (!_dictionary.ContainsKey(id))
			{
				var message = string.Format("Id '{0}' not found on '{1}' Library.", id, typeof(TValue).Name);
				throw new ArgumentException(message);
			}

			return _dictionary[id];
		}

		public bool ContainsId(string id)
		{
			return _dictionary.ContainsKey(id);
		}

		public IEnumerable<KeyValuePair<string, TValue>> GetKeyValuePairs()
		{
			return new Dictionary<string, TValue>(_dictionary);
		}

		public static Library<TValue> CreateFromJToken(JToken jToken)
		{
			if (jToken == null) throw new ArgumentNullException("jToken");

			return new Library<TValue>
			{
				_dictionary = jToken.ToObject<Dictionary<string, TValue>>()
			};
		}

		public static Library<TValue> CreateEmpty()
		{
			return new Library<TValue>
			{
				_dictionary = new Dictionary<string, TValue>()
			};
		}
	}

	public class Library<TKey, TValue> : IEnumerable<TValue>, ILibrary<TKey, TValue>
	{
		private Dictionary<TKey, TValue> _dictionary;

		private Library()
		{
		}

		public IEnumerator<TValue> GetEnumerator()
		{
			return _dictionary.Values.GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return _dictionary.Values.GetEnumerator();
		}

		public TValue GetById(TKey id)
		{
			if (id == null) throw new ArgumentNullException("id");

			if (!_dictionary.ContainsKey(id))
			{
				var message = string.Format("Id '{0}' not found on '{1}' Library.", id, typeof(TValue).Name);
				throw new ArgumentException(message);
			}

			return _dictionary[id];
		}

		public IEnumerable<KeyValuePair<TKey, TValue>> GetKeyValuePairs()
		{
			return new Dictionary<TKey, TValue>(_dictionary);
		}

		public static Library<TKey, TValue> CreateFromJToken(JToken jToken)
		{
			if (jToken == null) throw new ArgumentNullException("jToken");

			return new Library<TKey, TValue>
			{
				_dictionary = jToken.ToObject<Dictionary<TKey, TValue>>()
			};
		}

		public static Library<TKey, TValue> CreateEmpty()
		{
			return new Library<TKey, TValue>
			{
				_dictionary = new Dictionary<TKey, TValue>()
			};
		}
	}
}
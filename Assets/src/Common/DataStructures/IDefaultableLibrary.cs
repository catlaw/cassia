﻿namespace Common.DataStructures
{
	public interface IDefaultableLibrary<TValue>
	{
		TValue GetById(string id);
	}

	public interface IDefaultableLibrary<TKey, TValue>
	{
		TValue GetById(TKey key);
	}
}
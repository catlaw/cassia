﻿using System.Collections.Generic;

namespace Common.DataStructures
{
	public interface ILibrary<TValue>
	{
		bool ContainsId(string id);
		TValue GetById(string id);
		IEnumerator<TValue> GetEnumerator();
		IEnumerable<KeyValuePair<string, TValue>> GetKeyValuePairs();
	}

	public interface ILibrary<TKey, TValue>
	{
		TValue GetById(TKey id);
		IEnumerator<TValue> GetEnumerator();
		IEnumerable<KeyValuePair<TKey, TValue>> GetKeyValuePairs();
	}
}
﻿using System;
using System.Collections.Generic;

namespace Common.DataStructures
{
	public class TwoWayMap<TForward, TBackward>
	{
		private readonly Dictionary<TBackward, TForward> _backwardDictionary;
		private readonly Dictionary<TForward, TBackward> _forwardDictionary;

		public TwoWayMap()
		{
			_forwardDictionary = new Dictionary<TForward, TBackward>();
			_backwardDictionary = new Dictionary<TBackward, TForward>();
		}

		public void Add(TForward forward, TBackward backward)
		{
			if (_forwardDictionary.ContainsKey(forward))
			{
				var message = string.Format("Cannot add duplicate forward key '{0}'.", forward);
				throw new ArgumentException(message);
			}

			if (_backwardDictionary.ContainsKey(backward))
			{
				var message = string.Format("Cannot add duplicate backward key '{0}'.", backward);
				throw new ArgumentException(message);
			}

			_forwardDictionary.Add(forward, backward);
			_backwardDictionary.Add(backward, forward);
		}

		public TBackward GetForward(TForward forwardKey)
		{
			if (!_forwardDictionary.ContainsKey(forwardKey))
			{
				var message = string.Format("Unknown forward key '{0}'.", forwardKey);
				throw new KeyNotFoundException(message);
			}

			return _forwardDictionary[forwardKey];
		}

		public TForward GetBackward(TBackward backwardKey)
		{
			if (!_backwardDictionary.ContainsKey(backwardKey))
			{
				var message = string.Format("Unknown backward key '{0}'.", backwardKey);
				throw new KeyNotFoundException(message);
			}

			return _backwardDictionary[backwardKey];
		}

		public bool ContainsForward(TForward forwardKey)
		{
			return _forwardDictionary.ContainsKey(forwardKey);
		}

		public bool ContainsBackward(TBackward backwardKey)
		{
			return _backwardDictionary.ContainsKey(backwardKey);
		}
	}
}
﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;

namespace Common.DataStructures
{
	public class DefaultableLibrary<TValue> : IDefaultableLibrary<TValue>
	{
		private readonly TValue _defaultValue;
		private readonly Dictionary<string, TValue> _dictionary;

		public DefaultableLibrary(JToken jToken)
		{
			if (jToken == null) throw new ArgumentNullException("jToken");

			_defaultValue = jToken["Default"].ToObject<TValue>();
			_dictionary = jToken["Values"].ToObject<Dictionary<string, TValue>>();
		}

		public TValue GetById(string id)
		{
			if (!_dictionary.ContainsKey(id)) return _defaultValue;

			return _dictionary[id];
		}
	}

	public class DefaultableLibrary<TKey, TValue> : IDefaultableLibrary<TKey, TValue>
	{
		private readonly TValue _defaultValue;
		private readonly Dictionary<TKey, TValue> _dictionary;

		public DefaultableLibrary(JToken jToken)
		{
			if (jToken == null) throw new ArgumentNullException("jToken");

			_defaultValue = jToken["Default"].ToObject<TValue>();
			_dictionary = jToken["Values"].ToObject<Dictionary<TKey, TValue>>();
		}

		public TValue GetById(TKey key)
		{
			if (_dictionary == null)
			{
				var message = "Cannot get before loading dictionary.";
				throw new InvalidOperationException(message);
			}

			if (!_dictionary.ContainsKey(key)) return _defaultValue;

			return _dictionary[key];
		}
	}
}
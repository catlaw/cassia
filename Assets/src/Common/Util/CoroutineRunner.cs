﻿using System.Collections;
using strange.extensions.context.api;
using UnityEngine;

namespace Common.Util
{
	public class CoroutineRunner : ICoroutineRunner
	{
		private CoroutineCreator _monoBehaviour;

		[Inject(ContextKeys.CONTEXT_VIEW)]
		public GameObject contextView { get; set; }

		public Coroutine StartCoroutine(IEnumerator coroutine)
		{
			return _monoBehaviour.StartCoroutine(coroutine);
		}

		public float DeltaTime
		{
			get { return Time.deltaTime; }
		}

		[PostConstruct]
		public void PostConstruct()
		{
			_monoBehaviour = contextView.AddComponent<CoroutineCreator>();
		}
	}
}
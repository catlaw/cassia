﻿namespace Common.Util
{
	public interface IRandomNumberGenerator
	{
		/// <summary>
		///     Generates a number between 0.0 and 1.0 inclusive.
		/// </summary>
		float GetValue();
	}
}
﻿using System.Collections;
using UnityEngine;

namespace Common.Util
{
	public interface ICoroutineRunner
	{
		float DeltaTime { get; }
		Coroutine StartCoroutine(IEnumerator coroutine);
	}
}
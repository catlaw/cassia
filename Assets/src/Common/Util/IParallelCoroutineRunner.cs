﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Common.Util
{
    public interface IParallelCoroutineRunner
    {
        Coroutine RunInParallel(IList<IEnumerator> iEnumerators);
        Coroutine RunInParallel(IList<Coroutine> coroutines);
    }
}
﻿using System.Collections;
using UnityEngine;

namespace Common.Util
{
    public class CoroutineObserver
    {
        public CoroutineObserver(ICoroutineRunner runner, Coroutine coroutine)
        {
            IsRunning = true;
            runner.StartCoroutine(coMonitorCoroutine(coroutine));
        }

        public IEnumerator coMonitorCoroutine(Coroutine coroutine)
        {
            yield return coroutine;
            IsRunning = false;
        }

        public bool IsRunning { get; private set; }
    }
}
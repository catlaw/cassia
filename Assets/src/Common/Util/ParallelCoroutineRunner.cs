﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Common.Util
{
    // not tested. coroutines are weird as hell.
    public class ParallelCoroutineRunner : IParallelCoroutineRunner
    {
        private readonly ICoroutineRunner _runner;

        public ParallelCoroutineRunner(ICoroutineRunner runner)
        {
            _runner = runner;
        }

        public Coroutine RunInParallel(IList<Coroutine> coroutines)
        {
            if (coroutines == null) throw new ArgumentNullException("coroutines");

            return _runner.StartCoroutine(coRunCoroutines(coroutines));
        }

        private IEnumerator coRunCoroutines(IList<Coroutine> coroutines)
        {
            var coroutineObservers = coroutines.Where(x => x != null).Select(x => new CoroutineObserver(_runner, x)).ToList();
            while (coroutineObservers.Any(x => x.IsRunning))
            {
                yield return null;
            }
        }

        public Coroutine RunInParallel(IList<IEnumerator> iEnumerators)
        {
            if (iEnumerators == null) throw new ArgumentNullException("iEnumerators");

            var coroutines = iEnumerators.Where(x => x != null).Select(x => _runner.StartCoroutine(x)).ToList();
            return RunInParallel(coroutines);
        }
    }
}
﻿using UnityEngine;

namespace Common.Util
{
	public class RandomNumberGenerator : IRandomNumberGenerator
	{
		public float GetValue()
		{
			return Random.value;
		}
	}
}
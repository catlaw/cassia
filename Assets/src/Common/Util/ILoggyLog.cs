﻿namespace Common.Util
{
    public interface ILoggyLog
    {
        void Log(string message);
        void LogError(string message);
        void LogWarning(string message);
    }
}
﻿using System;
using strange.framework.api;

namespace Common.Configurables
{
	public class ConfigurableFactory<T, TConfig> : IConfigurableFactory<T, TConfig> where T : IConfigurable<TConfig>
	{
		private readonly IInstanceProvider _instanceProvider;

		public ConfigurableFactory(IInstanceProvider _instanceProvider)
		{
			this._instanceProvider = _instanceProvider;
		}

		public T GetConfiguredInstance(TConfig config)
		{
			if (config == null) throw new ArgumentNullException("config");

			var instance = _instanceProvider.GetInstance<T>();
			instance.LoadConfig(config);
			return instance;
		}
	}
}
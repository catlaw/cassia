﻿using Common.DataStructures;

namespace Common.Configurables
{
	public interface IConfigurableStore<T, TConfig> where T : IConfigurable<TConfig>
	{
		bool RequiresLoading { get; }
		T GetById(string id);
		void LoadConfigs(ILibrary<TConfig> configs);
	}
}
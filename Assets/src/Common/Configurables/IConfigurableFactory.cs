﻿namespace Common.Configurables
{
	public interface IConfigurableFactory<T, TConfig> where T : IConfigurable<TConfig>
	{
		T GetConfiguredInstance(TConfig config);
	}
}
﻿namespace Common.Configurables
{
	public interface IConfigurable<TConfig>
	{
		void LoadConfig(TConfig config);
	}
}
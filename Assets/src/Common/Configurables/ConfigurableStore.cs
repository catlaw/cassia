﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common.DataStructures;

namespace Common.Configurables
{
	public class ConfigurableStore<T, TConfig> : IConfigurableStore<T, TConfig> where T : IConfigurable<TConfig>
	{
		private readonly IConfigurableFactory<T, TConfig> _factory;
		private Dictionary<string, T> _configurables;

		public ConfigurableStore(IConfigurableFactory<T, TConfig> factory)
		{
			_factory = factory;
		}

		public void LoadConfigs(ILibrary<TConfig> configs)
		{
			if (configs == null) throw new ArgumentNullException("configs");

			_configurables = configs.GetKeyValuePairs().ToDictionary(
				x => x.Key,
				y => _factory.GetConfiguredInstance(y.Value));
		}

		public T GetById(string id)
		{
			if (RequiresLoading) throw new InvalidOperationException("Cannot get before loading.");
			if (id == null) throw new ArgumentNullException("id");

			if (!_configurables.ContainsKey(id))
			{
				var message = string.Format("Could not get {0} with id '{1}'.", typeof(T).Name, id);
				throw new ArgumentException(message);
			}

			return _configurables[id];
		}

		public bool RequiresLoading
		{
			get { return _configurables == null; }
		}
	}
}
﻿using UnityEngine;

namespace Instantiator
{
	public class Decoration : IDecoration
	{
		public IAsset Asset { get; set; }
		public Vector3 Position { get; set; }
		public Vector3 Rotation { get; set; }
	}
}
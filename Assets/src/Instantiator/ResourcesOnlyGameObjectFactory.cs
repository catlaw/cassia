﻿using System;
using UnityEngine;

namespace Instantiator
{
    public class ResourcesOnlyGameObjectFactory : IGameObjectFactory
    {
        private readonly IGameObjectInstantiator _gameObjectInstantiator;
        private readonly IResourceLoader _resourceLoader;

        public ResourcesOnlyGameObjectFactory(IResourceLoader resourceLoader, 
            IGameObjectInstantiator gameObjectInstantiator)
        {
            _resourceLoader = resourceLoader;
            _gameObjectInstantiator = gameObjectInstantiator;
        }

        public GameObject Instantiate(IAsset asset)
        {
            if (asset == null) throw new ArgumentNullException("asset");

            var prefab = _resourceLoader.Load<GameObject>(asset.Path);
            return _gameObjectInstantiator.InstantiatePrefab(prefab);
        }

        public void Destroy(GameObject gameObject)
        {
            if (gameObject == null) throw new ArgumentNullException("gameObject");

            _gameObjectInstantiator.Destroy(gameObject);
        }
    }
}
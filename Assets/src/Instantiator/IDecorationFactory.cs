﻿using Configs;

namespace Instantiator
{
	public interface IDecorationFactory
	{
		IDecoration CreateDecoration(DecorationConfig config);
	}
}
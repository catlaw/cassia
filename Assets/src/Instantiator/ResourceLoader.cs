﻿using System;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Instantiator
{
	public class ResourceLoader : IResourceLoader
	{
		public T Load<T>(string path) where T : Object
		{
			if (path == null) throw new ArgumentNullException("path");

			var resource = Resources.Load<T>(path);

			if (resource == null)
			{
				var message = string.Format("Could not find resource '{0}'.", path);
				throw new ArgumentException(message);
			}

			return resource;
		}
	}
}
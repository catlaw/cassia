﻿using UnityEngine;

namespace Instantiator
{
	public interface IResourceLoader
	{
		T Load<T>(string path) where T : Object;
	}
}
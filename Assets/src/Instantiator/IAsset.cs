﻿namespace Instantiator
{
	public interface IAsset
	{ 
		string Bundle { get; }
		string Path { get; }
	}
}
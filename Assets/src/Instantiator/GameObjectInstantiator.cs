﻿using System;
using Game.ConfigActions.GameObjects;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Instantiator
{
	public class GameObjectInstantiator : IGameObjectInstantiator
	{
		public GameObject InstantiatePrefab(GameObject prefab)
		{
			if (prefab == null) throw new ArgumentNullException("prefab");

			return Object.Instantiate(prefab);
		}

		public void Destroy(GameObject gameObject)
		{
			Object.Destroy(gameObject);
		}
	}
}
﻿using System;
using Configs;

namespace Instantiator
{
	public class DecorationFactory : IDecorationFactory
	{
		public IDecoration CreateDecoration(DecorationConfig config)
		{
			return new Decoration
			{
				Asset = config.Asset,
				Position = config.Position,
				Rotation = config.Rotation
			};
		}
	}
}
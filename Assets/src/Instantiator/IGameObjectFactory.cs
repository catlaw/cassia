﻿using UnityEngine;

namespace Instantiator
{
    public interface IGameObjectFactory
    {
        GameObject Instantiate(IAsset asset);
        void Destroy(GameObject gameObject);
    }
}
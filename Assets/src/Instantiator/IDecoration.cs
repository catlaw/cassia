﻿using UnityEngine;

namespace Instantiator
{
	public interface IDecoration
	{
		IAsset Asset { get; }
		Vector3 Position { get; }
		Vector3 Rotation { get; }
	}
}
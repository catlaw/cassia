﻿using Common.DataContainers;
using Configs;
using Game.Models.Occupants.Units;

namespace Instantiator.Units.Views
{
	public class InstantiatableUnitView : IInstantiatableUnitView
	{
		public IUnit Unit { get; internal set; }
		public IUnitViewData Data { get; internal set; }
		public GridPosition StartPosition { get; set; }
	}
}
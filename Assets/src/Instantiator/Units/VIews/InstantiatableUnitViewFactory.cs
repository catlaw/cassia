﻿using System;
using Common.Configurables;
using Configs;
using Game.Models.Occupants.Units;

namespace Instantiator.Units.Views
{
	public class InstantiatableUnitViewFactory : IInstantiatableUnitViewFactory
	{
        private readonly IConfigurableFactory<IUnit, UnitConfig> _unitFactory;
        private readonly IUnitViewDataFactory _unitViewDataFactory;

        public InstantiatableUnitViewFactory(IConfigurableFactory<IUnit, UnitConfig> unitFactory,
            IUnitViewDataFactory unitViewDataFactory)
        {
            _unitFactory = unitFactory;
            _unitViewDataFactory = unitViewDataFactory;
        }

        public IInstantiatableUnitView Build(LevelUnitConfig config, UnitViewConfig viewConfig)
		{
            return new InstantiatableUnitView
            {
                Unit = _unitFactory.GetConfiguredInstance(config.Unit),
                StartPosition = config.StartPosition,
                Data = _unitViewDataFactory.CreateUnitViewData(viewConfig)
            };
		}
	}
}
﻿using System.Collections.Generic;
using Common.DataContainers;
using Configs;
using Game.Models.Occupants.Units;

namespace Instantiator.Units.Views
{
	public interface IInstantiatableUnitView
	{
		GridPosition StartPosition { get; }
		IUnit Unit { get; }
	    IUnitViewData Data { get; }
	}
}
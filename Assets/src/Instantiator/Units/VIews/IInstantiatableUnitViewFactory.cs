﻿using Configs;

namespace Instantiator.Units.Views
{
	public interface IInstantiatableUnitViewFactory
	{
		IInstantiatableUnitView Build(LevelUnitConfig config, UnitViewConfig viewConfig);
	}
}
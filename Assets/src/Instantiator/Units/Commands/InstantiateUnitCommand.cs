﻿using Configs;
using Game.Models.Occupants.Units;
using Game.Models.Stage;
using Instantiator.Stage.Signals.ViewSignals;
using Instantiator.Units.Views;
using strange.extensions.command.impl;

namespace Instantiator.Units.Commands
{
	public class InstantiateUnitCommand : Command
	{
		private readonly IInstantiatableUnitViewFactory _instantiatableUnitViewFactory;
	    private readonly IUnitViewDataFactory _unitViewDataFactory;
		private readonly IInstantiateUnitViewSignal _instantiatableUnitViewSignal;
		private readonly LevelUnitConfig _levelUnitConfig;
		private readonly IMap _map;
		private readonly UnitViewConfig _unitViewConfig;
		private readonly IUnitViewMap _unitViewMap;

		public InstantiateUnitCommand(IMap map,
			IInstantiatableUnitViewFactory instantiatableUnitViewFactory,
            IUnitViewDataFactory unitViewDataFactory,
			IInstantiateUnitViewSignal instantiatableUnitViewSignal,
			IUnitViewMap unitViewMap,
			LevelUnitConfig levelUnitConfig,
			UnitViewConfig unitViewConfig)
		{
			_map = map;
			_instantiatableUnitViewFactory = instantiatableUnitViewFactory;
		    _unitViewDataFactory = unitViewDataFactory;
			_instantiatableUnitViewSignal = instantiatableUnitViewSignal;
			_unitViewMap = unitViewMap;
			_levelUnitConfig = levelUnitConfig;
			_unitViewConfig = unitViewConfig;
		}

		public override void Execute()
		{
			var instantiatableUnitView = _instantiatableUnitViewFactory.Build(_levelUnitConfig, _unitViewConfig);
			instantiatableUnitView.Unit.Facing = _levelUnitConfig.StartPosition.Facing;

			_map.SetOccupant(instantiatableUnitView.Unit, _levelUnitConfig.StartPosition.GridCoords);
			_instantiatableUnitViewSignal.Dispatch(instantiatableUnitView, _levelUnitConfig.StartPosition.GridCoords);

		    var unitViewData = _unitViewDataFactory.CreateUnitViewData(_unitViewConfig);

			_unitViewMap.Add(instantiatableUnitView.Unit, unitViewData);
		}
	}
}
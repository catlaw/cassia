﻿using Common.DataContainers;

namespace Instantiator.Stage.Models.MeshModel
{
	public interface IChunkFactory
	{
		IChunk CreateChunkAt(IntVector3 position);
	}
}
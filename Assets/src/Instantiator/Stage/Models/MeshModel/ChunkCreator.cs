﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common.DataContainers;
using Common.Exceptions;
using Configs;

namespace Instantiator.Stage.Models.MeshModel
{
	public class ChunkCreator
	{
		private IChunkFactory _factory;

		private ChunkCreator()
		{
		}

		public static ChunkCreator CreateWithChunkFactory(IChunkFactory chunkFactory)
		{
			return new ChunkCreator
			{
				_factory = chunkFactory
			};
		}

		public IList<IChunk> CreateChunks(int[,] heights, SquareViewConfig[,] squareViews, MapViewConfig mapViewConfig)
		{
			if (heights == null) throw new ArgumentNullException("heights");
			if (squareViews == null) throw new ArgumentNullException("squareViews");
			if (mapViewConfig == null) throw new ArgumentNullException("mapViewConfig");

			if (heights.GetLength(0) != squareViews.GetLength(0) ||
			    heights.GetLength(1) != squareViews.GetLength(1))
			{
				var message =
					string.Format(
						"Cannot create chunks because Map and MapView sizes do not match. Map is size ({0}, {1}), MapView is size ({2}, {3}).",
						heights.GetLength(0), heights.GetLength(1), squareViews.GetLength(0), squareViews.GetLength(1));
				throw new InvalidConfigException(message);
			}

			var chunks = new List<IChunk>();

			var mapWidth = heights.GetLength(0);
			var mapDepth = heights.GetLength(1);

			for (var column = 0; column < mapDepth; ++column)
			{
				for (var row = 0; row < mapWidth; ++row)
				{
					var height = heights[row, column];
					var view = squareViews[row, column];
					var topTextures = new Queue<string>(view.TopTextures);
					var rightTextures = new Queue<string>(view.RightTextures);
					var frontTextures = new Queue<string>(view.FrontTextures);

					var maxHeight = height - 1;
					for (var currentHeight = maxHeight; currentHeight >= 0; --currentHeight)
					{
						var position = new IntVector3(row, currentHeight, column);
						var chunk = _factory.CreateChunkAt(position);

						if (currentHeight == maxHeight)
							chunk.AddTopFaceWithTextureTileName(topTextures.Any() 
                                ? topTextures.Dequeue() 
                                : mapViewConfig.DefaultTopTexture);

						if (IsFurthestRight(heights, row) || HasNoRightCoveringChunk(heights, currentHeight, new GridCoords(row, column)))
							chunk.AddRightFaceWithTextureTileName(rightTextures.Any() 
                                ? rightTextures.Dequeue() 
                                : mapViewConfig.DefaultRightTexture);

						if (IsFurthestFront(heights, column) ||
						    HasNoFrontCoveringChunk(heights, currentHeight, new GridCoords(row, column)))
							chunk.AddFrontFaceWithTextureTileName(frontTextures.Any() 
                                ? frontTextures.Dequeue() 
                                : mapViewConfig.DefaultFrontTexture);

						chunks.Add(chunk);
					}
				}
			}

			return chunks;
		}

		private static bool IsFurthestRight(int[,] heights, int row)
		{
			var mapWidth = heights.GetLength(0);
			return row + 1 >= mapWidth;
		}

		private static bool HasNoRightCoveringChunk(int[,] heights, int currentHeight, GridCoords gridCoords)
		{
			return heights[gridCoords.Row + 1, gridCoords.Column] <= currentHeight;
		}

		private static bool HasNoFrontCoveringChunk(int[,] heights, int currentHeight, GridCoords gridCoords)
		{
			return heights[gridCoords.Row, gridCoords.Column + 1] <= currentHeight;
		}

		private static bool IsFurthestFront(int[,] heights, int column)
		{
			var mapDepth = heights.GetLength(1);
			return column + 1 >= mapDepth;
		}
	}
}
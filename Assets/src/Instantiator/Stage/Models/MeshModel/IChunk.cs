﻿using System.Collections.Generic;

namespace Instantiator.Stage.Models.MeshModel
{
	public interface IChunk
	{
		IEnumerable<ChunkFace> GetFaces();

		void AddTopFaceWithTextureTileName(string textureTileName);
		void AddRightFaceWithTextureTileName(string tileTextureName);
		void AddRightFaceWithTextureTileName();
		void AddBackFaceWithTextureTileName();
		void AddFrontFaceWithTextureTileName(string tileTextureName);
	}
}
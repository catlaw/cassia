﻿using System;
using System.Collections.Generic;
using Common.DataContainers;
using UnityEngine;

namespace Instantiator.Stage.Models.MeshModel
{
	public class ChunkFace
	{
		private IList<IntVector3> _corners;
		private IList<Vector2> _uv;

		public static ChunkFace CreateWithCornersAndUv(IList<IntVector3> corners, IList<Vector2> uv)
		{
			if (corners == null)
			{
				throw new ArgumentNullException("corners");
			}

			if (corners.Count != 4)
			{
				var message = string.Format("Expected a list of 4 corners, but got {0} instead.", corners.Count);
				throw new ArgumentException(message);
			}

			if (uv == null)
			{
				throw new ArgumentNullException("uv");
			}

			if (uv.Count != 4)
			{
				var message = string.Format("Expected a list of 4 uvs, but got {0} instead.", uv.Count);
				throw new ArgumentException(message);
			}

			return new ChunkFace
			{
				_corners = corners,
				_uv = uv
			};
		}

		public IEnumerable<IntVector3> GetCornerPositions()
		{
			return _corners;
		}

		public IEnumerable<Vector2> GetUv()
		{
			return _uv;
		}
	}
}
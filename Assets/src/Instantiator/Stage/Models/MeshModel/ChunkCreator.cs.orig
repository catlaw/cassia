<<<<<<< HEAD:Assets/Scripts/LevelBuilder/MeshModel/ChunkCreator.cs
﻿using System;
using System.Collections.Generic;
using DataContainers;
using LevelBuilder.Config;
using UnityEngine;

namespace LevelBuilder.MeshModel
{
	public class ChunkCreator
	{
		private IChunkFactory _factory;

		private ChunkCreator() { }

		public static ChunkCreator CreateWithChunkFactory(IChunkFactory chunkFactory)
		{
			return new ChunkCreator
			{
				_factory = chunkFactory
			};
		}

		public IList<IChunk> CreateChunksForConfig(LevelConfig levelConfig, TextureConfig textureConfig)
		{
		    if (levelConfig == null) throw new ArgumentNullException("levelConfig");
            if (textureConfig == null) throw new ArgumentNullException("textureConfig");

		    if (levelConfig.Squares == null)
		    {
		        var message = "Could not create a LevelConfig because Squares is null.";
                throw new ArgumentException(message);
		    }

			var chunks = new List<IChunk>();

		    var squares = levelConfig.Squares;
			var mapWidth = squares.GetLength(0);
			var mapDepth = squares.GetLength(1);

			for (var column = 0; column < mapDepth; ++column)
			{
				for (var row = 0; row < mapWidth; ++row)
				{
				    var square = squares[row, column];
					var topFaceTexturer = FaceTexturer.CreateWithNameAndTextures("Top", square.TopTextures);
					var rightFaceTexturer = FaceTexturer.CreateWithNameAndTextures("Right", square.RightTextures);
					var frontFaceTexturer = FaceTexturer.CreateWithNameAndTextures("Front", square.FrontTextures);

					var maxHeight = square.Height - 1; // require 0 height squares
					for (var currentHeight = 0; currentHeight <= maxHeight; ++currentHeight)
					{
						var position = new IntVector3(row, currentHeight, column);
						var chunk = _factory.CreateChunkAt(position);

						try
						{
							if (currentHeight == maxHeight)
								chunk.AddTopFaceWithTextureTileName(topFaceTexturer.GetNextTextureTile());

							if (IsFurthestRight(squares, row) || HasNoRightCoveringChunk(squares, currentHeight, new GridCoords(row, column)))
								chunk.AddRightFaceWithTextureTileName(rightFaceTexturer.GetNextTextureTile());

							if (IsFurthestFront(squares, column) || HasNoFrontCoveringChunk(squares, currentHeight, new GridCoords(row, column)))
								chunk.AddFrontFaceWithTextureTileName(frontFaceTexturer.GetNextTextureTile());
						}
						catch (FaceTexturerException e)
						{
							string message = string.Format("Not enough {0} Textures at square {1}, {2}.", e.FaceName, row, column);
							throw new ArgumentException(message);
						}

						chunks.Add(chunk);
					}

					try
					{
						topFaceTexturer.AssertEmpty();
						rightFaceTexturer.AssertEmpty();
						frontFaceTexturer.AssertEmpty();
					}
					catch (FaceTexturerException e)
					{
						string message = string.Format("Too many {0} Textures at square {1}, {2}.", e.FaceName, row, column);
						throw new ArgumentException(message);
					}
				}
			}

			return chunks;
		}

		private static bool IsFurthestRight(Square[,] squares, int row)
		{
			var mapWidth = squares.GetLength(0);
			return row + 1 >= mapWidth;
		}

		private static bool HasNoRightCoveringChunk(Square[,] squares, int currentHeight, GridCoords gridCoords)
		{
			return squares[gridCoords.Row + 1, gridCoords.Column].Height <= currentHeight;
		}

		private static bool HasNoFrontCoveringChunk(Square[,] squares, int currentHeight, GridCoords gridCoords)
		{
			return squares[gridCoords.Row, gridCoords.Column + 1].Height <= currentHeight;
		}

		private static bool IsFurthestFront(Square[,] squares, int column)
		{
			var mapDepth = squares.GetLength(1);
            return column + 1 >= mapDepth;
		}
	}
}
=======
﻿using System;
using System.Collections.Generic;
using DataContainers;

namespace LevelBuilder.Model.MeshModel
{
	public class ChunkCreator
	{
		private IChunkFactory _factory;

		private ChunkCreator() { }

		public static ChunkCreator CreateWithChunkFactory(IChunkFactory chunkFactory)
		{
			return new ChunkCreator
			{
				_factory = chunkFactory
			};
		}

		public IList<IChunk> CreateChunksForHeightMap(int[,] heightMap)
		{
			if (heightMap == null)
			throw new ArgumentNullException("heightMap");

			var chunks = new List<IChunk>();

			var mapWidth = heightMap.GetLength(0);
			var mapDepth = heightMap.GetLength(1);


			for (var column = 0; column < mapDepth; ++column)
			{
				for (var row = 0; row < mapWidth; ++row)
				{
					var maxHeight = heightMap[row, column] - 1; // require 0 height squares
					for (int currentHeight = 0; currentHeight <= maxHeight; ++currentHeight)
					{
						var position = new IntVector3(row, currentHeight, column);

						var chunk = _factory.CreateChunkAt(position);

					    if (currentHeight == maxHeight)
					    {
                            chunk.AddTopFace();
					    }

                        if (IsFurthestRight(heightMap, row) || HasNoRightCoveringChunk(heightMap, currentHeight, new GridCoords(row, column)))
						{
							chunk.AddLeftFace();
						}

						if (IsFurthestFront(heightMap, column) || HasNoFrontCoveringChunk(heightMap, currentHeight, new GridCoords(row, column)))
						{
							chunk.AddFrontFace();
						}

						chunks.Add(chunk);
					}
				}
			}

			return chunks;
		}

		private static bool IsFurthestRight(int[,] heightMap, int row)
		{
			var mapWidth = heightMap.GetLength(0);
			return row + 1 >= mapWidth;
		}

		private static bool HasNoRightCoveringChunk(int[,] heightMap, int currentHeight, GridCoords gridCoords)
		{
			return heightMap[gridCoords.Row + 1, gridCoords.Column] <= currentHeight;
		}

		private static bool HasNoFrontCoveringChunk(int[,] heightMap, int currentHeight, GridCoords gridCoords)
		{
			return heightMap[gridCoords.Row, gridCoords.Column + 1] <= currentHeight;
		}

		private static bool IsFurthestFront(int[,] heightMap, int column)
		{
			var mapDepth = heightMap.GetLength(1);
            return column + 1 >= mapDepth;
		}
	}
}
>>>>>>> 2023643ee50bed6cc53cd1efb3b90de58b9fa2c0:Assets/Scripts/LevelBuilder/Model/MeshModel/ChunkCreator.cs

﻿using System;
using System.Collections.Generic;
using Common.DataContainers;
using Configs;
using UnityEngine;

namespace Instantiator.Stage.Models.MeshModel
{
	public class ChunkFaceFactory
	{
		private const int Offset = 1;
		private Dictionary<string, List<Vector2>> _textureTiles;

		public static ChunkFaceFactory Create(MapViewConfig mapViewConfig)
		{
			return new ChunkFaceFactory
			{
				_textureTiles = mapViewConfig.TextureTiles
			};
		}

		private List<Vector2> GetUv(string textureTileName)
		{
			List<Vector2> uv;

			if (textureTileName == null)
			{
				uv = new List<Vector2> {Vector2.zero, Vector2.zero, Vector2.zero, Vector2.zero};
			}
			else if (_textureTiles.ContainsKey(textureTileName))
			{
				uv = _textureTiles[textureTileName];
			}
			else
			{
				var message = string.Format("Could not find texture tile name '{0}'.", textureTileName);
				throw new ArgumentException(message);
			}

			return uv;
		}

		public ChunkFace CreateTopFaceAtPosition(IntVector3 position, string textureTileName)
		{
			var corners = new List<IntVector3>
			{
				new IntVector3(position.x + Offset, position.y + Offset, position.z),
				new IntVector3(position.x, position.y + Offset, position.z),
				new IntVector3(position.x, position.y + Offset, position.z + Offset),
				new IntVector3(position.x + Offset, position.y + Offset, position.z + Offset)
			};

			return ChunkFace.CreateWithCornersAndUv(corners, GetUv(textureTileName));
		}

		public ChunkFace CreateRightFaceAtPosition(IntVector3 position, string textureTileName)
		{
			var corners = new List<IntVector3>
			{
				new IntVector3(position.x + Offset, position.y + Offset, position.z),
				new IntVector3(position.x + Offset, position.y + Offset, position.z + Offset),
				new IntVector3(position.x + Offset, position.y, position.z + Offset),
				new IntVector3(position.x + Offset, position.y, position.z)
			};

			return ChunkFace.CreateWithCornersAndUv(corners, GetUv(textureTileName));
		}

		public ChunkFace CreateFrontFaceAtPosition(IntVector3 position, string textureTileName)
		{
			var corners = new List<IntVector3>
			{
				new IntVector3(position.x + Offset, position.y + Offset, position.z + Offset),
				new IntVector3(position.x, position.y + Offset, position.z + Offset),
				new IntVector3(position.x, position.y, position.z + Offset),
				new IntVector3(position.x + Offset, position.y, position.z + Offset)
			};

			return ChunkFace.CreateWithCornersAndUv(corners, GetUv(textureTileName));
		}
	}
}
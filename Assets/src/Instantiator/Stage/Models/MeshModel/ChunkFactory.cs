﻿using Common.DataContainers;
using Configs;

namespace Instantiator.Stage.Models.MeshModel
{
	public class ChunkFactory : IChunkFactory
	{
		private ChunkFaceFactory _factory;

		private ChunkFactory()
		{
		}

		public IChunk CreateChunkAt(IntVector3 position)
		{
			return new Chunk
			{
				FaceFactory = _factory,
				Position = position
			};
		}

		public static ChunkFactory Create(MapViewConfig mapViewConfig)
		{
			return new ChunkFactory
			{
				_factory = ChunkFaceFactory.Create(mapViewConfig)
			};
		}
	}
}
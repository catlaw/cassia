﻿using System;
using Common.DataContainers;

namespace Instantiator.Stage.Models.HeightMap
{
	public class HeightMapGrid
	{
		private HeightMapSquare[,] _squares;

		private HeightMapGrid()
		{
		}

		public int Width { get; private set; }
		public int Depth { get; private set; }

		public static HeightMapGrid CreateWithDimensions(int width, int depth)
		{
			return new HeightMapGrid
			{
				_squares = new HeightMapSquare[depth, width],
				Width = width,
				Depth = depth
			};
		}

		public HeightMapSquare GetSquare(GridCoords gridCoords)
		{
			AssertGridCoordsInRange(gridCoords);

			var square = _squares[gridCoords.Row, gridCoords.Column];
			return square ?? AddSquareWithHeight(0, gridCoords);
		}

		public HeightMapSquare AddSquareWithHeight(int height, GridCoords gridCoords)
		{
			AssertGridCoordsInRange(gridCoords);

			if (_squares[gridCoords.Row, gridCoords.Column] != null)
			{
				var message = string.Format("Could not add a square at position ({0}, {1}) because it already contains one.",
					gridCoords.Row, gridCoords.Column);
				throw new InvalidOperationException(message);
			}

			var square = HeightMapSquare.CreateAtPosition(gridCoords.Column, gridCoords.Row);
			square.CreateTilesForHeight(height);
			_squares[gridCoords.Row, gridCoords.Column] = square;

			return square;
		}

		private void AssertGridCoordsInRange(GridCoords gridCoords)
		{
			if (gridCoords.Row < 0 || gridCoords.Row >= Depth || gridCoords.Column < 0 || gridCoords.Column >= Width)
				throw new ArgumentOutOfRangeException("gridCoords");
		}
	}
}
﻿using UnityEngine;

namespace Instantiator.Stage.Models.MeshFactory
{
	public interface ILevelMeshFactory
	{
		Mesh CreateMesh();
	}
}
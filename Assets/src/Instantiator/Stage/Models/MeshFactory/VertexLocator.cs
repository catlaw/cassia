﻿using Common.DataContainers;
using UnityEngine;

namespace Instantiator.Stage.Models.MeshFactory
{
	public class VertexLocator : IVertexLocator
	{
		private float _chunkHeight;
		private float _chunkSize;
		private Vector3 _offset;

		private VertexLocator()
		{
		}

		public Vector3 GetWorldPosition(IntVector3 gridPoint)
		{
			return _offset + new Vector3(
				gridPoint.x*_chunkSize,
				gridPoint.y*_chunkHeight,
				gridPoint.z*_chunkSize);
		}

		public static VertexLocator CreateForDimensions(float chunkSize, float chunkHeight)
		{
			var locator = new VertexLocator
			{
				_chunkSize = chunkSize,
				_chunkHeight = chunkHeight
			};
			return locator;
		}

		public void SetOffset(Vector3 offset)
		{
			_offset = offset;
		}
	}

	public interface IVertexLocator
	{
		Vector3 GetWorldPosition(IntVector3 gridPoint);
	}
}
﻿using System.Collections.Generic;
using System.Linq;
using Configs;
using Instantiator.Stage.Models.MeshModel;
using UnityEngine;

namespace Instantiator.Stage.Models.MeshFactory
{
	public class LevelMeshFactory : ILevelMeshFactory
	{
		private readonly IConfig<LevelConfig> _levelConfig;
		private readonly IConfig<MapViewConfig> _mapViewConfig;

		public LevelMeshFactory(IConfig<MapViewConfig> mapViewConfig, IConfig<LevelConfig> levelConfig)
		{
			_mapViewConfig = mapViewConfig;
			_levelConfig = levelConfig;
		}

		public Mesh CreateMesh()
		{
			var mapViewConfig = _mapViewConfig.Get();
			var heights = _levelConfig.Get().MapHeights;

			var chunkFactory = ChunkFactory.Create(mapViewConfig);

			var vertexLocator = VertexLocator.CreateForDimensions(mapViewConfig.ChunkSize, mapViewConfig.ChunkHeight);
			vertexLocator.SetOffset(GetCenterOffset(heights));

			var vertices = new List<Vector3>();
			var triangles = new List<int>();
			var uv = new List<Vector2>();

			var chunkCreator = ChunkCreator.CreateWithChunkFactory(chunkFactory);
			var chunks = chunkCreator.CreateChunks(heights, mapViewConfig.Squares, mapViewConfig);

			foreach (var chunk in chunks)
			{
				var faces = chunk.GetFaces();
				foreach (var face in faces)
				{
					triangles.AddRange(GetFaceTriangles(vertices.Count));
					uv.AddRange(face.GetUv());
					vertices.AddRange(face.GetCornerPositions().Select(x => vertexLocator.GetWorldPosition(x)));
				}
			}

			return CreateMesh(vertices, triangles, uv);
		}

		private Vector3 GetCenterOffset(int[,] heights)
		{
			var chunkSize = _mapViewConfig.Get().ChunkSize;
			var centerOffsetCalculator = CenterOffsetCalculator.CreateWithDimensions(chunkSize);
			return centerOffsetCalculator.GetCenterOffset(heights);
		}

		private IEnumerable<int> GetFaceTriangles(int offset)
		{
			return new List<int> {0 + offset, 1 + offset, 2 + offset, 2 + offset, 3 + offset, 0 + offset};
		}

		private Mesh CreateMesh(IEnumerable<Vector3> vertices, IEnumerable<int> triangles, IEnumerable<Vector2> uv)
		{
			var mesh = new Mesh
			{
				vertices = vertices.ToArray(),
				triangles = triangles.ToArray(),
				uv = uv.ToArray()
			};
			mesh.RecalculateNormals();
			return mesh;
		}
	}
}
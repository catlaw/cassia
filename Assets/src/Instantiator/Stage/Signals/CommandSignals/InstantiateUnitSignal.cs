﻿using Common.Signals;
using Configs;
using strange.extensions.signal.impl;

namespace Instantiator.Stage.Signals.CommandSignals
{
	public class InstantiateUnitSignal : Signal<LevelUnitConfig, UnitViewConfig>, IInstantiateUnitSignal
	{
	}

	public interface IInstantiateUnitSignal : ISignal<LevelUnitConfig, UnitViewConfig>
	{
	}
}
﻿using System;
using Configs;
using Game.ConfigActions.GameObjects;
using Game.Views;
using Instantiator.Units;
using Instantiator.Units.Views;
using strange.extensions.mediation.impl;
using UnityEngine;

namespace Instantiator.Stage.Views
{
	public class GameInstantiatorView : View
	{
        [Inject]
        public IGameObjectFactory GameObjectFactory { get; set; }

		private GameObject _map;

		public void BuildLevel(Mesh mesh)
		{
            // TODO this should be in config too
			var material = UnityEngine.Resources.Load("Materials/tiles") as Material;

			_map = new GameObject("Map");
			_map.transform.SetParent(transform);
			_map.transform.position = Vector3.zero;

			_map.AddComponent<MeshRenderer>().material = material;
			_map.AddComponent<MeshFilter>().mesh = mesh;
			_map.AddComponent<MeshCollider>();
			_map.AddComponent<MapView>();
			_map.AddComponent<SquareHighlightsView>();
		}

		public void BuildUnit(IInstantiatableUnitView instantiatableUnit, Vector3 position, Quaternion rotation)
		{
			if (_map == null)
			{
				var message = "Cannot build unit if map has not been built";
				throw new InvalidOperationException(message);
			}

			var unitViewGameObject = new GameObject(instantiatableUnit.Unit.Name);
			var unitView = unitViewGameObject.AddComponent<UnitView>();
			var boxCollider = unitViewGameObject.AddComponent<BoxCollider>();
			boxCollider.center = new Vector3(0, 0.75f, 0); // TODO this should be in config
			boxCollider.size = new Vector3(1, 1.5f, 1); // TODO this should be in config

			unitView.SetUnit(instantiatableUnit.Unit);
			unitView.SetViewConfig(instantiatableUnit.Data);

			BuildUnitViewComponent(unitViewGameObject, instantiatableUnit.Data.Unit);
			BuildUnitViewComponent(unitViewGameObject, instantiatableUnit.Data.UI);

			foreach (var decoration in instantiatableUnit.Data.Decorations)
			{
				BuildUnitViewComponent(unitViewGameObject, decoration);
			}

			unitViewGameObject.transform.SetParent(_map.transform);

			unitView.Ready();
			unitView.SetTo(position, rotation);
		}

		private void BuildUnitViewComponent(GameObject unitViewGameObject, IDecoration decoration)
        {
		    var componentGameObject = GameObjectFactory.Instantiate(decoration.Asset);
			componentGameObject.transform.SetParent(unitViewGameObject.transform);
			componentGameObject.transform.localPosition = decoration.Position;
			componentGameObject.transform.localRotation = Quaternion.Euler(decoration.Rotation);
		}
	}
}
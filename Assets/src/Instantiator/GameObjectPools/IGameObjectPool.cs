﻿using UnityEngine;

namespace Instantiator.GameObjectPools
{
    public interface IGameObjectPool
    {
        GameObject GetPooled(IAsset asset);
        void ReturnToPool(GameObject gameObject);
    }
}
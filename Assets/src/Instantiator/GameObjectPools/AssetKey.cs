namespace Instantiator.GameObjectPools
{
    internal class AssetKey
    {
        internal AssetKey(IAsset asset)
        {
            Bundle = asset.Bundle;
            Path = asset.Path;
        }

        protected bool Equals(AssetKey other)
        {
            return string.Equals(Bundle, other.Bundle) && string.Equals(Path, other.Path);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((AssetKey) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return ((Bundle != null ? Bundle.GetHashCode() : 0)*397) ^ (Path != null ? Path.GetHashCode() : 0);
            }
        }

        public static bool operator ==(AssetKey left, AssetKey right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(AssetKey left, AssetKey right)
        {
            return !Equals(left, right);
        }

        public string Bundle { get; private set; }
        public string Path { get; private set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Instantiator.GameObjectPools
{
    public class LazyGameObjectPool : IGameObjectPool
    {
        private readonly IGameObjectFactory _gameObjectFactory;
        private readonly IDictionary<AssetKey, Stack<GameObject>> _pools;
        private readonly IDictionary<GameObject, IAsset> _loanedObjects;

        public LazyGameObjectPool(IGameObjectFactory gameObjectFactory)
        {
            _gameObjectFactory = gameObjectFactory;
            _pools = new Dictionary<AssetKey, Stack<GameObject>>();
            _loanedObjects = new Dictionary<GameObject, IAsset>();
        }

        public GameObject GetPooled(IAsset asset)
        {
            if (asset == null) throw new ArgumentNullException("asset");

            var pool = GetPool(asset);
            var gameObject = !pool.Any() ? _gameObjectFactory.Instantiate(asset) : pool.Pop();

            gameObject.SetActive(true);
            _loanedObjects.Add(gameObject, asset);

            return gameObject;
        }

        private Stack<GameObject> GetPool(IAsset asset)
        {
            var assetKey = new AssetKey(asset);
            if (!_pools.ContainsKey(assetKey))
            {
                _pools.Add(assetKey, new Stack<GameObject>());
            }

            return _pools[assetKey];
        }

        public void ReturnToPool(GameObject gameObject)
        {
            if (gameObject == null) throw new ArgumentNullException("gameObject");

            if (!_loanedObjects.ContainsKey(gameObject))
            {
                var message = "Cannot return unborrowed GameObject to pool.";
                throw new ArgumentException(message);
            }

            var asset = _loanedObjects[gameObject];
            _loanedObjects.Remove(gameObject);

            var pool = GetPool(asset);
            gameObject.SetActive(false);
            pool.Push(gameObject);
        }
    }
}
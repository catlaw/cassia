﻿using UnityEngine;

namespace Instantiator
{
	public interface IGameObjectInstantiator
	{
		GameObject InstantiatePrefab(GameObject prefab);
		void Destroy(GameObject gameObject);
	}
}
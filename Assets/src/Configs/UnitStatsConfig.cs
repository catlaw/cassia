﻿using System.Collections.Generic;
using Game.Models.Occupants.Units;

namespace Configs
{
	public class UnitStatsConfig : List<StatConfig>
	{
	}

	public class StatConfig
	{
		public StatType StatType { get; set; }
		public int Value { get; set; }
	}
}
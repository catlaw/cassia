﻿using Game.Models.Occupants.Units;

namespace Configs
{
	public class StatsMultiplierConfig
	{
		public StatType StatType { get; set; }
		public float Value { get; set; }
	}
}
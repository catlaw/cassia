﻿using System.IO;
using Newtonsoft.Json;
using UnityEngine;

namespace Configs
{
	public class ConfigLoader
	{
		private string _path;

		private ConfigLoader()
		{
		}

		public static ConfigLoader CreateForPath(string path)
		{
			return new ConfigLoader
			{
				_path = path
			};
		}

		public T LoadConfig<T>(string configName)
		{
			var configFile = Resources.Load<TextAsset>(_path + configName);

			if (configFile == null)
			{
				var message = string.Format("File '{0}' not found at path '{1}'.", configName, _path);
				throw new FileNotFoundException(message);
			}

			return JsonConvert.DeserializeObject<T>(configFile.text);
		}
	}
}
using System.Collections.Generic;

namespace Configs
{
	public class EffectorConfig
	{
		public EffectorConfig()
		{
			TargetEffects = new List<ActionEffectConfig>();
			ActorEffects = new List<ActionEffectConfig>();
		}

		public List<ActionEffectConfig> TargetEffects { get; set; }
		public List<ActionEffectConfig> ActorEffects { get; set; }
	}
}
﻿using Game.Views.Animations;

namespace Configs
{
	public class PlayableUnitAnimationConfig : IPlayableUnitAnimation
	{
		public string TriggerName { get; set; }
		public float HitDelay { get; set; }
		public float EndDelay { get; set; }
	}
}
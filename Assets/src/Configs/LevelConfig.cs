﻿using System.Collections.Generic;

namespace Configs
{
	public class LevelConfig
	{
		public LevelConfig()
		{
			MapHeights = new int[0, 0];
			Units = new List<LevelUnitConfig>();
		}

		public int[,] MapHeights { get; set; }
		public List<LevelUnitConfig> Units { get; set; }
	}
}
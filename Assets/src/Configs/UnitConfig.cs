﻿using System.Collections.Generic;
using Configs.JsonConverters;
using Newtonsoft.Json;

namespace Configs
{
	public class UnitConfig
	{
		public UnitConfig()
		{
			EquippedActionIds = new List<string>();
		}

		public string Name { get; set; }
		public List<string> EquippedActionIds { get; set; }

		[JsonConverter(typeof(StatsConverter))]
		public UnitStatsConfig Stats { get; set; }
	}
}
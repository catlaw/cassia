﻿namespace Configs
{
	public interface IConfig<T>
	{
		T Get();
		void LoadConfig(T config);
	}
}
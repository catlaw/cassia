﻿using Common.DataStructures;
using Configs.JsonConverters;
using Newtonsoft.Json;

namespace Configs
{
	public class UnitViewsConfig
	{
		[JsonConverter(typeof(LibraryConverter<UnitViewConfig>))]
		public Library<UnitViewConfig> UnitViews { get; set; }
	}
}
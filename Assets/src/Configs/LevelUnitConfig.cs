﻿using Common.DataContainers;
using Configs.JsonConverters;
using Newtonsoft.Json;

namespace Configs
{
	public class LevelUnitConfig
	{
		public UnitConfig Unit { get; set; }
		public string UnitViewId { get; set; }

		[JsonConverter(typeof(GridPositionConverter))]
		public GridPosition StartPosition { get; set; }
	}
}
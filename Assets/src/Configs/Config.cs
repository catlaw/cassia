﻿using System;

namespace Configs
{
	public class Config<T> : IConfig<T>
	{
		private T _config;

		public void LoadConfig(T config)
		{
			if (config == null) throw new ArgumentNullException("config");

			_config = config;
		}

		public T Get()
		{
			if (_config == null)
			{
				var message = string.Format("Cannot get config '{0}' before loading it.", typeof(T).Name);
				throw new InvalidOperationException(message);
			}

			return _config;
		}
	}
}
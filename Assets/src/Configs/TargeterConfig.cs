using System.Collections.Generic;
using Common.DataContainers;

namespace Configs
{
	public class TargeterConfig
	{
		public TargeterConfig()
		{
			TargetRange = new List<GridCoordsOffset>();
			AreaOfEffect = new List<GridCoordsOffset>();
		}

		public List<GridCoordsOffset> TargetRange { get; set; }
		public List<GridCoordsOffset> AreaOfEffect { get; set; }
	}
}
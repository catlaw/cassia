﻿using System;
using Common.DataStructures;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Configs.JsonConverters
{
	public class LibraryConverter<TValue> : JsonConverter
	{
		public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
		{
			throw new NotImplementedException("WriteJson is not implemented for LibraryConverter.");
		}

		public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
		{
			var jToken = JToken.Load(reader);
			return Library<TValue>.CreateFromJToken(jToken);
		}

		public override bool CanConvert(Type objectType)
		{
			return typeof(ILibrary<TValue>).IsAssignableFrom(objectType);
		}
	}

	public class LibraryConverter<TKey, TValue> : JsonConverter
	{
		public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
		{
			throw new NotImplementedException("WriteJson is not implemented for LibraryConverter.");
		}

		public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
		{
			var jToken = JToken.Load(reader);
			return Library<TKey, TValue>.CreateFromJToken(jToken);
		}

		public override bool CanConvert(Type objectType)
		{
			return typeof(ILibrary<TKey, TValue>).IsAssignableFrom(objectType);
		}
	}
}
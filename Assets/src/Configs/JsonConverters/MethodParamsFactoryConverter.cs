﻿using System;
using Game.ConfigActions.Params;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Configs.JsonConverters
{
	public class MethodParamsFactoryConverter : JsonConverter
	{
		public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
		{
			throw new NotImplementedException("WriteJson is not implemented for MethodParamsFactoryConverter.");
		}

		public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
		{
			var jObject = JObject.Load(reader);
            var methodParamParserFactory = new MethodParamParserFactory();
			return new MethodParamsFactory(jObject, methodParamParserFactory);
		}

		public override bool CanConvert(Type objectType)
		{
			return typeof(IMethodParamsFactory).IsAssignableFrom(objectType);
		}
	}
}
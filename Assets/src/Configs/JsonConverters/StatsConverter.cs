﻿using System;
using System.Linq;
using Game.Models.Occupants.Units;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Configs.JsonConverters
{
	public class StatsConverter : JsonConverter
	{
		public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
		{
			throw new NotImplementedException("WriteJson is not implemented for StatsConverter.");
		}

		public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
		{
			var statsConfig = new UnitStatsConfig();

			var jObjectProperties = JObject.Load(reader).Properties();
			statsConfig.AddRange(jObjectProperties.Select(property => new StatConfig
			{
				StatType = GetStatType(property.Name),
				Value = property.Value.Value<int>()
			}));

			return statsConfig;
		}

		private StatType GetStatType(string statName)
		{
			try
			{
				return (StatType) Enum.Parse(typeof(StatType), statName);
			}
			catch (Exception)
			{
				var message = string.Format("{0} is not a StatType.", statName);
				throw new JsonReaderException(message);
			}
		}

		public override bool CanConvert(Type objectType)
		{
			return typeof(UnitStatsConfig).IsAssignableFrom(objectType);
		}
	}
}
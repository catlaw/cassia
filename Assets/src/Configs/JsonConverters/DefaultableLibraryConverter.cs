﻿using System;
using Common.DataStructures;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Configs.JsonConverters
{
	public class DefaultableLibraryConverter<TKey, TValue> : JsonConverter
	{
		public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
		{
			throw new NotImplementedException("WriteJson is not implemented for DefaultableLibraryConverter.");
		}

		public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
		{
			var jToken = JToken.Load(reader);
			return new DefaultableLibrary<TKey, TValue>(jToken);
		}

		public override bool CanConvert(Type objectType)
		{
			return true;
			var genericTypes = objectType.GetGenericArguments();

			return typeof(IDefaultableLibrary<,>).IsAssignableFrom(objectType)
			       && typeof(TKey).IsAssignableFrom(genericTypes[0])
			       && typeof(TValue).IsAssignableFrom(genericTypes[1]);
		}
	}
}
﻿using Instantiator;

namespace Configs
{
	public class AssetConfig : IAsset
	{
		public string Bundle { get; set; }
		public string Path { get; set; }
	}
}
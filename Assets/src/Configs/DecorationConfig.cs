﻿using UnityEngine;

namespace Configs
{
	public class DecorationConfig
	{
		public AssetConfig Asset { get; set; }
		public Vector3 Position { get; set; }
		public Vector3 Rotation { get; set; }
	}
}
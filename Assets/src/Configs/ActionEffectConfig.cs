﻿using Game.Models.Occupants.Units;
using Game.Models.Occupants.Units.UnitActions;

namespace Configs
{
	public class ActionEffectConfig
	{
		public StatType TargetStatType { get; set; }
		public StatsMultiplierConfig ActorMultiplier { get; set; }
		public StatsMultiplierConfig TargetMultiplier { get; set; }
		public float Accuracy { get; set; }
		public AbnormalStatusType? AbnormalStatus { get; set; }
		public int? Duration { get; set; }
		public bool IsStackable { get; set; }
		public bool IsRemoveable { get; set; }
	}
}
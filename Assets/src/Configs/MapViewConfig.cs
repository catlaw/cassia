﻿using System.Collections.Generic;
using UnityEngine;

namespace Configs
{
	public class MapViewConfig
	{
		public MapViewConfig()
		{
			Squares = new SquareViewConfig[0, 0];
			TextureTiles = new Dictionary<string, List<Vector2>>();
		}

        public string DefaultTopTexture { get; set; }
        public string DefaultRightTexture { get; set; }
        public string DefaultFrontTexture { get; set; }
        public SquareViewConfig[,] Squares { get; set; }
		public Dictionary<string, List<Vector2>> TextureTiles { get; set; }
		public float ChunkSize { get; set; }
		public float ChunkHeight { get; set; }
	}
}
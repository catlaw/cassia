﻿namespace Configs
{
	public class SquareViewConfig
	{
		public SquareViewConfig()
		{
			TopTextures = new string[0];
			RightTextures = new string[0];
			FrontTextures = new string[0];
		}

		public string[] TopTextures { get; set; }
		public string[] RightTextures { get; set; }
		public string[] FrontTextures { get; set; }
	}
}
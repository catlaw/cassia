﻿namespace Configs
{
	public class UnitActionConfig
	{
		public string Id { get; set; }
		public string Name { get; set; }
		public string Description { get; set; }
		public TargeterConfig Target { get; set; }
		public EffectorConfig Effects { get; set; }
		public string ViewId { get; set; }
	}
}
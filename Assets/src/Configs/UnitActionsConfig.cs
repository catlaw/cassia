﻿using System.Collections.Generic;
using Common.DataStructures;
using Configs.JsonConverters;
using Newtonsoft.Json;

namespace Configs
{
	public class UnitActionsConfig
	{
		public UnitActionsConfig()
		{
			DefaultUnitActionIds = new List<string>();
		}

		public IEnumerable<string> DefaultUnitActionIds { get; set; }

		[JsonConverter(typeof(LibraryConverter<UnitActionConfig>))]
		public ILibrary<UnitActionConfig> UnitActions { get; set; }
	}
}
using System.Collections.Generic;
using Game.ConfigActions.Methods;
using Game.Views;
using Game.Views.Animations;

namespace Configs
{
	public class UnitActionViewConfig : IUnitActionView
	{
		public UnitActionViewConfig()
		{
			ConfigActions = new Dictionary<UnitActionEventType, ConfigActionSequenceConfig>();
		}

		public IDictionary<UnitActionEventType, ConfigActionSequenceConfig> ConfigActions { get; set; }
	}
}
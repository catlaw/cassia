﻿using System.Collections.Generic;
using Common.DataStructures;
using Configs.JsonConverters;
using Game.Models.Occupants.Units;
using Game.Views.Animations;
using Newtonsoft.Json;

namespace Configs
{
	public class UnitViewConfig
	{
	    public UnitViewConfig()
	    {
	        MoveSpeed = 1;
	        MaximumWalkHeight = 0;
			Decorations = new List<DecorationConfig>();
	    }

		public DecorationConfig Unit { get; set; }
		public DecorationConfig UI { get; set; }
		public IEnumerable<DecorationConfig> Decorations { get; set; }

		[JsonConverter(typeof(LibraryConverter<AnimationType, IList<PlayableUnitAnimationConfig>>))]
		public ILibrary<AnimationType, IList<PlayableUnitAnimationConfig>> Animations { get; set; }
        public float MoveSpeed { get; set; }
        public int MaximumWalkHeight { get; set; }
	}
}
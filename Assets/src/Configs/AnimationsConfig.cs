using System.Collections.Generic;
using Common.DataStructures;
using Configs.JsonConverters;
using Game.ConfigActions.Methods;
using Newtonsoft.Json;

namespace Configs
{
	public class AnimationsConfig
	{
	    public AnimationsConfig()
	    {
	        ActionEffects = new Dictionary<ActionEffectEventType, ConfigActionSequenceConfig>();
	    }

        public IDictionary<ActionEffectEventType, ConfigActionSequenceConfig> ActionEffects { get; set; }

        [JsonConverter(typeof(LibraryConverter<UnitActionViewConfig>))]
		public ILibrary<UnitActionViewConfig> UnitActions { get; set; }
	}
}
﻿using System.Collections.Generic;
using Configs.JsonConverters;
using Game.ConfigActions.Methods;
using Game.ConfigActions.Params;
using Newtonsoft.Json;

namespace Configs
{
	public class ConfigActionConfig
	{
		public ConfigActionMethodType Method { get; set; }

		[JsonProperty("Params"), JsonConverter(typeof(MethodParamsFactoryConverter))]
		public IMethodParamsFactory MethodParamsFactory { get; set; }

		public IEnumerable<string> Next { get; set; }
	}
}
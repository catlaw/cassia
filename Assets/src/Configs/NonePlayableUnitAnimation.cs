using Game.Views.Animations;

namespace Configs
{
	public class NonePlayableUnitAnimation : IPlayableUnitAnimation
	{
		public string TriggerName
		{
			get { return ""; }
		}

		public float HitDelay
		{
			get { return 0; }
		}

		public float EndDelay
		{
			get { return 0; }
		}
	}
}
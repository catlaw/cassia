using System.Collections.Generic;

namespace Configs
{
	public class ConfigActionSequenceConfig
	{
		public string Root { get; set; }
		public IDictionary<string, ConfigActionConfig> Actions { get; set; }
	}
}
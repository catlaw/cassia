﻿using strange.extensions.command.impl;
using UnityEngine.SceneManagement;

namespace Main.Controllers
{
	public class MainStartupCommand : Command
	{
		public override void Execute()
		{
			SceneManager.LoadScene("game");
		}
	}
}
﻿using strange.extensions.context.impl;

namespace Main.Contexts
{
	public class MainContextView : ContextView
	{
		private void Awake()
		{
			context = new RootContext(this, true);
			context.Start();
		}
	}
}